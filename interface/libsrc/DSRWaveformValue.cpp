/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRWaveformValue.h"
#include "jInterface.h"


static inline DSRWaveformReferenceValue *getAddressOfDSRWaveformReferenceValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRWaveformReferenceValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRWaveformReferenceValue (JNIEnv *env, jobject obj, DSRWaveformReferenceValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    getSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRWaveformValue_getSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    const char *string = ref->getSOPClassUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    getSOPClassName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRWaveformValue_getSOPClassName
  (JNIEnv *env, jobject obj)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    const char *uid = ref->getSOPClassUID().c_str();
    if ((uid != NULL) && (strlen(uid) > 0))
    {
        const char *name = dcmFindNameOfUID(uid);
        if (name == NULL)
        {
            OFString string = "unknown SOP class";
            if (uid != NULL)
            {
                string += " (";
                string += uid;
                string += ")";
            }
            return env->NewStringUTF (string.c_str());
        }
        return env->NewStringUTF (name);
    }
    return env->NewStringUTF ("");
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    getSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRWaveformValue_getSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    const char *string = ref->getSOPInstanceUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    setReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRWaveformValue_setReference
  (JNIEnv *env, jobject obj, jstring sopClassUID, jstring sopInstanceUID)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    char *string1 = (char *) env->GetStringUTFChars (sopClassUID, 0);
    char *string2 = (char *) env->GetStringUTFChars (sopInstanceUID, 0);

    OFCondition res = ref->setReference((string1) ? string1 : "", (string2) ? string2 : "");

    env->ReleaseStringUTFChars (sopClassUID, string1);
    env->ReleaseStringUTFChars (sopInstanceUID, string2);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    clearChannelList
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRWaveformValue_clearChannelList
  (JNIEnv *env, jobject obj)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    ref->getChannelList().clear();
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    getNumberOfChannels
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRWaveformValue_getNumberOfChannels
  (JNIEnv *env, jobject obj)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    return (jint) ref->getChannelList().getNumberOfItems();
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    getChannel
 * Signature: (ILJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRWaveformValue_getChannel
  (JNIEnv *env, jobject obj, jint idx, jobject multiplexGroupNumber, jobject channelNumber)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    Uint16 mgnValue, cnValue;

    OFCondition res = ref->getChannelList().getItem((size_t)idx, mgnValue, cnValue);

    env->SetIntField (multiplexGroupNumber, env->GetFieldID (env->GetObjectClass (multiplexGroupNumber), "value", "I"), mgnValue);
    env->SetIntField (channelNumber, env->GetFieldID (env->GetObjectClass (channelNumber), "value", "I"), cnValue);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    addChannel
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRWaveformValue_addChannel
  (JNIEnv *env, jobject obj, jint multiplexGroupNumber, jint channelNumber)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    ref->getChannelList().addOnlyNewItem(DSRWaveformChannelItem((Uint16)multiplexGroupNumber, (Uint16)channelNumber));
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    removeChannel
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRWaveformValue_removeChannel
  (JNIEnv *env, jobject obj, jint idx)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    return (jint) ref->getChannelList().removeItem((size_t)idx).status();
}


/*
 * Class:     J2Ci_jDSRWaveformValue
 * Method:    appliesToChannel
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRWaveformValue_appliesToChannel
  (JNIEnv *env, jobject obj, jint multiplexGroupNumber, jint channelNumber)
{
    DSRWaveformReferenceValue *ref = getAddressOfDSRWaveformReferenceValue (env, obj);

    return (jboolean) ref->appliesToChannel((Uint16)multiplexGroupNumber, (Uint16)channelNumber);
}


/*
 *  CVS Log
 *  $Log: $
 *
*/
