/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 * Author :      $Author: Schroeter $
 * Last update : $Date: 1999/05/14 10:02:31 $
 * Revision :    $Revision: 1.2 $
 * State :       $State: Exp $
*/


#include "J2Ci_jDVPSGraphicObject.h"
#include "jInterface.h"


inline DVPSGraphicObject* getAddressOfDVPSGraphicObject (JNIEnv *env, jobject obj)
{
	jclass cls = env->GetObjectClass (obj);
	jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

	if (fid == 0) exit (-1);

	return (DVPSGraphicObject*) env->GetLongField (obj, fid);
}

inline void setAddressOfDVPSGraphicObject (JNIEnv *env, jobject obj, DVPSGraphicObject* dvi)
{
	jclass cls = env->GetObjectClass (obj);
	jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

	if (fid == 0) exit (-1);

	env->SetLongField (obj, fid, (jlong) dvi);
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    createObjOfDVPSGraphicObject
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSGraphicObject_createObjOfDVPSGraphicObject
  (JNIEnv *env, jobject obj)
{
	DVPSGraphicObject *go = new DVPSGraphicObject;

	setAddressOfDVPSGraphicObject (env, obj, go);
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    CopyConstructor
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSGraphicObject_CopyConstructor
  (JNIEnv *env, jobject obj, jlong fromCppObj)
{
	const DVPSGraphicObject *go = (DVPSGraphicObject *) fromCppObj;

	DVPSGraphicObject *clone = new DVPSGraphicObject (*go);

	setAddressOfDVPSGraphicObject (env, obj, clone);
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    getAnnotationUnits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_getAnnotationUnits
  (JNIEnv *env, jobject obj)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (jint) go->getAnnotationUnits ();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    getNumberOfPoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_getNumberOfPoints
  (JNIEnv *env, jobject obj)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (jint) go->getNumberOfPoints ();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    getPoint
 * Signature: (ILJ2Ci/jDoubleByRef;LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_getPoint
  (JNIEnv *env, jobject obj, jint idx, jobject x, jobject y)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	Float32 xx, yy;

	E_Condition res = go->getPoint (idx, xx, yy);

	if (res != EC_Normal) return (jint) res.status();

	jclass xDoubleByRefcls = env->GetObjectClass (x);
	jclass yDoubleByRefcls = env->GetObjectClass (y);

	jfieldID xvalue = env->GetFieldID (xDoubleByRefcls, "value", "D");
	jfieldID yvalue = env->GetFieldID (yDoubleByRefcls, "value", "D");

	env->SetDoubleField (x, xvalue, (jdouble) xx);
	env->SetDoubleField (y, yvalue, (jdouble) yy);

	return (jint) EC_Normal.status();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    getGraphicType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_getGraphicType
  (JNIEnv *env, jobject obj)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (jint) go->getGraphicType ();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    isFilled
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSGraphicObject_isFilled
  (JNIEnv *env, jobject obj)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (go->isFilled () != 0) ? JNI_TRUE : JNI_FALSE;
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    setData
 * Signature: (I[FI)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_setData
  (JNIEnv *env, jobject obj, jint number, jfloatArray data, jint unit)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	float *ddata;
	jboolean isCopy;

	ddata = env->GetFloatArrayElements(data, &isCopy);

	OFCondition res = go->setData (number, ddata, (DVPSannotationUnit) unit);

	env->ReleaseFloatArrayElements (data, ddata, 0);

	return (jint) res.status();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    setGraphicType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_setGraphicType
  (JNIEnv *env, jobject obj, jint gtype)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (jint) go->setGraphicType ((DVPSGraphicType) gtype).status();
}



/*
 * Class:     J2Ci_jDVPSGraphicObject
 * Method:    setFilled
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSGraphicObject_setFilled
  (JNIEnv *env, jobject obj, jboolean filled)
{
	DVPSGraphicObject *go = getAddressOfDVPSGraphicObject (env, obj);

	return (jint) go->setFilled ((filled != 0) ? OFTrue : OFFalse).status();
}





/*
 *  CVS Log
 *  $Log: DVPSGraphicObject.cpp,v $
 *  Revision 1.2  1999/05/14 10:02:31  Schroeter
 *  Log-Tags for CVS corrected
 *
 *  Revision 1.1.1.1  1999/05/14 09:33:11  Schroeter
 *  initial commit of release 1.0
 *
*/
