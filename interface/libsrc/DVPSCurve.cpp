/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 * Author :      $Author: Schroeter $
 * Last update : $Date: 1999/05/14 10:02:31 $
 * Revision :    $Revision: 1.2 $
 * State :       $State: Exp $
*/


#include "J2Ci_jDVPSCurve.h"
#include "jInterface.h"


inline DVPSCurve* getAddressOfDVPSCurve (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DVPSCurve*) env->GetLongField (obj, fid);
}

inline void setAddressOfDVPSCurve (JNIEnv *env, jobject obj, DVPSCurve* dvi)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) dvi);
}




/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    createObjOfDVPSCurve
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSCurve_createObjOfDVPSCurve
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = new DVPSCurve;

    setAddressOfDVPSCurve (env, obj, cur);
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    CopyConstructor
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSCurve_CopyConstructor
  (JNIEnv *env, jobject obj, jlong fromCppObj)
{
    const DVPSCurve *cur = (DVPSCurve *) fromCppObj;

    DVPSCurve *clone = new DVPSCurve (*cur);

    setAddressOfDVPSCurve (env, obj, clone);
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getCurveGroup
 * Signature: ()B
 */
JNIEXPORT jbyte JNICALL Java_J2Ci_jDVPSCurve_getCurveGroup
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    return (jbyte) cur->getCurveGroup ();
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getNumberOfPoints
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSCurve_getNumberOfPoints
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    return (jint) cur->getNumberOfPoints ();
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getTypeOfData
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSCurve_getTypeOfData
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    return (jint) cur->getTypeOfData ();
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getPoint
 * Signature: (ILJ2Ci/jDoubleByRef;LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSCurve_getPoint
  (JNIEnv *env, jobject obj, jint idx, jobject x, jobject y)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    double xx, yy;

    E_Condition res = cur->getPoint (idx, xx, yy);

    if (res != EC_Normal) return (jint) res.status();

    jclass xDoubleByRefcls = env->GetObjectClass (x);
    jclass yDoubleByRefcls = env->GetObjectClass (y);

    jfieldID xvalue = env->GetFieldID (xDoubleByRefcls, "value", "D");
    jfieldID yvalue = env->GetFieldID (yDoubleByRefcls, "value", "D");

    env->SetDoubleField (x, xvalue, (jdouble) xx);
    env->SetDoubleField (y, yvalue, (jdouble) yy);

    return (jint) EC_Normal.status();
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getCurveDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSCurve_getCurveDescription
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    const char* res = cur->getCurveDescription ();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getCurveLabel
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSCurve_getCurveLabel
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    const char* res = cur->getCurveLabel ();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getCurveAxisUnitsX
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSCurve_getCurveAxisUnitsX
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    const char* res = cur->getCurveAxisUnitsX ();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVPSCurve
 * Method:    getCurveAxisUnitsY
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSCurve_getCurveAxisUnitsY
  (JNIEnv *env, jobject obj)
{
    DVPSCurve *cur = getAddressOfDVPSCurve (env, obj);

    const char* res = cur->getCurveAxisUnitsY ();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}



/*
 *  CVS Log
 *  $Log: DVPSCurve.cpp,v $
 *  Revision 1.2  1999/05/14 10:02:31  Schroeter
 *  Log-Tags for CVS corrected
 *
 *  Revision 1.1.1.1  1999/05/14 09:33:11  Schroeter
 *  initial commit of release 1.0
 *
*/
