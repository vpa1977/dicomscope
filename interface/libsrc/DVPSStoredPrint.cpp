/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 * Author :      $Author: Schroeter $
 * Last update : $Date: 1999/05/14 10:02:31 $
 * Revision :    $Revision: 1.2 $
 * State :       $State: Exp $
*/


#include "J2Ci_jDVPSStoredPrint.h"
#include "jInterface.h"

inline DVPSStoredPrint* getAddressOfDVPSStoredPrint (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DVPSStoredPrint*) env->GetLongField (obj, fid);
}

inline void setAddressOfDVPSStoredPrint (JNIEnv *env, jobject obj, DVPSStoredPrint* dvi)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) dvi);
}



/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    clear
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSStoredPrint_clear
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    sp->clear();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setOriginator
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setOriginator
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setOriginator (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setDestination
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setDestination
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setDestination (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setPrinterName
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setPrinterName
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setPrinterName (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setImageDisplayFormat
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setImageDisplayFormat
  (JNIEnv *env, jobject obj, jlong columns, jlong rows)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->setImageDisplayFormat((unsigned long)columns, (unsigned long)rows).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setFilmSizeID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setFilmSizeID
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setFilmSizeID (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setMagnificationType
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setMagnificationType
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setMagnificationType (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setSmoothingType
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setSmoothingType
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setSmoothingType (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setConfigurationInformation
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setConfigurationInformation
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setConfigurationInformation (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setResolutionID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setResolutionID
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setResolutionID (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setFilmOrientation
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setFilmOrientation
  (JNIEnv *env, jobject obj, jint value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    OFCondition res = sp->setFilmOrientation ((DVPSFilmOrientation) value);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setTrim
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setTrim
  (JNIEnv *env, jobject obj, jint value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    OFCondition res = sp->setTrim ((DVPSTrimMode) value);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setRequestedDecimateCropBehaviour
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setRequestedDecimateCropBehaviour
  (JNIEnv *env, jobject obj, jint value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    OFCondition res = sp->setRequestedDecimateCropBehaviour ((DVPSDecimateCropBehaviour) value);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    newPrinter
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_newPrinter
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    OFCondition res = sp->newPrinter ();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getOriginator
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getOriginator
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getOriginator();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getDestination
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getDestination
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getDestination();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getPrinterName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getPrinterName
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getPrinterName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getImageDisplayFormatColumns
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPSStoredPrint_getImageDisplayFormatColumns
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jlong) sp->getImageDisplayFormatColumns ();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getImageDisplayFormatRows
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPSStoredPrint_getImageDisplayFormatRows
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jlong) sp->getImageDisplayFormatRows ();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getFilmOrientation
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getFilmOrientation
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getFilmOrientation ();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getTrim
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getTrim
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getTrim ();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getRequestedDecimateCropBehaviour
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getRequestedDecimateCropBehaviour
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getRequestedDecimateCropBehaviour ();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getFilmSizeID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getFilmSizeID
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getFilmSizeID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getMagnificationType
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getMagnificationType
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getMagnificationType();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getSmoothingType
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getSmoothingType
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getSmoothingType();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getConfigurationInformation
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getConfigurationInformation
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getConfigurationInformation();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getResolutionID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getResolutionID
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getResolutionID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getNumberOfImages
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getNumberOfImages
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getNumberOfImages();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    deleteImage
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_deleteImage
  (JNIEnv *env, jobject obj, jint index)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->deleteImage(index).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    deleteMultipleImages
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_deleteMultipleImages
  (JNIEnv *env, jobject obj, jint number)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->deleteMultipleImages(number).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    addImageBox
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_addImageBox
  (JNIEnv *env, jobject obj, jstring retrieveaetitle, jstring refsopinstanceuid, jstring requestedimagesize, jstring patientid)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *ra = (char*) env->GetStringUTFChars (retrieveaetitle, 0);
    char *rs = (char*) env->GetStringUTFChars (refsopinstanceuid, 0);
    char *ri = (char*) env->GetStringUTFChars (requestedimagesize, 0);
    char *pi = (char*) env->GetStringUTFChars (patientid, 0);

    OFCondition res = sp->addImageBox (ra, rs, ri, pi);

    env->ReleaseStringUTFChars (retrieveaetitle, ra);
    env->ReleaseStringUTFChars (refsopinstanceuid, rs);
    env->ReleaseStringUTFChars (requestedimagesize, ri);
    env->ReleaseStringUTFChars (patientid, pi);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setInstanceUID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setInstanceUID
  (JNIEnv *env, jobject obj, jstring uid)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (uid, 0);

    OFCondition res = sp->setInstanceUID (val);

    env->ReleaseStringUTFChars (uid, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    imageHasAdditionalSettings
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSStoredPrint_imageHasAdditionalSettings
  (JNIEnv *env, jobject obj, jint index)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    OFBool res = sp->imageHasAdditionalSettings(index);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setImageMagnificationType
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setImageMagnificationType
  (JNIEnv *env, jobject obj, jint index, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setImageMagnificationType (index, val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setImageSmoothingType
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setImageSmoothingType
  (JNIEnv *env, jobject obj, jint index, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setImageSmoothingType (index, val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setImageConfigurationInformation
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setImageConfigurationInformation
  (JNIEnv *env, jobject obj, jint index, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setImageConfigurationInformation (index, val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getImageMagnificationType
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getImageMagnificationType
  (JNIEnv *env, jobject obj, jint index)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getImageMagnificationType(index);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getImageSmoothingType
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getImageSmoothingType
  (JNIEnv *env, jobject obj, jint index)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getImageSmoothingType(index);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getImageConfigurationInformation
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getImageConfigurationInformation
  (JNIEnv *env, jobject obj, jint index)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getImageConfigurationInformation(index);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setDefaultPresentationLUT
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setDefaultPresentationLUT
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->setDefaultPresentationLUT().status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setPresentationLUTShape
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setPresentationLUTShape
  (JNIEnv *env, jobject obj, jint type)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return  (jint) sp->setPresentationLUTShape ((DVPSPresentationLUTType) type).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    convertODtoPValue
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_convertODtoPValue
  (JNIEnv *env, jobject obj, jint density, jint bits)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return sp->convertODtoPValue ((Uint16)density, bits);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setBorderDensity
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setBorderDensity
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setBorderDensity (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setEmtpyImageDensity
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setEmtpyImageDensity
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setEmtpyImageDensity (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getBorderDensity
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getBorderDensity
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getBorderDensity();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getEmtpyImageDensity
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getEmtpyImageDensity
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getEmtpyImageDensity();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setMaxDensity
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setMaxDensity
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setMaxDensity (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setMinDensity
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setMinDensity
  (JNIEnv *env, jobject obj, jstring value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = sp->setMinDensity (val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getMaxDensity
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getMaxDensity
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getMaxDensity();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getMinDensity
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSStoredPrint_getMinDensity
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    const char* res = sp->getMinDensity();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setPrintIllumination
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setPrintIllumination
  (JNIEnv *env, jobject obj, jint value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->setPrintIllumination((short)value).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getPrintIllumination
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getPrintIllumination
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getPrintIllumination();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    setPrintReflectedAmbientLight
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_setPrintReflectedAmbientLight
  (JNIEnv *env, jobject obj, jint value)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->setPrintReflectedAmbientLight((short)value).status();
}


/*
 * Class:     J2Ci_jDVPSStoredPrint
 * Method:    getPrintReflectedAmbientLight
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSStoredPrint_getPrintReflectedAmbientLight
  (JNIEnv *env, jobject obj)
{
    DVPSStoredPrint *sp = getAddressOfDVPSStoredPrint (env, obj);

    return (jint) sp->getPrintReflectedAmbientLight();
}



/*
 *  CVS Log
 *  $Log$
 *
*/
