/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 * Author :      $Author: Schroeter $
 * Last update : $Date: 1999/05/14 10:02:31 $
 * Revision :    $Revision: 1.2 $
 * State :       $State: Exp $
*/


#include "J2Ci_jDVPSTextObject.h"
#include "jInterface.h"


inline DVPSTextObject* getAddressOfDVPSTextObject (JNIEnv *env, jobject obj)
{
	jclass cls = env->GetObjectClass (obj);
	jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

	if (fid == 0) exit (-1);

	return (DVPSTextObject*) env->GetLongField (obj, fid);
}

inline void setAddressOfDVPSTextObject (JNIEnv *env, jobject obj, DVPSTextObject* dvi)
{
	jclass cls = env->GetObjectClass (obj);
	jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

	if (fid == 0) exit (-1);

	env->SetLongField (obj, fid, (jlong) dvi);
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    createObjOfDVPSTextObject
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_createObjOfDVPSTextObject
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = new DVPSTextObject;

	setAddressOfDVPSTextObject (env, obj, to);
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    CopyConstructor
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_CopyConstructor
  (JNIEnv *env, jobject obj, jlong fromCppObj)
{
	const DVPSTextObject *to = (DVPSTextObject *) fromCppObj;

	DVPSTextObject *clone = new DVPSTextObject (*to);

	setAddressOfDVPSTextObject (env, obj, clone);
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    haveAnchorPoint
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_haveAnchorPoint
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (to->haveAnchorPoint () != 0) ? JNI_TRUE : JNI_FALSE;
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    haveBoundingBox
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_haveBoundingBox
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (to->haveBoundingBox () != 0) ? JNI_TRUE : JNI_FALSE;
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setAnchorPoint
 * Signature: (DDIZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setAnchorPoint
  (JNIEnv *env, jobject obj, jdouble x, jdouble y, jint unit, jboolean isVisible)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jint) to->setAnchorPoint (x, y, (DVPSannotationUnit) unit, (isVisible != 0) ? OFTrue : OFFalse).status();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setBoundingBox
 * Signature: (DDDDII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setBoundingBox
  (JNIEnv *env, jobject obj, jdouble TLHC_x, jdouble TLHC_y, jdouble BRHC_x, jdouble BRHC_y, jint unit, jint justification)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jint) to->setBoundingBox (TLHC_x, TLHC_y, BRHC_x, BRHC_y, (DVPSannotationUnit) unit, (DVPSTextJustification) justification).status();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    setText
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_setText
  (JNIEnv *env, jobject obj, jstring text)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(text, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = to->setText(OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }


	return (jint) res.status();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    removeAnchorPoint
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_removeAnchorPoint
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	to->removeAnchorPoint ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    removeBoundingBox
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPSTextObject_removeBoundingBox
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	to->removeBoundingBox ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getText
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPSTextObject_getText
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	const char* res = to->getText();

	if (res == NULL) return NULL;

	return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxTLHC_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxTLHC_1x
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getBoundingBoxTLHC_x ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxTLHC_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxTLHC_1y
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getBoundingBoxTLHC_y ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxBRHC_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxBRHC_1x
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getBoundingBoxBRHC_x ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxBRHC_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxBRHC_1y
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getBoundingBoxBRHC_y ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxAnnotationUnits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxAnnotationUnits
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jint) to->getBoundingBoxAnnotationUnits ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPoint_x
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPoint_1x
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getAnchorPoint_x ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPoint_y
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPoint_1y
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jdouble) to->getAnchorPoint_y ();
}



/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    anchorPointIsVisible
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPSTextObject_anchorPointIsVisible
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (to->anchorPointIsVisible () != 0) ? JNI_TRUE : JNI_FALSE;
}




/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getAnchorPointAnnotationUnits
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getAnchorPointAnnotationUnits
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jint) to->getAnchorPointAnnotationUnits ();
}




/*
 * Class:     J2Ci_jDVPSTextObject
 * Method:    getBoundingBoxHorizontalJustification
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPSTextObject_getBoundingBoxHorizontalJustification
  (JNIEnv *env, jobject obj)
{
	DVPSTextObject *to = getAddressOfDVPSTextObject (env, obj);

	return (jint) to->getBoundingBoxHorizontalJustification ();
}




/*
 *  CVS Log
 *  $Log: DVPSTextObject.cpp,v $
 *  Revision 1.2  1999/05/14 10:02:31  Schroeter
 *  Log-Tags for CVS corrected
 *
 *  Revision 1.1.1.1  1999/05/14 09:33:11  Schroeter
 *  initial commit of release 1.0
 *
*/
