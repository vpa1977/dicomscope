/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRImageValue.h"
#include "jInterface.h"


static inline DSRImageReferenceValue *getAddressOfDSRImageReferenceValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRImageReferenceValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRImageReferenceValue (JNIEnv *env, jobject obj, DSRImageReferenceValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getImageSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getImageSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *string = ref->getSOPClassUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getImageSOPClassName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getImageSOPClassName
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *uid = ref->getSOPClassUID().c_str();
    if ((uid != NULL) && (strlen(uid) > 0))
    {
        const char *name = dcmFindNameOfUID(uid);
        if (name == NULL)
        {
            OFString string = "unknown SOP class";
            if (uid != NULL)
            {
                string += " (";
                string += uid;
                string += ")";
            }
            return env->NewStringUTF (string.c_str());
        }
        return env->NewStringUTF (name);
    }
    return env->NewStringUTF ("");
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getImageSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getImageSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *string = ref->getSOPInstanceUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    setImageReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRImageValue_setImageReference
  (JNIEnv *env, jobject obj, jstring sopClassUID, jstring sopInstanceUID)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    char *string1 = (sopClassUID) ? (char *) env->GetStringUTFChars (sopClassUID, 0) : NULL;
    char *string2 = (sopInstanceUID) ? (char *) env->GetStringUTFChars (sopInstanceUID, 0) : NULL;

    OFCondition res = ref->setReference(string1, string2);

    env->ReleaseStringUTFChars (sopClassUID, string1);
    env->ReleaseStringUTFChars (sopInstanceUID, string2);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getPStateSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getPStateSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *string = ref->getPresentationState().getSOPClassUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getPStateSOPClassName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getPStateSOPClassName
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *uid = ref->getPresentationState().getSOPClassUID().c_str();
    if ((uid != NULL) && (strlen(uid) > 0))
    {
        const char *name = dcmFindNameOfUID(uid);
        if (name == NULL)
        {
            OFString string = "unknown SOP class";
            if (uid != NULL)
            {
                string += " (";
                string += uid;
                string += ")";
            }
            return env->NewStringUTF (string.c_str());
        }
        return env->NewStringUTF (name);
    }
    return env->NewStringUTF ("");
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getPStateSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRImageValue_getPStateSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    const char *string = ref->getPresentationState().getSOPInstanceUID().c_str();

    if (string == NULL) return NULL;
    return env->NewStringUTF (string);
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    setPStateReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRImageValue_setPStateReference
  (JNIEnv *env, jobject obj, jstring sopClassUID, jstring sopInstanceUID)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    char *string1 = (sopClassUID) ? (char *) env->GetStringUTFChars (sopClassUID, 0) : NULL;
    char *string2 = (sopInstanceUID) ? (char *) env->GetStringUTFChars (sopInstanceUID, 0) : NULL;

    OFCondition res = ref->setPresentationState(DSRCompositeReferenceValue(string1, string2));

    env->ReleaseStringUTFChars (sopClassUID, string1);
    env->ReleaseStringUTFChars (sopInstanceUID, string2);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    clearFrameList
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRImageValue_clearFrameList
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    ref->getFrameList().clear();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getNumberOfFrames
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRImageValue_getNumberOfFrames
  (JNIEnv *env, jobject obj)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    return (jint) ref->getFrameList().getNumberOfItems();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    getFrame
 * Signature: (ILJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRImageValue_getFrame
  (JNIEnv *env, jobject obj, jint idx, jobject frameNumber)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    Sint32 fnValue;

    OFCondition res = ref->getFrameList().getItem((size_t)idx, fnValue);

    env->SetIntField (frameNumber, env->GetFieldID (env->GetObjectClass (frameNumber), "value", "I"), fnValue);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    addFrame
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRImageValue_addFrame
  (JNIEnv *env, jobject obj, jint frameNumber)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    ref->getFrameList().addOnlyNewItem((Sint32)frameNumber);
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    removeFrame
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRImageValue_removeFrame
  (JNIEnv *env, jobject obj, jint idx)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    return (jint) ref->getFrameList().removeItem((size_t)idx).status();
}


/*
 * Class:     J2Ci_jDSRImageValue
 * Method:    appliesToFrame
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRImageValue_appliesToFrame
  (JNIEnv *env, jobject obj, jint frameNumber)
{
    DSRImageReferenceValue *ref = getAddressOfDSRImageReferenceValue (env, obj);

    return (jboolean) ref->appliesToFrame((Sint32)frameNumber);
}


/*
 *  CVS Log
 *  $Log: $
 *
*/
