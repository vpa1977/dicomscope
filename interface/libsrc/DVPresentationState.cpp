/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: Schroeter $
 *  Last update : $Date: 1999/05/14 10:02:31 $
 *  Revision :    $Revision: 1.2 $
 *  State :       $State: Exp $
*/


#include "J2Ci_jDVPresentationState.h"
#include "jInterface.h"


inline DVPresentationState* getAddressOfDVPresentationState (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DVPresentationState*) env->GetLongField (obj, fid);
}


inline void setAddressOfDVPresentationState (JNIEnv *env, jobject obj, DVPresentationState* dvi)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) dvi);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getInstanceUID
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getInstanceUID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getSOPClassUID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getAttachedImageSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getAttachedImageSOPClassUID
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getAttachedImageSOPClassUID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getAttachedImageSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getAttachedImageSOPInstanceUID
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getAttachedImageSOPInstanceUID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPresentationLabel
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getPresentationLabel
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getPresentationLabel();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setPresentationLabel
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setPresentationLabel
  (JNIEnv *env, jobject obj, jstring newLabel)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(newLabel, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->setPresentationLabel(OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPresentationDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getPresentationDescription
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getPresentationDescription();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setPresentationDescription
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setPresentationDescription
  (JNIEnv *env, jobject obj, jstring newDescr)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(newDescr, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->setPresentationDescription(OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPresentationCreatorsName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getPresentationCreatorsName
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getPresentationCreatorsName();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setPresentationCreatorsName
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setPresentationCreatorsName
  (JNIEnv *env, jobject obj, jstring newName)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(newName, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->setPresentationCreatorsName(OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPixelData
 * Signature: (LJ2Ci/jDVPrStateParam_GetPixelData;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPixelData__LJ2Ci_jDVPrStateParam_1GetPixelData_2
  (JNIEnv *env, jobject obj, jobject pixel)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const void *pixelData = NULL;
    unsigned long width = 0;
    unsigned long height = 0;

    OFCondition res = ps->getPixelData (pixelData, width, height);

    jclass pixelcls = env->GetObjectClass (pixel);
    jfieldID widthfid = env->GetFieldID (pixelcls, "width", "J");
    jfieldID heightfid = env->GetFieldID (pixelcls, "height", "J");
    jfieldID datafid = env->GetFieldID (pixelcls, "pixelData", "[B");

    env->SetLongField (pixel, widthfid, (jlong) width);
    env->SetLongField (pixel, heightfid, (jlong) height);


    jbyteArray array = env->NewByteArray ((jsize) (width*height)); // !!!
    env->SetByteArrayRegion (array, 0, width*height, (jbyte*) pixelData);

    env->SetObjectField (pixel, datafid, array);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRotation
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRotation
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->getRotation();

}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setRotation
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setRotation
  (JNIEnv *env, jobject obj, jint rotation)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->setRotation ((DVPSRotationType) rotation).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getFlip
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_getFlip
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    if (ps->getFlip()) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    clear
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_clear
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->clear();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setFlip
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setFlip
  (JNIEnv *env, jobject obj, jboolean isFlipped)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setFlip (isFlipped == JNI_TRUE ? OFTrue : OFFalse).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addImageReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_addImageReference
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring sopClassUID,
   jstring instanceUID, jstring frames, jstring aetitle, jstring filesetID, jstring filesetUID)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *sop = (char*) env->GetStringUTFChars (sopClassUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);
    char *fra = (char*) env->GetStringUTFChars (frames, 0);
    char *aet = (char*) env->GetStringUTFChars (aetitle, 0);
    char *fid = (char*) env->GetStringUTFChars (filesetID, 0);
    char *fui = (char*) env->GetStringUTFChars (filesetUID, 0);

    OFCondition res = ps->addImageReference (stu, ser, sop, ins, fra, aet, fid, fui);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (sopClassUID, sop);
    env->ReleaseStringUTFChars (instanceUID, ins);
    env->ReleaseStringUTFChars (frames, fra);
    env->ReleaseStringUTFChars (aetitle, aet);
    env->ReleaseStringUTFChars (filesetID, fid);
    env->ReleaseStringUTFChars (filesetUID, fui);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addImageReferenceAttached
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_addImageReferenceAttached
  (JNIEnv *env, jobject obj, jstring aetitle, jstring filesetID, jstring filesetUID)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    char *aet = (char*) env->GetStringUTFChars (aetitle, 0);
    char *fid = (char*) env->GetStringUTFChars (filesetID, 0);
    char *fui = (char*) env->GetStringUTFChars (filesetUID, 0);

    OFCondition res = ps->addImageReferenceAttached (aet, fid, fui);

    env->ReleaseStringUTFChars (aetitle, aet);
    env->ReleaseStringUTFChars (filesetID, fid);
    env->ReleaseStringUTFChars (filesetUID, fui);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeImageReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeImageReference
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = ps->removeImageReference (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeImageReferenceAttached
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeImageReferenceAttached
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeImageReferenceAttached ().status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageNumberOfFrames
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageNumberOfFrames
  (JNIEnv *env, jobject obj, jobject frame)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long fr;

    OFCondition res = ps->getImageNumberOfFrames (fr);

    if (res != EC_Normal) return (jint) res.status();

    jclass IntByRefcls = env->GetObjectClass (frame);
    jfieldID value = env->GetFieldID (IntByRefcls, "value", "I");

    env->SetIntField (frame, value, (jint) fr);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    selectImageFrameNumber
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_selectImageFrameNumber
  (JNIEnv *env, jobject obj, jint frame)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->selectImageFrameNumber (frame).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getSelectedImageFrameNumber
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getSelectedImageFrameNumber
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getSelectedImageFrameNumber();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPresentationLUT
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPresentationLUT
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getPresentationLUT ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    havePresentationLookupTable
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_havePresentationLookupTable
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->havePresentationLookupTable () != 0) ? JNI_TRUE : JNI_FALSE;
}



/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRectShutterLV
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRectShutterLV
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getRectShutterLV ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRectShutterRV
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRectShutterRV
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getRectShutterRV ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRectShutterUH
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRectShutterUH
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getRectShutterUH ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRectShutterLH
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRectShutterLH
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getRectShutterLH ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setRectShutter
 * Signature: (IIII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setRectShutter
  (JNIEnv *env, jobject obj, jint lv, jint rv, jint uh, jint lh)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setRectShutter (lv, rv, uh, lh).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    haveShutter
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_haveShutter
  (JNIEnv *env, jobject obj, jint type)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFBool res = ps->haveShutter ((DVPSShutterType) type);

    return (res == OFTrue) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setCurrentPresentationLUT
 * Signature: (I)Z
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setCurrentPresentationLUT
  (JNIEnv *env, jobject obj, jint type)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setCurrentPresentationLUT ((DVPSPresentationLUTType) type).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurrentPresentationLUTExplanation
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getCurrentPresentationLUTExplanation
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getCurrentPresentationLUTExplanation();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPresentationLUTExplanation
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getPresentationLUTExplanation
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getPresentationLUTExplanation();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCenterOfCircularShutter_x
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getCenterOfCircularShutter_1x
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getCenterOfCircularShutter_x ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCenterOfCircularShutter_y
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getCenterOfCircularShutter_1y
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getCenterOfCircularShutter_y ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getRadiusOfCircularShutter
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getRadiusOfCircularShutter
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getRadiusOfCircularShutter ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setCircularShutter
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setCircularShutter
  (JNIEnv *env, jobject obj, jint centerX, jint centerY, jint radius)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setCircularShutter (centerX, centerY, radius).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfPolyShutterVertices
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfPolyShutterVertices
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfPolyShutterVertices ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setPolyShutterOrigin
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setPolyShutterOrigin
  (JNIEnv *env, jobject obj, jint x, jint y)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setPolyShutterOrigin (x, y).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addPolyShutterVertex
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_addPolyShutterVertex
  (JNIEnv *env, jobject obj, jint x, jint y)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->addPolyShutterVertex (x, y).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPolyShutterVertex
 * Signature: (ILjava/awt/Point;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPolyShutterVertex
  (JNIEnv *env, jobject obj, jint idx, jobject pointXY)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    Sint32 x, y;

    OFCondition res = ps->getPolyShutterVertex (idx, x, y);

    if (res != EC_Normal) return (jint) res.status();

    jclass pointcls = env->GetObjectClass (pointXY);
    jfieldID xfid = env->GetFieldID (pointcls, "x", "I");
    jfieldID yfid = env->GetFieldID (pointcls, "y", "I");

    env->SetIntField (pointXY, xfid, (jint) x);
    env->SetIntField (pointXY, yfid, (jint) y);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getShutterPresentationValue
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getShutterPresentationValue
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getShutterPresentationValue ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setShutterPresentationValue
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setShutterPresentationValue
  (JNIEnv *env, jobject obj, jint pvalue)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setShutterPresentationValue ((Uint16) pvalue).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    sortGraphicLayers
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_sortGraphicLayers
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->sortGraphicLayers ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfGraphicLayers
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfGraphicLayers
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfGraphicLayers ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicLayerName
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getGraphicLayerName
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getGraphicLayerName(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicLayerIndex
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getGraphicLayerIndex
  (JNIEnv *env, jobject obj, jstring name)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    char *nam = (char*) env->GetStringUTFChars (name, 0);

    int res = ps->getGraphicLayerIndex (nam);

    env->ReleaseStringUTFChars (name, nam);

    return  (jint) res;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicLayerDescription
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getGraphicLayerDescription
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getGraphicLayerDescription(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDisplayedAreaPresentationSizeMode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getDisplayedAreaPresentationSizeMode
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->getDisplayedAreaPresentationSizeMode ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDisplayedAreaPresentationPixelAspectRatio
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPresentationState_getDisplayedAreaPresentationPixelAspectRatio
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jdouble) ps->getDisplayedAreaPresentationPixelAspectRatio ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getStandardDisplayedArea
 * Signature: (LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getStandardDisplayedArea
  (JNIEnv *env, jobject obj, jobject tlhcX, jobject tlhcY, jobject brhcX, jobject brhcY)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    long tx, ty, bx, by;

    OFCondition res = ps->getStandardDisplayedArea (tx, ty, bx, by);

    if (res != EC_Normal) return (jint) res.status();

    jclass txIntByRefcls = env->GetObjectClass (tlhcX);
    jclass tyIntByRefcls = env->GetObjectClass (tlhcY);
    jclass bxIntByRefcls = env->GetObjectClass (brhcX);
    jclass byIntByRefcls = env->GetObjectClass (brhcY);

    jfieldID txvalue = env->GetFieldID (txIntByRefcls, "value", "I");
    jfieldID tyvalue = env->GetFieldID (tyIntByRefcls, "value", "I");
    jfieldID bxvalue = env->GetFieldID (bxIntByRefcls, "value", "I");
    jfieldID byvalue = env->GetFieldID (byIntByRefcls, "value", "I");

    env->SetIntField (tlhcX, txvalue, (jint) tx);
    env->SetIntField (tlhcY, tyvalue, (jint) ty);
    env->SetIntField (brhcX, bxvalue, (jint) bx);
    env->SetIntField (brhcY, byvalue, (jint) by);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageRelativeDisplayedArea
 * Signature: (LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageRelativeDisplayedArea
  (JNIEnv *env, jobject obj, jobject tlhcX, jobject tlhcY, jobject brhcX, jobject brhcY)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    long tx, ty, bx, by;

    OFCondition res = ps->getImageRelativeDisplayedArea (tx, ty, bx, by);

    if (res != EC_Normal) return (jint) res.status();

    jclass txIntByRefcls = env->GetObjectClass (tlhcX);
    jclass tyIntByRefcls = env->GetObjectClass (tlhcY);
    jclass bxIntByRefcls = env->GetObjectClass (brhcX);
    jclass byIntByRefcls = env->GetObjectClass (brhcY);

    jfieldID txvalue = env->GetFieldID (txIntByRefcls, "value", "I");
    jfieldID tyvalue = env->GetFieldID (tyIntByRefcls, "value", "I");
    jfieldID bxvalue = env->GetFieldID (bxIntByRefcls, "value", "I");
    jfieldID byvalue = env->GetFieldID (byIntByRefcls, "value", "I");

    env->SetIntField (tlhcX, txvalue, (jint) tx);
    env->SetIntField (tlhcY, tyvalue, (jint) ty);
    env->SetIntField (brhcX, bxvalue, (jint) bx);
    env->SetIntField (brhcY, byvalue, (jint) by);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDisplayedAreaPresentationPixelSpacing
 * Signature: (LJ2Ci/jDoubleByRef;LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getDisplayedAreaPresentationPixelSpacing
  (JNIEnv *env, jobject obj, jobject x, jobject y)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double xx, yy;

    OFCondition res = ps->getDisplayedAreaPresentationPixelSpacing (xx, yy);

    if (res != EC_Normal) return (jint) res.status();

    jclass xDoubleByRefcls = env->GetObjectClass (x);
    jclass yDoubleByRefcls = env->GetObjectClass (y);

    jfieldID xvalue = env->GetFieldID (xDoubleByRefcls, "value", "D");
    jfieldID yvalue = env->GetFieldID (yDoubleByRefcls, "value", "D");

    env->SetDoubleField (x, xvalue, xx);
    env->SetDoubleField (y, yvalue, yy);

    return (jint) EC_Normal.status();
}

/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDisplayedAreaPresentationPixelMagnificationRatio
 * Signature: (LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getDisplayedAreaPresentationPixelMagnificationRatio
  (JNIEnv *env, jobject obj, jobject magnification)
{
DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double m;

    OFCondition res = ps->getDisplayedAreaPresentationPixelMagnificationRatio (m);

    if (res != EC_Normal) return (jint) res.status();

    jclass mDoubleByRefcls = env->GetObjectClass (magnification);

    jfieldID mvalue = env->GetFieldID (mDoubleByRefcls, "value", "D");

    env->SetDoubleField (magnification, mvalue, m);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    canUseDisplayedAreaTrueSize
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_canUseDisplayedAreaTrueSize
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    if (ps->canUseDisplayedAreaTrueSize()) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setStandardDisplayedArea
 * Signature: (IIIIIDI)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setStandardDisplayedArea
  (JNIEnv *env, jobject obj, jint sizeMode, jint tlhcX, jint tlhcY, jint brhcX, jint brhcY,
   jdouble magnification, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->setStandardDisplayedArea ((DVPSPresentationSizeMode) sizeMode, tlhcX,
                   tlhcY, brhcX, brhcY, magnification, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setImageRelativeDisplayedArea
 * Signature: (IIIIIDI)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setImageRelativeDisplayedArea
  (JNIEnv *env, jobject obj, jint sizeMode, jint tlhcX, jint tlhcY, jint brhcX, jint brhcY,
   jdouble magnification, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->setImageRelativeDisplayedArea ((DVPSPresentationSizeMode) sizeMode, tlhcX,
                   tlhcY, brhcX, brhcY, magnification, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    haveGraphicLayerRecommendedDisplayValue
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_haveGraphicLayerRecommendedDisplayValue
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->haveGraphicLayerRecommendedDisplayValue (idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeGraphicLayerRecommendedDisplayValue
 * Signature: (IZZ)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_removeGraphicLayerRecommendedDisplayValue
  (JNIEnv *env, jobject obj, jint idx, jboolean rgb, jboolean monochrome)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFBool ofrgb, ofmono;
    ofrgb = (rgb == JNI_TRUE) ? OFTrue : OFFalse;
    ofmono = (monochrome == JNI_TRUE) ? OFTrue : OFFalse;

    ps->removeGraphicLayerRecommendedDisplayValue (idx, ofrgb, ofmono);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicLayerRecommendedDisplayValueGray
 * Signature: (ILJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getGraphicLayerRecommendedDisplayValueGray
  (JNIEnv *env, jobject obj, jint idx, jobject gray)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    Uint16 gr;

    OFCondition res = ps->getGraphicLayerRecommendedDisplayValueGray (idx, gr);

    if (res != EC_Normal) return (jint) res.status();

    jclass IntByRefcls = env->GetObjectClass (gray);
    jfieldID value = env->GetFieldID (IntByRefcls, "value", "I");

    env->SetIntField (gray, value, (jint) gr);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicLayerRecommendedDisplayValueRGB
 * Signature: (ILJ2Ci/jIntByRef;LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getGraphicLayerRecommendedDisplayValueRGB
  (JNIEnv *env, jobject obj, jint idx, jobject r, jobject g, jobject b)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    Uint16 rr, gg, bb;

    OFCondition res = ps->getGraphicLayerRecommendedDisplayValueRGB (idx, rr, gg, bb);

    if (res != EC_Normal) return (jint) res.status();

    jclass rIntByRefcls = env->GetObjectClass (r);
    jclass gIntByRefcls = env->GetObjectClass (g);
    jclass bIntByRefcls = env->GetObjectClass (b);

    jfieldID rvalue = env->GetFieldID (rIntByRefcls, "value", "I");
    jfieldID gvalue = env->GetFieldID (gIntByRefcls, "value", "I");
    jfieldID bvalue = env->GetFieldID (bIntByRefcls, "value", "I");

    env->SetIntField (r, rvalue, (jint) rr);
    env->SetIntField (g, gvalue, (jint) gg);
    env->SetIntField (b, bvalue, (jint) bb);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setGraphicLayerRecommendedDisplayValueGray
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setGraphicLayerRecommendedDisplayValueGray
  (JNIEnv *env, jobject obj, jint idx, jint gray)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setGraphicLayerRecommendedDisplayValueGray (idx, (Uint16) gray).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setGraphicLayerRecommendedDisplayValueRGB
 * Signature: (IIII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setGraphicLayerRecommendedDisplayValueRGB
  (JNIEnv *env, jobject obj, jint idx, jint r, jint g, jint b)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setGraphicLayerRecommendedDisplayValueRGB (idx, (Uint16) r, (Uint16) g, (Uint16) b).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setGraphicLayerName
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setGraphicLayerName
  (JNIEnv *env, jobject obj, jint idx, jstring name)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    char *nam = (char *) env->GetStringUTFChars (name, 0);

    OFCondition res = ps->setGraphicLayerName (idx, nam);

    env->ReleaseStringUTFChars (name, nam);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setGraphicLayerDescription
 * Signature: (ILjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setGraphicLayerDescription
  (JNIEnv *env, jobject obj, jint idx, jstring descr)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(descr, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->setGraphicLayerDescription(idx, OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    toFrontGraphicLayer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_toFrontGraphicLayer
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->toFrontGraphicLayer (idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    toBackGraphicLayer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_toBackGraphicLayer
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->toBackGraphicLayer (idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addGraphicLayer
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_addGraphicLayer
  (JNIEnv *env, jobject obj, jstring gLayer, jstring gLayerDescr)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(gLayerDescr, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            char *lay = (char*) env->GetStringUTFChars (gLayer, 0);
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->addGraphicLayer (lay, OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseStringUTFChars (gLayer, lay);
            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeGraphicLayer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeGraphicLayer
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeGraphicLayer (idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeShutter
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_removeShutter
  (JNIEnv *env, jobject obj, jint type)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->removeShutter ((DVPSShutterType) type);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfTextObjects
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfTextObjects
  (JNIEnv *env, jobject obj, jint layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfTextObjects (layer);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getTextObjectN
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getTextObjectN
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->getTextObject (layer, idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addTextObjectN
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_addTextObjectN
  (JNIEnv *env, jobject obj, jint layer, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->addTextObject (layer, (DVPSObjectApplicability) applicability);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeTextObject
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeTextObject
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeTextObject (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    moveTextObject
 * Signature: (IIII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_moveTextObject
  (JNIEnv *env, jobject obj, jint old_layer, jint idx, jint new_layer, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->moveTextObject (old_layer, idx, new_layer, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfGraphicObjects
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfGraphicObjects
  (JNIEnv *env, jobject obj, jint layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfGraphicObjects (layer);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getGraphicObjectN
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getGraphicObjectN
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->getGraphicObject (layer, idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addGraphicObjectN
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_addGraphicObjectN
  (JNIEnv *env, jobject obj, jint layer, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->addGraphicObject (layer, (DVPSObjectApplicability) applicability);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeGraphicObject
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeGraphicObject
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeGraphicObject (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    moveGraphicObject
 * Signature: (IIII)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_moveGraphicObject
  (JNIEnv *env, jobject obj, jint old_layer, jint idx, jint new_layer, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->moveGraphicObject (old_layer, idx, new_layer, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    detachImage
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_detachImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->detachImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setCharset
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setCharset
  (JNIEnv *env, jobject obj, jint charset)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setCharset ((DVPScharacterSet) charset).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCharset
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getCharset
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getCharset ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCharsetString
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getCharsetString
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getCharsetString();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfCurves
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfCurves
  (JNIEnv *env, jobject obj, jint layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfCurves (layer);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurveN
 * Signature: (II)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getCurveN
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->getCurve (layer, idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfCurvesInImage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfCurvesInImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfCurvesInImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurveInImageN
 * Signature: (I)J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getCurveInImageN
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jlong) ps->getCurveInImage (idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    addCurve
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_addCurve
  (JNIEnv *env, jobject obj, jint layer, jint curveidxinimage)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->addCurve (layer, curveidxinimage).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeCurve
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeCurve
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeCurve (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    moveCurve
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_moveCurve
  (JNIEnv *env, jobject obj, jint old_layer, jint idx, jint new_layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->moveCurve (old_layer, idx, new_layer).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurrentWindowWidth
 * Signature: (LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getCurrentWindowWidth
  (JNIEnv *env, jobject obj, jobject w)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double ww;

    OFCondition res = ps->getCurrentWindowWidth (ww);

    if (res != EC_Normal) return (jint) res.status();

    jclass wDoubleByRefcls = env->GetObjectClass (w);

    jfieldID wvalue = env->GetFieldID (wDoubleByRefcls, "value", "D");

    env->SetDoubleField (w, wvalue, ww);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurrentWindowCenter
 * Signature: (LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getCurrentWindowCenter
  (JNIEnv *env, jobject obj, jobject c)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double cc;

    OFCondition res = ps->getCurrentWindowCenter (cc);

    if (res != EC_Normal) return (jint) res.status();

    jclass cDoubleByRefcls = env->GetObjectClass (c);

    jfieldID cvalue = env->GetFieldID (cDoubleByRefcls, "value", "D");

    env->SetDoubleField (c, cvalue, (jdouble) cc);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    haveActiveVOIWindow
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_haveActiveVOIWindow
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->haveActiveVOIWindow () != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    haveActiveVOILUT
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_haveActiveVOILUT
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->haveActiveVOILUT () != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurrentVOIDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getCurrentVOIDescription
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getCurrentVOIDescription();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfVOILUTsInImage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfVOILUTsInImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfVOILUTsInImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfVOIWindowsInImage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfVOIWindowsInImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfVOIWindowsInImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDescriptionOfVOILUTsInImage
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getDescriptionOfVOILUTsInImage
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getDescriptionOfVOILUTsInImage(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDescriptionOfVOIWindowsInImage
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getDescriptionOfVOIWindowsInImage
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getDescriptionOfVOIWindowsInImage(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setVOILUTFromImage
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setVOILUTFromImage
  (JNIEnv *env, jobject obj, jint idx, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setVOILUTFromImage (idx, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setVOIWindowFromImage
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setVOIWindowFromImage
  (JNIEnv *env, jobject obj, jint idx, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->setVOIWindowFromImage (idx, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setVOIWindow
 * Signature: (DDLjava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setVOIWindow
  (JNIEnv *env, jobject obj, jdouble wCenter, jdouble wWidth, jstring descr, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = EC_IllegalCall;

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(descr, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            res = ps->setVOIWindow (wCenter, wWidth, OFString((char *)string, env->GetArrayLength(array)).c_str(),
                                    (DVPSObjectApplicability) applicability);

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    deactivateVOI
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_deactivateVOI
  (JNIEnv *env, jobject obj, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->deactivateVOI ((DVPSObjectApplicability) applicability);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setGammaVOILUT
 * Signature: (DI)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setGammaVOILUT
  (JNIEnv *env, jobject obj, jdouble gammaValue, jint applicability)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->setGammaVOILUT (gammaValue, (DVPSObjectApplicability) applicability).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfActiveOverlays
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfActiveOverlays
  (JNIEnv *env, jobject obj, jint layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfActiveOverlays (layer);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getActiveOverlayGroup
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getActiveOverlayGroup
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getActiveOverlayGroup (layer, idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getActiveOverlayLabel
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getActiveOverlayLabel
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getActiveOverlayLabel(layer, idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getActiveOverlayDescription
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getActiveOverlayDescription
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getActiveOverlayDescription(layer, idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    activeOverlayIsROI
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_activeOverlayIsROI
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->activeOverlayIsROI (layer, idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayData
 * Signature: (LJ2Ci/jDVPrStateParam_GetOverlayData;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getOverlayData
  (JNIEnv *env, jobject obj, jobject od)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    int layer;
    int idx;

    const void *overlayData;
    unsigned int width;
    unsigned int height;
    unsigned int left_pos;
    unsigned int top_pos;
    unsigned short foreground;
    unsigned short bits;
    OFBool isROI;


    jclass ovdatcls = env->GetObjectClass (od);
    jfieldID layerfid = env->GetFieldID (ovdatcls, "layer", "I");
    jfieldID idxfid = env->GetFieldID (ovdatcls, "idx", "I");
    jfieldID bitsfid = env->GetFieldID (ovdatcls, "bits", "I");

    layer = env->GetIntField (od, layerfid);
    idx = env->GetIntField (od, idxfid);
    bits = (unsigned short) env->GetIntField (od, bitsfid);

    OFCondition res = ps->getOverlayData (layer, idx, overlayData, width, height, left_pos, top_pos, isROI, foreground, bits);

    if (res != EC_Normal) return res.status(); // error??


    jfieldID datafid = env->GetFieldID (ovdatcls, "overlayData", "[B");
    jfieldID data12fid = env->GetFieldID (ovdatcls, "overlayData12", "[S");
    jfieldID widthfid = env->GetFieldID (ovdatcls, "width", "I");
    jfieldID heightfid = env->GetFieldID (ovdatcls, "height", "I");
    jfieldID leftfid = env->GetFieldID (ovdatcls, "left", "I");
    jfieldID topfid = env->GetFieldID (ovdatcls, "top", "I");
    jfieldID isroifid = env->GetFieldID (ovdatcls, "isROI", "Z");
    jfieldID foregroundfid = env->GetFieldID (ovdatcls, "foreground", "S");

    env->SetIntField (od, widthfid, (jint) width);
    env->SetIntField (od, heightfid, (jint) height);
    env->SetIntField (od, leftfid, (jint) left_pos);
    env->SetIntField (od, topfid, (jint) top_pos);
    env->SetShortField (od, foregroundfid, (jshort) foreground);
    env->SetBooleanField (od, isroifid, (jboolean) (isROI == OFTrue) ? JNI_TRUE : JNI_FALSE);

    if (bits == 8)
    {
        jbyteArray array = env->NewByteArray ((jsize) (width*height));
        env->SetByteArrayRegion (array, 0, width*height, (jbyte*) overlayData);

        env->SetObjectField (od, datafid, array);
    }
    else // bits == 12
    {
        jshortArray array = env->NewShortArray ((jsize) (width*height));
        env->SetShortArrayRegion (array, 0, width*height, (jshort*) overlayData);

        env->SetObjectField (od, data12fid, array);
    }

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfOverlaysInImage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfOverlaysInImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfOverlaysInImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInImageGroup
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getOverlayInImageGroup
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getOverlayInImageGroup (idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInImageLabel
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getOverlayInImageLabel
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getOverlayInImageLabel(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInImageDescription
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getOverlayInImageDescription
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getOverlayInImageDescription(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInImageActivationLayer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getOverlayInImageActivationLayer
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getOverlayInImageActivationLayer (idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    overlayInImageIsROI
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_overlayInImageIsROI
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->overlayInImageIsROI (idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getNumberOfOverlaysInPresentationState
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getNumberOfOverlaysInPresentationState
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getNumberOfOverlaysInPresentationState ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInPresentationStateGroup
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getOverlayInPresentationStateGroup
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getOverlayInPresentationStateGroup (idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInPresentationStateLabel
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getOverlayInPresentationStateLabel
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getOverlayInPresentationStateLabel(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInPresentationStateDescription
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getOverlayInPresentationStateDescription
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getOverlayInPresentationStateDescription(idx);

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getOverlayInPresentationStateActivationLayer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getOverlayInPresentationStateActivationLayer
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->getOverlayInPresentationStateActivationLayer (idx);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    overlayIsBitmapShutter
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_overlayIsBitmapShutter
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->overlayIsBitmapShutter (idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    overlayInPresentationStateIsROI
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_overlayInPresentationStateIsROI
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->overlayInPresentationStateIsROI (idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    removeOverlayFromPresentationState
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_removeOverlayFromPresentationState
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->removeOverlayFromPresentationState (idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    changeOverlayGroupInPresentationState
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_changeOverlayGroupInPresentationState
  (JNIEnv *env, jobject obj, jint idx, jint newGroup)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->changeOverlayGroupInPresentationState (idx, (short) newGroup).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    overlayIsSuitableAsBitmapShutter
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_overlayIsSuitableAsBitmapShutter
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (ps->overlayIsSuitableAsBitmapShutter (idx) != 0) ? JNI_TRUE : JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    activateOverlayInImage
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_activateOverlayInImage
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->activateOverlayInImage (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    activateOverlayInPresentationState
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_activateOverlayInPresentationState
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->activateOverlayInPresentationState (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    activateOverlayAsBitmapShutter
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_activateOverlayAsBitmapShutter
  (JNIEnv *env, jobject obj, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->activateOverlayAsBitmapShutter (idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    deactivateOverlay
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_deactivateOverlay
  (JNIEnv *env, jobject obj, jint layer, jint idx)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->deactivateOverlay (layer, idx).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    moveOverlay
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_moveOverlay
  (JNIEnv *env, jobject obj, jint old_layer, jint idx, jint new_layer)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->moveOverlay (old_layer, idx, new_layer).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageMinMaxPixelRange
 * Signature: (LJ2Ci/jDoubleByRef;LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageMinMaxPixelRange
  (JNIEnv *env, jobject obj, jobject minValue, jobject maxValue)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double min, max;

    max = min = -123.45;

    OFCondition res = ps->getImageMinMaxPixelRange (min, max);

    if (res != EC_Normal) return (jint) res.status();

    jclass minDoubleByRefcls = env->GetObjectClass (minValue);
    jclass maxDoubleByRefcls = env->GetObjectClass (maxValue);

    jfieldID minvalue = env->GetFieldID (minDoubleByRefcls, "value", "D");
    jfieldID maxvalue = env->GetFieldID (maxDoubleByRefcls, "value", "D");

    env->SetDoubleField (minValue, minvalue, min);
    env->SetDoubleField (maxValue, maxvalue, max);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageMinMaxPixelValue
 * Signature: (LJ2Ci/jDoubleByRef;LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageMinMaxPixelValue
  (JNIEnv *env, jobject obj, jobject minValue, jobject maxValue)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    double min, max;

    max = min = -123.45;

    OFCondition res = ps->getImageMinMaxPixelValue (min, max);

    if (res != EC_Normal) return (jint) res.status();

    jclass minDoubleByRefcls = env->GetObjectClass (minValue);
    jclass maxDoubleByRefcls = env->GetObjectClass (maxValue);

    jfieldID minvalue = env->GetFieldID (minDoubleByRefcls, "value", "D");
    jfieldID maxvalue = env->GetFieldID (maxDoubleByRefcls, "value", "D");

    env->SetDoubleField (minValue, minvalue, min);
    env->SetDoubleField (maxValue, maxvalue, max);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageWidth
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageWidth
  (JNIEnv *env, jobject obj, jobject width)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long w;

    OFCondition res = ps->getImageWidth (w);

    if (res != EC_Normal) return (jint) res.status();

    jclass wIntByRefcls = env->GetObjectClass (width);

    jfieldID wvalue = env->GetFieldID (wIntByRefcls, "value", "I");

    env->SetIntField (width, wvalue, w);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageHeight
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageHeight
  (JNIEnv *env, jobject obj, jobject height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long h;

    OFCondition res = ps->getImageHeight (h);

    if (res != EC_Normal) return (jint) res.status();

    jclass hIntByRefcls = env->GetObjectClass (height);

    jfieldID hvalue = env->GetFieldID (hIntByRefcls, "value", "I");

    env->SetIntField (height, hvalue, h);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    numberOfImageReferences
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_numberOfImageReferences
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->numberOfImageReferences ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getImageReference
 * Signature: (ILJ2Ci/jDVPrStateParam_GetImageReference;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getImageReference
  (JNIEnv *env, jobject obj, jint idx, jobject param)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFString st, se, so, in, fr, ae, fid, fuid;

    OFCondition res = ps->getImageReference (idx, st, se, so, in, fr, ae, fid, fuid);

    if (res != EC_Normal) return res.status();

    jclass paramcls = env->GetObjectClass (param);
    jfieldID stfid = env->GetFieldID (paramcls, "studyUID", "Ljava/lang/String;");
    jfieldID sefid = env->GetFieldID (paramcls, "seriesUID", "Ljava/lang/String;");
    jfieldID sofid = env->GetFieldID (paramcls, "sopclassUID", "Ljava/lang/String;");
    jfieldID infid = env->GetFieldID (paramcls, "instanceUID", "Ljava/lang/String;");
    jfieldID frfid = env->GetFieldID (paramcls, "frames", "Ljava/lang/String;");
    jfieldID aefid = env->GetFieldID (paramcls, "aetitle", "Ljava/lang/String;");
    jfieldID fifid = env->GetFieldID (paramcls, "filesetID", "Ljava/lang/String;");
    jfieldID fufid = env->GetFieldID (paramcls, "filesetUID", "Ljava/lang/String;");

    jclass strcls = env->FindClass ("Ljava/lang/String;"); // string class
    jmethodID constr= env->GetMethodID (strcls, "<init>", "()V");  // constructor ID

    // create strings ...
    jobject stobj = env->NewObject (strcls, constr, env->NewStringUTF (st.c_str()));
    jobject seobj = env->NewObject (strcls, constr, env->NewStringUTF (se.c_str()));
    jobject soobj = env->NewObject (strcls, constr, env->NewStringUTF (so.c_str()));
    jobject inobj = env->NewObject (strcls, constr, env->NewStringUTF (in.c_str()));
    jobject frobj = env->NewObject (strcls, constr, env->NewStringUTF (fr.c_str()));
    jobject aeobj = env->NewObject (strcls, constr, env->NewStringUTF (ae.c_str()));
    jobject fiobj = env->NewObject (strcls, constr, env->NewStringUTF (fid.c_str()));
    jobject fuobj = env->NewObject (strcls, constr, env->NewStringUTF (fuid.c_str()));

    // ... and set parameter
    env->SetObjectField (param, stfid, stobj);
    env->SetObjectField (param, sefid, seobj);
    env->SetObjectField (param, sofid, soobj);
    env->SetObjectField (param, infid, inobj);
    env->SetObjectField (param, frfid, frobj);
    env->SetObjectField (param, aefid, aeobj);
    env->SetObjectField (param, fifid, fiobj);
    env->SetObjectField (param, fufid, fuobj);

    return (jint) res.status();
}



/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    exchangeGraphicLayers
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_exchangeGraphicLayers
  (JNIEnv *env, jobject obj, jint idx1, jint idx2)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jint) ps->exchangeGraphicLayers (idx1, idx2).status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    convertPValueToDDL
 * Signature: (II)S
 */
JNIEXPORT jshort JNICALL Java_J2Ci_jDVPresentationState_convertPValueToDDL
  (JNIEnv *env, jobject obj, jint pvalue, jint bits)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return  (jshort) ps->convertPValueToDDL ((unsigned short) pvalue, bits);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    createInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_createInstanceUID
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->createInstanceUID();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPixelData
 * Signature: ([BJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPixelData___3BJ
  (JNIEnv *env, jobject obj, jbyteArray pixelData, jlong size)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    void *array;
    jboolean isCopy;
    array = (void*) env->GetByteArrayElements (pixelData, &isCopy);

    OFCondition res = ps->getPixelData (array, (unsigned long) size);

    env->ReleaseByteArrayElements (pixelData, (jbyte *) array, 0);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    invertImage
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_invertImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = ps->invertImage();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    isInverse
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_isInverse
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    if (ps->isInverse()) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapSize
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapSize
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return ps->getPrintBitmapSize();
}



/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setMinimumPrintBitmapWidthHeight
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setMinimumPrintBitmapWidthHeight
  (JNIEnv *env, jobject obj, jlong width, jlong height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = ps->setMinimumPrintBitmapWidthHeight((unsigned long) width, (unsigned long) height);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setMaximumPrintBitmapWidthHeight
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setMaximumPrintBitmapWidthHeight
  (JNIEnv *env, jobject obj, jlong width, jlong height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = ps->setMaximumPrintBitmapWidthHeight((unsigned long) width, (unsigned long) height);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapWidthHeight
 * Signature: (LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapWidthHeight
  (JNIEnv *env, jobject obj, jobject width, jobject height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long w, h;

    OFCondition res = ps->getPrintBitmapWidthHeight (w, h);

    if (res != EC_Normal) return (jint) res.status();

    jclass widthCls = env->GetObjectClass (width);
    jclass heightCls = env->GetObjectClass (height);
    jfieldID wvalue = env->GetFieldID (widthCls, "value", "I");
    jfieldID hvalue = env->GetFieldID (heightCls, "value", "I");
    env->SetIntField (width, wvalue, (jint) w);
    env->SetIntField (height, hvalue, (jint) h);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapWidth
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapWidth
  (JNIEnv *env, jobject obj, jobject width)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long w;

    OFCondition res = ps->getPrintBitmapWidth (w);

    if (res != EC_Normal) return (jint) res.status();

    jclass widthCls = env->GetObjectClass (width);
    jfieldID wvalue = env->GetFieldID (widthCls, "value", "I");
    env->SetIntField (width, wvalue, (jint) w);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapHeight
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapHeight
  (JNIEnv *env, jobject obj, jobject height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long h;

    OFCondition res = ps->getPrintBitmapHeight (h);

    if (res != EC_Normal) return (jint) res.status();

    jclass heightCls = env->GetObjectClass (height);
    jfieldID hvalue = env->GetFieldID (heightCls, "value", "I");
    env->SetIntField (height, hvalue, (jint) h);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapPixelAspectRatio
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapPixelAspectRatio
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jdouble) ps->getPrintBitmapPixelAspectRatio();;
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmapRequestedImageSize
 * Signature: (LJ2Ci/jStringByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmapRequestedImageSize
  (JNIEnv *env, jobject obj, jobject requestedImageSize)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFString ris;

    OFCondition res = ps->getPrintBitmapRequestedImageSize (ris);

    if (res == EC_Normal)
    {
        jfieldID value = env->GetFieldID (env->GetObjectClass (requestedImageSize), "value", "Ljava/lang/String;");
        env->SetObjectField (requestedImageSize, value, env->NewStringUTF (ris.c_str()));
    }
    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPrintBitmap
 * Signature: ([SJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPrintBitmap
  (JNIEnv *env, jobject obj, jshortArray bitmap, jlong size)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    void *array;
    jboolean isCopy;
    array = (void*) env->GetShortArrayElements (bitmap, &isCopy);

    OFCondition res = ps->getPrintBitmap (array, (unsigned long) size);

    env->ReleaseShortArrayElements (bitmap, (jshort *) array, 0);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getCurrentImageModality
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVPresentationState_getCurrentImageModality
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    const char* res = ps->getCurrentImageModality();

    if (res == NULL) return NULL;

    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setDefaultPresentationLUTShape
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_setDefaultPresentationLUTShape
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFCondition res = ps->setDefaultPresentationLUTShape ();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    createPreviewImage
 * Signature: (IIZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_createPreviewImage
  (JNIEnv *env, jobject obj, jint maxWidth, jint maxHeight, jboolean clipMode)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    OFBool cm = clipMode == JNI_TRUE ? OFTrue : OFFalse;
    OFCondition res = ps->createPreviewImage (maxWidth, maxHeight, cm);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    deletePreviewImage
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_deletePreviewImage
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->deletePreviewImage ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPreviewImageSize
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVPresentationState_getPreviewImageSize
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jlong) ps->getPreviewImageSize ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPreviewImageWidthHeight
 * Signature: (LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPreviewImageWidthHeight
  (JNIEnv *env, jobject obj, jobject width, jobject height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long ww, hh;

    OFCondition res = ps->getPreviewImageWidthHeight (ww, hh);

    if (res != EC_Normal) return (jint) res.status();

    jclass wIntByRefcls = env->GetObjectClass (width);
    jclass hIntByRefcls = env->GetObjectClass (height);

    jfieldID wvalue = env->GetFieldID (wIntByRefcls, "value", "I");
    jfieldID hvalue = env->GetFieldID (hIntByRefcls, "value", "I");

    env->SetIntField (width, wvalue, (jint) ww);
    env->SetIntField (height, hvalue, (jint) hh);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPreviewImageWidth
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPreviewImageWidth
  (JNIEnv *env, jobject obj, jobject width)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long ww;

    OFCondition res = ps->getPreviewImageWidth (ww);

    if (res != EC_Normal) return (jint) res.status();

    jclass wIntByRefcls = env->GetObjectClass (width);
    jfieldID wvalue = env->GetFieldID (wIntByRefcls, "value", "I");
    env->SetIntField (width, wvalue, (jint) ww);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPreviewImageHeight
 * Signature: (LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPreviewImageHeight
  (JNIEnv *env, jobject obj, jobject height)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    unsigned long hh;

    OFCondition res = ps->getPreviewImageHeight (hh);

    if (res != EC_Normal) return (jint) res.status();

    jclass hIntByRefcls = env->GetObjectClass (height);
    jfieldID hvalue = env->GetFieldID (hIntByRefcls, "value", "I");
    env->SetIntField (height, hvalue, (jint) hh);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getPreviewImageBitmap
 * Signature: ([BJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getPreviewImageBitmap
  (JNIEnv *env, jobject obj, jbyteArray bitmap, jlong size)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    void *array;
    jboolean isCopy;
    array = (void*) env->GetByteArrayElements (bitmap, &isCopy);

    OFCondition res = ps->getPreviewImageBitmap (array, (unsigned long) size);

    env->ReleaseByteArrayElements (bitmap, (jbyte *) array, 0);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getDisplayTransform
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVPresentationState_getDisplayTransform
  (JNIEnv *env, jobject obj)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    return (jint) ps->getDisplayTransform ();
}


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    setDisplayTransform
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVPresentationState_setDisplayTransform
  (JNIEnv *env, jobject obj, jint transform)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    ps->setDisplayTransform ((DVPSDisplayTransform)transform);
}


// ----------------------------------------------------------------------------------


/*
 * Class:     J2Ci_jDVPresentationState
 * Method:    getScreenData
 * Signature: (LJ2Ci/jDVPrStateParam_GetScreenData;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVPresentationState_getScreenData
  (JNIEnv *env, jobject obj, jobject info)
{
    DVPresentationState *ps = getAddressOfDVPresentationState (env, obj);

    jclass infocls = env->GetObjectClass (info);
    jfieldID pixarrayfid = env->GetFieldID (infocls, "pixelArray", "[B");
    jfieldID widthfid = env->GetFieldID (infocls, "width", "I");
    jfieldID heightfid = env->GetFieldID (infocls, "height", "I");
    jfieldID offsetxfid = env->GetFieldID (infocls, "offsetX", "I");
    jfieldID offsetyfid = env->GetFieldID (infocls, "offsetY", "I");
    jfieldID tlhcxfid = env->GetFieldID (infocls, "TLHC_x", "I");
    jfieldID tlhcyfid = env->GetFieldID (infocls, "TLHC_y", "I");
    jfieldID brhcxfid = env->GetFieldID (infocls, "BRHC_x", "I");
    jfieldID brhcyfid = env->GetFieldID (infocls, "BRHC_y", "I");
    jfieldID zoomfid = env->GetFieldID (infocls, "zoomfactor", "D");
    jfieldID scalexfid = env->GetFieldID (infocls, "scale_x", "D");
    jfieldID scaleyfid = env->GetFieldID (infocls, "scale_y", "D");
    jfieldID interpolfid = env->GetFieldID (infocls, "interpolate", "Z");

    int width, height, offx, offy, tlhcx, tlhcy;
    int brhcx=0, brhcy=0;
    double zoom, scalex, scaley;
    int interpolate;
    unsigned char *array;

    width = env->GetIntField (info, widthfid);
    height = env->GetIntField (info, heightfid);
    offx = env->GetIntField (info, offsetxfid);
    offy = env->GetIntField (info, offsetyfid);
    tlhcx = env->GetIntField (info, tlhcxfid);
    tlhcy = env->GetIntField (info, tlhcyfid);

    zoom = env->GetDoubleField (info, zoomfid);
    scalex = env->GetDoubleField (info, scalexfid);
    scaley = env->GetDoubleField (info, scaleyfid);

    jbyteArray parr = (jbyteArray) env->GetObjectField (info, pixarrayfid);

    jboolean isCopy;
    array = (unsigned char*) env->GetByteArrayElements (parr, &isCopy);
    interpolate = (env->GetBooleanField (info, interpolfid) != 0) ? -1 : 0;


    // ------------------------------ Werte sind in C++-Typen drin


    if (width == 0 || height == 0 || offx < 0 ||
        offy < 0 || zoom <= 0 || scalex <= 0 ||
        scaley <= 0) return JNI_FALSE;          // Fehler in Eingabewerten

    OFCondition res;

    const char *orgPixData;
    unsigned long oWidth;
    unsigned long oHeight;

    res = ps->getPixelData ((const void *&) orgPixData, oWidth, oHeight);
    if (res != EC_Normal) return JNI_FALSE;

    int px, py, xr, yr;

    double scx = zoom * scalex;
    double scy = zoom * scaley;
    double ipolx, ipoly;

    int m;          // F�r Gleichung zur Interpolation X/Y-Richtung
    int e1, e2, e3; // Ergebnisse f�r 3 Gleichungen
    int thisPoint;
    int arrPt, dstPt, dstPtnL;
    double xrscx, yrscy;

    for (int y = 0; y < height; y++)
    {
        yr = y - offy;
        yrscy = yr / scy;
        py = tlhcy + (int) yrscy;

        for (int x = 0; x < width; x++)
        {
            xr = x - offx;
            arrPt = y*width+x;

            if (xr >= 0 && yr >= 0) // Out of border ?
            {
                xrscx = xr / scx;
                px = tlhcx + (int) xrscx; // Point
                dstPt = py*oWidth+px;


                if ((px < (int) oWidth && py < (int) oHeight) &&
                    (px >= 0 && py >= 0))
                {
                    if (interpolate == 0)
                    {
                        array[arrPt] = orgPixData[dstPt];
                    }
                    else
                    {
                        ipolx = xrscx - (int) xrscx;
                        ipoly = yrscy - (int) yrscy;


                        dstPtnL = (py+1)*oWidth+px;

                        // F�r die 2D-interpolation brauchen wir 4 Punkte, zwischen
                        // denen interpoliert werden soll (2 Punkte pro Richtung).
                        // Wenn die Werte au�erhab des zul�ssigen Bereiches liegen,
                        // dann werden die aktuellen Werte stattdessen benutzt.
                        // Davon wird pro Richtung jeweils eine Gleichung ermittelt.

                        thisPoint = ((int) orgPixData[dstPt]) & 0xff;

                        if (px+1 >= (int) oWidth || py+1 >= (int) oHeight)
                        {
                            e3 = thisPoint; // Randbezirk
                        }
                        else
                        {
                            if (ipolx != 0 && ipoly != 0)
                            {
                                m = (orgPixData[dstPt+1] & 0xff) - thisPoint; // 1.
                                e1 = (int) (m*ipolx) + thisPoint;

                                m = (orgPixData[dstPtnL+1] & 0xff) - (orgPixData[dstPtnL] & 0xff);
                                e2 = (int) (m*ipolx) + (orgPixData[dstPtnL] & 0xff);

                                e3 = (int) ((e2-e1)*ipoly) + e1;
                            }
                            else if (ipolx != 0 && ipoly == 0)
                            {
                                m = (orgPixData[dstPt+1] & 0xff) - (orgPixData[dstPt] & 0xff);
                                e3 = (int) (m*ipolx) + (orgPixData[dstPt] & 0xff);

                            }
                            else if (ipolx == 0 && ipoly != 0)
                            {
                                m = (orgPixData[dstPtnL] & 0xff) - (orgPixData[dstPt] & 0xff);
                                e3 = (int) (m*ipoly) + (orgPixData[dstPt] & 0xff);
                            }
                            else // beide gleich 0
                            {
                                e3 = thisPoint;
                            }

                        }

                        array[arrPt] = (unsigned char) e3;

                    }

                    brhcx = px; // maximale sichtbare untere rechte Eckpos merken
                    brhcy = py;
                }
                else
                {
                    array[arrPt] = 0; // schwarz
                }
            }
            else
            {
                array[arrPt] = 0; // schwarz
            }
        }
    }

    env->SetIntField (info, brhcxfid, brhcx);
    env->SetIntField (info, brhcyfid, brhcy);

    // ------------------------------- Array zur�ckschreiben

    env->ReleaseByteArrayElements (parr, (jbyte *) array, 0);

    return JNI_TRUE;
}


/*
 *  CVS Log
 *  $Log: DVPresentationState.cpp,v $
 *  Revision 1.2  1999/05/14 10:02:31  Schroeter
 *  Log-Tags for CVS corrected
 *
 *  Revision 1.1.1.1  1999/05/14 09:33:11  Schroeter
 *  initial commit of release 1.0
 *
*/
