/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 * Author :      $Author: Schroeter $
 * Last update : $Date: 1999/05/14 10:02:31 $
 * Revision :    $Revision: 1.2 $
 * State :       $State: Exp $
*/


#include "J2Ci_jDVInterface.h"
#include "J2Ci_jDVPresentationState.h"
#include "J2Ci_jDSRDocument.h"

#include "jInterface.h"

#include "dcrledrg.h"      /* for DcmRLEDecoderRegistration */
#include "djdecode.h"      /* for dcmjpeg decoders */


// ------------------- accessing C++-Objects

inline DVInterface* getAddressOfDVInterface (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DVInterface*) env->GetLongField (obj, fid);
}

inline void setAddressOfDVInterface (JNIEnv *env, jobject obj, DVInterface* dvi)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) dvi);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    createObjOfDVInterface
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_createObjOfDVInterface
  (JNIEnv *env, jobject obj, jstring configfile)
{
    char *fn = (char*) env->GetStringUTFChars (configfile, 0);

    DVInterface *dvi = new DVInterface (fn, OFTrue);

    env->ReleaseStringUTFChars (configfile, fn);

    setAddressOfDVInterface (env, obj, dvi);

    // register RLE decompression codec
    DcmRLEDecoderRegistration::registerCodecs();
    // register JPEG decompression codecs
    DJDecoderRegistration::registerCodecs();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    removeObjOfDVInterface
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_removeObjOfDVInterface
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    delete dvi;

    // deregister RLE decompression codecs
    DcmRLEDecoderRegistration::cleanup();
    // deregister JPEG decompression codecs
    DJDecoderRegistration::cleanup();
}


// ----------------------------------------------------------------------------
// native Methods
// ----------------------------------------------------------------------------


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadImage
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadImage__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Z
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->loadImage (stu, ser, ins, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadImage
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadImage__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);

    OFCondition res = dvi->loadImage (fn);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadReferencedImage
 * Signature: (IZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadReferencedImage
  (JNIEnv *env, jobject obj, jint idx, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->loadReferencedImage (idx, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadPState
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadPState__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Z
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->loadPState (stu, ser, ins, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadPState
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadPState__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring pstName)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *psn = (char*) env->GetStringUTFChars (pstName, 0);

    OFCondition res = dvi->loadPState (psn);

    env->ReleaseStringUTFChars (pstName, psn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadPState
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadPState__Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring pstName, jstring imgName)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *psn = (char*) env->GetStringUTFChars (pstName, 0);
    char *imn = (char*) env->GetStringUTFChars (imgName, 0);

    OFCondition res = dvi->loadPState (psn, imn);

    env->ReleaseStringUTFChars (pstName, psn);
    env->ReleaseStringUTFChars (imgName, imn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadStructuredReport
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadStructuredReport__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Z
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->loadStructuredReport(stu, ser, ins, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadStructuredReport
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadStructuredReport__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);

    OFCondition res = dvi->loadStructuredReport(fn);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadSRTemplate
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadSRTemplate
  (JNIEnv *env, jobject obj, jstring reportID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (reportID, 0);

    OFCondition res = dvi->loadSRTemplate(id);

    env->ReleaseStringUTFChars (reportID, id);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    savePState
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_savePState__Z
  (JNIEnv *env, jobject obj, jboolean replaceSOPInstanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->savePState(replaceSOPInstanceUID == JNI_TRUE ? OFTrue : OFFalse);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    savePState
 * Signature: (Ljava/lang/String;ZZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_savePState__Ljava_lang_String_2ZZ
  (JNIEnv *env, jobject obj, jstring filename, jboolean replaceSOPInstanceUID, jboolean explicitVR)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char *) env->GetStringUTFChars (filename, 0);
    OFBool explVR  = (explicitVR == JNI_TRUE) ? OFTrue : OFFalse;
    OFBool replUID = (replaceSOPInstanceUID == JNI_TRUE) ? OFTrue : OFFalse;

    OFCondition res = dvi->savePState(fn, replUID, explVR);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveCurrentImage
 * Signature: (Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveCurrentImage
  (JNIEnv *env, jobject obj, jstring filename, jboolean explicitVR)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);
    OFBool expVR = explicitVR == JNI_TRUE ? OFTrue : OFFalse;

    OFCondition res = dvi->saveCurrentImage(fn, expVR);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveStructuredReport
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveStructuredReport__
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->saveStructuredReport();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveStructuredReport
 * Signature: (Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveStructuredReport__Ljava_lang_String_2Z
  (JNIEnv *env, jobject obj, jstring filename, jboolean explicitVR)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char *) env->GetStringUTFChars (filename, 0);
    OFBool expVR = (explicitVR == JNI_TRUE) ? OFTrue : OFFalse;

    OFCondition res = dvi->saveStructuredReport(fn, expVR);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    addImageReferenceToPState
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_addImageReferenceToPState
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->addImageReferenceToPState (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfImageReferences
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfImageReferences
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfImageReferences ();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentPStateN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVInterface_getCurrentPStateN
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    DVPresentationState *ps = &(dvi->getCurrentPState ());

    return (jlong) ps;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentReportN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVInterface_getCurrentReportN
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    DSRDocument *doc = &(dvi->getCurrentReport());

    return (jlong) doc;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrintHandlerN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVInterface_getPrintHandlerN
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    DVPSStoredPrint *ps = &(dvi->getPrintHandler ());

    return (jlong) ps;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    resetPresentationState
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_resetPresentationState
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->resetPresentationState();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveCurrentPStateForReset
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveCurrentPStateForReset
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->saveCurrentPStateForReset();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    releaseDatabase
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_releaseDatabase
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->releaseDatabase();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getFilename
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getFilename__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    const char* res = dvi->getFilename (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfStudies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfStudies
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfStudies();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectStudy
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectStudy__I
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->selectStudy(idx);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectStudy
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectStudy__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring studyUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);

    OFCondition res = dvi->selectStudy (stu);

    env->ReleaseStringUTFChars (studyUID, stu);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getStudyStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getStudyStatus
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getStudyStatus();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getStudyUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getStudyUID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getStudyUID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getStudyDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getStudyDescription
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getStudyDescription();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getStudyDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getStudyDate
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getStudyDate();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getStudyTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getStudyTime
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getStudyTime();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getReferringPhysiciansName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getReferringPhysiciansName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getReferringPhysiciansName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getAccessionNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getAccessionNumber
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getAccessionNumber();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNameOfPhysiciansReadingStudy
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getNameOfPhysiciansReadingStudy
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getNameOfPhysiciansReadingStudy();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPatientName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPatientName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPatientName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPatientID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPatientID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPatientID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPatientBirthDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPatientBirthDate
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPatientBirthDate();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPatientSex
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPatientSex
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPatientSex();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPatientBirthTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPatientBirthTime
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPatientBirthTime();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getOtherPatientNames
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getOtherPatientNames
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getOtherPatientNames();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getOtherPatientID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getOtherPatientID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getOtherPatientID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getEthnicGroup
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getEthnicGroup
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getEthnicGroup();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfSeries
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfSeries
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfSeries();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectSeries
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectSeries__I
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->selectSeries(idx);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectSeries
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectSeries__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring seriesUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);

    OFCondition res = dvi->selectSeries (ser);

    env->ReleaseStringUTFChars (seriesUID, ser);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesUID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesUID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getSeriesStatus
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getSeriesStatus();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getSeriesType
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getSeriesType();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getInstanceType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getInstanceType
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getInstanceType();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesNumber
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesNumber();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesDate
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesDate
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesDate();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesTime
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesTime();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesDescription
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesDescription();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesPerformingPhysiciansName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesPerformingPhysiciansName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesPerformingPhysiciansName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesProtocolName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesProtocolName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesProtocolName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getSeriesOperatorsName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getSeriesOperatorsName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getSeriesOperatorsName();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getModality
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getModality
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getModality();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfInstances
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfInstances
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfInstances();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectInstance
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectInstance__I
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->selectInstance(idx);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectInstance
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectInstance__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->selectInstance (ins);

    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectInstance
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectInstance__Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring instanceUID, jstring sopClassUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);
    char *sop = (char*) env->GetStringUTFChars (sopClassUID, 0);

    OFCondition res = dvi->selectInstance (ins, sop);

    env->ReleaseStringUTFChars (instanceUID, ins);
    env->ReleaseStringUTFChars (sopClassUID, sop);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectInstance
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectInstance__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->selectInstance (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getInstanceUID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getInstanceUID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getImageNumber
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getImageNumber
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getImageNumber();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getFilename
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getFilename__
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getFilename();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getInstanceStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getInstanceStatus
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getInstanceStatus();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getInstanceDescription
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getInstanceDescription
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getInstanceDescription();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPresentationLabel
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPresentationLabel
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPresentationLabel();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    instanceReviewed
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_instanceReviewed
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->instanceReviewed(stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    deleteInstance
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_deleteInstance
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->deleteInstance(stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    deleteSeries
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_deleteSeries
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);

    OFCondition res = dvi->deleteSeries(stu, ser);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    deleteStudy
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_deleteStudy
  (JNIEnv *env, jobject obj, jstring studyUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);

    OFCondition res = dvi->deleteStudy(stu);

    env->ReleaseStringUTFChars (studyUID, stu);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    startReceiver
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_startReceiver
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->startReceiver();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    terminateReceiver
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_terminateReceiver
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->terminateReceiver();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    startQueryRetrieveServer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_startQueryRetrieveServer
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->startQueryRetrieveServer();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    terminateQueryRetrieveServer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_terminateQueryRetrieveServer
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->terminateQueryRetrieveServer();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    newInstancesReceived
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_newInstancesReceived
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->newInstancesReceived();

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    sendStudy
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_sendStudy
  (JNIEnv *env, jobject obj, jstring targetID, jstring studyUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFCondition res = dvi->sendStudy(tid, stu);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    sendSeries
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_sendSeries
  (JNIEnv *env, jobject obj, jstring targetID, jstring studyUID, jstring seriesUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);

    OFCondition res = dvi->sendSeries(tid, stu, ser);

    env->ReleaseStringUTFChars (targetID, tid);
    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    sendIOD
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_sendIOD
  (JNIEnv *env, jobject obj, jstring targetID, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->sendIOD(tid, stu, ser, ins);

    env->ReleaseStringUTFChars (targetID, tid);
    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    dumpIOD
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_dumpIOD__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);

    OFCondition res = dvi->dumpIOD (fn);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    dumpIOD
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_dumpIOD__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->dumpIOD (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    checkIOD
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_checkIOD__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);

    OFCondition res = dvi->checkIOD (fn);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    checkIOD
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_checkIOD__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->checkIOD (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveDICOMImage
 * Signature: (Ljava/lang/String;[BJJD)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveDICOMImage__Ljava_lang_String_2_3BJJD
  (JNIEnv *env, jobject obj, jstring filename, jbyteArray pixelData, jlong width,
   jlong height, jdouble aspectRatio)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fname = (char*) env->GetStringUTFChars (filename, 0);

    const void *array;
    jboolean isCopy;
    array = (const void*) env->GetByteArrayElements (pixelData, &isCopy);

    OFCondition res = dvi->saveDICOMImage (fname, array, (long) width, (long) height, aspectRatio);

    env->ReleaseStringUTFChars (filename, fname);
    env->ReleaseByteArrayElements (pixelData, (jbyte *) array, JNI_ABORT);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveDICOMImage
 * Signature: (Ljava/lang/String;[BJJDZLjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveDICOMImage__Ljava_lang_String_2_3BJJDZLjava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename, jbyteArray pixelData, jlong width, jlong height,
   jdouble aspectRatio, jboolean explicitVR, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fname = (char*) env->GetStringUTFChars (filename, 0);
    char *iUID = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFBool expVR = explicitVR == JNI_TRUE ? OFTrue : OFFalse;

    const void *array;
    jboolean isCopy;
    array = (const void*) env->GetByteArrayElements (pixelData, &isCopy);

    OFCondition res = dvi->saveDICOMImage (fname, array, (long) width, (long) height, aspectRatio, expVR, iUID);

    env->ReleaseStringUTFChars (filename, fname);
    env->ReleaseStringUTFChars (instanceUID, iUID);
    env->ReleaseByteArrayElements (pixelData, (jbyte *) array, JNI_ABORT);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveDICOMImage
 * Signature: ([BJJD)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveDICOMImage___3BJJD
  (JNIEnv *env, jobject obj, jbyteArray pixelData, jlong width, jlong height, jdouble aspectRatio)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const void *array;
    jboolean isCopy;
    array = (const void*) env->GetByteArrayElements (pixelData, &isCopy);

    OFCondition res = dvi->saveDICOMImage (array, (long) width, (long) height, aspectRatio);

    env->ReleaseByteArrayElements (pixelData, (jbyte *) array, JNI_ABORT);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveGrayscaleHardcopyImage
 * Signature: (Ljava/lang/String;[SJJDZLjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveGrayscaleHardcopyImage__Ljava_lang_String_2_3SJJDZLjava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename, jshortArray pixelData, jlong width, jlong height,
   jdouble aspectRatio, jboolean explicitVR, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);
    char *ui = (char*) env->GetStringUTFChars (instanceUID, 0);


    void *array;
    jboolean isCopy;
    array = (void*) env->GetShortArrayElements (pixelData, &isCopy);

    OFBool explVR = (explicitVR == JNI_TRUE) ? OFTrue : OFFalse;

    OFCondition res = dvi->saveHardcopyGrayscaleImage (fn, array, (unsigned long) width, (unsigned long) height, aspectRatio, explVR, ui);

    env->ReleaseStringUTFChars (filename, fn);
    env->ReleaseStringUTFChars (instanceUID, ui);
    env->ReleaseShortArrayElements (pixelData, (jshort *) array, 0);

    return res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveGrayscaleHardcopyImage
 * Signature: ([SJJD)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveGrayscaleHardcopyImage___3SJJD
  (JNIEnv *env, jobject obj, jshortArray pixelData, jlong width, jlong height, jdouble aspectRatio)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    void *array;
    jboolean isCopy;
    array = (void*) env->GetShortArrayElements (pixelData, &isCopy);

    OFCondition res = dvi->saveHardcopyGrayscaleImage (array, (unsigned long) width, (unsigned long) height, aspectRatio);

    env->ReleaseShortArrayElements (pixelData, (jshort *) array, 0);

    return res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadStoredPrint
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadStoredPrint__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_String_2Z
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->loadStoredPrint (stu, ser, ins, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadStoredPrint
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadStoredPrint__Ljava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);

    OFCondition res = dvi->loadStoredPrint (fn);

    env->ReleaseStringUTFChars (filename, fn);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveStoredPrint
 * Signature: (Ljava/lang/String;ZZLjava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveStoredPrint__Ljava_lang_String_2ZZLjava_lang_String_2
  (JNIEnv *env, jobject obj, jstring filename, jboolean writeRequestedImageSize, jboolean explicitVR, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *fn = (char*) env->GetStringUTFChars (filename, 0);
    char *ui = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFBool explVR = (explicitVR == JNI_TRUE) ? OFTrue : OFFalse;
    OFBool wrRqImgS = (writeRequestedImageSize == JNI_TRUE) ? OFTrue : OFFalse;

    OFCondition res = dvi->saveStoredPrint (fn, wrRqImgS, explVR, ui);

    env->ReleaseStringUTFChars (filename, fn);
    env->ReleaseStringUTFChars (instanceUID, ui);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    saveStoredPrint
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_saveStoredPrint__Z
  (JNIEnv *env, jobject obj, jboolean writeRequestedImageSize)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool wrRqImgS = (writeRequestedImageSize == JNI_TRUE) ? OFTrue : OFFalse;

    OFCondition res = dvi->saveStoredPrint (wrRqImgS);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfPrintPreviews
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfPrintPreviews
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfPrintPreviews();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    loadPrintPreview
 * Signature: (IZZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_loadPrintPreview
  (JNIEnv *env, jobject obj, jint idx, jboolean printLUT, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->loadPrintPreview(idx, printLUT == JNI_TRUE ? OFTrue : OFFalse, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    unloadPrintPreview
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_unloadPrintPreview
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->unloadPrintPreview();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrintPreviewSize
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDVInterface_getPrintPreviewSize
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jlong) dvi->getPrintPreviewSize();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setMaxPrintPreviewWidthHeight
 * Signature: (II)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setMaxPrintPreviewWidthHeight
  (JNIEnv *env, jobject obj, jint width, jint height)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setMaxPrintPreviewWidthHeight(width, height);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrintPreviewWidthHeight
 * Signature: (LJ2Ci/jIntByRef;LJ2Ci/jIntByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getPrintPreviewWidthHeight
  (JNIEnv *env, jobject obj, jobject width, jobject height)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    unsigned long w, h;

    OFCondition res = dvi->getPrintPreviewWidthHeight (w, h);

    if (res != EC_Normal) return (jint) res.status();

    jclass wIntByRefcls = env->GetObjectClass (width);
    jclass hIntByRefcls = env->GetObjectClass (height);

    jfieldID wvalue = env->GetFieldID (wIntByRefcls, "value", "I");
    jfieldID hvalue = env->GetFieldID (hIntByRefcls, "value", "I");

    env->SetIntField (width, wvalue, (jint) w);
    env->SetIntField (height, hvalue, (jint) h);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrintPreviewBitmap
 * Signature: ([BJ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getPrintPreviewBitmap
  (JNIEnv *env, jobject obj, jbyteArray bitmap, jlong size)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    void *array;
    jboolean isCopy;
    array = (void*) env->GetByteArrayElements (bitmap, &isCopy);

    OFCondition res = dvi->getPrintPreviewBitmap (array, (unsigned long) size);

    env->ReleaseByteArrayElements (bitmap, (jbyte *) array, 0);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    disablePState
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_disablePState
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->disablePState();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    enablePState
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_enablePState
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->enablePState();

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfPStates
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfPStates
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfPStates();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectPState
 * Signature: (IZ)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectPState
  (JNIEnv *env, jobject obj, jint idx, jboolean changeStatus)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->selectPState(idx, changeStatus == JNI_TRUE ? OFTrue : OFFalse);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPStateDescription
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPStateDescription
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPStateDescription(idx);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPStateLabel
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPStateLabel
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPStateLabel(idx);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    isDisplayTransformPossible
 * Signature: (I)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_isDisplayTransformPossible
  (JNIEnv *env, jobject obj, jint transform)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->isDisplayTransformPossible ((DVPSDisplayTransform)transform);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setAmbientLightValue
 * Signature: (D)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setAmbientLightValue
  (JNIEnv *env, jobject obj, jdouble alv)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFCondition res = dvi->setAmbientLightValue(alv);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getAmbientLightValue
 * Signature: (LJ2Ci/jDoubleByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getAmbientLightValue
  (JNIEnv *env, jobject obj, jobject alv)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    double _alv = 0.0;

    OFCondition res = dvi->getAmbientLightValue(_alv);

    if (res != EC_Normal) return (jint) res.status();

    jclass DoubleByRefcls = env->GetObjectClass (alv);

    jfieldID value = env->GetFieldID (DoubleByRefcls, "value", "D");

    env->SetDoubleField (alv, value, _alv);

    return (jint) EC_Normal.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setCurrentPrinter
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setCurrentPrinter
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFCondition res = dvi->setCurrentPrinter(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentPrinter
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getCurrentPrinter
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getCurrentPrinter();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterMediumType
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterMediumType
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *val = (char *) env->GetStringUTFChars (value, 0);

    OFCondition res = dvi->setPrinterMediumType(val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterMediumType
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrinterMediumType
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrinterMediumType();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterFilmDestination
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterFilmDestination
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *val = (char *) env->GetStringUTFChars (value, 0);

    OFCondition res = dvi->setPrinterFilmDestination(val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterFilmDestination
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrinterFilmDestination
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrinterFilmDestination();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterFilmSessionLabel
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterFilmSessionLabel
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *val = (char *) env->GetStringUTFChars (value, 0);

    OFCondition res = dvi->setPrinterFilmSessionLabel(val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


  /*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterFilmSessionLabel
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrinterFilmSessionLabel
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrinterFilmSessionLabel();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterPriority
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterPriority
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = dvi->setPrinterPriority(val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}



/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterPriority
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrinterPriority
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrinterPriority();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterOwnerID
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterOwnerID
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *val = (char*) env->GetStringUTFChars (value, 0);

    OFCondition res = dvi->setPrinterOwnerID(val);

    env->ReleaseStringUTFChars (value, val);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterOwnerID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrinterOwnerID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrinterOwnerID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrinterNumberOfCopies
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_setPrinterNumberOfCopies
  (JNIEnv *env, jobject obj, jint value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->setPrinterNumberOfCopies((long) value).status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrinterNumberOfCopies
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getPrinterNumberOfCopies
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getPrinterNumberOfCopies();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    clearFilmSessionSettings
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_clearFilmSessionSettings
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->clearFilmSessionSettings();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectDisplayPresentationLUT
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectDisplayPresentationLUT
  (JNIEnv *env, jobject obj, jstring lutID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *lid = (char*) env->GetStringUTFChars (lutID, 0);

    OFCondition res = dvi->selectDisplayPresentationLUT(lid);

    env->ReleaseStringUTFChars (lutID, lid);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getDisplayPresentationLUTID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getDisplayPresentationLUTID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getDisplayPresentationLUTID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    selectPrintPresentationLUT
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_selectPrintPresentationLUT
  (JNIEnv *env, jobject obj, jstring lutID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *lid = (char*) env->GetStringUTFChars (lutID, 0);

    OFCondition res = dvi->selectPrintPresentationLUT(lid);

    env->ReleaseStringUTFChars (lutID, lid);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrintPresentationLUTID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getPrintPresentationLUTID
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getPrintPresentationLUTID();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    spoolPrintJob
 * Signature: (Z)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_spoolPrintJob
  (JNIEnv *env, jobject obj, jboolean deletePrintedImages)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->spoolPrintJob((deletePrintedImages == JNI_TRUE) ? OFTrue : OFFalse).status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    startPrintSpooler
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_startPrintSpooler
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->startPrintSpooler().status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    terminatePrintSpooler
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_terminatePrintSpooler
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->terminatePrintSpooler().status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    startPrintServer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_startPrintServer
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->startPrintServer().status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    terminatePrintServer
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_terminatePrintServer
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->terminatePrintServer().status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    addToPrintHardcopyFromDB
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_addToPrintHardcopyFromDB
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->addToPrintHardcopyFromDB (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    spoolStoredPrintFromDB
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_spoolStoredPrintFromDB
  (JNIEnv *env, jobject obj, jstring studyUID, jstring seriesUID, jstring instanceUID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *stu = (char*) env->GetStringUTFChars (studyUID, 0);
    char *ser = (char*) env->GetStringUTFChars (seriesUID, 0);
    char *ins = (char*) env->GetStringUTFChars (instanceUID, 0);

    OFCondition res = dvi->spoolStoredPrintFromDB (stu, ser, ins);

    env->ReleaseStringUTFChars (studyUID, stu);
    env->ReleaseStringUTFChars (seriesUID, ser);
    env->ReleaseStringUTFChars (instanceUID, ins);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    isActiveAnnotation
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_isActiveAnnotation
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->isActiveAnnotation ();

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrependDateTime
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getPrependDateTime
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->getPrependDateTime ();

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrependPrinterName
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getPrependPrinterName
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->getPrependPrinterName ();

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getPrependLighting
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getPrependLighting
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    OFBool res = dvi->getPrependLighting ();

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getAnnotationText
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getAnnotationText
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getAnnotationText();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setActiveAnnotation
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setActiveAnnotation
  (JNIEnv *env, jobject obj, jboolean value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setActiveAnnotation(value == JNI_TRUE ? OFTrue : OFFalse);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrependDateTime
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setPrependDateTime
  (JNIEnv *env, jobject obj, jboolean value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setPrependDateTime(value == JNI_TRUE ? OFTrue : OFFalse);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrependPrinterName
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setPrependPrinterName
  (JNIEnv *env, jobject obj, jboolean value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setPrependPrinterName(value == JNI_TRUE ? OFTrue : OFFalse);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setPrependLighting
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setPrependLighting
  (JNIEnv *env, jobject obj, jboolean value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setPrependLighting(value == JNI_TRUE ? OFTrue : OFFalse);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setAnnotationText
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setAnnotationText
  (JNIEnv *env, jobject obj, jstring value)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    jmethodID mid = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes", "(Ljava/lang/String;)[B");

    if (mid)
    {
        jbyteArray array = (jbyteArray) env->CallObjectMethod(value, mid, env->NewStringUTF(JAVA_ENCODING_STRING));

        if (array)
        {
            jbyte *string = env->GetByteArrayElements(array, 0);

            dvi->setAnnotationText(OFString((char *)string, env->GetArrayLength(array)).c_str());

            env->ReleaseByteArrayElements(array, string, 0);
        }
    }
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    verifyUserPassword
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_verifyUserPassword
  (JNIEnv *env, jobject obj, jstring userID, jstring passwd)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *str1 = (char*) env->GetStringUTFChars (userID, 0);
    char *str2 = (char*) env->GetStringUTFChars (passwd, 0);

    OFBool res = dvi->verifyUserPassword (str1, str2);

    env->ReleaseStringUTFChars (userID, str1);
    env->ReleaseStringUTFChars (passwd, str2);

    return (jboolean) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    verifyAndSignStructuredReport
 * Signature: (Ljava/lang/String;Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_verifyAndSignStructuredReport
  (JNIEnv *env, jobject obj, jstring userID, jstring passwd, jint mode)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *str1 = (char*) env->GetStringUTFChars (userID, 0);
    char *str2 = (char*) env->GetStringUTFChars (passwd, 0);

    OFCondition res = dvi->verifyAndSignStructuredReport (str1, str2, (DVPSVerifyAndSignMode) mode);

    env->ReleaseStringUTFChars (userID, str1);
    env->ReleaseStringUTFChars (passwd, str2);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    setLogFilter
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_setLogFilter
  (JNIEnv *env, jobject obj, jint level)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->setLogFilter ((DVPSLogMessageLevel)level);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    writeLogMessage
 * Signature: (ILjava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_writeLogMessage
  (JNIEnv *env, jobject obj, jint level, jstring module, jstring message)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *mod = (char*) env->GetStringUTFChars (module, 0);
    char *msg = (char*) env->GetStringUTFChars (message, 0);

    OFCondition res = dvi->writeLogMessage ((DVPSLogMessageLevel)level, mod, msg);

    env->ReleaseStringUTFChars (module, mod);
    env->ReleaseStringUTFChars (message, msg);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentSignatureValidationHTML
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getCurrentSignatureValidationHTML
  (JNIEnv *env, jobject obj, jint objtype)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getCurrentSignatureValidationHTML ((DVPSObjectType) objtype);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentSignatureValidationOverview
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getCurrentSignatureValidationOverview
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getCurrentSignatureValidationOverview ();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCurrentSignatureStatus
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getCurrentSignatureStatus
  (JNIEnv *env, jobject obj, jint objtype)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getCurrentSignatureStatus((DVPSObjectType) objtype);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getCombinedImagePStateSignatureStatus
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getCombinedImagePStateSignatureStatus
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getCombinedImagePStateSignatureStatus();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfCorrectSignatures
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfCorrectSignatures
  (JNIEnv *env, jobject obj, jint objtype)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfCorrectSignatures((DVPSObjectType) objtype);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfUntrustworthySignatures
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfUntrustworthySignatures
  (JNIEnv *env, jobject obj, jint objtype)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfUntrustworthySignatures((DVPSObjectType) objtype);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfCorruptSignatures
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfCorruptSignatures
  (JNIEnv *env, jobject obj, jint objtype)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfCorruptSignatures((DVPSObjectType) objtype);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    disableImageAndPState
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDVInterface_disableImageAndPState
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    dvi->disableImageAndPState();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfTargets
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfTargets
  (JNIEnv *env, jobject obj, jint peerType)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    int res = dvi->getNumberOfTargets((DVPSPeerType) peerType);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetID
 * Signature: (II)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetID
  (JNIEnv *env, jobject obj, jint idx, jint peerType)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getTargetID(idx, (DVPSPeerType) peerType);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetDescription
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetDescription
  (JNIEnv *env , jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    const char *res = dvi->getTargetDescription (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetHostname
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetHostname
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    const char* res = dvi->getTargetHostname(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPort
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPort
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPort(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetType
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetType
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    DVPSPeerType res = dvi->getTargetType(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetAETitle
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetAETitle
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    const char* res = dvi->getTargetAETitle (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetMaxPDU
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetMaxPDU
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetMaxPDU (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetImplicitOnly
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetImplicitOnly
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetImplicitOnly (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetDisableNewVRs
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetDisableNewVRs
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetDisableNewVRs(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetBitPreservingMode
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetBitPreservingMode
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    OFBool res = dvi->getTargetBitPreservingMode(tid);
    env->ReleaseStringUTFChars (targetID, tid);
    if (res == OFTrue) return JNI_TRUE; else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetUseTLS
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetUseTLS
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    OFBool res = dvi->getTargetUseTLS(tid);
    env->ReleaseStringUTFChars (targetID, tid);
    if (res == OFTrue) return JNI_TRUE; else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetNumberOfCipherSuites
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetNumberOfCipherSuites
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    jint res = (jint) dvi->getTargetNumberOfCipherSuites(tid);
    env->ReleaseStringUTFChars (targetID, tid);
    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetCipherSuite
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetCipherSuite
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);
    OFString param;
    const char* res = dvi->getTargetCipherSuite (tid, idx, param);
    env->ReleaseStringUTFChars (targetID, tid);
    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPeerAuthentication
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPeerAuthentication
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    DVPSCertificateVerificationType res = dvi->getTargetPeerAuthentication(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupportsPresentationLUT
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupportsPresentationLUT
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupportsPresentationLUT (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupports12BitTransmission
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupports12BitTransmission
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupports12BitTransmission (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupportsRequestedImageSize
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupportsRequestedImageSize
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupportsRequestedImageSize (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupportsDecimateCrop
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupportsDecimateCrop
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupportsDecimateCrop (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupportsTrim
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupportsTrim
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupportsTrim (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfBorderDensities
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfBorderDensities
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfBorderDensities (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterBorderDensity
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterBorderDensity
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterBorderDensity (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfMaxDensities
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfMaxDensities
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    Uint32 res = dvi->getTargetPrinterNumberOfMaxDensities(tid);
    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterMaxDensity
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterMaxDensity
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);
    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString param;

    const char* res = dvi->getTargetPrinterMaxDensity (tid, idx, param);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfMinDensities
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfMinDensities
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    Uint32 res = dvi->getTargetPrinterNumberOfMinDensities(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterMinDensity
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterMinDensity
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString param;
    const char* res = dvi->getTargetPrinterMinDensity (tid, idx, param);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfPortraitDisplayFormats
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfPortraitDisplayFormats
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    Uint32 res = dvi->getTargetPrinterNumberOfPortraitDisplayFormats(tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterPortraitDisplayFormatRows
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterPortraitDisplayFormatRows
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    Uint32 res = dvi->getTargetPrinterPortraitDisplayFormatRows(tid, idx);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterPortraitDisplayFormatColumns
 * Signature: (Ljava/lang/String;I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterPortraitDisplayFormatColumns
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    Uint32 res = dvi->getTargetPrinterPortraitDisplayFormatColumns(tid, idx);

    env->ReleaseStringUTFChars (targetID, tid);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSupportsAnnotation
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSupportsAnnotation
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFBool res = dvi->getTargetPrinterSupportsAnnotation (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfFilmSizeIDs
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfFilmSizeIDs
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfFilmSizeIDs (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterFilmSizeID
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterFilmSizeID
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{

    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterFilmSizeID (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfMediumTypes
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfMediumTypes
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfMediumTypes (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterMediumType
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterMediumType
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterMediumType (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfPrinterResolutionIDs
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfPrinterResolutionIDs
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfPrinterResolutionIDs (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterResolutionID
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterResolutionID
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterResolutionID (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfMagnificationTypes
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfMagnificationTypes
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfMagnificationTypes (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterMagnificationType
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterMagnificationType
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterMagnificationType (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfSmoothingTypes
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfSmoothingTypes
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfSmoothingTypes (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterSmoothingType
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterSmoothingType
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterSmoothingType (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfConfigurationSettings
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfConfigurationSettings
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfConfigurationSettings (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterConfigurationSetting
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterConfigurationSetting
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    const char *res = dvi->getTargetPrinterConfigurationSetting (tid, idx);

    env->ReleaseStringUTFChars (targetID, tid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterNumberOfEmptyImageDensities
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getTargetPrinterNumberOfEmptyImageDensities
  (JNIEnv *env, jobject obj, jstring targetID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    int res = dvi->getTargetPrinterNumberOfEmptyImageDensities (tid);

    env->ReleaseStringUTFChars (targetID, tid);

    return res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getTargetPrinterEmptyImageDensity
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getTargetPrinterEmptyImageDensity
  (JNIEnv *env, jobject obj, jstring targetID, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *tid = (char*) env->GetStringUTFChars (targetID, 0);

    OFString st;
    dvi->getTargetPrinterEmptyImageDensity (tid, idx, st);

    env->ReleaseStringUTFChars (targetID, tid);

    return env->NewStringUTF (st.c_str());
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getMessagePort
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getMessagePort
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getMessagePort();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNetworkAETitle
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getNetworkAETitle
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getNetworkAETitle();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}



/*
 * Class:     J2Ci_jDVInterface
 * Method:    getDatabaseFolder
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getDatabaseFolder
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getDatabaseFolder();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getMonitorCharacteristicsFile
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getMonitorCharacteristicsFile
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getMonitorCharacteristicsFile();

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getMonitorPixelWidth
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVInterface_getMonitorPixelWidth
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jdouble) dvi->getMonitorPixelWidth();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getMonitorPixelHeight
 * Signature: ()D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVInterface_getMonitorPixelHeight
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jdouble) dvi->getMonitorPixelHeight();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getGUIConfigEntry
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getGUIConfigEntry
  (JNIEnv *env, jobject obj, jstring key)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *ckey = (char*) env->GetStringUTFChars (key, 0);

    const char* res = dvi->getGUIConfigEntry(ckey);

    env->ReleaseStringUTFChars (key, ckey);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getGUIConfigEntryBool
 * Signature: (Ljava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDVInterface_getGUIConfigEntryBool
  (JNIEnv *env, jobject obj, jstring key, jboolean dfl)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *ckey = (char*) env->GetStringUTFChars (key, 0);
    OFBool cdfl = dfl == JNI_TRUE ? OFTrue : OFFalse;

    OFBool res = dvi->getGUIConfigEntryBool(ckey, cdfl);

    env->ReleaseStringUTFChars (key, ckey);

    if (res == OFTrue) return JNI_TRUE;
    else return JNI_FALSE;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfLUTs
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfLUTs
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfLUTs();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getLUTID
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getLUTID
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getLUTID(idx);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getLUTDescription
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getLUTDescription
  (JNIEnv *env, jobject obj, jstring lutID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *lid = (char*) env->GetStringUTFChars (lutID, 0);

    const char* res = dvi->getLUTDescription(lid);

    env->ReleaseStringUTFChars (lutID, lid);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfReports
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfReports
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfReports();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getReportID
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getReportID
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getReportID(idx);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getReportDescription
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getReportDescription
  (JNIEnv *env, jobject obj, jstring reportID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (reportID, 0);

    const char* res = dvi->getReportDescription(id);

    env->ReleaseStringUTFChars (reportID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfVOIPresets
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfVOIPresets
  (JNIEnv *env, jobject obj, jstring modality)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *mod = (char*) env->GetStringUTFChars (modality, 0);

    Uint32 res = dvi->getNumberOfVOIPresets(mod);

    env->ReleaseStringUTFChars (modality, mod);

    return (jint) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getVOIPresetDescription
 * Signature: (Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getVOIPresetDescription
  (JNIEnv *env, jobject obj, jstring modality, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *mod = (char*) env->GetStringUTFChars (modality, 0);

    const char* res = dvi->getVOIPresetDescription(mod, idx);

    env->ReleaseStringUTFChars (modality, mod);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getVOIPresetWindowCenter
 * Signature: (Ljava/lang/String;I)D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVInterface_getVOIPresetWindowCenter
  (JNIEnv *env, jobject obj, jstring modality, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *mod = (char*) env->GetStringUTFChars (modality, 0);

    double res = dvi->getVOIPresetWindowCenter(mod, idx);

    env->ReleaseStringUTFChars (modality, mod);

    return (jdouble) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getVOIPresetWindowWidth
 * Signature: (Ljava/lang/String;I)D
 */
JNIEXPORT jdouble JNICALL Java_J2Ci_jDVInterface_getVOIPresetWindowWidth
  (JNIEnv *env, jobject obj, jstring modality, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *mod = (char*) env->GetStringUTFChars (modality, 0);

    double res = dvi->getVOIPresetWindowWidth(mod, idx);

    env->ReleaseStringUTFChars (modality, mod);

    return (jdouble) res;
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getNumberOfUsers
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDVInterface_getNumberOfUsers
  (JNIEnv *env, jobject obj)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    return (jint) dvi->getNumberOfUsers();
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserID
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserID
  (JNIEnv *env, jobject obj, jint idx)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    const char* res = dvi->getUserID(idx);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserLogin
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserLogin
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    const char* res = dvi->getUserLogin(id);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserName
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserName
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    const char* res = dvi->getUserName(id);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserDICOMName
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserDICOMName
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    const char* res = dvi->getUserDICOMName(id);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserOrganization
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserOrganization
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    const char* res = dvi->getUserOrganization(id);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserCodingSchemeDesignator
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserCodingSchemeDesignator
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    OFString string;
    const char* res = dvi->getUserCodingSchemeDesignator(id, string);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserCodingSchemeVersion
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserCodingSchemeVersion
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    OFString string;
    const char* res = dvi->getUserCodingSchemeVersion(id, string);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserCodeValue
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserCodeValue
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    OFString string;
    const char* res = dvi->getUserCodeValue(id, string);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 * Class:     J2Ci_jDVInterface
 * Method:    getUserCodeMeaning
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDVInterface_getUserCodeMeaning
  (JNIEnv *env, jobject obj, jstring userID)
{
    DVInterface *dvi = getAddressOfDVInterface (env, obj);

    char *id = (char*) env->GetStringUTFChars (userID, 0);

    OFString string;
    const char* res = dvi->getUserCodeMeaning(id, string);

    env->ReleaseStringUTFChars (userID, id);

    if (res == NULL) return NULL;
    return env->NewStringUTF (res);
}


/*
 *  CVS Log
 *  $Log: DVInterface.cpp,v $
 *  Revision 1.2  1999/05/14 10:02:31  Schroeter
 *  Log-Tags for CVS corrected
 *
 *  Revision 1.1.1.1  1999/05/14 09:33:11  Schroeter
 *  initial commit of release 1.0
 *
*/
