/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRTCoordValue.h"
#include "jInterface.h"


static inline DSRTemporalCoordinatesValue *getAddressOfDSRTemporalCoordinatesValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRTemporalCoordinatesValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRTemporalCoordinatesValue (JNIEnv *env, jobject obj, DSRTemporalCoordinatesValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRTCoordValue
 * Method:    getTemporalRangeType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRTCoordValue_getTemporalRangeType
  (JNIEnv *env, jobject obj)
{
    DSRTemporalCoordinatesValue *tco = getAddressOfDSRTemporalCoordinatesValue (env, obj);

    return (jint) tco->getTemporalRangeType();
}


/*
 * Class:     J2Ci_jDSRTCoordValue
 * Method:    setTemporalRangeType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRTCoordValue_setTemporalRangeType
  (JNIEnv *env, jobject obj, jint temporalRangeType)
{
    DSRTemporalCoordinatesValue *tco = getAddressOfDSRTemporalCoordinatesValue (env, obj);

    return (jint) tco->setTemporalRangeType((DSRTypes::E_TemporalRangeType)temporalRangeType).status();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/
