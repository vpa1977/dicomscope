/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


#include "J2Ci_jDSRSCoordValue.h"
#include "jInterface.h"


static inline DSRSpatialCoordinatesValue *getAddressOfDSRSpatialCoordinatesValue (JNIEnv *env, jobject obj)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    return (DSRSpatialCoordinatesValue *) env->GetLongField (obj, fid);
}


static inline void setAddressOfDSRSpatialCoordinatesValue (JNIEnv *env, jobject obj, DSRSpatialCoordinatesValue *ptr)
{
    jclass cls = env->GetObjectClass (obj);
    jfieldID fid = env->GetFieldID (cls, "cppClassAddress", "J");

    if (fid == 0) exit (-1);

    env->SetLongField (obj, fid, (jlong) ptr);
}


// --------------------------- native methods ------------------------------


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    getGraphicType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRSCoordValue_getGraphicType
  (JNIEnv *env, jobject obj)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    return (jint) sco->getGraphicType();
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    setGraphicType
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRSCoordValue_setGraphicType
  (JNIEnv *env, jobject obj, jint graphicType)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    return (jint) sco->setGraphicType((DSRTypes::E_GraphicType)graphicType).status();
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    clearGraphicData
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRSCoordValue_clearGraphicData
  (JNIEnv *env, jobject obj)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    sco->getGraphicDataList().clear();
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    getNumberOfPixels
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRSCoordValue_getNumberOfPixels
  (JNIEnv *env, jobject obj)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    return (jint) sco->getGraphicDataList().getNumberOfItems();
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    getPixel
 * Signature: (ILJ2Ci/jFloatByRef;LJ2Ci/jFloatByRef;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRSCoordValue_getPixel
  (JNIEnv *env, jobject obj, jint idx, jobject column, jobject row)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    Float32 columnValue, rowValue;

    OFCondition res = sco->getGraphicDataList().getItem((size_t)idx, columnValue, rowValue);

    env->SetFloatField (column, env->GetFieldID (env->GetObjectClass (column), "value", "F"), columnValue);
    env->SetFloatField (row, env->GetFieldID (env->GetObjectClass (row), "value", "F"), rowValue);

    return (jint) res.status();
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    addPixel
 * Signature: (FF)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRSCoordValue_addPixel
  (JNIEnv *env, jobject obj, jfloat column, jfloat row)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    sco->getGraphicDataList().addItem((Float32)column, (Float32)row);
}


/*
 * Class:     J2Ci_jDSRSCoordValue
 * Method:    removePixel
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRSCoordValue_removePixel
  (JNIEnv *env, jobject obj, jint idx)
{
    DSRSpatialCoordinatesValue *sco = getAddressOfDSRSpatialCoordinatesValue (env, obj);

    return (jint) sco->getGraphicDataList().removeItem((size_t)idx).status();
}


/*
 *  CVS Log
 *  $Log: $
 *
*/
