/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class J2Ci_jDSRCompositeValue */

#ifndef _Included_J2Ci_jDSRCompositeValue
#define _Included_J2Ci_jDSRCompositeValue
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPClassUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPClassUID
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPClassName
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPClassName
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    getSOPInstanceUID
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRCompositeValue_getSOPInstanceUID
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRCompositeValue
 * Method:    setReference
 * Signature: (Ljava/lang/String;Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRCompositeValue_setReference
  (JNIEnv *, jobject, jstring, jstring);

#ifdef __cplusplus
}
#endif
#endif
