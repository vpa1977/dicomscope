/*
 *
 *  Copyright (C) 1999..2000, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: $
 *  Last update : $Date: $
 *  Revision :    $Revision: $
 *  State:        $State: $
*/


// Common Header-File

#include "errno.h"
#include "osconfig.h"
#include "dviface.h"	// DVInterface, DVPresentationState
#include "dcmimage.h"	// DicomImage
#include "dvpstx.h"		// DVPSTextObject
#include "dvpsgr.h"		// DVPSGraphicObject
#include "dvpscu.h"		// DVPSCurve
#include "dvpssp.h"		// DVPSStoredPrint
#include "dvpstyp.h"	// DVPSStoredPrint
#include "dsrdoc.h"     // DSRDocument
#include <stdio.h>


// Global Definitions

#define JAVA_ENCODING_STRING "ISO8859_1"    // Character set used for strings passed to C++ classes
