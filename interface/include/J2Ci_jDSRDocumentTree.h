/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class J2Ci_jDSRDocumentTree */

#ifndef _Included_J2Ci_jDSRDocumentTree
#define _Included_J2Ci_jDSRDocumentTree
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    canAddContentItem
 * Signature: (III)Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_canAddContentItem
  (JNIEnv *, jobject, jint, jint, jint);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    addContentItem
 * Signature: (III)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_addContentItem
  (JNIEnv *, jobject, jint, jint, jint);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    removeCurrentContentItem
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_removeCurrentContentItem
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    removeSignatures
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_removeSignatures
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    unmarkAllContentItems
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_unmarkAllContentItems
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoRootNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoRootNode
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoPreviousNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoPreviousNode
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoNextNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoNextNode
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoParentNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoParentNode
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoFirstChildNode
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoFirstChildNode
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    iterateNodes
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_iterateNodes
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    gotoNode
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_gotoNode
  (JNIEnv *, jobject, jint);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    isCurrentItemValid
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_isCurrentItemValid
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    isCurrentItemMarked
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_J2Ci_jDSRDocumentTree_isCurrentItemMarked
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentItemMark
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentItemMark
  (JNIEnv *, jobject, jboolean);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentValueType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentValueType
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentRelationshipType
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentRelationshipType
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentNodeID
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentNodeID
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentReferencedNodeID
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentReferencedNodeID
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentLevel
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentLevel
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentStringValue
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentStringValue
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentStringValue
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentStringValue
  (JNIEnv *, jobject, jstring);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentCodeValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentCodeValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentNumValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentNumValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentSCoordValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentSCoordValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentTCoordValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentTCoordValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentCompositeValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentCompositeValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentImageValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentImageValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentWaveformValueN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentWaveformValueN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentContinuityOfContentFlag
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentContinuityOfContentFlag
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentContinuityOfContentFlag
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentContinuityOfContentFlag
  (JNIEnv *, jobject, jint);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentConceptNameN
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentConceptNameN
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    getCurrentObservationDateTime
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_J2Ci_jDSRDocumentTree_getCurrentObservationDateTime
  (JNIEnv *, jobject);

/*
 * Class:     J2Ci_jDSRDocumentTree
 * Method:    setCurrentObservationDateTime
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_J2Ci_jDSRDocumentTree_setCurrentObservationDateTime
  (JNIEnv *, jobject, jstring);

#ifdef __cplusplus
}
#endif
#endif
