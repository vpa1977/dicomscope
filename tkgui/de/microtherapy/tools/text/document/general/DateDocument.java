/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.general;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
/**
* @author Klaus Kleber
*/
public class DateDocument extends PlainDocument
{
    
    
    /**
    * Contstructor.
    */
    public DateDocument()
    {
        super();
    }
    
    /**
    * Insert a String and checks if the String is a valid input.
    */
    public void insertString(   int offset, 
                                String s, 
                                AttributeSet attributeSet) throws BadLocationException
    {
        if (s != null)
        {
            s = s.replace(' ', '0');
            if (s.length()==10)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (i != 4 || i!=7) insertString(offset+i, s.substring(i,i+1), attributeSet);
                     
                }
                return;
            }
            try
            {
                Integer.parseInt(s);
                
            }
            catch(Exception e)
            {
                Toolkit.getDefaultToolkit().beep();
                
                return;
            }
        }
        super.insertString(offset, s, attributeSet);
        
    }
    
}
/*
 *  CVS Log
 *  $Log: DateDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
