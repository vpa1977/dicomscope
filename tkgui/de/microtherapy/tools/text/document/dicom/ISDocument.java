/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
/**
* Contains a document for DICOM Integer String (IS) values.
* <p>
* A string of characters representing an Integer
* in base-10 (decimal), shall contain only the
* characters 0 - 9, with an optional leading "+" or
* "-". It may be padded with leading and/or
* trailing spaces. Embedded spaces are not allowed.
* <p>
* The integer, n, represented shall be in the range:
* -2^31 <= n <= (2^31 - 1). �0�-�9�, �+�, �-" of
* Default Character Repertoire can be used
* <b>
* 12 bytes maximum
*/
public class ISDocument extends PlainDocument
{
    /**
    * Max Size
    */
    private int maxSize = 12;
    
    /**
    * Constructor.
    */
    public ISDocument( )
    {
        super();
    }
    /**
    * Inserts some content into the document.
    * <p>
    * The content will be checked. All uppercase characters, �0�-�9�, the SPACE
    * character, and underscore �_�, of the Default Character Repertoire will be accepted
    * lowercase characters will be converted into uppercase
    * <p>
    * Inserting content causes a write lock to be held while the actual changes are taking place, 
    * followed by notification to the observers on the thread that grabbed the write lock. 
    * <p>
    * This method is thread safe, although most Swing methods are not.
    * Please see Threads and Swing for more information.
    * @param offs The starting offset >= 0 
    * @param str The string to insert; does nothing with null/empty strings
    * @param a The attributes for the inserted content 
    * @exception BadLocationException The given insert position is not a valid position within the document
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        
        int size = s.length();
        
        int pos = offset; 
         
        for (int j = 0; j < size; j++)
        {
            if (pos >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            char c = s.charAt(j);
            if(pos==0)
            {
                if (c== ' ') continue;
                else if (c != '+' && c!= '-' && !DICOMDocumentUtils.isDigit(c))
                {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
            }
            if (!DICOMDocumentUtils.isDigit(c))
            {
                
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            super.insertString(pos, new Character(c).toString(), attributeSet);
            pos++;
        }
        
    }
    
    
    
}
/*
 *  CVS Log
 *  $Log: ISDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
