/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
/**
* Contains a document for DICOM Unique Identifies (UI) values.
* A UI contains  all digits and the character '.' and has
* a limited size of 64.
* @author Klaus Kleber
*/
public class UIDocument extends PlainDocument
{
    private int maxSize = 64;
    /**
    * Contstructor.
    */
    public UIDocument()
    {
        super();
    }
    /**
    * Inserts some content into the document.
    * <p>
    * The content will be checked. Only digits and 
    * '.' will be accepted with a linited maximum size of 64.
    * <p>
    * Inserting content causes a write lock to be held while the actual changes are taking place, 
    * followed by notification to the observers on the thread that grabbed the write lock. 
    * <p>
    * This method is thread safe, although most Swing methods are not.
    * Please see Threads and Swing for more information.
    * @param offs The starting offset >= 0 
    * @param str The string to insert; does nothing with null/empty strings
    * @param a The attributes for the inserted content 
    * @exception BadLocationException The given insert position is not a valid position within the document
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        char c;
        if (s == null) return;
        
        int size = s.length();
        
        for (int j = 0; j < size; j++)
        {
            
            //Check limited Size
            if (offset+j >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            try   
            {
                c = s.charAt(j);
                if (c!= '.'&&!Character.isDigit(c))
                {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                }
                
            }
            catch(Exception e)
            {
                
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            super.insertString(offset+j, new Character(c).toString(), attributeSet);
        }
        
    }
    
    
}
/*
 *  CVS Log
 *  $Log: UIDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
