/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;

/**
* This class contains some utils the DICOM documents 
* @author Klaus Kleber
* @since 01.01.2001
*/
public  class DICOMDocumentUtils
{
    
    
    /**
    * Checks if the specified char is a uppercase 
    * character of the default DICOM character repertoire
    * @param ch Specifies the char to be checked
    * @return True if the specified char is a uppercase character 
    * of the default DICOM character repertoire
    */
    public static final boolean isUpperCase(char ch)
    {
       if  (ch >= 'A' && ch <= 'Z') return true;
       else return false;
    }
    /**
    * Checks if the specified char is a lowercase 
    * character of the default DICOM character repertoire
    * @param ch Specifies the char to be checked
    * @return True if the specified char is a lowercase character 
    * of the default DICOM character repertoire
    */
    public static final boolean isLowerCase(char ch)
    {
       if  (ch >= 'a' && ch <= 'z') return true;
       else return false;
    }
    /**
    * Checks if the specified char is a letter 
    * character of the default DICOM character repertoire
    * @param ch Specifies the char to be checked
    * @return True if the specified char is a letter character 
    * of the default DICOM character repertoire
    */
    public static final boolean isDigit(char ch)
    {
       if  (ch >= '0' && ch <= '9') return true;
       else return false;
    }
    /**
    * Checks if the specified char is a letter 
    * character of the default DICOM character repertoire
    * @param ch Specifies the char to be checked
    * @return True if the specified char is a letter character 
    * of the default DICOM character repertoire
    */
    public static final boolean isLetter(char ch)
    {
       if  (isLowerCase(ch )||isUpperCase(ch)) return true;
       else return false;
    }
    /**
    * Checks if the specified char is a letter or a digit
    * character of the default DICOM character repertoire
    * @param ch Specifies the char to be checked
    * @return True if the specified char is a letter or digt character 
    * of the default DICOM character repertoire
    */
    public static final boolean isLetterOrDigit(char ch)
    {
       if  (isLetter(ch )||isDigit(ch)) return true;
       else return false;
    }
    
    /**
    * Checks if the specified char is a Line Feed
    * in the DICOM default character repertoire
    * @param ch The char to be checked
    * @return True if the checked char is the 
    * Line Feed in the DICOM default character repertoire
    */
    public static final boolean isLineFeed(char ch)
    {
        if (ch == '\n') return true;
        else return false;
    }
    /**
    * Checks if the specified char is a Form Feed
    * in the DICOM default character repertoire
    * @param ch The char to be checked
    * @return True if the checked char is the 
    * Form Feed in the DICOM default character repertoire
    */
    public static final boolean isFormFeed(char ch)
    {
        if (ch == '\f') return true;
        else return false;
    }
    /**
    * Checks if the specified char is a Carriage Return
    * in the DICOM default character repertoire
    * @param ch The char to be checked
    * @return True if the checked char is the 
    * Carriage Return in the DICOM default character repertoire
    */
    public static final boolean isCarriageReturn(char ch)
    {
        if (ch == '\r') return true;
        else return false;
    }
    /**
    * Checks if the specified char is a Escape
    * in the DICOM default character repertoire
    * @param ch The char to be checked
    * @return True if the checked char is the 
    * Escape in the DICOM default character repertoire
    */
    public static final boolean isEscape(char ch)
    {
        if (ch == '\u001B') return true;
        else return false;
    }
    /**
    * Checks if the specified char is a Backslash
    * in the DICOM default character repertoire
    * @param ch The char to be checked
    * @return True if the checked char is the 
    * Backslash in the DICOM default character repertoire
    */
    public static final boolean isBackslash(char ch)
    {
        if (ch == '\\') return true;
        else return false;
        
    }
    /**
    * Checks if the specified char is a DICOM Control Character
    * The DICOM Control Characters are
    * <ul>
    * <li> Line Feed </li>
    * <li> Form Feed </li>
    * <li> Carriage Return</li>
    * <li> Escape </li>
    * </ul>
    * @param ch The char th be checked
    * @return True if the checked char is a DICOM Control Character
    */
    public static final boolean isDICOMControlCharacter(char ch)
    {
        if (    isLineFeed(ch) ||
                isFormFeed(ch) ||
                isCarriageReturn(ch)||
                isEscape(ch)) return true;
        else return false;
    }
    /**
    * Checks if the specified char is a DICOM Default Character
    * The default repertoire for character strings in DICOM is the Basic G0 Set of the International Reference
    * Version of ISO 646:1990 (ISO IR-6). In addition, the four Control Characters LF, FF, CR, and ESC are
    * supported. These control characters are a subset of the C0 set defined in ISO 646:1990 and ISO
    * 6429:1990.
    * <p>
    * The chars are in the range '\u0020' (space) to  '\u007D' (~). 
    * @param ch The char th be checked
    * @return True if the checked char is a DICOM Control Character
    */
    public static final boolean isDefaultCharacter(char ch)
    {
        if (ch >= '\u0020'  && ch <= '\u007D' ) return true;
        else return false;
    }
    
    /**
    * Checks if the specified char is in the DICOM Default Character Repertoire.
    * This is the case if the char is a valid DICOM Control Character {@link isDICOMControlCharacter}
    * or if the char is a valid DICOM Default Character {@link isDefaultCharacter}
    * <p>
    * The DICOM Default Character are in the range '\u0020' (space) to  '\u007D' (~). 
    * <br> 
    * The DICOM Control Characters are
    * <ul>
    * <li> Line Feed </li>
    * <li> Form Feed </li>
    * <li> Carriage Return</li>
    * <li> Escape </li>
    * </ul>
    * @param ch The char th be checked
    * @return True if the checked char is in the DICOM Default Character Repertoire.
    */
    public static final boolean isInDICOMDefaultCharacterRepertoire(char ch)
    {
        if (isDICOMControlCharacter(ch) ||isDefaultCharacter(ch)) return true;
        else return false;
    }
}
/*
 *  CVS Log
 *  $Log: DICOMDocumentUtils.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
