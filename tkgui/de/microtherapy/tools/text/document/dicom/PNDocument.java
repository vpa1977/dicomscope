/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package de.microtherapy.tools.text.document.dicom;
import javax.swing.text.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
/**
* Contains a document for DICOM Person Name (PN) values.
* @author Klaus Kleber
*/
public class PNDocument extends PlainDocument
{
    private int maxSize = 64;
    private int maxNumberOfSeps = 4;
    private int numSep = 0;
    private String sep= "^";
    /**
    * Contstructor.
    */
    public PNDocument()
    {
        super();
    }
    /**
    * Insert a String and checks if the String is a valid input.
    */
    public void insertString(int offset, String s, AttributeSet attributeSet) throws BadLocationException
    {
        char c;
        if (s == null) return;
        
        int size = s.length();
        
        for (int j = 0; j < size; j++)
        {
            if (offset+j >= maxSize) 
            {
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            try   
            {
                
                c = s.charAt(j);
                
                
                if ((c== '\n')||
                    (c=='\r')||
                     (c=='\f')||
                     (c=='\\')) 
                {
                    Toolkit.getDefaultToolkit().beep();
                            return;
                }
                if (s.equals(sep)) 
                {
                    if (numSep>=maxNumberOfSeps) 
                    {
                        Toolkit.getDefaultToolkit().beep();
                            return;
                    }
                    else numSep ++;
                }
                
            }
            catch(Exception e)
            {
                
                    Toolkit.getDefaultToolkit().beep();
                    return;
            }
            
            super.insertString(offset+j, new Character(c).toString(), attributeSet);
        }
        
    }
    
    public void remove(int offs,
                   int len) throws BadLocationException
    {
        String s = getText(offs, len);
        int num = s.indexOf(sep);
        while (num >= 0) 
        {
            numSep--;
            num = s.indexOf(sep,num+1);
        }
        super.remove(offs, len);
        
    }
    
}
/*
 *  CVS Log
 *  $Log: PNDocument.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
