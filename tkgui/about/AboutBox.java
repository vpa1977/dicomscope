/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 08:59:30 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
*/

package about;
 
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import java.net.*;


import jToolkit.gui.*;
import java.util.*;

/**
 * Displays an about box in a panel. The content will be get from a html-page
 * in the Aboutbox-folder.  Hyperlinks in the content will open
 * the default web browser with the relating site.
 * 
 * @author Andreas Schroeter
 * @since 30.04.1999
 */
public class AboutBox extends JPanel implements HyperlinkListener
{
    
    
    
    private JEditorPane html;
    private boolean iconsOnTop = true; // false = right side
    
    /**
     * Constructor. Sets up the panel.
     * 
     * @param config Configuration for the AboutBox
     * @since 30.04.1999
     */
    public AboutBox(Hashtable config)
    {
        super();
        
        setLayout(new BorderLayout(10,10));
	setBackground (Color.white);
	    
	html = new JEditorPane ();
	html.setBackground (Color.white);
        html.setContentType ("text/html");
        html.setEditable (false);
        html.addHyperlinkListener(this);
        try {		    
	
	        Class c = getClass();            
                ClassLoader cl = c.getClassLoader();
                URL url = cl.getResource ("icons/about.html");
            
                html.setPage (url);                                   
            
                if ( html.getScrollableTracksViewportWidth() )
                {
                    this.setMinimumSize( html.getPreferredScrollableViewportSize() );
                }
            
                add ("Center", html);
                        
        }
        catch (Exception e)
        {
            add ("Center", new JLabel ("Error in Aboutbox!"));
            e.printStackTrace();
        }
    }
    
    
    
   
    /**
     * Handler for hyperlink clicks
     * 
     * @param e Hyperlink to the WWW
     * @since 30.04.1999
     */
    public void hyperlinkUpdate (HyperlinkEvent e)
    {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
        {         
            BrowserControl.displayURL (e.getURL().toExternalForm());
        }
    }
}
/*
 *  CVS Log
 *  $Log: AboutBox.java,v $
 *  Revision 1.2  2003/09/08 08:59:30  kleber
 *  move DICOMscope to folder dicomscope.
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
