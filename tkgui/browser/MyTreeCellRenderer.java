/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package browser;

import java.lang.*;
import javax.swing.tree.*;
import javax.swing.*;
import java.awt.*;
import jToolkit.io.*;
import main.MainContext;
import J2Ci.*;

/**
 * Draws a tree element with the correct icon in the tree 
 * 
 * @author Andreas Schroeter
 * @since 30.04.1999
*/
public class MyTreeCellRenderer implements TreeCellRenderer
{
      // Icons
      
    /** 
     * Icon used to show nodes as Presentation State.
    */
    transient protected Icon pSIcon;
      
    /** 
     * Icon used to show nodes as Structure Report .
    */
    transient protected Icon srIcon;
    /** 
     * Icon used to show nodes as Study.
    */
    transient protected Icon studyIcon;
    
    /** 
     * Icon used to show nodes as Series.
    */
    transient protected Icon seriesIcon;
    
    /** 
     * Icon used to show nodes as Instance.
    */
    transient protected Icon instanceIcon;
    
    /** 
     * Icon used to show nodes as default.
    */
    transient protected Icon defaultIcon;
    
    /** 
     * Icon used to show nodes as new Entry.
    */
    transient protected Icon newIcon;
    
    /** 
     * Icon used to show nodes as new Entry.
    */
    transient protected Icon containsNewIcon;
    /** 
     * Icon used to show nodes as new Entry.
    */
    transient protected Icon hcIcon;
    /** 
     * Icon used to show nodes as new Entry.
    */
    transient protected Icon spIcon;
    // ----------------
      
    /** Icon used to show non-leaf nodes that aren't expanded. */
    transient protected Icon closedIcon;

    /** Icon used to show leaf nodes. */
    transient protected Icon leafIcon;

    /** Icon used to show non-leaf nodes that are expanded. */
    transient protected Icon openIcon;

     // Colors
    /** Color to use for the foreground for selected nodes. */
    protected Color textSelectionColor;

    /** Color to use for the foreground for non-selected nodes. */
    protected Color textNonSelectionColor;

    /** Color to use for the background when a node is selected. */
    protected Color backgroundSelectionColor;

    /** Color to use for the background when the node isn't selected. */
    protected Color backgroundNonSelectionColor;

    /** Color to use for the background when the node isn't selected. */
    protected Color borderSelectionColor;

    protected JLabel label=new JLabel();

    protected Font textSelectionFont;
    protected Font textNonSelectionFont;


    public MyTreeCellRenderer()
    {
        label.setOpaque(true);
        Font f=label.getFont();
	  IconRetriever ir = new IconRetriever();        


        if (MainContext.instance().highQualitySystem)
        {
            textSelectionFont=new Font(f.getName(), f.getStyle() | Font.ITALIC, f.getSize());
            textNonSelectionFont=new Font(f.getName(), f.getStyle(), f.getSize());
            
            pSIcon = ir.getIcon (MainContext.iconPath + "bigps.gif"); 
            srIcon = ir.getIcon (MainContext.iconPath + "sr32.gif"); 
            //new ImageIcon (MainContext.iconPath + "bigps.gif");

            instanceIcon = ir.getIcon (MainContext.iconPath + "biginstance.gif");
            seriesIcon = ir.getIcon (MainContext.iconPath + "bigseries.gif");
            studyIcon = ir.getIcon (MainContext.iconPath + "bigstudy.gif");        
            defaultIcon = ir.getIcon (MainContext.iconPath + "bigdefault.gif");
            newIcon = ir.getIcon (MainContext.iconPath + "bignew.gif");
            hcIcon = ir.getIcon (MainContext.iconPath + "hc32.gif");
            spIcon = ir.getIcon (MainContext.iconPath + "sp32.gif");
            containsNewIcon = ir.getIcon (MainContext.iconPath + "containsNew32.gif");
            /*
            InstanceIcon = new ImageIcon (MainContext.iconPath + "biginstance.gif");
            SeriesIcon = new ImageIcon (MainContext.iconPath + "bigseries.gif");
            StudyIcon = new ImageIcon (MainContext.iconPath + "bigstudy.gif");        
            defaultIcon = new ImageIcon (MainContext.iconPath + "bigdefault.gif");
            newIcon = new ImageIcon (MainContext.iconPath + "bignew.gif");
            hcIcon = new ImageIcon (MainContext.iconPath + "hc32.gif");
            spIcon = new ImageIcon (MainContext.iconPath + "sp32.gif");
*/
        }
        else
        {
            textSelectionFont=new Font(f.getName(),f.getStyle()|Font.ITALIC,f.getSize());
            textNonSelectionFont=f;

            pSIcon = ir.getIcon (MainContext.iconPath + "ps.gif");
            instanceIcon = ir.getIcon(MainContext.iconPath + "instance.gif");
            seriesIcon = ir.getIcon (MainContext.iconPath + "series.gif");
            studyIcon = ir.getIcon (MainContext.iconPath + "study.gif");        
            defaultIcon = ir.getIcon(MainContext.iconPath + "default.gif");
            newIcon = ir.getIcon (MainContext.iconPath + "new.gif");
            containsNewIcon = ir.getIcon (MainContext.iconPath + "containsNew.gif");
            hcIcon = ir.getIcon (MainContext.iconPath + "hc.gif");
            spIcon = ir.getIcon (MainContext.iconPath + "sp.gif");
            srIcon = ir.getIcon (MainContext.iconPath + "sr.gif");

/*
		PSIcon = new ImageIcon (MainContext.iconPath + "ps.gif");
            InstanceIcon = new ImageIcon (MainContext.iconPath + "instance.gif");
            SeriesIcon = new ImageIcon (MainContext.iconPath + "series.gif");
            StudyIcon = new ImageIcon (MainContext.iconPath + "study.gif");        
            defaultIcon = new ImageIcon (MainContext.iconPath + "default.gif");
            newIcon = new ImageIcon (MainContext.iconPath + "new.gif");
            hcIcon = new ImageIcon (MainContext.iconPath + "hc.gif");
            spIcon = new ImageIcon (MainContext.iconPath + "sp.gif");
*/
        }
        setTextSelectionColor(UIManager.getColor("Tree.textSelectionColor"));
        setTextNonSelectionColor(UIManager.getColor("Tree.textNonSelectionColor"));
        setBackgroundSelectionColor(UIManager.getColor("Tree.selectionBackground"));
        setBackgroundNonSelectionColor(UIManager.getColor("Tree.backgroundNonSelectionColor"));
        setBorderSelectionColor(UIManager.getColor("Tree.borderSelectionColor"));
    }

    /**
     * Returns the default icon used to represent non-leaf nodes that are expanded.
    */
    public Icon getDefaultOpenIcon() 
    { 
        return openIcon;
    }

    /**
     * Returns the default icon used to represent non-leaf nodes that are not
     * expanded.
    */
    public Icon getDefaultClosedIcon() 
    {
        return closedIcon;
    }

    /**
     * Returns the default icon used to represent leaf nodes.
    */
    public Icon getDefaultLeafIcon() 
    {
        return leafIcon;
    }

    /**
     * Sets the icon used to represent non-leaf nodes that are expanded.
    */
    public void setOpenIcon(Icon newIcon) 
    {
        openIcon = newIcon;
    }

    /**
     * Returns the icon used to represent non-leaf nodes that are expanded.
    
    public Icon getOpenIcon() 
    {
        return openIcon;
    }
*/
    /**
     * Sets the icon used to represent non-leaf nodes that are not expanded.
    */
    public void setClosedIcon(Icon newIcon) 
    {
        closedIcon = newIcon;
    }

    /**
     * Returns the icon used to represent non-leaf nodes that are not
     * expanded.
    */
    public Icon getClosedIcon() 
    {
        return closedIcon;
    }

    /**
     * Sets the icon used to represent leaf nodes.
    */
    public void setLeafIcon(Icon newIcon) 
    {
        leafIcon = newIcon;
    }

    /**
     * Returns the icon used to represent leaf nodes.
    */
    public Icon getLeafIcon() 
    {
        return leafIcon;
    }
    
    /**
     * Sets the color the text is drawn with when the node is selected.
    */
    public void setTextSelectionColor(Color newColor) 
    {
        textSelectionColor = newColor;
    }

    /**
     * Returns the color the text is drawn with when the node is selected.
    */
    public Color getTextSelectionColor() 
    {
        return textSelectionColor;
    }

    /**
     * Sets the color the text is drawn with when the node isn't selected.
    */
    public void setTextNonSelectionColor(Color newColor) 
    {
        textNonSelectionColor = newColor;
    }

    /**
     * Returns the color the text is drawn with when the node isn't selected.
    */
    public Color getTextNonSelectionColor() 
    {
        return textNonSelectionColor;
    }

    /**
     * Sets the color to use for the background if node is selected.
    */
    public void setBackgroundSelectionColor(Color newColor) 
    {
        backgroundSelectionColor = newColor;
    }


    /**
     * Returns the color to use for the background if node is selected.
    */
    public Color getBackgroundSelectionColor() 
    {
        return backgroundSelectionColor;
    }

    /**
     * Sets the background color to be used for non selected nodes.
    */
    public void setBackgroundNonSelectionColor(Color newColor) 
    {
        backgroundNonSelectionColor = newColor;
    }

    /**
     * Returns the background color to be used for non selected nodes.
    */
    public Color getBackgroundNonSelectionColor() 
    {
        return backgroundNonSelectionColor;
    }

    /**
     * Sets the color to use for the border.
    */
    public void setBorderSelectionColor(Color newColor) 
    {
        borderSelectionColor = newColor;
    }

    /**
      * Returns the color the border is drawn.
      */
    public Color getBorderSelectionColor() 
    {
        return borderSelectionColor;
    }

    /**
     * Renders the Component.
    */
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, 
                                                  boolean expanded, boolean leaf, int row, 
                                                  boolean hasFocus)
    {
        DefaultMutableTreeNode node=(DefaultMutableTreeNode)value;
        Object userObject=node.getUserObject();   
        TreeElement te = (TreeElement) userObject;
        
        
        
        if(selected)
        {
            label.setForeground(getTextSelectionColor());
            label.setBackground(getBackgroundSelectionColor());
            label.setFont(textSelectionFont);
        }
        else
        {
            label.setForeground(getTextNonSelectionColor());
            label.setBackground(getBackgroundNonSelectionColor());
            label.setFont(textNonSelectionFont);
        }
        if (te == null) 
        {
        label.setText("no Value");
        label.setToolTipText(" no tip");
            label.setIcon (defaultIcon);
            return label;
        }
        label.setText(te.toString());
        label.setToolTipText(te.toString());
        
        

        if (TreeElement.showNewWithIcon && (te.getStatus()>jDVIFhierarchyStatus.DVIF_objectIsNotNew) && !te.isRoot()) 
        {
            if (te.getStatus() ==jDVIFhierarchyStatus.DVIF_objectIsNew)label.setIcon (newIcon);
            else label.setIcon(containsNewIcon);
        }
        else if (te.isPS()) label.setIcon (pSIcon);
	else if (te.isHC()&&te.isInstance()) label.setIcon(hcIcon);
	else if (te.isST()&&te.isInstance()) label.setIcon(spIcon);
	else if (te.isSR()&&te.isInstance()) label.setIcon(srIcon);
	    
	else if (te.isInstance()) label.setIcon (instanceIcon);
	else if (te.isSeries()) label.setIcon (seriesIcon);
	else if (te.isStudy()) label.setIcon (studyIcon);
	else label.setIcon (defaultIcon);
        Dimension di=label.getMinimumSize();
        di.width+=16;
        di.height+=1;
        label.setPreferredSize(di);
        
        return label;
    }
}

/*
 *  CVS Log
 *  $Log: MyTreeCellRenderer.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
