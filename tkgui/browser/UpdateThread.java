/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package browser;


import J2Ci.*;
import java.awt.*;
import main.*;
import javax.swing.*;
/**
 * This Thread class asks the database for new instances. If an instance was received
 * the tree will be updated.
 *
 * @author Andreas Schroeter
 * @since 30.04.1999 
*/
public class UpdateThread extends Thread
{
    StudyMan client;
    jDVInterface dvi;
   
    
    public UpdateThread (StudyMan sm, jDVInterface dvi)
    {
        client = sm;
        this.dvi = dvi;
        
        
    }
    
/**
	* The Thread-Method for checking for new instances in the database
    */
    public void run()
    {
	while (true)
	{
	    try
	    {
	        Thread.sleep (2000); // wait 2 secs / idle
	        SwingUtilities.invokeLater(new UpdateRun());
	     }
	    catch(Exception e)
	    {
	       
	    }
	        
	}
    }
	
    public class UpdateRun implements Runnable
    {
	public UpdateRun()
	{
	}
	public void run()
	{
	    if (dvi.newInstancesReceived()) 
	    {
	    	client.refreshTree();
	    	Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.BROWSER,"New Instances Arrived!"));
	        Toolkit.getDefaultToolkit().beep();
	    }  	                   
	    
	}
    }
}

/*
 *  CVS Log
 *  $Log: UpdateThread.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
