/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import java.net.*;
import jToolkit.io.IconRetriever;

import J2Ci.*;
/**
* Contains the GUI for the status line.
* The is context sensitve. For each DSComponentType
* There is a seperate view.
*/
public  class SignatureOverview extends JDialog 
{
    private JTabbedPane tab = new JTabbedPane();
    private jDVInterface dvi;
    
    private int numberOfValidPSSignatures;
    private int numberOfUntrustedPSSignatures;
    private int numberOfInvalidPSSignatures;
    
    private int numberOfValidImageSignatures;
    private int numberOfUntrustedImageSignatures;
    private int numberOfInvalidImageSignatures;
    
    private int numberOfValidSRSignatures;
    private int numberOfUntrustedSRSignatures;
    private int numberOfInvalidSRSignatures;
    
    
    private InfoPanel imagePanel;
    private InfoPanel psPanel;
    private InfoPanel srPanel;
    private OverviewPanel overViewPanel; 
    /**
    * Contains a virtual URL. If the JEditorPane 
    * will be initialized with a String anchor in the 
    * HTML side cannot be resolved. Futhermore there HyperlinksEvents 
    * doesn't contain any URL. For this reason we set this ULR as base for
    * the HTMLDocument in the JEditorPane
    */
    private String helpCodeBase = "file://help/text.html";
    public SignatureOverview(jDVInterface dvi)
    {
        super();
        setTitle("Signature Status");
        setModal(true);
        this.dvi = dvi;
        setLocation(200,200);
        setSize(500,300);
        JPanel centerPanel = new JPanel (new BorderLayout());
        centerPanel.setBorder(new EtchedBorder());
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(centerPanel, BorderLayout.CENTER);
        
        overViewPanel= new OverviewPanel();
        imagePanel = new InfoPanel(jDVPSObjectType.DVPSS_image);
        psPanel = new InfoPanel(jDVPSObjectType.DVPSS_presentationState);
        srPanel = new InfoPanel(jDVPSObjectType.DVPSS_structuredReport);
        
        tab.add("Overview",overViewPanel);
        tab.add("Structured Report", srPanel);
        tab.add("Image",imagePanel);
        tab.add("Presentation State", psPanel);
        
        
        centerPanel.add(tab);
        JPanel buttonPanel = new JPanel();
        JButton okButton = new JButton("OK");
        okButton.setToolTipText("Close");
        okButton.addActionListener(new CloseAction());
        buttonPanel.add(okButton);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        setLocation(200,200);
        
    }
    public void setVisible(boolean flag, String type)
    {
        if (type.equals("Report"))tab.setSelectedComponent(srPanel);
        else tab.setSelectedComponent(overViewPanel);
        this.setVisible(flag);
    }
    
    
    public void setVisible(boolean flag)
    {
        if (flag)
        {
            imagePanel.updateHTML();
            psPanel.updateHTML();
            srPanel.updateHTML();
            overViewPanel.updateValues();
        }
        super.setVisible(flag);
        
    }
    /**
    * CloseAction
    */
    public class CloseAction extends AbstractAction
    {
        public CloseAction()
        {
            super("Close");
        }
        public void actionPerformed(java.awt.event.ActionEvent e)
        {
            setVisible(false);
        }
    }
    public class OverviewPanel extends JScrollPane
    {
        
        JLabel numberOfValidPSSignatureLabel = new JLabel();
        JLabel numberOfUntrustedPSSignatureLabel= new JLabel();
        JLabel numberOfInvalidPSSignatureLabel= new JLabel();
        
        JLabel numberOfValidImageSignatureLabel= new JLabel();
        JLabel numberOfUntrustedImageSignatureLabel= new JLabel();
        JLabel numberOfInvalidImageSignatureLabel= new JLabel();
        
        JLabel numberOfValidSRSignatureLabel= new JLabel();
        JLabel numberOfUntrustedSRSignatureLabel= new JLabel();
        JLabel numberOfInvalidSRSignatureLabel= new JLabel();
        
        JLabel totalNumberOfValidSignatureLabel= new JLabel();
        JLabel totalNumberOfUntrustedSignatureLabel= new JLabel();
        JLabel totalNumberOfInvalidSignatureLabel= new JLabel();
        
            int strutSize =5;
        
        public OverviewPanel()
        {
            super();
            
            JPanel p = new JPanel();
            p.setBackground(Color.white);
            GridBagLayout gbl = new GridBagLayout();
            GridBagConstraints gbc = new GridBagConstraints();
            p.setLayout(gbl);
            
            //First line : header
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridwidth = 1;
            gbc.weightx = 0;
            gbc.insets = new Insets(0,0,0,0);
            p.add(Box.createVerticalStrut(strutSize));
            
            p.add(new JLabel(""), gbc);
            
            gbc.anchor = GridBagConstraints.CENTER;
            
            jToolkit.io.IconRetriever ir = new jToolkit.io.IconRetriever ();
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(new JLabel(ir.getIcon(main.MainContext.iconPath+"valid.gif")), gbc);
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(new JLabel(ir.getIcon(main.MainContext.iconPath+"untrusted.gif")), gbc);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(new JLabel(ir.getIcon(main.MainContext.iconPath+"invalid.gif")), gbc);
            
            //Second line
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridwidth = 1;
            gbc.weightx = 0;
            gbc.insets = new Insets(0,0,0,0);
            p.add(Box.createVerticalStrut(strutSize));
            
            JLabel nl = new JLabel("Structured Report: ");
            Font f = nl.getFont();
            Font headerFont = new Font(f.getName(), f.BOLD+f.ITALIC, ( f.getSize()+(int)( f.getSize()*0.2f)));
            nl.setFont(headerFont);
            
            p.add(nl, gbc);
            gbc.anchor = GridBagConstraints.CENTER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfValidSRSignatureLabel, gbc);
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfUntrustedSRSignatureLabel, gbc);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfInvalidSRSignatureLabel, gbc);
            
            p.add(Box.createVerticalStrut(strutSize));
            
            //line 3: image
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridwidth = 1;
            gbc.weightx = 0;
            gbc.insets = new Insets(0,0,0,0);
            
            nl = new JLabel("Image: ");
            nl.setFont(headerFont);
            p.add(nl, gbc);
            gbc.anchor = GridBagConstraints.CENTER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfValidImageSignatureLabel, gbc);
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfUntrustedImageSignatureLabel, gbc);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfInvalidImageSignatureLabel, gbc);
            
            p.add(Box.createVerticalStrut(strutSize));
            
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridwidth = 1;
            gbc.weightx = 0;
            gbc.insets = new Insets(0,0,0,0);
            
        
            
            nl = new JLabel("Presentation State: ");
            nl.setFont(headerFont);
            p.add(nl, gbc);
            gbc.anchor = GridBagConstraints.CENTER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfValidPSSignatureLabel, gbc);
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfUntrustedPSSignatureLabel, gbc);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(numberOfInvalidPSSignatureLabel, gbc);
            
            
            gbc.anchor= GridBagConstraints.NORTH;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.insets = new Insets(0,0,10,0);
            p.add(new JSeparator(),  gbc);
            
            p.add(Box.createVerticalStrut(strutSize));
            
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.gridwidth = 1;
            gbc.weightx = 0;
            gbc.insets = new Insets(0,0,0,0);
            
            nl = new JLabel("Total: ");
            nl.setFont(headerFont);
            p.add(nl, gbc);
            
            gbc.anchor = GridBagConstraints.CENTER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(totalNumberOfValidSignatureLabel, gbc);
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(totalNumberOfUntrustedSignatureLabel, gbc);
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            p.add(Box.createHorizontalStrut(strutSize));
            p.add(totalNumberOfInvalidSignatureLabel, gbc);
            
            setViewportView(p);
            
        }
        
        public void updateValues()
        {
            int type; 
            
            type = jDVPSObjectType.DVPSS_presentationState;
            int numberOfValidPSSignatures = dvi.getNumberOfCorrectSignatures(type);
            int numberOfUntrustedPSSignatures = dvi.getNumberOfUntrustworthySignatures(type);
            int numberOfInvalidPSSignatures= dvi.getNumberOfCorruptSignatures(type);
            numberOfValidPSSignatureLabel.setText(new Integer(numberOfValidPSSignatures).toString());
            numberOfUntrustedPSSignatureLabel.setText(new Integer(numberOfUntrustedPSSignatures).toString());
            numberOfInvalidPSSignatureLabel.setText(new Integer(numberOfInvalidPSSignatures).toString());
        
            type = jDVPSObjectType.DVPSS_image;
            int numberOfValidImageSignatures= dvi.getNumberOfCorrectSignatures(type);
            int numberOfUntrustedImageSignatures= dvi.getNumberOfUntrustworthySignatures(type);
            int numberOfInvalidImageSignatures= dvi.getNumberOfCorruptSignatures(type);
            numberOfValidImageSignatureLabel.setText(new Integer(numberOfValidImageSignatures).toString());
            numberOfUntrustedImageSignatureLabel.setText(new Integer(numberOfUntrustedImageSignatures).toString());
            numberOfInvalidImageSignatureLabel.setText(new Integer(numberOfInvalidImageSignatures).toString());
        
            type = jDVPSObjectType.DVPSS_structuredReport;
            int numberOfValidSRSignatures= dvi.getNumberOfCorrectSignatures(type);
            int numberOfUntrustedSRSignatures= dvi.getNumberOfUntrustworthySignatures(type);
            int numberOfInvalidSRSignatures = dvi.getNumberOfCorruptSignatures(type);
            numberOfValidSRSignatureLabel.setText(new Integer(numberOfValidSRSignatures).toString());
            numberOfUntrustedSRSignatureLabel.setText(new Integer(numberOfUntrustedSRSignatures).toString());
            numberOfInvalidSRSignatureLabel.setText(new Integer(numberOfInvalidSRSignatures).toString());
        
            
            int totalNumberOfValidSignatures= numberOfValidPSSignatures+numberOfValidImageSignatures+numberOfValidSRSignatures;
            int totalNumberOfUntrustedSignatures=numberOfUntrustedPSSignatures+numberOfUntrustedImageSignatures+numberOfUntrustedSRSignatures;
            int totalNumberOfInvalidSignatures = numberOfInvalidPSSignatures+numberOfInvalidImageSignatures+numberOfInvalidSRSignatures;
            totalNumberOfValidSignatureLabel.setText(new Integer(totalNumberOfValidSignatures).toString());
            totalNumberOfUntrustedSignatureLabel.setText(new Integer(totalNumberOfUntrustedSignatures).toString());
            totalNumberOfInvalidSignatureLabel.setText(new Integer(totalNumberOfInvalidSignatures).toString());
            repaint();
            
        }
    }
    
    private class InfoPanel extends JScrollPane
    {
        int type;
        boolean overview;
        JReferenceEditorPane  editorPane = new JReferenceEditorPane();
        public InfoPanel()
        {
            this(-1, true);
        }
        public InfoPanel(int type)
        {
            this(type, false);
        }
        private InfoPanel(int type, boolean overview)
        {
            super();
            this.type = type;
            this.overview = overview;
            HTMLEditorKit eKit;
            HTMLDocument doc ;
            //JEditorPane
            editorPane.setEditable(false);
        
            editorPane.setContentType("text/html");
            editorPane.setEditable(false);
        
            eKit = new HTMLEditorKit();
            editorPane.setEditorKit(eKit);
            
           
            setViewportView(editorPane);   
        }
        public void updateHTML()
        {
            String s= "";
            if (overview)s= dvi.getCurrentSignatureValidationOverview();
            else  s= dvi.getCurrentSignatureValidationHTML(type);
            editorPane.setText(s);
        }
    }
}
/*
 *  CVS Log
 *  $Log: SignatureOverview.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
