/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import jToolkit.gui.*;
import viewer.main.*;
import main.*;
import java.util.*;
import viewer.presentation.*;
import java.awt.event.*;
/**
* Contains the layer manager
*/
public class LayerJPanel extends JDialog implements CommandButtonListener
{
	public static final int EDIT =0;
	public static final int DEL =1;
	public static final int NEW =2;
	public static final int MOVETO= 3;
	public static final int DELAN= 4;
	public static final int DEACTIVATE= 5;
	public static final int ACTIVATE= 6;
	
	public static final int CLOSE = 10;
	CommandJButton moveButton;
    CommandJButton delAnButton;
    CommandJButton editButton;
    CommandJButton delButton;
    CommandJButton newButton;
    CommandJButton closeButton;
	JComboBox layerCombo;
	JTextArea txtArea;
	DefaultListModel model;
	JComboBox moveToCombo;
	JList list;
	ScreenImageHandler screenImageHandler;
    LayerNameItemListener layerNameItemListener = new LayerNameItemListener();
    /**
     * if true the deaktivate list will be visbile.
     *
     * @since 30.03.1999
     */
    
    public boolean deactiveAnnotationListisVisible = false;
	public LayerJPanel(ScreenImageHandler screenImageHandler, JFrame parent, Component relativeLocation)
	{
            super(parent, "Layer Manager" , true);
        setLocation(350,350);
                GridBagLayout gbl = new GridBagLayout();
                GridBagConstraints gbc = new GridBagConstraints();
                this.screenImageHandler = screenImageHandler;
                //setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
                getContentPane().setLayout(new BorderLayout(5,5));
                // row 1
                JPanel layerPanel = new JPanel();
                layerPanel.setBorder(new TitledBorder("Layer Settings"));
                layerPanel.setLayout(gbl);
                
                gbc.anchor = GridBagConstraints.NORTHWEST;
                layerPanel.add(new JLabel("Graphic Layer"));
                layerPanel.add(Box.createHorizontalStrut(10),gbc);
                
                layerCombo= new JComboBox();
                layerCombo.addItemListener(layerNameItemListener);
                gbc.fill = GridBagConstraints.BOTH;
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                gbc.weightx = 1.0;
                layerPanel.add(layerCombo,gbc);
                
                // row 2
                gbc.fill = GridBagConstraints.NONE;
                gbc.weightx = 0.0;
                layerPanel.add(Box.createVerticalStrut(3),gbc);
                gbc.gridwidth = 1;
                layerPanel.add(new JLabel("Description"),gbc);
                
                layerPanel.add(Box.createHorizontalStrut(10),gbc);
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                gbc.weightx = 1.0;
                txtArea = new JTextArea(10,20);
                txtArea.setEditable(false);
                gbc.fill = GridBagConstraints.BOTH;
                //txtArea.setRows(10);
                JScrollPane sp = new JScrollPane(txtArea,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
                layerPanel.add(sp,gbc);
                
                
                //row 3
                gbc.fill = GridBagConstraints.NONE;
                gbc.weightx = 0.0;
                layerPanel.add(Box.createVerticalStrut(3),gbc);
                gbc.gridwidth = 1;
                layerPanel.add(new JLabel(""),gbc);
                
                
                JPanel buttonPanel = new JPanel();
                editButton = new CommandJButton( "Edit",null,this,EDIT);
                delButton = new CommandJButton( "Del",null,this,DEL);
                newButton = new CommandJButton( "New",null,this,NEW);
                buttonPanel.add(editButton);
                buttonPanel.add(newButton);
                buttonPanel.add(delButton);
                
                gbc.weightx = 1.0;
                layerPanel.add(Box.createVerticalStrut(3),gbc);
                gbc.fill = GridBagConstraints.BOTH;
                layerPanel.add(buttonPanel,gbc);
                
            //add
                getContentPane().add(layerPanel,BorderLayout.NORTH);
                
                
                //Annotation
                JPanel annPanel = new JPanel(new BorderLayout(5,5));
                annPanel.setBorder(new TitledBorder("Annotation"));
                layerPanel.setLayout(gbl);
                //Object[] o = {"Klaus","Heiz"};
                model = new DefaultListModel();
                list = new JList(model);
                JScrollPane spList = new JScrollPane(list);
                list.setVisibleRowCount(10);
                list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
                annPanel.add(spList,BorderLayout.CENTER);
                
                
                moveButton = new CommandJButton( "Move to",null,this,MOVETO);
                delAnButton = new CommandJButton( "Del",null,this,DELAN);
                
                JPanel downPanel = new JPanel(new GridBagLayout());
                GridBagConstraints gbcPanel = new GridBagConstraints();
                
                gbcPanel.anchor = GridBagConstraints.NORTHWEST;
                downPanel.add(delAnButton,gbcPanel);
                gbcPanel.weightx = 0.0;
                downPanel.add(moveButton,gbcPanel);
                
                gbcPanel.gridwidth = GridBagConstraints.REMAINDER;
                gbcPanel.weightx = 1.0;
                gbcPanel.fill = GridBagConstraints.BOTH;
                moveToCombo= new JComboBox();
                downPanel.add(moveToCombo,gbcPanel);
                
                
                annPanel.add(downPanel,BorderLayout.SOUTH);
                getContentPane().add(annPanel,BorderLayout.CENTER);
                closeButton = new CommandJButton( "Close",null,this,CLOSE);
                JPanel closePanel = new JPanel();
                closePanel.add(closeButton);
                getContentPane().add(closePanel,BorderLayout.SOUTH);
                pack();
	            setValue();
	
	}
    public void buttonClicked (int ID)
    {
            if (ID == CLOSE)
            {
                setVisible(false);
                dispose();
                return;
            }
            if (ID == DEL) 
            {
                if ((layerCombo.getSelectedIndex()>=0)&&((layerCombo.getSelectedIndex()!=screenImageHandler.presentationStateGraphicsHandler.getListPresentationStateGraphicLayer().size())))
                {
                    screenImageHandler.presentationStateGraphicsHandler.deleteLayer(layerCombo.getSelectedIndex());
                    screenImageHandler.buildImageBuffer(false,false,false,true, true);
         Controller.instance().fireNotification(new SendLayerEvent(this,screenImageHandler.getLayers(),screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex()));
                    setValue();
                }
                return;
                
            }
            if (ID == DELAN) 
            {
               System.out.println("Delete");
               if ((list.getSelectedIndex()>=0)&&(list.getSelectedIndex() < screenImageHandler.presentationStateGraphicsHandler.getPresentationStateGraphicLayer(layerCombo.getSelectedIndex()).listPresentationStateObject.size()))
               {
                   
                    screenImageHandler.presentationStateGraphicsHandler.getPresentationStateGraphicLayer(layerCombo.getSelectedIndex()).deleteObjectAt(list.getSelectedIndex());
                    screenImageHandler.buildImageBuffer(false,false,false,true, true);
                    setValue();
                    
                    
               }
                //deletes overlay
                else
                {
                    if ((list.getSelectedIndex()>=0)|| !deactiveAnnotationListisVisible)
                    {
                        int index = list.getSelectedIndex()- screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().listPresentationStateObject.size();
                        screenImageHandler.presentationStateGraphicsHandler.ps.deactivateOverlay(layerCombo.getSelectedIndex(),index);
                        screenImageHandler.buildImageBuffer(false,false,false,true, true);
                        setValue();
                    }
                }
                
            }
        
        if (ID == EDIT) 
        {
            if ((layerCombo.getSelectedIndex()>=0)&&((layerCombo.getSelectedIndex()!=screenImageHandler.presentationStateGraphicsHandler.getListPresentationStateGraphicLayer().size())))
           {
            (new EditLayerPanel(this, screenImageHandler ,true)).show();
            Controller.instance().fireNotification(new SendLayerEvent(this,screenImageHandler.getLayers(),screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex()));
            setValue();
           }
            
        }
         if (ID == NEW)
         {
            (new EditLayerPanel(this,screenImageHandler ,false)).show();
         Controller.instance().fireNotification(new SendLayerEvent(this,screenImageHandler.getLayers(),screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex()));
            setValue();
            
         }
          if (ID == MOVETO) 
          {
            moveLayer();
          }
    }
    /*
     * Updates the content of this panel.
     * 
     * @since 30.03.1999
     */
    public void setValue()
    {
        
        
        Vector listGraphicLayer = screenImageHandler.presentationStateGraphicsHandler.getListPresentationStateGraphicLayer();
        PresentationStateGraphicLayer nextLayer = null; 
        int nextLayerIndex = -1;
        model.removeAllElements();
        layerCombo.removeItemListener(layerNameItemListener);
        if (layerCombo.getItemCount() > 0)layerCombo.removeAllItems();
        if (moveToCombo.getItemCount() > 0)moveToCombo.removeAllItems();
        
  
        //if a presentation state is available then fill the widgets
        if (screenImageHandler.presentationStateGraphicsHandler.havePresentationState)
        {
            //fill the layerNameChoice and the moveToChoce with the GraphicLayers of the presentation state
            for (int i = 0; i < listGraphicLayer.size(); i++)
            {
                nextLayer =   (PresentationStateGraphicLayer) (listGraphicLayer.elementAt(i));
                layerCombo.addItem(new String(i + " " +nextLayer.getName()));
                moveToCombo.addItem(new String(i + " " +nextLayer.getName()));
            }
            
            //Adds an abstract layer in the layerNameChoice which contains the deactive layers
            if (screenImageHandler.presentationStateGraphicsHandler.overlayList.haveOverlays())
                layerCombo.addItem(new String("inactive"));
            
            //if there is no current layer active then activate the first layer
            if (screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex() == -1) screenImageHandler.presentationStateGraphicsHandler.setCurrentLayerIndex(nextLayerIndex);
           
            //Fill the Annotation list
            if (listGraphicLayer.size() != 0) 
            {
                if (!deactiveAnnotationListisVisible)
                {
                    
                    layerCombo.setSelectedIndex(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex());
                   txtArea.setText(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getDescription());
                    for (int i = 0 ; i < screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().listPresentationStateObject.size(); i++)
                    {
                        model.addElement(((PresentationStateLayerObject) screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().listPresentationStateObject.elementAt(i)).getListText());
                        
                    }
                    for (int i = 0; i < screenImageHandler.presentationStateGraphicsHandler.ps.getNumberOfActiveOverlays(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex()); i++)
                    {
                        model.addElement(screenImageHandler.presentationStateGraphicsHandler.overlayList.getListText(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerIndex(),i));
                    }
                }
                else
                {
                    layerCombo.setSelectedIndex(listGraphicLayer.size());
                    txtArea.setText("inactive overlays");
                    
                    for (int i = 0; i < screenImageHandler.presentationStateGraphicsHandler.ps.getNumberOfOverlaysInImage(); i++)
                    {
                        if (screenImageHandler.presentationStateGraphicsHandler.ps.getOverlayInImageActivationLayer(i) == -1)
                            model.addElement(screenImageHandler.presentationStateGraphicsHandler.overlayList.getImageListText(i));
                        
                    }
                    for (int i = 0; i < screenImageHandler.presentationStateGraphicsHandler.ps.getNumberOfOverlaysInPresentationState(); i++)
                    {
                        if (screenImageHandler.presentationStateGraphicsHandler.ps.getOverlayInPresentationStateActivationLayer(i) == -1)
                            model.addElement(screenImageHandler.presentationStateGraphicsHandler.overlayList.getPresentationStateListText(i));
                        
                    }
                    
                }
            }
            list.setSelectedIndex(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerObjectIndex());
        } 
            
        else this.setEnabled(false);
        layerCombo.addItemListener(layerNameItemListener);
    }
    class LayerNameItemListener implements ItemListener
    { 
        public void itemStateChanged(ItemEvent e)
        {
            changeLayer( e);    
        }
    }
    public void changeLayer(ItemEvent e)
    {
	    
	    if (layerCombo.getItemCount() >0)
	    {
            //if selecteds item is a layer
            if (layerCombo.getSelectedIndex() != screenImageHandler.presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size())
            {
                deactiveAnnotationListisVisible = false;
                        
                screenImageHandler.presentationStateGraphicsHandler.setCurrentLayerIndex(layerCombo.getSelectedIndex());
                setValue();
        	}
        	//if selected Item is the deactive List
        	else
        	{
                deactiveAnnotationListisVisible = true;
                setValue();
        	            
        	}
	    }
        
    }
    public void moveLayer()
    {
        
            if ((moveToCombo.getSelectedIndex() >-1)&&(layerCombo.getSelectedIndex() >-1)&&(list.getSelectedIndex() >-1))
            {
                if (moveToCombo.getSelectedIndex() <screenImageHandler.presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size())
                {
                        
                    //Moves annotations and activate overlays
                    if (layerCombo.getSelectedIndex()<screenImageHandler.presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size())
                    {
                        //Moves annotations 
                        if (list.getSelectedIndex()< screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().listPresentationStateObject.size())
                        {
                            System.out.println("move Annotation to Layer");
                            screenImageHandler.presentationStateGraphicsHandler.moveToLayer( moveToCombo.getSelectedIndex(),
                                                                                                layerCombo.getSelectedIndex(), 
                                                                                                list.getSelectedIndex(), screenImageHandler.getApplyTo());
                        }
                        //Moves overlays
                        else
                        {
                            int status = screenImageHandler.presentationStateGraphicsHandler.ps.moveOverlay(layerCombo.getSelectedIndex(),
                                            list.getSelectedIndex()- screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().listPresentationStateObject.size(),
                                            moveToCombo.getSelectedIndex());
                            
                        }
                        //Repaints only if color is different
                        if (!screenImageHandler.presentationStateGraphicsHandler.getLayerAtIndex(moveToCombo.getSelectedIndex()).getColor(8).equals(screenImageHandler.presentationStateGraphicsHandler.getLayerAtIndex(layerCombo.getSelectedIndex()).getColor(8)))
                        {
                                screenImageHandler.buildImageBuffer(false,false, false,true, true);
                        }
                            
                    }
                    //Moves deactivate Overlays
                    else 
                    {
                            System.out.println("activate Overlay");
                            
                        screenImageHandler.presentationStateGraphicsHandler.overlayList.activateOverlay(list.getSelectedIndex(),moveToCombo.getSelectedIndex());
                        screenImageHandler.buildImageBuffer(false,false,false,true, true);
                        
                    }
                    setValue();
                }
            }
                
        }
        
    
}
/*
 *  CVS Log
 *  $Log: LayerJPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
