/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import de.microtherapy.tools.text.document.general.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import jToolkit.gui.*;
import viewer.main.*;
import J2Ci.*;
public class WindowingPreviewDialog extends JDialog implements CommandTextListener,MouseMotionListener, MouseListener
{
    JButton okButton = new JButton("Ok");
    JButton cancelButton = new JButton("Cancel");
    JButton applyButton = new JButton("Apply");
    WindowPreviewPanel previewPanel;
    CommandJTextField winTextField;
    CommandJTextField cenTextField;
    Point windowPoint;
    
    public static final int ID_WIN =0;
    public static final int ID_CEN = 1;
    double oldWin;
    double oldCen;
    double width;
    double center;
    public int previewHeight;
    public int previewWidth;
    ScreenImageHandler screenImageHandler;
    public jDVPresentationState ps; 
    
    
    public WindowingPreviewDialog(Frame parent,ScreenImageHandler screenImageHandler)
    {
        super(parent,"Preview Window",true);
        
        this.screenImageHandler = screenImageHandler;
        this.setLocation(450,450);
        ps = screenImageHandler.presentationStateGraphicsHandler.ps;
        previewPanel = new WindowPreviewPanel(this);
        
        getContentPane().setLayout(new BorderLayout());
        addMouseMotionListener(this);
        addMouseListener(this);
        
        jIntByRef pWidth = new jIntByRef();
        jIntByRef pHeight = new jIntByRef();
        ps.getPreviewImageWidthHeight(pWidth, pHeight);
        previewHeight = pHeight.value;
        previewWidth = pWidth.value;
        
       
        
        
        previewPanel.setPreferredSize(new Dimension(previewWidth, previewHeight));
        getContentPane().add(previewPanel, BorderLayout.CENTER);
         
         
         
         JPanel buttonPanel = new JPanel();
         buttonPanel.add(okButton);
         okButton.addActionListener(new OkAction());
         buttonPanel.add(cancelButton);
         cancelButton.addActionListener(new CancelAction());
         buttonPanel.add(applyButton);
         applyButton.addActionListener(new ApplyAction());
         
         JPanel textPanel = new JPanel();
         cenTextField = new CommandJTextField(5,this, ID_CEN);
         cenTextField.setDocument(new DoubleDocument());
         textPanel.add(new JLabel("c: "));
         textPanel.add(cenTextField);
        
         winTextField = new CommandJTextField(5,this,ID_WIN);
         winTextField.setDocument(new DoubleDocument());
         textPanel.add(new JLabel("w: "));
         textPanel.add(winTextField);
         
         
         
         JPanel downPanel = new JPanel(new GridLayout(2,1));
         downPanel.add(textPanel);
         downPanel.add(buttonPanel);
         set();
         getContentPane().add(downPanel, BorderLayout.SOUTH);
         pack();
    }
     
     public void setText (int ID, String text)
     {
        if ((cenTextField.getText()==null)||(cenTextField.getText().equals(""))) cenTextField.setText("0");
        if ((winTextField.getText()==null)||(winTextField.getText().equals(""))) winTextField.setText("1");
        center = new Double(cenTextField.getText()).doubleValue();
        width = new Double(winTextField.getText()).doubleValue();
        screenImageHandler.setNewWindow( center,width,false );
        set();
        repaint();
        
     }
    
    public void apply()
    {
        screenImageHandler.setNewWindow( new Double(cenTextField.getText()).doubleValue(),new Double(winTextField.getText()).doubleValue(),true );
        set();
    }
    public void set()
    {
        jDoubleByRef oWin = new jDoubleByRef();
        jDoubleByRef oCen = new jDoubleByRef();
        ps.getCurrentWindowCenter(oCen);
        ps.getCurrentWindowWidth(oWin);
        oldWin = oWin.value;
        oldCen = oCen.value;
        if ( new Double(oldWin).doubleValue() != -1d)
        {
            winTextField.setText(new Double(oldWin).toString());
            cenTextField.setText(new Double(oldCen).toString());
        }
        else
        {
            winTextField.setText("");
            cenTextField.setText("");
            
        }
        
    }
    class CancelAction extends AbstractAction
    {
        
        public CancelAction()
        {
            super();
           
        }
        public void actionPerformed(ActionEvent e)
        {
            ps.setVOIWindow(oldCen, oldWin, "", screenImageHandler.getApplyTo());
            setVisible(false);
        }
    }
    class OkAction extends AbstractAction
    {
        
        public OkAction()
        {
            super();
           
        }
        public void actionPerformed(ActionEvent e)
        {
            
            setVisible(false);
            apply();
        }
    }
    class ApplyAction extends AbstractAction
    {
        
        public ApplyAction()
        {
            super();
           
        }
        public void actionPerformed(ActionEvent e)
        {
            apply();
        }
    }
    /**
	 * Handles the MouseEvent in the same way as the mouseMove action.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseDragged(MouseEvent e)
    {
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==InputEvent.BUTTON3_MASK) 
        {
            
            if(windowPoint != null)
            {
                screenImageHandler.setNewWindow(windowPoint, e.getPoint(),false);
                repaint();
            }
            windowPoint = e.getPoint();
            
            set();
        }
        //mouseMoved(e);
    }
    public void mouseMoved(MouseEvent e)
    {
    }
    public void mouseClicked(MouseEvent e){}
    public void mouseEntered(MouseEvent e){}
    public void mouseExited(MouseEvent e){}
    public void mousePressed(MouseEvent e){windowPoint = e.getPoint();}
    public void mouseReleased(MouseEvent e){windowPoint = null;}



}
/*
 *  CVS Log
 *  $Log: WindowingPreviewDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
