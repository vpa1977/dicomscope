/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import javax.swing.border.*;
import viewer.main.*;
import java.awt.*;
import jToolkit.gui.*;
import de.microtherapy.tools.text.document.dicom.*;

/**
* Contains the GUI for the lediting the layer
*/
public class EditLayerPanel extends JDialog implements CommandButtonListener
{
    public static final int ID_OK = 0;
    public static final int ID_CANCEL = 1;
    CommandJButton okButton = new CommandJButton("OK", this, ID_OK);
    CommandJButton cancelButton = new CommandJButton("CANCEL", this,ID_CANCEL);
    
    ScreenImageHandler screenImageHandler;
    JTextField txtName;
    JTextField txtColor;
    JTextField txtR = new JTextField();
    JTextField txtG= new JTextField();
    JTextField txtB= new JTextField();
    
    JTextArea txtArea;
    boolean edit;
	/**
	 * true if layer has a gray color value, false if layer has a rgb color. 
	 *
	 * @since 30.04.1999
	 */
	public boolean isMonochrome = true;
	JFrame parent = null;
    public EditLayerPanel(Component parent,ScreenImageHandler screenImageHandler, boolean edit)
    {
        super((JFrame)null, "Layer Properties", true);
        setLocationRelativeTo(parent);
        getContentPane().setSize(400,300);
        this.screenImageHandler = screenImageHandler;
        this.edit = edit;
        getContentPane().setLayout(new BorderLayout(10,10));
        JPanel centerPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        centerPanel.setBorder(new TitledBorder("Layer Settings"));
        centerPanel.setLayout(gbl);
        
        gbc.anchor = GridBagConstraints.NORTHWEST;
        centerPanel.add(new JLabel("Layer Name:"));
        centerPanel.add(Box.createHorizontalStrut(10),gbc);
        
        txtName= new JTextField();
        txtName.setDocument(new CSDocument());
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        centerPanel.add(txtName,gbc);
        
        // row 2
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        centerPanel.add(Box.createVerticalStrut(3),gbc);
        gbc.gridwidth = 1;
        centerPanel.add(new JLabel("Color"),gbc);
        
        centerPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        txtColor = new JTextField(4);
        //txtArea.setRows(10);
        centerPanel.add(txtColor,gbc);
        
        // row 3
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        centerPanel.add(Box.createVerticalStrut(3),gbc);
        gbc.gridwidth = 1;
        centerPanel.add(new JLabel("Description"),gbc);
        
        centerPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        txtArea = new JTextArea(10,20);
        txtArea.setDocument(new LODocument());
        txtArea.setLineWrap(true);
        
        gbc.fill = GridBagConstraints.BOTH;
        //txtArea.setRows(10);
        JScrollPane sp = new JScrollPane(txtArea,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        centerPanel.add(sp,gbc);
        getContentPane().add(centerPanel, BorderLayout.CENTER);
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        getContentPane().add(buttonPanel,BorderLayout.SOUTH);
        pack();
	    if (edit)
	    {
	        txtName.setText(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getName());
	        txtArea.setText(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getDescription());
	     
	        if (screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().haveGraphicLayerRecommendedDisplayValue())
	        {
	            if (screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().isGray())
	            {
	                txtColor.setText(new Float(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getFloatGrayColorValue()).toString());
    	        
	            }   
	            else
	            {
	                isMonochrome = false; 
	                //rgbCheckbox.setState(true);
	                //txtR.setText(new Integer(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getRed()).toString());
	                //txtG.setText(new Integer(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getBlue()).toString());
	                //txtB.setText(new Integer(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getGreen()).toString());
	                txtColor.setText("");
    	        
	            }
	        }
	                
	        else txtColor.setText(new Float(screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().getFloatGrayColorValue()).toString());
	         
	        
	    }
	    else
	    {
	        txtName.setText("new layer ");
	        txtColor.setText("1");
	    }
        
    }
    public void buttonClicked (int ID)
    {
            if (ID == ID_OK) editLayer();
            if (ID == ID_CANCEL) setVisible (false);
    }
    public void editLayer()
    {
             
            int rValue;
            int gValue;
            int bValue;
            float mValue;
                    
            //is layer name not null
            if (txtName.getText().trim().equals("")||txtName.getText()== null) JOptionPane.showMessageDialog(null,"Please insert layer name");
            else 
            {
                //handle monochome layer
                if (isMonochrome) 
                {
                    mValue = getIntValue(txtColor.getText());
                    try
                    {
                        //change in int value
                        mValue = (new Float(txtColor.getText().trim())).floatValue();
                        if (((mValue <0.0f) || (mValue > 1f)))JOptionPane.showMessageDialog(null,"Please insert value 0.000 - 1.00 in field gray value");
                        else 
                        {
                            //insert new PresentationStateGraphicLayer with monochrome color
                            if (!edit)
                            {   
                                screenImageHandler.presentationStateGraphicsHandler.insertNewMonochromeLayer(txtName.getText(),txtArea.getText(),mValue);
                                screenImageHandler.presentationStateGraphicsHandler.setCurrentLayerIndex( screenImageHandler.presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size()-1);
                                    setVisible(false);
                            }
                            else
                            {
                                //edit  PresentationStateGraphicLayer
                                if (!screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setNewName(txtName.getText()))JOptionPane.showMessageDialog(null,"Layer name must be unique");
                                else
                                {
                                    screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setDescription(txtArea.getText());
                                    screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setFloatColorValue(mValue);
                                    screenImageHandler.buildImageBuffer(false,false, false,true, true);
                                    setVisible(false);
                                }   
                            }
                        }
                    }
                    catch(NumberFormatException t) 
                    {
                        JOptionPane.showMessageDialog(null,"Please insert value 0.000 - 1.00 in field gray value");
                    }
                }
                //handle color PresentationStateGraphicLayer
                else 
                {
                    rValue = getIntValue(txtR.getText());
                    gValue = getIntValue(txtG.getText());
                    bValue = getIntValue(txtB.getText());
                    if ((rValue ==Integer.MAX_VALUE) ||(rValue >= 256)||(rValue < 0)) JOptionPane.showMessageDialog(null,"Please insert value 0 -255 in field r");
                    else if ((gValue ==Integer.MAX_VALUE) ||(gValue >= 256)||(gValue < 0)) JOptionPane.showMessageDialog(null,"Please insert value 0 -255 in field g");
                    else if ((bValue ==Integer.MAX_VALUE) ||(bValue >= 256)||(bValue < 0)) JOptionPane.showMessageDialog(null,"Please insert value 0 -255 in field b");
                    else
                    {
                        // insert new PresentationStateGraphicLayer
                        if (!edit)
                        {   
                                
                            screenImageHandler.presentationStateGraphicsHandler.insertNewColorLayer(   txtName.getText(),
                                                                                                        txtArea.getText(),
                                                                                                        (new Integer(txtR.getText())).intValue(),
                                                                                                        (new Integer(txtG.getText())).intValue(),
                                                                                                        (new Integer(txtB.getText())).intValue());
                            screenImageHandler.presentationStateGraphicsHandler.setCurrentLayerIndex( screenImageHandler.presentationStateGraphicsHandler.listPresentationStateGraphicLayer.size()-1);
                        }
                        //edit old PresentationStateGraphicLayer
                        else
                        {
                            if (!screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setNewName(txtName.getText()))JOptionPane.showMessageDialog(null,"Layer name must be unique");
                            else
                            {
                                screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setDescription(txtArea.getText());
                                screenImageHandler.presentationStateGraphicsHandler.getCurrentLayer().setGraphicLayerRecommendedDisplayValueRGB(rValue, gValue, bValue);
                                screenImageHandler.buildImageBuffer(false,false, false,true, true);
                                    
                            }   
                                    
                        }
                    }
                }                                                                               
            }
           
    }
    /**
     * Convert a String to an int value.
     *
     * @param textValue Contains the String which should be converted to an int-value.
     * @since 30.04.1999
     */
    public int getIntValue(String textValue)
    {
        try
        {
            //Umwandelung der Eingabe in einen intWert
            return (new Integer(textValue.trim())).intValue();
            
        }
        catch(NumberFormatException t)
        {
            System.out.println("Number Format Exception" + t);
            return Integer.MIN_VALUE;
        }    
        
    }
    
}
/*
 *  CVS Log
 *  $Log: EditLayerPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
