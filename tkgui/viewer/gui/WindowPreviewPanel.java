/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import jToolkit.gui.*;
import viewer.main.*;
import J2Ci.*;
import java.awt.geom.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;

import main.*;
/**
* This class contains the preview image changing th window-level settings
* on a preview image. 
*/
public class WindowPreviewPanel extends JPanel 
{
    
    DataBufferByte dbb;
    WindowingPreviewDialog windowingPreviewDialog;
    BufferedImage previewImage;
    public WindowPreviewPanel(WindowingPreviewDialog windowingPreviewDialog)
    {
        super();
        this.windowingPreviewDialog = windowingPreviewDialog;
        
    }
    public void paintComponent(Graphics g)
    {
        if (g != null)
        {
           
            Graphics2D g2 = (Graphics2D)g;
            WritableRaster wr ;
            int bandOffsets[] = {0};
	    if (dbb == null)
            {
                dbb = new DataBufferByte(new byte[windowingPreviewDialog.previewWidth*windowingPreviewDialog.previewHeight],
                                        windowingPreviewDialog.previewWidth*windowingPreviewDialog.previewHeight);

            }
            System.out.println(" dbb.getSize() "+ dbb.getSize());
	    int status = windowingPreviewDialog.ps.getPreviewImageBitmap(dbb.getData(), (long) dbb.getSize());
    	    
	    if (status != jE_Condition.EC_Normal) System.out.println("Fehler bei ps.getPreviewImageBitmap " + status);
            else
            {
            //Create new Image
                wr = Raster.createInterleavedRaster(dbb,
                                                    windowingPreviewDialog.previewWidth,
                                                    windowingPreviewDialog.previewHeight,
                                                    windowingPreviewDialog.previewWidth,
                                                    1, bandOffsets,null);
                previewImage = new BufferedImage(MainContext.instance().getIndexColorModel(), wr, MainContext.instance().getIndexColorModel().isAlphaPremultiplied(), null);
                int h = (this.getSize().height-windowingPreviewDialog.previewHeight)/2;
                int w = (this.getSize().width-windowingPreviewDialog.previewWidth)/2;
                
                g2.drawImage(previewImage,w,h, this);
            }
    	  
        }
        
    }
}
/*
 *  CVS Log
 *  $Log: WindowPreviewPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
