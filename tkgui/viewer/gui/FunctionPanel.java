/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.gui;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import main.*;
    import java.util.*;

/**
 * This class contains the GUI for the function panel
 * 
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class FunctionPanel extends JPanel 
{
    
    public String imagePanelOrientation ="None";
    boolean integratedPaintFunctions = false;
    public  JPanel paintPanel;
    JPanel processingPanel;
    JPanel psPanel;
    JPanel infoPanel;
    JPanel mainPanel ;
    JPanel radioPanel;
     boolean visiblePaint = true;
     boolean visiblePS = true;
     boolean visibleProcessing = true;
     boolean visibleInfo = true;
     boolean  visibleTitledBorder = true;
    
    public int orientation = -1;
    public int paintPanelOrientation = BoxLayout.Y_AXIS;
    
    GuiComponents gui;
    
    /**
     * Constructor
     * 
     * @param orientation
     */
    public FunctionPanel()
    {
        gui =  GuiComponents.getInstance();
         
         
         
    }
	
	/**
	 * Builds the panel vertical
	 */
	
	public void buildY()
	{
		  removeAll();
		  
          GridBagConstraints gbc = new GridBagConstraints();
         gbc.anchor = GridBagConstraints.NORTHWEST;
          setLayout(new BorderLayout());
          
          
          mainPanel = new JPanel();
          mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
          
          /////////////////////////
          // Paint
          //////////////////////////
          if (visiblePaint&&integratedPaintFunctions)
          {
                paintPanel = new JPanel();
                paintPanel.setLayout(new BoxLayout(paintPanel,BoxLayout.Y_AXIS));
                 
                //Shutter
                JPanel shutterPanel = new JPanel();
                //shutterPanel.setLayout(new BoxLayout(shutterPanel,BoxLayout.X_AXIS));
                TitledBorder tb = new  TitledBorder("Shutter");
               
                
                shutterPanel.setBorder(tb);
                
                shutterPanel.add(gui.rectShButton);
                shutterPanel.add(gui.circleShButton);
                shutterPanel.add(gui.polyShButton);
                shutterPanel.add(gui.bmpShButton);
                shutterPanel.add(gui.optShButton);
                paintPanel.add(shutterPanel);
                  
                JPanel paintFkt = new JPanel();
                paintFkt.setLayout(new BoxLayout(paintFkt,BoxLayout.Y_AXIS));
                paintFkt.setBorder(new TitledBorder("Paint"));
                  
                JPanel fFkt = new JPanel();
                fFkt.add(gui.lineButton);
                fFkt.add(gui.rectButton);
                fFkt.add(gui.pointButton);
                fFkt.add(gui.circleButton);
                fFkt.add(gui.ellipseButton);
                paintFkt.add(fFkt);
                  
                JPanel sFkt = new JPanel();
                  
                sFkt.add(gui.polyButton);
                sFkt.add(gui.textButton);
                sFkt.add(gui.interpolatedButton);
                sFkt.add(gui.editButton);
                sFkt.add(gui.editLayerButton);
                paintFkt.add(sFkt);
                  
                  
                JPanel p1 = new JPanel();
                  
                p1.add(gui.filledCheckBox);
                p1.add(gui.imageRelCheckBox);
                paintFkt.add(p1);
                  
                JPanel p2 = new JPanel();
                p2.add(new JLabel("Layer: "));
                p2.add(gui.layerComboBox);
                paintFkt.add(p2);
                  
                paintPanel.add(paintFkt);
                mainPanel.add(paintPanel);
          }
          else buildPaint(); 
          //////////////////////////////
          //Processing
          /////////////////////////////
          if (visibleProcessing)
          {
                processingPanel = new JPanel(); 
                processingPanel.setLayout(new BoxLayout(processingPanel,BoxLayout.Y_AXIS));
                  
                JPanel proPanel = new JPanel();
                //proPanel.setLayout(new BoxLayout(proPanel,BoxLayout.X_AXIS));
                proPanel.setBorder(new TitledBorder("Processing"));
                proPanel.add(gui.resetButton);
                proPanel.add(gui.flipButton);
                proPanel.add(gui.rotButton);
                proPanel.add(gui.invertButton);
                proPanel.add(gui.presentationLutButton);
                proPanel.add(gui.psButton);
                if (!MainContext.instance().highQualitySystem)proPanel.add(gui.lutButton);
                processingPanel.add(proPanel,gbc);
                  
                  
                //zoomPanel
                JPanel zoomPanel = new JPanel();
                zoomPanel.setBorder(new TitledBorder("Zoom"));
                zoomPanel.setLayout(new GridBagLayout());
                zoomPanel.add(gui.zoomButton,gbc);
                zoomPanel.add(Box.createHorizontalStrut(10),gbc);
                zoomPanel.add(gui.zoomFitButton,gbc);
                zoomPanel.add(Box.createHorizontalStrut(10),gbc);
                zoomPanel.add(gui.zoom1to1Button,gbc);
                zoomPanel.add(Box.createHorizontalStrut(10),gbc);
                zoomPanel.add(new JLabel(" Factor:  "),gbc);
                zoomPanel.add(gui.zoomTextField,gbc);
                processingPanel.add(zoomPanel,gbc);
                  
                //windowPanel
                JPanel wlPanel = new JPanel();
                wlPanel.setBorder(new TitledBorder("Window Settings"));
                wlPanel.setLayout(new GridBagLayout());
                wlPanel.add(gui.winInFrameButton,gbc);
                wlPanel.add(Box.createHorizontalStrut(10),gbc);
                wlPanel.add(gui.voiButton,gbc);
                wlPanel.add(Box.createHorizontalStrut(10),gbc);
                wlPanel.add(new JLabel("c: "),gbc);
                wlPanel.add(gui.centerTextField,gbc);
                wlPanel.add(Box.createHorizontalStrut(5),gbc);
                wlPanel.add(new JLabel("w: "),gbc);
                wlPanel.add(gui.windowTextField,gbc);
                processingPanel.add(wlPanel,gbc);
                  
                mainPanel.add(processingPanel,gbc);
          
                radioPanel = new JPanel();
                radioPanel.setBorder(new TitledBorder("Presentation Size Mode"));
                radioPanel.add(gui.scaleRadio);
                radioPanel.add(gui.magnifyRadio);
                radioPanel.add(gui.trueSizeRadio);
                mainPanel.add(radioPanel);
          }
         
          //////////////////////////////
          //Presentation State
          /////////////////////////////
          if (visiblePS)
          {
                psPanel = new JPanel();
                psPanel.setLayout(new BoxLayout(psPanel,BoxLayout.Y_AXIS));
                psPanel.setBorder(new TitledBorder("Presentation State"));
                  
                JPanel namePanel = new JPanel();
                namePanel.setLayout(new GridBagLayout());
                  
                gbc.weightx = 0.0;
                gbc.gridwidth = 1;
                namePanel.add(new JLabel("Label"),gbc);
                //namePanel.add(Box.createHorizontalStrut(10),gbc);
                
                gbc.fill = GridBagConstraints.BOTH;
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                gbc.weightx = 1.0;
                namePanel.add(gui.nameTextField,gbc);
                
                // row 2
                gbc.fill = GridBagConstraints.NONE;
                gbc.weightx = 0.0;
                gbc.gridwidth = 1;
                namePanel.add(new JLabel("Creator"),gbc);
                  
                gbc.fill = GridBagConstraints.BOTH;
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                gbc.weightx = 1.0;
                namePanel.add(gui.creatorTextField,gbc);
                psPanel.add(namePanel);
                /*
                JPanel descrPanel = new JPanel(new BorderLayout());
                descrPanel.add(new JLabel("Description"), BorderLayout.WEST);
                descrPanel.add(gui.descrTextArea, BorderLayout.CENTER);
                psPanel.add(descrPanel);
                */
                mainPanel.add(psPanel);
          }
          /////////////////////////////#
          //Rado
          ////////////////////////////////////////////
          add(mainPanel, BorderLayout.NORTH);
          
          if(visibleInfo)
          {
          
            JPanel infoPanel = new JPanel(new BorderLayout());
            infoPanel.setBorder(new TitledBorder("Information"));
            gui.infoTextArea.setRows(10); 
            infoPanel.add(new JScrollBar(JScrollBar.HORIZONTAL).add(gui.infoTextArea),BorderLayout.CENTER);
            add(infoPanel, BorderLayout.CENTER);
		  }
	    
	}

	/**
	 * Builds the panel horizontal
	 */
	public void buildX()
	{
           GridBagConstraints gbc = new GridBagConstraints();
          gbc.anchor = GridBagConstraints.NORTHWEST;
		   removeAll();
           mainPanel = new JPanel();
           mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
          
            buildPaint();    
                
          //////////////////////////////
          //Processing
          /////////////////////////////
          if(visibleProcessing)
          {
                processingPanel = new JPanel(); 
                processingPanel.setLayout(new BoxLayout(processingPanel,BoxLayout.X_AXIS));
                JPanel proPanel = new JPanel();
                if (visibleTitledBorder) proPanel.setBorder(new TitledBorder("Processing"));
                proPanel.add(gui.resetButton);
                proPanel.add(gui.flipButton);
                proPanel.add(gui.invertButton);
                proPanel.add(gui.rotButton);
                proPanel.add(gui.presentationLutButton);
                proPanel.add(gui.psButton);
                if (!MainContext.instance().highQualitySystem)proPanel.add(gui.lutButton);
                processingPanel.add(proPanel);
                  
                  
                  
                JPanel zoomPanel = new JPanel();
                if (visibleTitledBorder) zoomPanel.setBorder(new TitledBorder("Zoom"));
                zoomPanel.add(gui.zoomButton);
                zoomPanel.add(gui.zoomFitButton);
                zoomPanel.add(gui.zoom1to1Button);
                zoomPanel.add(new JLabel(" Factor:  "));
                zoomPanel.add(gui.zoomTextField);
                processingPanel.add(zoomPanel);
                  
                //windowPanel
                JPanel wlPanel = new JPanel();
                if (visibleTitledBorder) wlPanel.setBorder(new TitledBorder("Window Settings"));
                wlPanel.add(gui.winInFrameButton);
                wlPanel.add(gui.voiButton);
                wlPanel.add(new JLabel("Center: "));
                wlPanel.add(gui.centerTextField);
                wlPanel.add(new JLabel("Width: "));
                wlPanel.add(gui.windowTextField);
                processingPanel.add(wlPanel);
                  
                radioPanel = new JPanel();
                if (visibleTitledBorder) radioPanel.setBorder(new TitledBorder("Presentation Size Mode"));
                radioPanel.add(gui.scaleRadio);
                radioPanel.add(gui.magnifyRadio);
                radioPanel.add(gui.trueSizeRadio);
                processingPanel.add(radioPanel);
                  
                  
                mainPanel.add(processingPanel,gbc);
          }
          //////////////////////////////
          //Presentation State
          /////////////////////////////
          if (visiblePS)
          {
                psPanel = new JPanel();
                psPanel.setLayout(new BoxLayout(psPanel,BoxLayout.X_AXIS));
                if (visibleTitledBorder) psPanel.setBorder(new TitledBorder("Presentation State"));
                  
                JPanel namePanel = new JPanel();
                namePanel.setLayout(new GridLayout(2,2));
                namePanel.add(new JLabel("Label"));
                namePanel.add(gui.nameTextField);
                namePanel.add(new JLabel("Creator"));
                namePanel.add(gui.creatorTextField);
                psPanel.add(namePanel);
                  
                add(gui.getSeperator());
                  
                JPanel descrPanel = new JPanel(new BorderLayout());
                descrPanel.add(new JLabel("    PS Description"), BorderLayout.WEST);
                descrPanel.add(gui.descrTextArea, BorderLayout.CENTER);
                psPanel.add(descrPanel);
                  
                  
                mainPanel.add(psPanel);
          }
	      add(mainPanel);
	}
    public void buildPaint()
    {
          /////////////////////////
          // Paint
          //////////////////////////
          if(visiblePaint)
          {
                //init paint
                JPanel paintFkt = new JPanel();
                paintFkt.add(gui.lineButton);
                paintFkt.add(gui.rectButton);
                paintFkt.add(gui.pointButton);
                paintFkt.add(gui.circleButton);
                paintFkt.add(gui.ellipseButton);
                paintFkt.add(gui.polyButton);
                paintFkt.add(gui.textButton);
                paintFkt.add(gui.interpolatedButton);
                paintFkt.add(gui.editButton);
                paintFkt.add(gui.editLayerButton);
                if (!integratedPaintFunctions)
                {
                
                    paintFkt.add(gui.selectLayerButton);
                    paintFkt.add(gui.filledButton);
                    paintFkt.add(gui.imageDisplayButton);
                }
                JPanel shutterPanel = new JPanel();
                shutterPanel.add(gui.rectShButton);
                shutterPanel.add(gui.circleShButton);
                shutterPanel.add(gui.polyShButton);
                shutterPanel.add(gui.bmpShButton);
                shutterPanel.add(gui.optShButton);
                
 
                if (visibleTitledBorder) 
                {
                    paintFkt.setBorder(new TitledBorder("Paint"));
                    shutterPanel.setBorder(new TitledBorder("Shutter"));
                }
                
                paintPanel = new JPanel();
                if (integratedPaintFunctions)
                {
                    paintFkt.add(gui.filledCheckBox);
                    paintFkt.add(gui.getSeperator());
                    paintFkt.add(gui.imageRelCheckBox);
                    paintFkt.add(gui.getSeperator());
                    paintFkt.add(new JLabel("Layer: "));
                    paintFkt.add(gui.layerComboBox);
                    
                    paintPanel.add(paintFkt);
                    paintPanel.add(shutterPanel);
                    paintPanel.setLayout(new BoxLayout(paintPanel,BoxLayout.X_AXIS));
                    mainPanel.add(paintPanel);
                }
                else
                {
                    if (paintPanelOrientation == BoxLayout.X_AXIS)paintPanel.setLayout(new BoxLayout(paintPanel,BoxLayout.X_AXIS));
                    else 
                    {
                        shutterPanel.setLayout(new BoxLayout(shutterPanel,BoxLayout.Y_AXIS));
                        paintFkt.setLayout(new BoxLayout(paintFkt,BoxLayout.Y_AXIS));
                        paintPanel.setLayout(new BoxLayout(paintPanel,BoxLayout.Y_AXIS));
                    }
                    paintPanel.add(paintFkt);
                    paintPanel.add(shutterPanel);
                    
                    
                }
                    
                
                    
                    
          }
        
    }
	public void setConfiguration(Hashtable config, String orient)
	{
            imagePanelOrientation ="None";
            if (config.containsKey("PSPanelOn"))
            {
               if(((Boolean)config.get("PSPanelOn")).booleanValue()) visiblePS = true;
               else visiblePS  = false;
            
            }
            if (config.containsKey("ProcessingPanelOn"))
            {
               if(((Boolean)config.get("ProcessingPanelOn")).booleanValue())  visibleProcessing = true;
               else visibleProcessing  = false;
            
            }
            if (config.containsKey("PaintPanelPlacement"))
            {
                String value = (String)config.get("PaintPanelPlacement");
                if (value.equals("None") ) visiblePaint  = false;
                else if (value.equals("Toolbar") ) visiblePaint  = false;
                else 
                {
                    visiblePaint = true;
                    if (value.equals("Integrated")||(value.equals(orient)))
                    {
                        integratedPaintFunctions = true;
                        
                    }
                    else 
                    {
                        integratedPaintFunctions = false;
                        imagePanelOrientation = value;
                        if (value.equals("West")||value.equals("East"))paintPanelOrientation = BoxLayout.Y_AXIS;
                        else paintPanelOrientation = BoxLayout.X_AXIS;
                        
                    }
                }
               
            }
        if (orient.equals("West") ||orient.equals("East")) orientation =BoxLayout.Y_AXIS;
        else orientation =BoxLayout.X_AXIS;
        if (orientation == BoxLayout.X_AXIS) buildX();
	    else buildY();  
	    this.orientation = orientation;
            
	    
	}
    
}
/*
 *  CVS Log
 *  $Log: FunctionPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
