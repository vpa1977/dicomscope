/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.paint;

import java.awt.*;
import java.awt.geom.*;

/**
 * This class contains methods for creating a line.
 * <br>
 * These objects can be display or image relative.
 *
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PaintObject
 */

public class Line2DObject extends PaintObject 
{
    
    /**
     * Contains the geometic form of the line.
     * 
     * @since 30.04.1999
     */
    Line2D thisShape = null;
    
    /**
    * Contructs an new line.
    *
    * @param start Startpoint of the line.
    * @param end Endpoint of the line.
    * @since 30.04.1999
    */
    public Line2DObject(Point2D.Float start, Point2D.Float end)
    {
        super();
        setNewPoint(start);
        setNewPoint(end);
    }
    /**
     * Constucts a new object with the specified parameter.
     * 
     * @param isDisplayRelative true if the object should be display relative.
     * @since 30.04.1999
     */
    public Line2DObject( boolean isDisplayRelative)
    {
        super();
        
        this.isDisplayRelative = isDisplayRelative;
    }
    
    
    
    /**
     * Constructs a new object.
     * 
     * @since 30.04.1999
     */
    public Line2DObject()
    {
        super();
    }
   
    /**
     * Gets the Shape of the Circle2DObject.
     * 
     * @return The Shape of the Circle2DObject.
     * @since 30.04.1999
     */
    public  Shape getShape()
    {
        return thisShape;
    }
    /**
     * Insert a new point to the objectPoints. You can only insert 2 points.
     * 
     * @param The new point of the line
     * @since 30.04.1999
     */
    public  void setNewPoint(Point2D.Float newPoint)
    {
      
      if (objectPoints.size() == 0)
      {
         status = STATUS_BEGIN;
         
      }
      else
      {
         status = STATUS_STOP;
         thisShape = new Line2D.Float((Point2D.Float)objectPoints.elementAt(0),(Point2D.Float) newPoint);
      }
      objectPoints.add(newPoint);
    }
    /**
    * Returns the part of the Line2DObject which should be redraw if the Line2DObject will be created. 
    *
    * @param nextPoint The next drawing Point. 
    * @return The part of the PaintObject which should be redraw. 
    * @see PaintObject#getMovePaintObject
    */
    public  PaintObject getMovePaintObject(Point2D.Float nextPoint)
    {
      return new Line2DObject(getCopyPointAt(0), nextPoint);
    } 
    /**
    * Returns a real copy of the Line2DObject.
    *
    * @return a real copy of this Line2DObject.
    * @since 30.04.1999
    */
    public PaintObject copy()
    {
      Line2DObject returnAnnotation = new Line2DObject();
      for (int i = 0; i< objectPoints.size(); i++)
      {
         returnAnnotation.setNewPoint(((Point2D.Float)objectPoints.elementAt(i)));
      }
      returnAnnotation.setStatus(this.getStatus());
      returnAnnotation.setFilled(filled);
      return returnAnnotation;
    }
    /**
    * Draws the Shape of the Line2DObject  in the specified Graphics context.
    *
    * @param g The Graphics context in which the Shape should be drawn.
    * @since 30.04.1999
    */
   public  void drawShape(Graphics2D g)
   {
      if ((g!=null) && (thisShape!= null))
      {
         g.draw(thisShape);
         g.fill(thisShape);
      }
   }
    
    /**
    * Returns new Line2DObject with the  same properties as the current object.
    *
    * @return A new Line2DObject with the same properties.
    * @since 30.04.1999
    */
    public  PaintObject getNewPaintObject()
    {
      return new Line2DObject(isDisplayRelative);
    }
   
}

/*
 *  CVS Log
 *  $Log: Line2DObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
