/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.sr;

import java.lang.*;
import javax.swing.tree.*;
import javax.swing.*;
import java.awt.*;
import jToolkit.io.*;
import main.MainContext;
import J2Ci.*;

/**
 * Draws a tree element with the correct icon in the tree 
 * 
 * @author Andreas Schroeter
 * @since 30.04.1999
*/
public class SREditTreeCellRenderer implements TreeCellRenderer
{
      // Icons
      
    protected static Icon rootIcon;
    protected static Icon headerIcon;
    
    protected static Icon textUnSignIcon;
    protected static Icon textSignIcon;
    
    protected static Icon codeSignIcon;
    protected static Icon codeUnSignIcon;
    
    protected static Icon numSignIcon;
    protected static Icon numUnSignIcon;
    
    protected static Icon dateTimeSignIcon;
    protected static Icon dateTimeUnSignIcon;
    
    protected static Icon dateSignIcon;
    protected static Icon dateUnSignIcon;
    
    protected static Icon timeSignIcon;
    protected static Icon timeUnSignIcon;
    
    protected static Icon uidRefSignIcon;
    protected static Icon uidRefUnSignIcon;
    
    protected static Icon pnSignIcon;
    protected static Icon pnUnSignIcon;
    
    protected static Icon scSignIcon;
    protected static Icon scUnSignIcon;
    
    protected static Icon tcSignIcon;
    protected static Icon tcUnSignIcon;
    
    protected static Icon compositeSignIcon;
    protected static Icon compositeUnSignIcon;
    
    protected static Icon imageSignIcon;
    protected static Icon imageUnSignIcon;
    
    protected static Icon waveformSignIcon;
    protected static Icon waveformUnSignIcon;
    
    protected static Icon containerSignIcon;
    protected static Icon containerUnSignIcon;

    protected static Icon refSignIcon;
    protected static Icon refUnSignIcon;
     // Colors
    /** Color to use for the foreground for selected nodes. */
    protected Color textSelectionColor;

    /** Color to use for the foreground for non-selected nodes. */
    protected Color textNonSelectionColor;

    /** Color to use for the background when a node is selected. */
    protected Color backgroundSelectionColor;

    /** Color to use for the background when the node isn't selected. */
    protected Color backgroundNonSelectionColor;

    /** Color to use for the background when the node isn't selected. */
    protected Color borderSelectionColor;


    protected Font textSelectionFont;
    protected Font textNonSelectionFont;

    static
    {
	    IconRetriever ir = new IconRetriever();        
            rootIcon = ir.getIcon(MainContext.iconPath + "default.gif");
            headerIcon = ir.getIcon(MainContext.iconPath + "header.gif");
            
            textUnSignIcon= ir.getIcon(MainContext.iconPath + "text_unsign.gif");
            textSignIcon= ir.getIcon(MainContext.iconPath + "text_sign.gif");
            
            codeSignIcon= ir.getIcon(MainContext.iconPath + "code_sign.gif");
            codeUnSignIcon= ir.getIcon(MainContext.iconPath + "code_unsign.gif");
            
            numSignIcon= ir.getIcon(MainContext.iconPath + "num_sign.gif");
            numUnSignIcon= ir.getIcon(MainContext.iconPath + "num_unsign.gif");
            
            dateTimeSignIcon= ir.getIcon(MainContext.iconPath + "datetime_sign.gif");
            dateTimeUnSignIcon= ir.getIcon(MainContext.iconPath + "datetime_unsign.gif");
            
            dateSignIcon= ir.getIcon(MainContext.iconPath + "date_sign.gif");
            dateUnSignIcon= ir.getIcon(MainContext.iconPath + "date_unsign.gif");
            
            timeSignIcon= ir.getIcon(MainContext.iconPath + "time_sign.gif");
            timeUnSignIcon= ir.getIcon(MainContext.iconPath + "time_unsign.gif");
            
            pnSignIcon= ir.getIcon(MainContext.iconPath + "pname_sign.gif");
            pnUnSignIcon= ir.getIcon(MainContext.iconPath + "pname_unsign.gif");
            
            scSignIcon= ir.getIcon(MainContext.iconPath + "scoord_sign.gif");
            scUnSignIcon= ir.getIcon(MainContext.iconPath + "scoord_unsign.gif");
            
            tcSignIcon= ir.getIcon(MainContext.iconPath + "tcoord_sign.gif");
            tcUnSignIcon= ir.getIcon(MainContext.iconPath + "tcoord_unsign.gif");
            
            compositeSignIcon= ir.getIcon(MainContext.iconPath + "composite_sign.gif");
            compositeUnSignIcon= ir.getIcon(MainContext.iconPath + "composite_unsign.gif");
            
            imageSignIcon= ir.getIcon(MainContext.iconPath + "image_sign.gif");
            imageUnSignIcon= ir.getIcon(MainContext.iconPath + "image_unsign.gif");
            
            waveformSignIcon= ir.getIcon(MainContext.iconPath + "waveform_sign.gif");
            waveformUnSignIcon= ir.getIcon(MainContext.iconPath + "waveform_unsign.gif");
            
            containerSignIcon= ir.getIcon(MainContext.iconPath + "container_sign.gif");
            containerUnSignIcon= ir.getIcon(MainContext.iconPath + "container_unsign.gif");
            
            uidRefSignIcon= ir.getIcon(MainContext.iconPath + "uid_sign.gif");
            uidRefUnSignIcon= ir.getIcon(MainContext.iconPath + "uid_unsign.gif");
        
            refSignIcon= ir.getIcon(MainContext.iconPath + "ref_sign.gif");
            refUnSignIcon= ir.getIcon(MainContext.iconPath + "ref_unsign.gif");
            
    }
    public SREditTreeCellRenderer()
    {
            Font f=(new JLabel()).getFont();


        if (MainContext.instance().highQualitySystem)
        {
            textSelectionFont=new Font(f.getName(), f.getStyle() | Font.ITALIC, f.getSize());
            textNonSelectionFont=new Font(f.getName(), f.getStyle(), f.getSize());
            /*
            pSIcon = ir.getIcon (MainContext.iconPath + "bigps.gif"); 
            srIcon = ir.getIcon (MainContext.iconPath + "sr32.gif"); 
            //new ImageIcon (MainContext.iconPath + "bigps.gif");

            instanceIcon = ir.getIcon (MainContext.iconPath + "biginstance.gif");
            seriesIcon = ir.getIcon (MainContext.iconPath + "bigseries.gif");
            studyIcon = ir.getIcon (MainContext.iconPath + "bigstudy.gif");        
            defaultIcon = ir.getIcon (MainContext.iconPath + "bigdefault.gif");
            newIcon = ir.getIcon (MainContext.iconPath + "bignew.gif");
            hcIcon = ir.getIcon (MainContext.iconPath + "hc32.gif");
            spIcon = ir.getIcon (MainContext.iconPath + "sp32.gif");
            containsNewIcon = ir.getIcon (MainContext.iconPath + "containsNew32.gif");
            */
        }
        else
        {
            textSelectionFont=new Font(f.getName(),f.getStyle()|Font.ITALIC,f.getSize());
            textNonSelectionFont=f;

        }
        setTextSelectionColor(UIManager.getColor("Tree.textSelectionColor"));
        setTextNonSelectionColor(UIManager.getColor("Tree.textNonSelectionColor"));
        setBackgroundSelectionColor(UIManager.getColor("Tree.selectionBackground"));
        setBackgroundNonSelectionColor(UIManager.getColor("Tree.backgroundNonSelectionColor"));
        setBorderSelectionColor(UIManager.getColor("Tree.borderSelectionColor"));
    }

    
    /**
     * Sets the color the text is drawn with when the node is selected.
    */
    public void setTextSelectionColor(Color newColor) 
    {
        textSelectionColor = newColor;
    }

    /**
     * Returns the color the text is drawn with when the node is selected.
    */
    public Color getTextSelectionColor() 
    {
        return textSelectionColor;
    }

    /**
     * Sets the color the text is drawn with when the node isn't selected.
    */
    public void setTextNonSelectionColor(Color newColor) 
    {
        textNonSelectionColor = newColor;
    }

    /**
     * Returns the color the text is drawn with when the node isn't selected.
    */
    public Color getTextNonSelectionColor() 
    {
        return textNonSelectionColor;
    }

    /**
     * Sets the color to use for the background if node is selected.
    */
    public void setBackgroundSelectionColor(Color newColor) 
    {
        backgroundSelectionColor = newColor;
    }


    /**
     * Returns the color to use for the background if node is selected.
    */
    public Color getBackgroundSelectionColor() 
    {
        return backgroundSelectionColor;
    }

    /**
     * Sets the background color to be used for non selected nodes.
    */
    public void setBackgroundNonSelectionColor(Color newColor) 
    {
        backgroundNonSelectionColor = newColor;
    }

    /**
     * Returns the background color to be used for non selected nodes.
    */
    public Color getBackgroundNonSelectionColor() 
    {
        return backgroundNonSelectionColor;
    }

    /**
     * Sets the color to use for the border.
    */
    public void setBorderSelectionColor(Color newColor) 
    {
        borderSelectionColor = newColor;
    }

    /**
      * Returns the color the border is drawn.
      */
    public Color getBorderSelectionColor() 
    {
        return borderSelectionColor;
    }

    /**
     * Renders the Component.
    */
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, 
                                                  boolean expanded, boolean leaf, int row, 
                                                  boolean hasFocus)
    {
       
        DefaultMutableTreeNode node=(DefaultMutableTreeNode)value;
        Object userObject=node.getUserObject();   
        SRTreeElement te = (SRTreeElement) userObject;
        JLabel label = new JLabel();
        label.setOpaque(true);
        
        
        if(selected)
        {
            label.setForeground(getTextSelectionColor());
            label.setBackground(getBackgroundSelectionColor());
            label.setFont(textSelectionFont);
        }
        else
        {
            label.setForeground(getTextNonSelectionColor());
            label.setBackground(getBackgroundNonSelectionColor());
            label.setFont(textNonSelectionFont);
        }
        label.setToolTipText(te.getToolTipText());
        label.setText(te.getText());
         int signedStatus = te.getSignStatus();
        if (row ==0)label.setIcon(rootIcon);
        else if (row ==1)label.setIcon(headerIcon);
        else 
        {
        
            switch(te.getSRType())
            {
                case jDSRE_ValueType.VT_invalid:
                    break;
                case jDSRE_ValueType.VT_Text:
                    if (signedStatus == 1)label.setIcon(textSignIcon);
                    if (signedStatus == 0)label.setIcon(textUnSignIcon);
                    
                    break;
                case jDSRE_ValueType.VT_Code:
                    if (signedStatus == 1)label.setIcon(codeSignIcon);
                    if (signedStatus == 0)label.setIcon(codeUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Num:
                    if (signedStatus == 1)label.setIcon(numSignIcon);
                    if (signedStatus == 0)label.setIcon(numUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_DateTime:
                    if (signedStatus == 1)label.setIcon(dateTimeSignIcon);
                    if (signedStatus == 0)label.setIcon(dateTimeUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Date:
                    if (signedStatus == 1)label.setIcon(dateSignIcon);
                    if (signedStatus == 0)label.setIcon(dateUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Time:
                    if (signedStatus == 1)label.setIcon(timeSignIcon);
                    if (signedStatus == 0)label.setIcon(timeUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_PName:
                    if (signedStatus == 1)label.setIcon(pnSignIcon);
                    if (signedStatus == 0)label.setIcon(pnUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_SCoord:
                    if (signedStatus == 1)label.setIcon(scSignIcon);
                    if (signedStatus == 0)label.setIcon(scUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_TCoord:
                    if (signedStatus == 1)label.setIcon(tcSignIcon);
                    if (signedStatus == 0)label.setIcon(tcUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Composite:
                    if (signedStatus == 1)label.setIcon(compositeSignIcon);
                    if (signedStatus == 0)label.setIcon(compositeUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Image:
                    if (signedStatus == 1)label.setIcon(imageSignIcon);
                    if (signedStatus == 0)label.setIcon(imageUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Waveform:
                    if (signedStatus == 1)label.setIcon(waveformSignIcon);
                    if (signedStatus == 0)label.setIcon(waveformUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_Container:
                    if (signedStatus == 1)label.setIcon(containerSignIcon);
                    if (signedStatus == 0)label.setIcon(containerUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_UIDRef:
                    if (signedStatus == 1)label.setIcon(uidRefSignIcon);
                    if (signedStatus == 0)label.setIcon(uidRefUnSignIcon);
                    break;
                case jDSRE_ValueType.VT_byReference:
                    if (signedStatus == 1)label.setIcon(refSignIcon);
                    if (signedStatus == 0)label.setIcon(refUnSignIcon);
                    
                    break;
                

            }
        }
        Dimension di=label.getMinimumSize();
        di.width+=16;
        di.height+=1;
        label.setPreferredSize(di);
        
        return label;
    }
    
}

/*
 *  CVS Log
 *  $Log: SREditTreeCellRenderer.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
