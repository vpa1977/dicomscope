/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;


import java.awt.*;
import java.awt.event.*;
import  de.microtherapy.tools.text.document.dicom.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.border.*;
import main.*;
import J2Ci.*;
/**
 * This class contains a class
 * for editing Code Value Content Item in a SR.
 * @see SRGeneralContentItemEditPanel
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SRCodeValueContentItemEditPanel extends SRGeneralContentItemEditPanel
{
    /**
    *  Content 
    */
    SRCodeChangePanel srUnitCodeChangePanel;
    public SRCodeValueContentItemEditPanel(jDSRDocumentTree documentTree,int nodeId)
    {
        super(documentTree,nodeId,jDSRE_ValueType.VT_Code,SRCode.CONTEXT_GROUP_NAME_CODE);
        
        updateContentItem();
    }
    
    /**
    * Updates the value from the jDSRDocumentTree.
    */
    private void updateContentItem()
    {
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        isListenerEnabled= true;
    }
    
    /**
    * Changes the value in the jDSRDocumentTree
    */
    private void changeContentItem()
    {
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        
        isListenerEnabled= true;
        
    }
    /**
    * Initialise and returns  a JComponent containting a GUI for editing the 
    * value of the Content Item. 
    * @return JScrollbar with JTextArea containing  the 
    * value of the Content Item.
    */
    public JComponent initContentItemGUI()
    {
        srUnitCodeChangePanel = new SRCodeChangePanel(documentTree, 
                                                        nodeId, 
                                                        jDSRE_ValueType.VT_Num, 
                                                        SRCode.CONTEXT_GROUP_NAME_CODE, 
                                                        SRCodeChangePanel.CODE);
        JPanel p = new JPanel(new BorderLayout());
        p.setBackground(Color.lightGray);
        JPanel textPanel = new JPanel(new BorderLayout(10,10));
        textPanel.setBorder(new EtchedBorder());
        
        textPanel.add(new JLabel(" "), BorderLayout.NORTH);
        textPanel.add(new JLabel(" "), BorderLayout.SOUTH);
        
        JPanel centerPanel = new JPanel (new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        //Description Label
        //Pateint Name
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridwidth = 1;
        gbc.weightx = 0;
        
        gbc.insets = new Insets(0,0,0,0);
        
        centerPanel.add(new JLabel("Code: " ), gbc);
        
        centerPanel.add(Box.createHorizontalStrut(5));
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        centerPanel.add(srUnitCodeChangePanel, gbc);
        
        
        textPanel.add(centerPanel,BorderLayout.CENTER);
        p.add(textPanel, BorderLayout.NORTH);
        
        
        return p;
    }
    
}
/*
 *  CVS Log
 *  $Log: SRCodeValueContentItemEditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
