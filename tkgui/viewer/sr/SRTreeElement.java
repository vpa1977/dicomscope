/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.sr;

import J2Ci.*;


/**
 * This class represents a tree element with all possible contents.
 *
 * @author Andreas Schroeter
 * @since 30.04.1999
*/
public class SRTreeElement
{
    
    /**
    * SR Root Id;
    */
    public int srNodeId;
    String description="";
    public int srType;
    public int signStatus = 0;
    
    /**
    *
    */
    public String  relationShip = null;
    public int getSignStatus()
    {
        return signStatus;
    }
    public void setSignStatus(int signStatus)
    {
         this.signStatus=signStatus;
    }
    public int getNodeId()
    {
        return srNodeId;
    }
    public int getSRType()
    {
        return srType;
    }
    
    public String name = "";
    
    public SRTreeElement(String name, String description)
    {
        this.name = name;
        this.description = description;
        this.signStatus = signStatus;
        
    }
    public SRTreeElement(   String name, 
                            int srNodeId, 
                            int srType, int signStatus)
    {
        this.name = name;
        this.srType = srType;
        this.srNodeId = srNodeId;
        relationShip = "SR Tree";
        this.signStatus = signStatus;
    }
    public SRTreeElement(   String name, 
                            int srNodeId, 
                            String relationShip, 
                            int srType, int signStatus)
    {
        this.name = name;
        this.srType = srType;
        this.srNodeId = srNodeId;
        this.relationShip = relationShip;
        this.signStatus = signStatus;
    }
    public String getToolTipText()
    {
        if (relationShip != null) return relationShip + " " + name;
        return name;
    }
    public String getText()
    {
        if (relationShip != null) return description + relationShip;
        else return description;
    }
    public String toString()
    {
        if (relationShip != null) return relationShip + " " + name;
        return name;
    }
}

/*
 *  CVS Log
 *  $Log: SRTreeElement.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
