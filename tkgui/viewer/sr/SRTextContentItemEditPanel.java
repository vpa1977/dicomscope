/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;

import main.*;
import J2Ci.*;
/**
 * This class contains a class
 * for editing a Text Content Item in a SR.
 * @see SRGeneralContentItemEditPanel
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SRTextContentItemEditPanel extends SRGeneralContentItemEditPanel
{
    /**
    *  Content 
    */
    private JTextArea text;
    public SRTextContentItemEditPanel(jDSRDocumentTree documentTree,int nodeId)
    {
        super(documentTree,nodeId,jDSRE_ValueType.VT_Text,SRCode.CONTEXT_GROUP_NAME_REPORT_ELEMENT);
        updateContentItem();
    }
    
    /**
    * Updates the value from the jDSRDocumentTree.
    */
    private void updateContentItem()
    {
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        text.setText(documentTree.getCurrentStringValue());
        isListenerEnabled= true;
    }
    
    /**
    * Changes the value in the jDSRDocumentTree
    */
    private void changeContentItem()
    {
        
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        String t = text.getText();
        try
        {
             
            //documentTree.setCurrentStringValue(StringUtil.convertToISOLatin1(t));
            documentTree.setCurrentStringValue(t);
        }
        catch(Exception e)
        {
            documentTree.setCurrentStringValue(t);
            System.err.println(e);
        }
        isListenerEnabled= true;
        
    }
    /**
    * Initialise and returns  a JComponent containting a GUI for editing the 
    * value of the Content Item. 
    * @return JScrollbar with JTextArea containing  the 
    * value of the Content Item.
    */
    public JComponent initContentItemGUI()
    {
        text = new JTextArea();
        text.setLineWrap(true);
        text.setWrapStyleWord(true);
        text.addFocusListener(new FocusListener()
        {
            public void focusGained(FocusEvent e)
            {}


            public void focusLost(FocusEvent e)
            {
                changeContentItem();
            }
        });
        
        return new JScrollPane(text);
    }
    
}
/*
 *  CVS Log
 *  $Log: SRTextContentItemEditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
