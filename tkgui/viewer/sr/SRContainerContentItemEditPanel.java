/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;


import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.border.*;
import main.*;
import J2Ci.*;
/**
 * This class contains a class
 * for editing a Text Content Item in a SR.
 * @see SRGeneralContentItemEditPanel
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SRContainerContentItemEditPanel extends SRGeneralContentItemEditPanel
{
    /**
    *  Content 
    */
    
    private JRadioButton separateRadioButton;
    private JRadioButton continuousRadioButton;
    boolean isRoot = false;
    public SRContainerContentItemEditPanel(jDSRDocumentTree documentTree,int nodeId)
    {
        super(documentTree,nodeId,jDSRE_ValueType.VT_Container,SRCode.CONTEXT_GROUP_NAME_SECTION_HEADING);
        documentTree.gotoNode(nodeId);
        if (documentTree.getCurrentLevel()==1)
        {
            setContextGroupName(SRCode.CONTEXT_GROUP_NAME_DOCUMENT_TITLE);
        }
        updateContentItem();
    }
    
    /**
    * Updates the value from the jDSRDocumentTree.
    */
    private void updateContentItem()
    {
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        int result = documentTree.getCurrentContinuityOfContentFlag();
        if (result == jDSRE_ContinuityOfContent.COC_Continuous)
        {
             continuousRadioButton.setSelected(true);
        }
        else if (result == jDSRE_ContinuityOfContent.COC_Separate)
        {
            separateRadioButton.setSelected(true);
        }
        else System.err.println("invalid Continuity Of Content: " + jDSRE_ContinuityOfContent.getName(result));
        isListenerEnabled= true;
    }
    
    /**
    * Changes the value in the jDSRDocumentTree
    */
    private void changeContentItem()
    {
        
        isListenerEnabled= false;
        documentTree.gotoNode(getNodeId());
        if (separateRadioButton.isSelected())documentTree.setCurrentContinuityOfContentFlag(jDSRE_ContinuityOfContent.COC_Separate);
        else if (continuousRadioButton.isSelected())documentTree.setCurrentContinuityOfContentFlag(jDSRE_ContinuityOfContent.COC_Continuous);
        isListenerEnabled= true;
        
    }
    /**
    * Initialise and returns  a JComponent containting a GUI for editing the 
    * value of the Content Item. 
    * @return JScrollbar with JTextArea containing  the 
    * value of the Content Item.
    */
    public JComponent initContentItemGUI()
    {
        ButtonGroup bg= new ButtonGroup();
        separateRadioButton = new JRadioButton("Separate");
        continuousRadioButton = new JRadioButton("Continuous");
        bg.add(separateRadioButton);
        bg.add(continuousRadioButton);
        JPanel p = new JPanel(new BorderLayout());
        p.setBackground(Color.lightGray);
        JPanel radioPanel = new JPanel(new GridLayout(2,1));
        radioPanel.setBorder(new TitledBorder("Continuity of content" ));
        radioPanel.add(separateRadioButton);
        radioPanel.add(continuousRadioButton);
        p.add(radioPanel, BorderLayout.NORTH);
        
        separateRadioButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                changeContentItem();
            }
        });
        continuousRadioButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                changeContentItem();
            }
        });
        
        return p;
    }
    
}
/*
 *  CVS Log
 *  $Log: SRContainerContentItemEditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
