/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;

import java.util.*;
import java.net.*;
import  de.microtherapy.tools.text.document.dicom.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.tree.*;

import main.*;
import J2Ci.*;

/**
 * This class contains an abstract class
 * for editing a Content Item in a SR.
 * Each Content Item contains of a Concept Name
 * and an optional ObservationDateTime.
 * This class is a JPanel with a BorderLayout.
 * In the north the GUI for visualising and changing the
 * Concept Name is placed. In the south the
 * field for the Observation Date Time is placed
 * <p>
 * In the constructor the JPanel is build. Each extending class 
 * have to implement the {@link initContentItemGUI} method. This method is used
 * for placing the returned component in the center of this panel.
 *
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public abstract class SRGeneralContentItemEditPanel extends JPanel 
{
    
    /**
    * Enable/disable the Listener
    */
    protected boolean isListenerEnabled = false;
    
   
    /**
    * Contains the documentTree.
    */ 
    protected jDSRDocumentTree documentTree;
    
    
    SRCodeChangePanel srConceptNamePanel;
    /**
    * Contains the Observation Date Time
    */
    private JLabel observationDateTimeLabel= new JLabel();
    
    
    
    /**
    * The node id oft the Content Item. This
    * value will be used for changing the value in node of the
    * jDSRDocumentTree 
    */
    protected int nodeId;
    /**
    * The identification of the Content Item  type.
    */
    protected int type;
    
    /**
    * The name of the context group 
    */
    private String contextGroupName;
    
    /**
     * Constructor. Be aware that initContentItemGUI()
     * will be called for placing the returned JComponent
     * in the center of this panel
     * @param documentTree jDSRDocumentTree
     * @param nodeId Contains id of the node
     * @param type The identification of the Content Item  type.
     * @param contextGroupName The name of the context group 
    */
    public SRGeneralContentItemEditPanel( jDSRDocumentTree documentTree, 
                                        int nodeId, 
                                        int type, 
                                        String contextGroupName)
    {
        this.documentTree = documentTree;
        this.nodeId = nodeId;
        this.type = type;
        this.contextGroupName = contextGroupName;
        
        srConceptNamePanel = new SRCodeChangePanel(documentTree, 
                                                    nodeId, type, contextGroupName, SRCodeChangePanel.CONCEPT_NAME);
        //Sets the type name of the Content Item
        setBorder(new TitledBorder(jDSRE_ValueType.getVTName(type)));
        
        setLayout(new BorderLayout(5,5));
        add(srConceptNamePanel, BorderLayout.NORTH);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(new TitledBorder("Value"));
        mainPanel.add(initContentItemGUI());
        add(mainPanel, BorderLayout.CENTER);
        add(getObservationDateTimePanel(), BorderLayout.SOUTH);
        
        update();
        
        
    }
    /**
    * Returns the node id oft the Content Item. This
    * value will be used for changing the value in node of the
    * jDSRDocumentTree 
    * @return Returns the node id of the Content Item.
    */
    public int getNodeId()
    {
        return nodeId;
    }
    
    
    
    /**
    * Return The identification of the Content Item  type.
    * @return The identification of the Content Item type.
    */
    public int getType()
    {
        return type;
    }
    
    /**
    * Return the name of the Context Group.
    * @Return the name of the Context Group.
    */
    public String getContextGroupName()
    {
        return contextGroupName;
    }
    /**
    * Sets the name of the Context Group.
    * @param the name of the Context Group.
    */
    public void setContextGroupName(String contextGroupName)
    {
         this.contextGroupName = contextGroupName;
        srConceptNamePanel.setContextGroupName(contextGroupName);
        
    }
    
    
    /**
    * Initialise and returns  a Component containting a GUI for editing the 
    * value of a Content Item. Each Content Type has to 
    * build his own GUI.
    * @return Component containting a GUI for editing the 
    * value of a Content Item.
    */
    public abstract JComponent initContentItemGUI();
    
    
    /**
    * Updates the codeValueBox and the observationLabelthe srEditComponent with
    * values from the node
    * <p>
    * For updating the srEditComponet the update() 
    * method of this object will be called
    */
    private void update()
    {
        //diable the listeners
        isListenerEnabled = false;
        
        
        srConceptNamePanel.updateCode();
        String observationDateTime = documentTree.getCurrentObservationDateTime();
        if (observationDateTimeLabel != null) observationDateTimeLabel.setText(observationDateTime);
        
        
        isListenerEnabled= true;
    }
    
    
    
          
    /***
    * Creates and returns the GUI for the observation date and time
    * @return the GUI for the observation date and time
    */
    private JPanel getObservationDateTimePanel()
    {
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        p.add(new JLabel("Observation Date Time: "));
        p.add(observationDateTimeLabel);
        return p;
    }
}
/*
 *  CVS Log
 *  $Log: SRGeneralContentItemEditPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
