/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.sr;

import java.util.*;
import java.net.*;
import  de.microtherapy.tools.text.document.dicom.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import javax.swing.tree.*;

import main.*;
import J2Ci.*;

/**
 *
 * @author Klaus Kleber
 * @since 20.08.1999
 */
public class SRCodeEditDialog extends JDialog
{
             
        /**
        *  If true the SRCode should inserted
        */
        private boolean insertMode = true;
             
        /**
        * Text for the Code Value
        */
        private JTextField codeValueText = new JTextField();
        /**
        * Text for the Code Meaning
        */
        private JTextField codeMeaningText= new JTextField();
        /**
        * Text for the Coding Scheme Designator
        */
        private JTextField codingSchemeDesignatorText= new JTextField();
        /**
        * Text for the Coding Scheme Version
        */
        private JTextField   codingSchemeVersionText= new JTextField();
             
        /**
        * Text for the Code Value
        */
        private JLabel contextGroupLabel ;
             
        private Component parent;
             
    public boolean cancelAction = true;
            
    public SRCodeEditDialog(  Component parent, 
                            String contextName
                            )
                                        
    {
        this(parent, true, contextName, null,null,null,null);
    }
    public SRCodeEditDialog(  Component parent, 
                            boolean insertMode,
                            String contextName,
                            String codeValue,
                            String codeMeaning,
                            String codingSchemeDesignator,
                            String codingSchemeVersion)
                                        
    {
        super();
        setModal(true);
        this.parent = parent;
        this.insertMode = insertMode;
                
        setLocationRelativeTo(parent);
        codeMeaningText.setDocument(new LODocument());
        codeValueText.setDocument(new SHDocument());
        codingSchemeDesignatorText.setDocument(new SHDocument());
        codingSchemeVersionText.setDocument(new SHDocument());
        if (insertMode == false) 
        {
            setTitle("Code Info");
        codeMeaningText.setEditable(false);
        codeValueText.setEditable(false);
        codingSchemeDesignatorText.setEditable(false);
        codingSchemeVersionText.setEditable(false);
                
        }
        else setTitle("Insert Code");
                    
        codeMeaningText.setText(codeMeaning);
        codeValueText.setText(codeValue);
        codingSchemeDesignatorText.setText(codingSchemeDesignator);
        codingSchemeVersionText.setText(codingSchemeVersion);
        if(contextName == null) contextName ="";
                
                
        getContentPane().setLayout(new BorderLayout(5,5));
                
        JButton okButton = new JButton("Ok");
	    okButton.setToolTipText ("OK");
        okButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                okAction();
            }
        });
                
                
                
        JButton cancelButton = new JButton("Cancel");
	    cancelButton.setToolTipText ("Cancel");
        cancelButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                setVisible(false);
            }
        });
        JPanel centerPanel = new JPanel(new GridLayout(4,2));
        centerPanel.setBorder(new TitledBorder(contextName));
                
        centerPanel.add(new JLabel("Code Value"));
        centerPanel.add(codeValueText);
                
        centerPanel.add(new JLabel("Code Meaning"));
        centerPanel.add(codeMeaningText);
                
        centerPanel.add(new JLabel("Scheme Designator"));
        centerPanel.add(codingSchemeDesignatorText);
                
        centerPanel.add(new JLabel("Scheme Version"));
        centerPanel.add(codingSchemeVersionText);
                
                
                
        getContentPane().add(centerPanel, BorderLayout.CENTER);
                
        JPanel bPanel = new JPanel();
        bPanel.add(okButton);
                
        if (insertMode)bPanel.add(cancelButton);
                
        getContentPane().add(bPanel, BorderLayout.SOUTH);
                
        pack();
    }
    private void okAction()
    {
        if (!insertMode) setVisible(false);
        else if (check())
        {
            cancelAction = false;
            setVisible(false);
        }
                
    }
            
    private boolean check()
    {
                
        if (codeValueText.getText() == null||codeValueText.getText().equals("")) 
        {
            JOptionPane.showMessageDialog(this,"Please insert Code Value");
            return false;
        }
        if (codeMeaningText.getText() == null|codeMeaningText.getText().equals("")) 
        {
            JOptionPane.showMessageDialog(this,"Please insert Code Meaning");
            return false;
        }
        if (codingSchemeDesignatorText.getText() == null||codingSchemeDesignatorText.getText().equals("")) 
        {
            JOptionPane.showMessageDialog(this,"Please insert Scheme Designator");
            return false;
        }
        if (codingSchemeVersionText.getText() == null)codingSchemeVersionText.setText(""); 
        return true;
    }
            
            
    public void setVisible(boolean flag)
    {
        if (flag == true) 
        {
            Point p =parent.getLocationOnScreen();
            setLocation(new Point(p.x-100, p.y+50));
        }
        super.setVisible(flag);
    }
    public String getCodeValue()
    {
        return codeValueText.getText();
    }
    public String getCodeMeaning()
    {
        return codeMeaningText.getText();
    }
    public String getSchemeDesignator()
    {
        return codingSchemeDesignatorText.getText();
    }
    public String getSchemeVersion()
    {
        return codingSchemeVersionText.getText();
    }
            
   
    
}
/*
 *  CVS Log
 *  $Log: SRCodeEditDialog.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
