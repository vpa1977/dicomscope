/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2003/09/08 10:17:26 $
 *  Revision :    $Revision: 1.2 $
 *  State:        $State: Exp $
 */
package viewer.presentation;

/**
 * Contains a DICOM display area.
 */
public class DisplayArea{
    
    
    
    public  int tlhc_x;
    public int tlhc_y;
    public int brhc_x;
    public int brhc_y;
    /**
     * @param tlhc_x
     * @param tlhc_y
     * @param brhc_x
     * @param brhc_y
     * @param rotation
     * @param flipped  */
    public DisplayArea( int tlhc_x, int tlhc_y, int brhc_x, int brhc_y) {
        
        this.tlhc_x = tlhc_x;
        this.tlhc_y = tlhc_y;
        this.brhc_x = brhc_x;
        this.brhc_y = brhc_y;
  
    }
 
   
    public String toString() {
        return new String("TLHC: = " + tlhc_x + "/" + tlhc_y+ ", BRHC: = " + brhc_x + "/" + brhc_y);
    }
    /**
     * @param zoom  */
    public void setZoom(double zoom) {
        tlhc_x = (int)(zoom*tlhc_x);
        tlhc_y = (int)(zoom*tlhc_y);
        brhc_y = (int)(zoom*brhc_y);
        brhc_x = (int)(zoom*brhc_x);
    }
    public int getDimensionX() {
        return brhc_x-tlhc_x;
    }
    public int getDimensionY() {
        return brhc_y-tlhc_y;
    }
    
    public boolean equals(Object  o) {
        if ( o instanceof DisplayArea) {
            DisplayArea displayedArea=(DisplayArea) o;
            
            if (displayedArea.tlhc_x==tlhc_x &&displayedArea.tlhc_y==tlhc_y
            &&displayedArea.brhc_x==brhc_x&&displayedArea.brhc_y==brhc_y){
                return true;
            } else {
                return false;
            }
        } else {
            throw new ClassCastException();
        }
    }
    
    public int getTlhcX() {
        return tlhc_x;
    }
    public int getTlhcY() {
        return tlhc_y;
    }
    public int getBrhcX() {
        return brhc_x;
    }
    public int getBrhcY() {
        return brhc_y;
    }
    
}
/*
 *  CVS Log
 *  $Log: DisplayArea.java,v $
 *  Revision 1.2  2003/09/08 10:17:26  kleber
 *  Bugfix: The Displayed Area is defined after the spatial transformation.
 *
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 */
