/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.presentation;

import J2Ci.*;

import java.awt.*;
import java.util.*;
import java.awt.geom.*;

/**
 * This class is the superclass for all objects which can be in 
 * a PresentationStateGraphicLayer. This Class in not ready yet. Especially 
 * the overlays and the curves (not yet implemented) are not based on this class.
 * maybe a next version will implement this.
 * This class and the derived classes implementing only the GUI-based methods.
 * The data are in the c++ interface jDVPresentationState abvailable. 
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see PresentationStateAnnotatonObject
 * @see PresentationStateGraphicObject
 * @see PresentationStateTextObject
 */

public abstract class PresentationStateLayerObject
{

    /**
     * Contains the index of the object in the jDVPersentationState object.
     * 
     * @since 30.04.1999
     */
    
    int index;
    
    /**
    * Contains the current transformation which must be applies if you 
    * draw the object.
    *
    * @since 30.04.1999
    */
    AffineTransform  aff = null;
    
    /**
    * If true means that the obejct should be drawn in every case. If false
    * there will not be drawn in the drawActive() methode.
    *
    * @since 30.04.1999
    */
    boolean active = true;
    

    /**
     * Constructs a new object.
     * 
     * @since 30.04.1999
     */
    public PresentationStateLayerObject()
    {
    }
    
    /**
     * Check if this object contains the specified point. 
     * 
     * @return true if this object contains the specified point.
     * @since 30.04.1999
     */
    public abstract boolean contains(Point2D.Float point);
    
    /**
     * Deletes the object. This method must be called for deleting.
     *
     * @since 30.04.1999
    */
    public abstract void deleteAll();
    
    /**
     * Returns a String representing this object.
     *
     * @return A String representing this object.
     * @since 30.04.1999
    */
    public abstract String getListText();
    
    /**
    * Moves this object analog to the specified points.
    * The first point is the start point and the secound point is the point where the
    * first point is moved to. The PresentationStateGraphicObject will be moved 
    * analog to the result vector.
    *
    * @param moveTo Start moving point.
    * @param point Stop moving point.
    * @since 30.04.1999
    */
    public abstract void moveTo(Point2D.Float moveTo,Point2D.Float point);
    
    /**
    * Draws this object after the transformation aff in the specified 
    * Graphics2D context.
    *
    * @param g2 The context in which this object will be drawn.
    * @param aff Contains the appling transformation.
    * @since 30.04.1999
    */
    public  abstract void draw(Graphics2D g2,AffineTransform aff, boolean scale);
    
    /**
    * Marked the object with bounding Rectangles and draws 
    * the rectangles in the specified Graphics2D context.
    *
    * @param g2 The context in which this object will be merked.
    * @param aff Contains the applying transformation.
    * @since 30.04.1999
    */
    public abstract void drawBounding(Graphics2D g2,AffineTransform aff);
   
   
   /**
   * Draws the object with the specified 
   * transformation in the specified Graphics2d context if the object is
   * active.
   * @param g2 The Graphics2D object in which the object will be drawn.
   * @param aff Contains the transformations which should be applied to the object before drawing..
   */
    public void drawActive(Graphics2D g2,AffineTransform aff, boolean scale)
    {
        if (isActive()) draw(g2,aff, scale);
    }
    
    
    /**
    * Marked the object with Rectangles and draws Rectangles and 
    * this object in the specified Graphics2D context.
    *
    * @param g2 The context in which this object will be merked.
    * @param aff Contains the applying transformation.
    * @since 30.04.1999
    */
    public void drawMarked(Graphics2D g2,AffineTransform aff)
    {
       
        if ((g2!=null) )
        {
            draw(g2,aff, false);
            drawBounding(g2,aff);
        }
    }
    
    /**
    * Returns a String representing this object
    *
    * @return A String representing this object
    * @since 30.04.1999
    */
    public abstract String getInfo();
    
    
    /**
    * Sets the active value.
    *
    * @return true if active.
    * @since 30.04.1999
    */
    public boolean isActive()
    {
        return active;
    }
    
    /**
    * Sets the active value.
    *
    * @param active The new active value.
    * @since 30.04.1999
    */
    public void setActive(boolean active)
    {
        this.active = active;
    }
 
    
    /**
    * Transforms the specified displayPoint with the inverse 
    * transformation of aff.
    * 
    * @param displayPoint Contains the transforming point.
    * @param aff Specifies the inverse transformation.
    * @return The inverse transformation of the displayPoint with the transformation aff. 
    * @since 30.04.1999
    */
    public Point2D.Float getInverseTransformedPoint(Point2D.Float displayPoint,AffineTransform transAff)
    {
        Point2D.Float dstPoint = new Point2D.Float();
        try
        {
            transAff.inverseTransform(displayPoint,dstPoint);
        }
        catch(NoninvertibleTransformException e)
        {
            System.out.println("NoninvertibleTransformException: " + e);
        }
        return dstPoint;
    }
    /**
    * Transforms the specified displayPoint with the inverse 
    * transformation of aff.
    * 
    * @param displayPoint Contains the transforming point.
    * @param aff Specifies the inverse transformation.
    * @return The inverse transformation of the displayPoint with the transformation aff. 
    * @since 30.04.1999
    */
    public Point2D.Double getInverseTransformedPoint(Point2D.Double displayPoint,AffineTransform transAff)
    {
        Point2D.Double dstPoint = new Point2D.Double();
        try
        {
            transAff.inverseTransform(displayPoint,dstPoint);
        }
        catch(NoninvertibleTransformException e)
        {
            System.out.println("NoninvertibleTransformException: " + e);
        }
        return dstPoint;
    }
    
    
    /**
    * Gets the index of this object.
    *
    * @return The index of this object.
    * @since 30.04.1999
    */
    public int getIndex()
    {
        return index;
    }
    
    /**
    * Sets the index of this object. The index of the object must be the same as the
    * index in the jDVPresentationState object.
    *
    * @since 30.04.1999
    */
    public void setIndex(int index)
    {
        this.index = index;
    }
    
} 

/*
 *  CVS Log
 *  $Log: PresentationStateLayerObject.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
