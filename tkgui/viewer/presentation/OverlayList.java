/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.presentation;
import J2Ci.*;
import java.awt.*;
import java.util.*;
import java.awt.geom.*;

/**
 * This class manages the overlays of a presentation state. 
 * The data of the overlays are 
 * capsulated in a jDVPrStateParam_GetOverlayData object. 
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see J2Ci.jDVPrStateParam_GetOverlayData
 * @see PresentationStateOverlayObject
 */
public  class OverlayList
{
   //int index;
    /**
     * Contains the current presentation state wich contains all overlays
     * 
     * @since 30.04.1999
     */
   public jDVPresentationState ps;
   
    /**
     * Constructs a new object.
     * 
     * @param ps Contains the current jDVPresentationState
     * @since 30.04.1999
     */
   public OverlayList(jDVPresentationState ps)
   {
        this.ps = ps;
        
        //Debug information
        /*
        System.out.println("getNumberOfOverlaysInImage: " + ps.getNumberOfOverlaysInImage());
        System.out.println("getNumberOfOverlaysInPresentationState: " + ps.getNumberOfOverlaysInPresentationState());
        
        for (int i = 0; i < ps.getNumberOfOverlaysInImage(); i++)
        {
            System.out.println("i: " + i + "Layer: "  + ps.getOverlayInImageActivationLayer(i));
        }
        for (int i = 0; i < ps.getNumberOfOverlaysInPresentationState(); i++)
        {
            System.out.println("i: " + i +"Layer: "  + ps.getOverlayInPresentationStateActivationLayer(i));
        }
        */ 
   }
    /**
    * Checks if the current jDVPresentationState have an overlay.
    *
    * @return True if an overlay exist.
    * @since 30.04.1999
    */
   public boolean haveOverlays()
   {
        if ((ps.getNumberOfOverlaysInImage()>= 0 )||(ps.getNumberOfOverlaysInPresentationState()>= 0)) return true;
        else return false;
   }
    
    /**
    * Draws all activeate overlay off one layer in the specified Graphics2D context. 
    * The layer is specified with his index in the current jDVPresentationState.
    * Before drawing the overlay the specified transformations will be applied.
    *
    * @param g2 Contains the Graphics2D context.
    * @param aff Contains the applying transformation. 
    * @param layerIndex Contains the index of the layers in the jDVPresentationState object.
    * @since 30.04.1999
    */
    public void draw(Graphics2D g2, AffineTransform aff, int layerIndex, int bits)
    {
        for (int i = 0; i < ps.getNumberOfActiveOverlays(layerIndex); i++)
        {
                PresentationStateOverlayObject overlayObject = new PresentationStateOverlayObject(ps,layerIndex, i, bits);
                overlayObject.draw(g2,aff);
        }
        
    }
    
    
    /**
    * Gets a textual representation of an image overlay.
    *
    * @param imageIndex Contains index off the image overlay in the jDVPresentationState.
    * @since 30.04.1999
    * @see J2Ci.jDVPresentationState#getOverlayInImageGroup
    */
    public String getImageListText(int imageIndex)
    {
        return new String("image overlay: " +getRepeatingGroup(ps.getOverlayInImageGroup(imageIndex)) + ", " +ps.getOverlayInImageLabel(imageIndex));
    }
    
    
    /**
    * Gets a textual representation of an overlay in the presentation state.
    *
    * @param imageIndex Contains index off the image overlay in the jDVPresentationState.
    * @since 30.04.1999
    * @see J2Ci.jDVPresentationState#getOverlayInPresentationStateGroup
    */
    public String getPresentationStateListText(int imageIndex)
    {
        return new String("prs. overlay: " +getRepeatingGroup(ps.getOverlayInPresentationStateGroup(imageIndex)) + ", " +ps.getOverlayInImageLabel(imageIndex));
    }
    
    /**
    * Gets a textual representation of an overlay 
    * in the specified layer
    *
    * @param layerIndex Contains the index off the layer in the jDVPresentationState
    * @param imageIndex Contains index off the image overlay in specified layer.
    * @since 30.04.1999
    * @see J2Ci.jDVPresentationState#getActiveOverlayGroup
    */
    public String getListText(int layerIndex, int imageIndex)
    {
        String returnString = new String ("Overlay: "+ getRepeatingGroup(ps.getActiveOverlayGroup(layerIndex,imageIndex)) + ", " + ps.getActiveOverlayLabel(layerIndex,imageIndex));
        return returnString;
    }
    
    /**
    * Gets a textual representation of repeating group. 
    *
    * @param repeatingGroupNumber Value of the reapeating group.
    * @since 30.04.1999
    */
    public String getRepeatingGroup(int repeatingGroupNumber)
    {
        if (repeatingGroupNumber == 24576) return new String("6000");
        if (repeatingGroupNumber == 24578) return new String("6002");
        if (repeatingGroupNumber == 24580) return new String("6004");
        if (repeatingGroupNumber == 24582) return new String("6006");
        if (repeatingGroupNumber == 24584) return new String("6008");
        if (repeatingGroupNumber == 24586) return new String("600A");
        return new Integer(repeatingGroupNumber).toString();
    }
    
    
    /**
    * Activates an daektivated overlay in the specified layer. The 
    * imageIndex is the index off the deactivated overlay. 
    *
    * @param layerIndex Contains the index off the layer in the jDVPresentationState
    * @param imageIndex Contains index off the deaktivated overlay.
    * @since 30.04.1999
    * @see J2Ci.jDVPresentationState#activateOverlayInImage
    * @see J2Ci.jDVPresentationState#activateOverlayInPresentationState
    */
    public void activateOverlay(int imageIndex, int layerIndex)
    {
        int searchIndex = -1;
        for (int i = 0; i < ps.getNumberOfOverlaysInImage(); i++)
        {
             
             if (ps.getOverlayInImageActivationLayer(i) == -1)
            { 
               searchIndex++;
                if (searchIndex == imageIndex)
                {
                    int status = ps.activateOverlayInImage(layerIndex,i);
                    return;
                }
            }           
        }
        
        //overlay is overlay in presentation state
        for (int i = 0; i < ps.getNumberOfOverlaysInPresentationState(); i++)
        {
            if (ps.getOverlayInPresentationStateActivationLayer(i) == -1)
            {
                searchIndex++;
                if (searchIndex == imageIndex)
                {
                    int status = ps.activateOverlayInPresentationState(layerIndex,i);
                        return;
                }
                    
            }
                            
        }
        
    }
    
} 



/*
 *  CVS Log
 *  $Log: OverlayList.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
