/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
 */
package viewer.presentation;

import junit.framework.*;

import java.awt.*; // list of common used packages
import java.awt.event.*;

import java.io.*;
import java.io.ByteArrayInputStream;

import java.util.*;

import javax.swing.*;


/**
 *  JUnit TestSuite for SimpleTest
 *  <BR>
 *     Start main to run testcases or
 *      execute junit.swingui.TestRunner.run( SimpleTest.class )
 *      to execute visual test.
 *
 *
 * <DL>
 *        <DT><B>Revision:</B></DT>
 *        <DD>$Revision: 1.2 $</DD>
 *        <DT><B>Date:</B></DT>
 *        <DD>$Date: 2002/11/29 17:31:40 $</DD>
 * </DL>
 *
 * @author  NAME
 * @version $Revision: 1.2 $
 */
public class DisplayAreaTest extends TestCase {
    //////////////////////////////////////////
    //  F I X T U T R E  T E S T   S E T S
    //  defines the test-sets
    //  used by every TestCase
    //////////////////////////////////////////
    //  TestSet-Data
    //  TestObject my_test_object_a = null;
    //  TestObject my_test_object_b = null;
    public DisplayAreaTest(java.lang.String testName) {
        super(testName);
    }
    
    /**
     *  initializes the testset.
     *  This method is being executed before
     *  any TestCase (testMethod) is being called.
     */
    public void setUp() {
    }
    
    /**
     *  Deinitializes the testset.
     *  This method is being executed before
     *  any TestCase (testMethod) is being called.
     */
    public void tearDown() {
        // my_test_object_a.dispose();
        // my_test_object_b.dispose();
    }
    
    public static void main(java.lang.String[] args) {
        junit.textui.TestRunner.run(suite());
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(DisplayAreaTest.class);
        return suite;
    }
    
 
}


/*
 * $Log:  $
 
 */
