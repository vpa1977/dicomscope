/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;

import viewer.gui.*;
import viewer.presentation.*;
import viewer.paint.*;
import viewer.main.*;
import javax.swing.*;

/**
 * This class handles the MouseEvents for creating a new Annotation
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see GUI.ImageCanvas
 * @see ImageCanvasMouseEventListener
 */
public  class ImageCanvasNewListener extends ImageCanvasMouseEventListener
{
	
	
	
	/**
	 * Construct an EventImageCanvas.
	 * 
	 * @param screenImageHandler Contains the current ScreenImageHandler.
	 * @since 30.04.1999
	 */
	public ImageCanvasNewListener(ScreenImageHandler screenImageHandler)
	{
	    super(screenImageHandler);
        screenImageHandler.imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
    }

    /**
     * Handles the mouse move action. If the mouse is moving the new Annotation must be paint.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseMoved(MouseEvent e)
    {
        System.out.println("mouseMoved");
        screenImageHandler.paintStructure.setPaintPoint(new Point2D.Float((float)(e.getX()),(float)( e.getY())));
    }

    /**
     * Handles the mouse released action. If the mouse will be released the new Annotation gets a new point.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
    public void mouseReleased(MouseEvent e)
    {
        System.out.println("mouseReleased");
         //Popuptrigger
         showPopup(e); 
        if (e.getModifiers() == MouseEvent.BUTTON1_MASK)
        {
            screenImageHandler.setNewPoint(new Point2D.Float(e.getX(), e.getY()));
        }
        
        
    }

   /**
    * Shows the PopupMenu for creating a new Anotation.
    * 
    * @param e
    * @since 30.04.1999
    */
    
   void handlePopup(MouseEvent e)
   {
        JPopupMenu m= new JPopupMenu();
        //create the itemes for a PolylineObject
        if (screenImageHandler.paintStructure.getCurrentPaintObject() instanceof PolylineObject)
        {
               
            m.add(screenImageHandler.getAction("deleteNew"));
            m.add(screenImageHandler.getAction("close"));
            m.add(screenImageHandler.getAction("stop"));
        }
        //create the itemes for a Line2DObject
        else if (screenImageHandler.paintStructure.getCurrentPaintObject() instanceof Line2DObject)
        {
            m.add(screenImageHandler.getAction("deleteNew"));
        }
        //create the itemes for a InterpolatedObject
        if (screenImageHandler.paintStructure.getCurrentPaintObject() instanceof InterpolatedObject)
        {
               
            m.add(screenImageHandler.getAction("deleteNew"));
            //m.add(screenImageHandler.getAction("close"));
            
            m.add(screenImageHandler.getAction("stop"));
        }
        screenImageHandler.popup = m;
        
        screenImageHandler.popup.show( screenImageHandler.getImageCanvas(),e.getX(),e.getY());
   }
}

/*
 *  CVS Log
 *  $Log: ImageCanvasNewListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
