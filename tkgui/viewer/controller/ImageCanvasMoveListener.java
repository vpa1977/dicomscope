/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;
import main.*;
import viewer.gui.*;
import viewer.main.*;
import viewer.presentation.*;

/**
 * This class handles the MouseEvents for moving a PresentationStateGraphicObject.
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see GUI.ImageCanvas
 * @see ImageCanvasMouseEventListener
 */
public  class ImageCanvasMoveListener extends ImageCanvasMouseEventListener
{
	
	public static int number = 0;
	
	/**
	 * Construct an EventImageCanvas.
	 * 
	 * @param screenImageHandler Contains the cruuent ScreenImageHandler
	 * @since 30.04.1999
	 */
	public ImageCanvasMoveListener(ScreenImageHandler screenImageHandler)
	{
	    super(screenImageHandler);
	    number++;
	    screenImageHandler.imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            Controller.instance().fireStatus(new StatusLineEvent(this,StatusLineEvent.SET_DES,DSComponentType.VIEWER,"Move Graphic Annotation"));
            
   }

    /**
     * Moves the PresentationStateGraphicObject.
     * 
     * @param e MouseEvent
     * @since 30.04.1999
     */
	   
    public void mouseMoved(MouseEvent e)
    {
        
        screenImageHandler.paintStructure.movePresentationStateGraphicObject(new Point2D.Float((float)(e.getX()),(float)( e.getY())));
    }

    /**
    * Overwrites the function form the superclass.
    * 
    * @param e MouseEvent
    * @since 30.04.1999
     */
    
    public void mouseDragged(MouseEvent e)
    {
        
        System.out.println("ImageCanvasMoveListener: mouseDragged");
        if ((e.getModifiers() & InputEvent.BUTTON3_MASK) ==InputEvent.BUTTON3_MASK) 
        {
            Point newPoint = e.getPoint();
            screenImageHandler.setNewWindow(windowPoint, newPoint,true);
            windowPoint = newPoint;
        }
        
    }

	/**
	 * Stops moving the PresentationStateGraphicObject.
	 *
	 * @param e MouseEvent
	 * @since 30.04.1999
	 */
	
    public void mousePressed(MouseEvent e)
    {
        screenImageHandler.stopMoveAnnotation();
    }


   /**
    * Overwrites the function from the superclass.
    * 
    * @param e MouseEvent
    * @since 30.04.1999
    */
    
   void handlePopup(MouseEvent e)
   {
   }
}

/*
 *  CVS Log
 *  $Log: ImageCanvasMoveListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
