/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package viewer.controller;

import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;

import viewer.gui.*;
import viewer.main.*;
import viewer.presentation.*;
import javax.swing.*;

/**
 * This class handles the MouseEvents for editing a PresentationStateLayerObject.
 * 
 * @author Klaus Kleber
 * @since 30.04.1999
 * @see GUI.ImageCanvas
 * @see ImageCanvasMouseEventListener
 */
public  class ImageCanvasEditListener extends ImageCanvasMouseEventListener
{
	
	
	
	/**
	 * Construct a ImageCanvasEditListener.
	 * 
	 * @param screenImageHandler Contains the current ScreenImageHandler.
	 * @since 30.04.1999
	 */
	public ImageCanvasEditListener(ScreenImageHandler screenImageHandler)
	{
	    super(screenImageHandler);
	   
	    
	    screenImageHandler.imageCanvas.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
   }

	/**
	 * Handles the mouse pressed action. If one PresentationStateLayerObject 
	 * contains the pressed point, these object will be marked.
	 * 
	 * @param e MouseEvent.
	 * @since 30.04.1999
	 */
	public void mousePressed(MouseEvent e)
	{
	   screenImageHandler.setClick(new Point2D.Float((float)(e.getX()),(float)( e.getY())));
         //PopUpTrigger
         showPopup(e); 
    }

    
   /**
    * Shows the PopupMenu for editing a PresentationStateGraphicObject.
    * 
    * @param e MouseEvent.
    * @since 30.04.1999
    */
   void handlePopup(MouseEvent e)
   {
        JPopupMenu m= new JPopupMenu();
        PresentationStateLayerObject handleObject = screenImageHandler.presentationStateGraphicsHandler.getCurrentLayerObject();
        if (handleObject!= null)
        {
            if (handleObject instanceof PresentationStateGraphicObject)
            {
                PresentationStateGraphicObject grObject = (PresentationStateGraphicObject)handleObject;
                m.add(screenImageHandler.getAction("delete"));
                m.add(screenImageHandler.getAction("move"));
                JCheckBoxMenuItem filled = new JCheckBoxMenuItem("Filled");
                JCheckBoxMenuItem imageRelative = new JCheckBoxMenuItem("Image Relative");
                filled.addActionListener(screenImageHandler.getAction("filled"));
                imageRelative.addActionListener(screenImageHandler.getAction("image relative"));
                if (grObject.getAnnotationUnits()== 0)imageRelative.setSelected(true);
                filled.setSelected(grObject.isfilled());
                m.add(filled);
                m.add(imageRelative);
            }
            
            if (handleObject instanceof PresentationStateTextObject)
            {
                PresentationStateTextObject clickedText= (PresentationStateTextObject)handleObject;
                m.add(screenImageHandler.getAction("delete"));
                m.add(screenImageHandler.getAction("editText"));
                m.addSeparator();
                if (!clickedText.haveBoundingBox() &&clickedText.haveAnchorPoint())
                {
                   m.add(screenImageHandler.getAction("insert bounding box display relative"));
                   m.add(screenImageHandler.getAction("insert bounding box image relative"));
                }
                if (clickedText.haveBoundingBox())
                {
                    m.add(screenImageHandler.getAction("move bounding box"));
                }
                if (clickedText.haveBoundingBox() &&clickedText.haveAnchorPoint())
                {
                    m.add(screenImageHandler.getAction("remove bounding box"));
                }
                
                //change type of bounding box form
                if (clickedText.haveBoundingBox() &&(clickedText.getBoundingBoxAnnotationUnits() ==0))
                {
                    m.add(screenImageHandler.getAction("set bounding box display relative"));
                }
                else if (clickedText.haveBoundingBox() &&(clickedText.getBoundingBoxAnnotationUnits() ==1))
                {
                    m.add(screenImageHandler.getAction("set bounding box image relative"));
                    
                }
                m.addSeparator();
                
                //insert anchor point
                if (clickedText.haveBoundingBox() &&!clickedText.haveAnchorPoint())
                {
                    m.add(screenImageHandler.getAction("insert anchor point display relative"));
                    m.add(screenImageHandler.getAction("insert anchor point image relative"));
                
                    
                }
                
                //remove boungin box
                if (clickedText.haveBoundingBox() &&clickedText.haveAnchorPoint())
                {
                    m.add(screenImageHandler.getAction("remove anchor point"));
                    
                }
                
                //move bounding box
                if (clickedText.haveAnchorPoint())
                {
                    m.add(screenImageHandler.getAction("move anchor point"));
                }
                
                //Change type of anchor point
                if (clickedText.haveAnchorPoint() &&(clickedText.getAnchorPointAnnotationUnits() ==0))
                {
                    m.add(screenImageHandler.getAction("set anchor point display relative"));
                }
                else if (clickedText.haveAnchorPoint() &&(clickedText.getAnchorPointAnnotationUnits() ==1))
                {
                    m.add(screenImageHandler.getAction("set anchor point image relative"));
                    
                }
            
                //Set anchor poiunt visible/unvisible
                if (clickedText.haveAnchorPoint() )
                {
                    
                    if (clickedText.anchorPointIsVisible())
                    {
                     m.add(screenImageHandler.getAction("set anchor point invisible"));
                        

                    }
                    else
                    {
                     m.add(screenImageHandler.getAction("set anchor point visible"));
                        

                        
                    }
                }
            
                
               
            }
            screenImageHandler.popup = m;
            screenImageHandler.popup.show( screenImageHandler.getImageCanvas(),e.getX(),e.getY());
        }
   }
}


/*
 *  CVS Log
 *  $Log: ImageCanvasEditListener.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
