/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import J2Ci.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class contains the names and the IDs of all possible status
 * The name of each status ID can be requested by {@link #getStatusName}
 *
 * @author Klaus Kleber
 * @since 16.10.2000
 */
public class ProcessStatusIDs 
{   
    /**
    * Operation was successful
    */
    public static final int STATUS_OK = 0;
    
    /**
    * Operation could be performed, but some unusual conditions were detected
    */
    public static final int STATUS_WARN= 1;
    
    /**
    * Operation has failed
    */
    public static final int STATUS_ERROR=2;
    
    
    /**
    * Contains for each status ID the name.
    */
    private static  Hashtable names;
    static
    {
           names = new Hashtable();
           names.put(new Integer(STATUS_OK),  "OK");
           names.put(new Integer(STATUS_WARN),  "warning");
           names.put(new Integer(STATUS_ERROR),  "error");
    }
    
    /**
    * Returns the name of the specified status
    * @param id Id specifieying the status
    * @return The name of the specified status
    */
    public static String getStatusName(int id)
    {
        return (String) names.get(new Integer(id));
    }
}

/*
 *  CVS Log
 *  $Log: ProcessStatusIDs.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
