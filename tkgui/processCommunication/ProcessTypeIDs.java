/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import J2Ci.*;
import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class contains the names and the IDs of all possible DICOMscope process
 * The name of each process ID can be requested by {@link #getProcessName}
 *
 * @author Klaus Kleber
 * @since 16.10.2000
 */
public class ProcessTypeIDs 
{   
    /**
    * Process is none of the other types described below
    */
    public static final int CLIENT_OTHER = 0;
    
    /**
    * Process is a DICOM Storage Service Class Provider.
    */
    public static final int CLIENT_STORE_SCP = 1;
    
    /**
    * Process is a DICOM Storage Service Class User.
    */
    public static final int CLIENT_STORE_SCU = 2;
    
    
    /**
    * Process is a DICOM Print Service Class Provider.
    */
    public static final int CLIENT_PRINT_SCP = 3;
    
    /**
    * Process is a DICOM Print Service Class User.
    */
    public static final int CLIENT_PRINT_SCU = 4;
    
    /**
    * Process is a DICOM Query/Retrieve Service Class User.
    */
    public static final int CLIENT_QUERY_RETRIEVE_SCU = 5;
    
    /**
    * Contains for each process ID the name.
    */
    private static  Hashtable names;
    static
    {
           names = new Hashtable();
           names.put(new Integer(CLIENT_OTHER),  "Other");
           names.put(new Integer(CLIENT_STORE_SCP),  "Store SCP");
           names.put(new Integer(CLIENT_STORE_SCU),  "Store SCU");
           names.put(new Integer(CLIENT_PRINT_SCP),  "Print SCP");
           names.put(new Integer(CLIENT_PRINT_SCU),  "Print SCU");
           names.put(new Integer(CLIENT_QUERY_RETRIEVE_SCU),  "Q/R SCU");
        
    }
    
    /**
    * Returns the name of the specified process
    * @param id Id specifying the process
    * @return The name of the specified process
    */
    public static String getProcessName(int id)
    {
        return (String) names.get(new Integer(id));
    }
}
/*
 *  CVS Log
 *  $Log: ProcessTypeIDs.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
