/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import main.*;
/**
 * This class logs all received DicomScopeMessage. 
 * @author Klaus Kleber
 * @since 16.10.2000
 */
public class ProcessIDDisplay extends JFrame implements ProcessLogListener, ActionListener
{   
    /**
    * TextArea for display the DicomScopeMessage
    */ 
    private JTextArea textArea = new JTextArea();
    
    /**
    * List containing the DicomScopeMessage.
    */ 
    private Vector messageList = new Vector();
    
    /**
    * Max lenght of messageList.
    */
    private int maxLength= 1500;
    
    /**
    * Number of DicomScopeMessages to be deleted if maxLenght is reached
    */
    private int deleteNumberOfMessage = 500;
    
    /**
    * ParentFrame
    */
    private JFrame parent;
    
    /**
    * Process ID of the message
    */
    int processID;
    public ProcessIDDisplay( Vector messageList, int processID)
    {
       super(new String ("Process: " + processID ));
       
       this.processID =processID;
       
       
       textArea.setWrapStyleWord(true);
       textArea.setEditable(false);
       initMessages(messageList);
       
       ProcessController.instance().addProcessLogListener(this);
	
	setSize(400,300);
	
	
	SymWindow aSymWindow = new SymWindow();
	this.addWindowListener(aSymWindow);
       
       
       getContentPane().setLayout(new BorderLayout(5,5));
       getContentPane().add(new JScrollPane(textArea), BorderLayout.CENTER);
       
       JButton closeButton = new JButton ("Close");
       closeButton.setActionCommand("close");
       closeButton.addActionListener(this);
       
       JPanel buttonPanel = new JPanel();
       buttonPanel.add(closeButton, BorderLayout.SOUTH);
       getContentPane().add(buttonPanel, BorderLayout.SOUTH);
       
       
    }
    
    /**
    * Initialized the textArea with the DicomScopeMessages contained in the
    * specified messageList
    */
    private synchronized void initMessages(Vector messageList)
    {
        this.messageList = messageList;
        for (int i = 0; i< messageList.size(); i++)
        textArea.append(messageList.elementAt(i).toString()+"\n");
        textArea.setCaretPosition(textArea.getText().length()-1);
        
    }
    
    /**
    * Received DicomScopeMessage
    */
    public synchronized  void logProcess (DicomScopeMessage e)
    {
        if (maxLength < messageList.size()) messageList.removeElementAt(0);
        if (e.getProcessId()== processID)
        {
            messageList.add(e);
            textArea.append(e.toString()+"\n");
            textArea.setCaretPosition(textArea.getText().length()-1);
            
        }
    }
    
    /**
    * Handles Action Events
    */ 
    public void actionPerformed(ActionEvent e)
    {
        String command = e.getActionCommand();
        if (command.equals("close")) close(this);
    }
    /**
    * Closes the application and terminates the network receiver.
    */
    class SymWindow extends java.awt.event.WindowAdapter
    {
	public void windowClosing(java.awt.event.WindowEvent event)
	{
		close(	(ProcessLogListener)event.getSource());
	}
    }
    
    /**
    * Close.
    */
    private void close(ProcessLogListener procLogList)
    {
            ProcessController.instance().removeProcessLogListener(procLogList);
	    
	    setVisible(false);
	    
	    dispose();
    }
    
}

/*
 *  CVS Log
 *  $Log: ProcessIDDisplay.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
