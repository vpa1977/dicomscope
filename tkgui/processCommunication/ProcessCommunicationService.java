/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import java.io.*;
import java.net.*;
import java.util.*;

/**
 * This class receives connection from any DICOMscope process and 
 * start for each connection ProcessCommunicationService in his own Thread 
 * @author Klaus Kleber
 * @since 15.10.2000
 */
public class ProcessCommunicationService implements Runnable
{   
    /**
    * Port
    */
    private int messagePort = 11000;
    
    
    private boolean keepMessagePortOpen=false;
    
    /**
    * Received Socket 
    */
    private Socket cSocket; 
    
    private boolean isReady = false;
    
    /**
    * Constuctor. Start the Receiver in his own Thread
    */
    public ProcessCommunicationService(int messagePort , boolean keepMessagePortOpen)
    {
        if(messagePort != 0 )
        {
            new Thread(this).start();
            messagePort = messagePort;
            keepMessagePortOpen = keepMessagePortOpen;
            waitForServerSocket();
        }
        // else isReady = true;
    }
   
    private synchronized void waitForServerSocket()
    {
            if (isReady == false) 
            {
               try
               {
                    wait();
               }
               catch (InterruptedException e)
               {
                    System.err.println(e);
               }
            }
    }
    
    public synchronized void serverSocketisReady()
    {
           isReady= true;
           notify();
    }
    
    /**
    * Run
    */
    public void run()
    {
        
        
        Thread.currentThread().setPriority(Thread.NORM_PRIORITY-1);
        ServerSocket sSocket= null;
        try
        {
        
           sSocket = new ServerSocket(messagePort);
            while(true)
            {
                if (sSocket != null)
                {
                    //System.err.println("wait on port: " + messagePort);
                    serverSocketisReady();
                    while ((cSocket = sSocket.accept()) != null)
                    {
                        //System.err.println("Receive new connection "  );
                        new ProcessCommunicationHandler(cSocket).start();
                        
                    }
                }
            }
         }   
        //Anmeldung an Socket nicht gelungen
        catch (IOException e)
        {
            System.err.println(e);
            
        }
        finally
        {
            try
            {
                sSocket.close();
            }
            catch(Exception t){}
        }
        
        
    }
}

/*
 *  CVS Log
 *  $Log: ProcessCommunicationService.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
