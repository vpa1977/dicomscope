/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package processCommunication;
import main.*;

import java.util.*;
import javax.swing.event.*;
/**
 * This class  provides an event-based communication in
 * an application. Parts of the application can add itself to this
 * server to recieve incoming events. Those parts must implement the
 * "ProcessLogListener"-interface.
 * Do add a ProcessLogListener you may use the following commands:
 * <code>
 * class SomeListener implements ProcessLogListener
 * {
 *  public SomeListener()
 *  {
 *      ProcessController es = ProcessController.instance().addMainListener(this);
 *  }
 *  
 *  public boolean logProcess (DicomScopeMessage e)
 *  {
 *      doSomething();
 *      return true;
 *  }
 * }
 * </code>
*/

public class ProcessController
{
    // Singleton Pattern: 
    /** 
    * The one and only instance is stored here. 
    */
    private static ProcessController theInstance = null;
    /**
    * EventListenerList
    */
    protected EventListenerList listenrList ;
    
    
    
    /**
     * The Constructor of this class. The one-and-only object of
     * this class can be created (or retrieved) with this method.
     *
     * @return an instance of this class.
    */
    public static ProcessController instance()
    {
        if (theInstance == null) theInstance = new ProcessController();
        
        return theInstance;
    }
    
    /**
     * The internal constructor of this class. Don't call direct.
    */
    protected ProcessController ()
    {
        // do not instanciate this way!
        
        listenrList = new EventListenerList();
    }
   
    /**
    * Adds a ProcessLogListener.
    * @param l The ProcessLogListener to be added
    */
    public void addProcessLogListener(ProcessLogListener l) 
    {
        listenrList.add(ProcessLogListener.class, l);
    }
    /**
    * Removes the specified ProcessLogListener
    * @param l The ProcessLogListener to be removed
    */
    public void removeProcessLogListener(ProcessLogListener l) 
    {
        listenrList.remove(ProcessLogListener.class, l);
    }

    /**
    * Notify all listeners that have registered interest for
    *  notification on this event type.  The event instance 
    * is lazily created using the parameters passed into 
    * the fire method.
    * @param e The DicomScopeMessage to be fired
    */
    public void fireProcessLog(DicomScopeMessage e) 
    {
        
        // Guaranteed to return a non-null array
        Object[] listeners = listenrList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length-2; i>=0; i-=2) 
        {
            if (listeners[i]==ProcessLogListener.class) 
            {
                // Lazily create the event:
                //if (jiveMessage == null) fooEvent = new FooEvent(this);
    
                ((ProcessLogListener)listeners[i+1]).logProcess(e);
            }
            
        }
    }	
    
}
/*
 *  CVS Log
 *  $Log: ProcessController.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
