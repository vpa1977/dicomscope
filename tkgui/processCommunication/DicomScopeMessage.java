/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package processCommunication;

import J2Ci.*;
import java.io.*;
import main.*;

/**
 * This class is the parent class for the communication messages received from 
 * and send to any DICPMScope process
 * @author Klaus Kleber
 * @since 30.04.1999
 */

public class DicomScopeMessage 
{   
    
    /**
    * Id of the message
    */
    public  int messageId;
    
    /**
    * Name of the Message
    */
    public   String messageName;
    
    
    /**
    * Contains the Id of the process
    */
    public int processId = -1;
    
    /**
    * Contains the type of the process
    */
    public int  processType;
    
    /**
    * Contains the name of the process
    */
    public String processTypeName;
    
    
    
    /**
    * Receiving date of the DICOMscopeMessage in ISO Format
    */
    public String date;
    
    
    /**
    * Status ID of the DICOMscopeMessage.
    */
    public int statusId = 0;
    
    /**
    * Status Name of the DICOMscopeMessage.
    */
    public String statusName = ProcessStatusIDs.getStatusName(ProcessStatusIDs.STATUS_OK);
    
    
    
    /**
    * Message Text
    */
    public String text ="";
    
    public DicomScopeMessage(   int messageId,
                                String messageName,
                                int processId, 
                                int processType,
                                String processTypeName,
                                String date,
                                int statusId,
                                String statusName,
                                String text)
    {
        this.messageId = messageId;
        this.messageName = messageName;
        this.processId = processId;
        this.processType = processType;
        this.processTypeName = processTypeName;
        this.date = date;
        this.statusId = statusId;
        this.statusName = statusName;
       // System.err.println("DicomScopeMessage: " );
       // System.err.println("Text: " + text);
        this.text = text;
        /** Old
        StringBuffer s = new StringBuffer(text.length() +100);
        int i = text.indexOf("\n");
        int lastIndex = 0;
        while (i != -1)
        {
            s.append(text.substring(lastIndex,i)+"\n\t");
            lastIndex= i+1;
            if (lastIndex<text.length()-1) i = text.indexOf("\n",lastIndex);
            else break;
                    
        }
        if (lastIndex<text.length()-1)s.append(text.substring(lastIndex,text.length() ));
        this.text = s.toString();
        */
    }
    
    /**
    * Returns the process id of the DicomScopeMessage. 
    * @return Process id of the DicomScopeMessage.
    */
    public int getProcessId()
    {
        return processId;
    }
    public String toString()
    {
        return new String("Process ID: " + processId+"\n\t" +ProcessMessageIDs.getMessageName(messageId)+  "\n " + text );
    }
    
    
}

/*
 *  CVS Log
 *  $Log: DicomScopeMessage.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
