/*
 *
 *  Copyright (C) 2001-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 * The class <em>jDVPSVerifyAndSignMode</em> represents the C++ enumeration
 * DVPSVerifyAndSignMode.
 *
 * @author 	Joerg Riesmeier
*/
public class jDVPSVerifyAndSignMode
{
    /** verify the document only
     */
    public static final int DVPSY_verify = 0;

    /** verify and digitally sign the document (apart from VerifyingObserver and SOPInstanceUID)
     */
    public static final int DVPSY_verifyAndSign = 1;

    /** verify and digitally sign the entire document (finalize it)
     */
    public static final int DVPSY_verifyAndSign_finalize = 2;
}


/*
 *  CVS Log
 *  $Log: jDVPSVerifyAndSignMode.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
