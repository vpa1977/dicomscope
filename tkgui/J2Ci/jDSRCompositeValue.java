/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRCompositeValue</em> represents the relating C++ class
 *  DSRReferenceValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRCompositeValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRCompositeValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRCompositeValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Composite Value --- */

    /** get SOP class UID
     ** @return current SOP class UID (might be invalid or an empty string)
     */
    public native String getSOPClassUID();

    /** get SOP class name.
     *  The SOP class name as defined in the OFFIS dcmtk is used if available,
     *  an empty string if the SOP class UID is also empty, the static string
     *  "unknown SOP class" plus the SOP class UID otherwise.
     ** @return name of the current SOP class (should never be null)
     */
    public native String getSOPClassName();

    /** get SOP instance UID
     ** @return current SOP instance UID (might be invalid or an empty string)
     */
    public native String getSOPInstanceUID();

    /** set SOP class UID and SOP instance UID value.
     *  Before setting the values they are checked (non-empty UIDs).  If the value
     *  pair is invalid the current value pair is not replaced and remains unchanged.
     ** @param  sopClassUID     SOP class UID to be set
     *  @param  sopInstanceUID  SOP instance UID to be set
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setReference(String sopClassUID,
                                   String sopInstanceUID);
}


/*
 *  CVS Log
 *  $Log: jDSRCompositeValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
