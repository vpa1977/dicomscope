/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPrStateParam_GetOverlayData</em> is responsible for parameters
 * passed to the method getOverlayData() of class DVPresentationState. This method
 * has some parameters (primitive types and array types) passed by reference (changed in this method),
 * and what Java can't manage. So an extra class had to be constructed - this class.
 * Create an object of this class before passing it to that method!
 * IN and OUT specifies the type of param (input param or output param).
 *
 * @author 	Andreas Schr�ter
 */
public class jDVPrStateParam_GetOverlayData
{   
    /**
     * layer : IN
    */
    public int layer;      
    
    /**
     * index : IN
    */
    public int idx;
    
    /**
     * Pixel Data in the case of 8 bits of data
     * overlayData : OUT
    */
    public byte[] overlayData = null;
    
    /**
     * Pixel Data in the case of 12 bits of data
     * overlayData : OUT
    */
    public short[] overlayData12 = null;
    
    
    
    /**
     * width : OUT
    */
    public int width = 0;
    
    /**
     * height : OUT
    */
    public int height = 0;
    
    /**
     * left : OUT
    */
    public int left = 0;
    
    /**
     * top : OUT
    */
    public int top = 0;
    
    /**
     * is ROI : OUT
    */
    public boolean isROI;
    
    /**
     * transp : OUT
    */
    public short foreground = 0;
    
    /**
     * used bits: IN
     * Defautlt = 8, possible values : 8 or 12
    */
    public int bits = 8;
}


/*
 *  CVS Log
 *  $Log: jDVPrStateParam_GetOverlayData.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
