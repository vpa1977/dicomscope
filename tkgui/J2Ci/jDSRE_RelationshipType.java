/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;
import java.util.*;


/** 
 * The class <em>jDSRE_RelationshipType</em> represents the C++ enumeration
 * DSRTypes::E_RelationshipType.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_RelationshipType
{
    /** internal type used to indicate an error
     */
    public static final int RT_invalid = 0;

    /** internal type used for the document root
     */
    public static final int RT_isRoot = 1;

    /** DICOM Relationship Type: CONTAINS
     */
    public static final int RT_contains = 2;

    /** DICOM Relationship Type: HAS OBS CONTEXT
     */
    public static final int RT_hasObsContext = 3;

    /** DICOM Relationship Type: HAS ACQ CONTEXT
     */
    public static final int RT_hasAcqContext = 4;

    /** DICOM Relationship Type: HAS CONCEPT MOD
     */
    public static final int RT_hasConceptMod = 5;

    /** DICOM Relationship Type: HAS PROPERTIES
     */
    public static final int RT_hasProperties = 6;

    /** DICOM Relationship Type: INFERRED FROM
     */
    public static final int RT_inferredFrom = 7;

    /** DICOM Relationship Type: SELECTED FROM
     */
    public static final int RT_selectedFrom = 8;
    
    /**
    * Contains the name for each ID.
    */
    public static Hashtable names; 
    static   
    {
        names = new Hashtable();
        names.put(new Integer(RT_invalid),       "<invalid>");
        names.put(new Integer(RT_isRoot),        "");
        names.put(new Integer(RT_contains),      "contains");
        names.put(new Integer(RT_hasObsContext), "has Obs Context");
        names.put(new Integer(RT_hasAcqContext), "has Acq Context");
        names.put(new Integer(RT_hasConceptMod), "has Concept Mod");
        names.put(new Integer(RT_hasProperties), "has Properties");
        names.put(new Integer(RT_inferredFrom),  "inferred from");
        names.put(new Integer(RT_selectedFrom),  "selected from");
    };
    
    /**
    * Contains the name for each ID.
    */
    public static Hashtable treNames; 
    static   
    {
        treNames = new Hashtable();
        treNames.put(new Integer(RT_invalid),       "<invalid>");
        treNames.put(new Integer(RT_isRoot),        "");
        treNames.put(new Integer(RT_contains),      "");
        treNames.put(new Integer(RT_hasObsContext), "Observation Context");
        treNames.put(new Integer(RT_hasAcqContext), "Acquisition Context");
        treNames.put(new Integer(RT_hasConceptMod), "Concept Modifier");
        treNames.put(new Integer(RT_hasProperties), "Properties");
        treNames.put(new Integer(RT_inferredFrom),  "Inferred");
        treNames.put(new Integer(RT_selectedFrom),  "Selected");
    };
    /**
    * Returns the name of the specified message
    * @param id Id specifying the message
    * @return The name of the specified message
    */
    public static final  String getName(int id)
    {
        return (String) names.get(new Integer(id));
    }
    /**
    * Returns the name of the specified message
    * @param id Id specifying the message
    * @return The name of the specified message
    */
    public static final  String getTreeName(int id)
    {
        return (String) treNames.get(new Integer(id));
    }    
    
}


/*
 *  CVS Log
 *  $Log: jDSRE_RelationshipType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
