/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPSPrintPresentationLUTType</em> represents the C++-enumeration
 * jDVPSPrintPresentationLUTType.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPSPrintPresentationLUTType
{
  /** 
   * Presentation LUT Shape with value 'IDENTITY'
  */
  public static final int DVPSQ_identity = 0;
  
  
  /** 
   * Presentation LUT look up table
  */
  public static final int DVPSQ_table = 1;
  
  
  /** 
   * Presentation LUT SOP Class not used
  */
  public static final int DVPSQ_none = 2;
  
}

/*
 *  CVS Log
 *  $Log: jDVPSPrintPresentationLUTType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
