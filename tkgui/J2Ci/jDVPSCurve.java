/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;


/** 
 * A <em>jDVPSCurve</em> is the Java-sided class for the C++-Class
 * DVPSCurve.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPSCurve
{
    /**
     * Constructor    
    */
    protected jDVPSCurve()
    {
        createObjOfDVPSCurve ();
    }
    
    
    /**
     * Constructor for attaching an existing C++-Object. FOR INTERNAL USE ONLY!
     * @param attachAdr Address of C++-Object
    */    
    public jDVPSCurve (long attachAdr)
    {
        cppClassAddress = attachAdr;
    }
    
    
    /**
     * Copy-Constructor
     * @param copy Curve-Object to copy
    */
    public jDVPSCurve (jDVPSCurve copy)
    {                
        CopyConstructor (copy.cppClassAddress);
    }

    private native void CopyConstructor (long fromCppObj);
    
    
    // -------------------------------- Methods for C++-Class Binding
    
    /**
     * Address of C++-Object for access in the DLL. Never change manually!!
    */
    private long cppClassAddress = (long) 0; // never change!
    
    
    /**
     * Creates a C++-Object of class DVPSCurve and attached it to this
     * current object. The address of the C++-object will be put into field 
     * cppClassAddress.
     *
     * @see J2Ci.jDVPSCurveObject#cppClassAddress
    */
    private native void createObjOfDVPSCurve ();
        
    // ----------------------------------- Methods of Class jDVPSCurve
    
    
    /** 
     * Get group number of curve repeating group managed by this object.
     * @return the lower byte of the curve group
    */
    public native byte getCurveGroup();


    /**      
     * Gets the number of points in the curve.
     * @return number of points
    */
    public native int getNumberOfPoints();


    /** 
     * Gets the type of data in the curve (ROI or POLY).
     * @return type of data in curve (from jDVPSCurveType).
    */
    public native int getTypeOfData();


    /** 
     * Gets one point from the curve data.
     * @param idx index of the curve point, must be < getNumberOfPoints();
     * @param x upon success the x value of the point is returned in this parameter.
     *        Must be created before passing to this method!
     * @param y upon success the y value of the point is returned in this parameter   
     *        Must be created before passing to this method!
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int getPoint(int idx, jDoubleByRef x, jDoubleByRef y);


    /** 
     * Gets the curve description string if present.
     * If the description string is absent, this method returns NULL or an empty string.
     * @return curve description
    */
    public native String getCurveDescription();


    /** 
     * Gets the curve label string if present.
     * If the label string is absent, this method returns NULL or an empty string.
     * @return curve label
    */
    public native String getCurveLabel();


    /** 
     * Gets the curve axis units string for the X dimension if present.
     * If the string is absent, this method returns NULL or an empty string.
     * @return curve description
    */
    public native String getCurveAxisUnitsX();

  
    /** 
     * Gets the curve axis units string for the Y dimension if present.
     * If the string is absent, this method returns NULL or an empty string.
     * @return curve description
    */
    public native String getCurveAxisUnitsY();

}

/*
 *  CVS Log
 *  $Log: jDVPSCurve.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
