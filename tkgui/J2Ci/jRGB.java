/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jRGB</em> represents a RGB-color value.
 *
 * @author 	Andreas Schr�ter
*/
public class jRGB
{
    /**
     * The red part of color.
    */
    public int red;
    
    
    /**
     * The green part of color.
    */
    public int green;
    
    
    /**
     * The blue part of color.
    */
    public int blue;
    
    
    
    /** 
     * Constructor; sets the values of r, g, b to the specified values.
     * @param r red value.
     * @param g green value.
     * @param b blue value.
    */
    public jRGB (int r, int g, int b)
    {
        red = r;
        green = g;
        blue = b;
    }
    
    
    /** 
     * Constructor; sets the values to black (0, 0, 0)
    */    
    public jRGB ()
    {
        red = 0;
        green = 0;
        blue = 0;
    }
}

/*
 *  CVS Log
 *  $Log: jRGB.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
