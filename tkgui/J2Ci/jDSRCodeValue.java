/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRCodeValue</em> represents the relating C++ class
 *  DSRCodedEntryValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRCodeValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRCodeValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRCodeValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Coded Entry Value --- */

    /** check whether the current code belongs to the private "dcmtk" coding scheme.
     *  This check is performed by comparing the coding scheme designator and the private
     *  coding scheme creator UID - a coding scheme version is currently not used.
     ** @return OFTrue if code belongs to the private "dcmtk" coding scheme, OFFalse otherwise
     */
    public native boolean isPrivateDcmtkCodingScheme();

    /** get code value.
     *  This is a identifier of the code that is unambiguous within the coding scheme.
     ** @return current code value (might be invalid or an empty string)
     */
    public native String getCodeValue();

    /** get coding scheme designator.
     *  This is a textual identifier of the table where the code value is linked to its
     *  code meaning.
     ** @return current coding scheme designator (might be invalid or an empty string)
     */
    public native String getCodingSchemeDesignator();

    /** get coding scheme version.
     *  Optional - Used when a coding scheme has multiple versions and the coding scheme
     *  designator does not explicitly (or adequately) specify the version number.
     ** @return current coding scheme version (might be invalid or an empty string)
     */
    public native String getCodingSchemeVersion();

    /** get code meaning.
     *  Human-readable translation of the code value.  Used for display when code dictionary
     *  is not available.
     ** @return current code meaning (might be invalid or an empty string)
     */
    public native String getCodeMeaning();

    /** set code value.
     *  Before setting the specified code it is checked (see checkCode()).  If the code is
     *  invalid the current code is not replaced and remains unchanged.
     ** @param  codeValue               identifier of the code to be set that is unambiguous
     *                                  within the coding scheme. (VR=SH, mandatory)
     *  @param  codingSchemeDesignator  textual identifier of the table where the 'codeValue'
     *                                  is linked to its 'codeMeaning'. (VR=SH, mandatory)
     *  @param  codingSchemeVersion     version of the coding scheme.  Used when a coding
     *                                  scheme has multiple versions and the 'codingScheme
     *                                  Designator' does not explicitly (or adequately)
     *                                  specify the version number. (VR=SH, optional)
     *  @param  codeMeaning             human-readable translation of the 'codeValue'.  Used
     *                                  for display when code dictionary is not available.
     *                                  (VR=LO, mandatory)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setCode(String codeValue,
                              String codingSchemeDesignator,
                              String codingSchemeVersion,
                              String codeMeaning);
}


/*
 *  CVS Log
 *  $Log: jDSRCodeValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
