/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPSDecimateCropBehaviour</em> represents the C++-enumeration
 * DVPSDecimateCropBehaviour.
 *
 * @author 	Andreas Schr�ter
*/
public class jDVPSDecimateCropBehaviour
{
  /** 
   * a magnification factor less than one to be applied to the image.
  */
  public static final int DVPSI_decimate = 0;
  
  
  /** 
   * some image rows and/or columns are to be deleted before printing.
  */
  public static final int DVPSI_crop = 1;
  
  
  /** 
   * the SCP shall not crop or decimate
  */
  public static final int DVPSI_fail = 2;
  
  
  /** 
   * printer default
  */
  public static final int DVPSI_default = 3;
}

/*
 *  CVS Log
 *  $Log: jDVPSDecimateCropBehaviour.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
