/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 *  <em>jDSRNumericValue</em> represents the relating C++ class
 *  DSRNumericMeasurementValue in Java.
 *
 *  @author 	Joerg Riesmeier
 */
public class jDSRNumericValue
{
    /**
     * Constructor is disabled !!!
    */
    private jDSRNumericValue()
    {
        // emtpy
    }


    /**
     * Constructor for attaching an existing C++ object. FOR INTERNAL USE ONLY!
     * @param attachAdr address of C++ object
    */
    public jDSRNumericValue(long attachAdr)
    {
        cppClassAddress = attachAdr;
    }


    // --------------------- methods for C++ class binding ---------------------

    /**
     * Address of relating C++ object [for access to the DLL].
     * Never change manually!
    */
    private long cppClassAddress = (long) 0; // never change!



    // --------------------------- native methods ------------------------------

    /* --- Numeric Value --- */

    /** get numeric value
     ** @return current numeric value (might be invalid or an empty string)
     */
    public native String getNumericValue();

    /** get measurement unit code value.
     *  This is a computer readable and computer searchable identifier of the code.
     *  This value contains the measurement unit.
     ** @return current code value (might be invalid or an empty string)
     */
    public native String getMeasurementUnitCodeValue();

    /** get measurement unit coding scheme designator.
     *  This is a textual identifier of the table where the code value is linked to its
     *  code meaning.
     ** @return current coding scheme designator (might be invalid or an empty string)
     */
    public native String getMeasurementUnitCodingSchemeDesignator();

    /** get measurement unit coding scheme version.
     *  Optional - Used when a coding scheme has multiple versions and the coding scheme
     *  designator does not explicitly (or adequately) specify the version number.
     ** @return current coding scheme version (might be invalid or an empty string)
     */
    public native String getMeasurementUnitCodingSchemeVersion();

    /** get measurement unit code meaning.
     *  This value contains the measurement name.
     ** @return current code meaning (might be invalid or an empty string)
     */
    public native String getMeasurementUnitCodeMeaning();

    /** set numeric value.
     *  Before setting the value it is checked (see checkNumericValue()).  If the value is
     *  invalid the current value is not replaced and remains unchanged.
     ** @param  numericValue     numeric value to be set (VR=DS)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setNumericValue(String numericValue);

    /** set measurement unit.
     *  Before setting the specified code it is checked (see checkCode()).  If the code is
     *  invalid the current code is not replaced and remains unchanged.
     ** @param  codeValue               corresponds to measurement unit. (VR=SH, mandatory)
     *  @param  codingSchemeDesignator  textual identifier of the table where the 'codeValue'
     *                                  is linked to its 'codeMeaning'. (VR=SH, mandatory)
     *  @param  codingSchemeVersion     version of the coding scheme.  Used when a coding
     *                                  scheme has multiple versions and the 'codingScheme
     *                                  Designator' does not explicitly (or adequately)
     *                                  specify the version number. (VR=SH, optional)
     *  @param  codeMeaning             corresponds to measurement name. (VR=LO, mandatory)
     ** @return status, EC_Normal if successful, an error code otherwise
     */
    public native int setMeasurementUnit(String codeValue,
                                         String codingSchemeDesignator,
                                         String codingSchemeVersion,
                                         String codeMeaning);
}


/*
 *  CVS Log
 *  $Log: jDSRNumericValue.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
