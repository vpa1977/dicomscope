/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPrStateParam_GetPixelData</em> is responsible for parameters
 * passed to the method getPixelData() of class DVPresentationState. This method
 * has three parameters (primitive types) passed by reference (changed in this method),
 * and what Java can't manage. So an extra class had to be constructed - this class.
 * Create an object of this class before passing it to that method!
 *
 * @author 	Andreas Schr�ter
 */
public class jDVPrStateParam_GetPixelData
{
    /**
     * Pixeldata as byte-Array.
    */
    public byte[] pixelData = null;
    
    /**
     * Width of image.
    */
	public long width = -1;
	
	/**
     * Height of image.
    */
	public long height = -1;
}

/*
 *  CVS Log
 *  $Log: jDVPrStateParam_GetPixelData.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
