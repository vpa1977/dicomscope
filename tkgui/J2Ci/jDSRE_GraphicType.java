/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;
import java.util.*;

/** 
 * The class <em>jDSRE_GraphicType</em> represents the C++ enumeration
 * DSRTypes::E_GraphicType.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_GraphicType
{
    /** internal type used to indicate an error
     */
    public static final int GT_invalid = 0;

    /** DICOM Graphic Type: POINT
     */
    public static final int GT_Point = 1;
    
    /** DICOM Graphic Type: MULTIPOINT
     */
    public static final int GT_Multipoint = 2;

    /** DICOM Graphic Type: POLYLINE
     */
    public static final int GT_Polyline = 3;

    /** DICOM Graphic Type: CIRCLE
     */
    public static final int GT_Circle = 4;

    /** DICOM Graphic Type: ELLIPSE
     */
    public static final int GT_Ellipse = 5;
    
    /**
    * Contains the name for each ID.
    */
    public static Hashtable names; 
    static   
    {
        names = new Hashtable();
        names.put(new Integer(GT_invalid),       "<invalid>");
        names.put(new Integer(GT_Point),        "Point");
        names.put(new Integer(GT_Multipoint),      "Multipoint");
        names.put(new Integer(GT_Polyline), "Polyline");
        names.put(new Integer(GT_Circle), "Circle");
        names.put(new Integer(GT_Ellipse), "Ellipse");
    };
    
    /**
    * Returns the name of the specified message
    * @param id Id specifying the message
    * @return The name of the specified message
    */
    public static final  String getName(int id)
    {
        return (String) names.get(new Integer(id));
    }
    
}


/*
 *  CVS Log
 *  $Log: jDSRE_GraphicType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
