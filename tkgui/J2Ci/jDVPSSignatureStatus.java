/*
 *
 *  Copyright (C) 2001-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;


/**
 * The class <em>jDVPSSignatureStatus</em> represents the C++ enumeration
 * DVPSSignatureStatus.
 *
 * @author 	Joerg Riesmeier
*/
public class jDVPSSignatureStatus
{
    /** no digital signatures are present
     */
    public static final int DVPSW_unsigned = 0;

    /** one or more digital signatures are present and have been successfully verified
     */
    public static final int DVPSW_signed_OK = 1;

    /** one or more digital signatures are present, and all of them are valid.
     *  However, at least one of them was created
     *  with a certificate issued by an unknown CA.
     */
    public static final int DVPSW_signed_unknownCA = 2;

    /** one or more digital signatures are present and at least one of them
     *  could not be successfully verified because it was corrupt or created
     *  with a certificate issued by an unknown CA.
     */
    public static final int DVPSW_signed_corrupt = 3;
}


/*
 *  CVS Log
 *  $Log: jDVPSSignatureStatus.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
