/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;
import java.util.*;

/** 
 * The class <em>jDSRE_AddMode</em> represents the C++ enumeration
 * DSRTypes::E_AddMode.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_AddMode
{
    /** add new node after current one (sibling)
     */
    public static final int AM_afterCurrent = 0;

    /** add new node before current one (sibling)
     */
    public static final int AM_beforeCurrent = 1;

    /** add new node below current one (child)
     */
    public static final int AM_belowCurrent = 2;
    /**
    * Contains for each ID the name.
    */
    public static Hashtable names;
    static
    {
         names = new Hashtable();
         names.put(new Integer(AM_afterCurrent),     "After");
         names.put(new Integer(AM_beforeCurrent),     "Before");
         names.put(new Integer(AM_belowCurrent),     "Below");
    };

    /**
    * Returns the name of the specified message
    * @param id Id specifieying the message
    * @return The name of the specified message
    */
    public static final  String getName(int id)
    {
        return (String) names.get(new Integer(id));
    }
    
}


/*
 *  CVS Log
 *  $Log: jDSRE_AddMode.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
