/*
 *
 *  Copyright (C) 2000-2003, OFFIS and Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  and
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;

import java.util.*;
/**
 * The class <em>jDSRE_ValueType</em> represents the C++ enumeration
 * DSRTypes::E_ValueType.
 *
 * @author 	Joerg Riesmeier
*/
public class jDSRE_ValueType
{

    /** internal type used to indicate an error
     */
    public static final int VT_invalid = 0;

    /** DICOM Value Type: TEXT
     */
    public static final int VT_Text = 1;

    /** DICOM Value Type: CODE
     */
    public static final int VT_Code = 2;

    /** DICOM Value Type: NUM
     */
    public static final int VT_Num = 3;

    /** DICOM Value Type: DATETIME
     */
    public static final int VT_DateTime = 4;

    /** DICOM Value Type: DATE
     */
    public static final int VT_Date = 5;

    /** DICOM Value Type: TIME
     */
    public static final int VT_Time = 6;

    /** DICOM Value Type: UIDREF
     */
    public static final int VT_UIDRef = 7;

    /** DICOM Value Type: PNAME
     */
    public static final int VT_PName = 8;

    /** DICOM Value Type: SCOORD
     */
    public static final int VT_SCoord = 9;

    /** DICOM Value Type: TCOORD
     */
    public static final int VT_TCoord = 10;

    /** DICOM Value Type: COMPOSITE
     */
    public static final int VT_Composite = 11;

    /** DICOM Value Type: IMAGE
     */
    public static final int VT_Image = 12;

    /** DICOM Value Type: WAVEFORM
     */
    public static final int VT_Waveform = 13;

    /** DICOM Value Type: CONTAINER
     */
    public static final int VT_Container = 14;

    /** type used to indicate by-reference relationships
     */
    public static final int VT_byReference = 15;

    /**
    * Contains for each ID the name.
    */
    public static Hashtable vtNames;
    static
    {
         vtNames = new Hashtable();
         vtNames.put(new Integer(VT_invalid),     "<invalid>");
         vtNames.put(new Integer(VT_Text),        "Text");
         vtNames.put(new Integer(VT_Code),        "Code");
         vtNames.put(new Integer(VT_Num),         "Numeric Value");
         vtNames.put(new Integer(VT_DateTime),    "Datetime");
         vtNames.put(new Integer(VT_Date),        "Date");
         vtNames.put(new Integer(VT_Time),        "Time");
         vtNames.put(new Integer(VT_UIDRef),      "UID Reference");
         vtNames.put(new Integer(VT_PName),       "Person Name");
         vtNames.put(new Integer(VT_SCoord),      "Spatial Coordinates");
         vtNames.put(new Integer(VT_TCoord),      "Temporal Coordinates");
         vtNames.put(new Integer(VT_Composite),   "Composite");
         vtNames.put(new Integer(VT_Image),       "Image");
         vtNames.put(new Integer(VT_Waveform),    "Waveform");
         vtNames.put(new Integer(VT_Container),   "Container");
         vtNames.put(new Integer(VT_byReference), "by-reference");
    };

    /**
    * Returns the name of the specified message
    * @param id Id specifieying the message
    * @return The name of the specified message
    */
    public static final  String getVTName(int id)
    {
        return (String) vtNames.get(new Integer(id));
    }
}


/*
 *  CVS Log
 *  $Log: jDSRE_ValueType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
 *
*/
