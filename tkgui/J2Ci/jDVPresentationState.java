/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy and OFFIS
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  and
 *
 *    Kuratorium OFFIS e.V.
 *    Healthcare Information and Communication Systems
 *    Escherweg 2
 *    D-26121 Oldenburg, Germany
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/


package J2Ci;

import java.awt.Point;


/**
 * A <em>jDVPresentationState</em> is the Java-sided class for the C++-Class
 * DVPresentationState. The Constructor is disabled !.
 *
 * @author 	Andreas Schroeter, Joerg Riesmeier
*/
public class jDVPresentationState
{
    /**
     * Constructor is disabled !!!
    */
    protected jDVPresentationState()
    {
        // emtpy
    }

    /**
     * Constructor for attaching an existing C++-Object. FOR INTERNAL USE ONLY!
     * @param attachAdr Address of C++-Object
    */
    public jDVPresentationState (long attachAdr)
    {
        cppClassAddress = attachAdr;
    }

    // -------------------------------- Methods for C++-Class Binding

    /**
     * Address of C++-Object for access in the DLL. Never change manually!!
    */
    private long cppClassAddress = (long) 0; // never change!


    // ----------------------------------- Methods of Class jDVPresentationState


    /** returns the current SOP Instance UID for the Presentation State.
     *  @return SOP Instance UID if present, NULL otherwise.
     */
    public native String getInstanceUID();


    /** returns the (currently hard-coded) SOP Class UID of the Presentation State.
     *  @return SOP Class UID of the presentation state
     */
    public native String getSOPClassUID();


    /** returns the SOP Class UID of the currently attached image.
     *  @return SOP class UID of current image, NULL if absent
     */
    public native String getAttachedImageSOPClassUID();


    /** returns the SOP Instance UID of the currently attached image.
     *  @return SOP instance UID of current image, NULL if absent
     */
    public native String getAttachedImageSOPInstanceUID();


    /**
     * Returns a label for the presentation state.
     * If no label is available, null is returned.
     * @return a pointer to a string or null.
    */
    public native String getPresentationLabel ();


    /**
     * Sets the presentation state label.
     * The passed string must be a valid DICOM Code String
     * (i.e. max 16 characters, only uppercase and numbers).
     * @param label the new presentation state label
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setPresentationLabel (String newLabel);


    /**
     * Returns a description for the presentation state.
     * If no description is available, NULL is returned.
     * @return a pointer to a string or NULL.
    */
    public native String getPresentationDescription ();


    /**
     * Sets the presentation state description.
     * The passed string must be a valid DICOM Long String
     * (i.e. max 64 characters, no backslash or control chars).
     * @param descr the new presentation state description
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setPresentationDescription (String descr);


    /**
     * Returns the creator's name for the presentation state.
     * If no name is available, NULL is returned.
     * @return a pointer to a string or NULL.
    */
    public native String getPresentationCreatorsName ();


    /**
     * Sets the presentation state creator's name.
     * The passed string must be a valid DICOM Person Name String
     * (see NEMA PS3.5:1998).
     * @param name the new creator's name
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setPresentationCreatorsName (String newName);


    /**
     * Gets the current rotation status of the presentation state.
     * @return the current rotation status (from jDVPSRotationType).
    */
    public native int getRotation ();


    /**
     * Sets rotation status of the presentation state.
     * @param rotation the rotation to be set (from jDVPSRotationType).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setRotation (int rotation);


    /**
     * Gets the current horizontal flip status of the presentation state.
     * @return true if flip is on, false if flip is off.
    */
    public native boolean getFlip ();


    /**
     * Sets horizontal flip status of the presentation state.
     * @param isFlipped the flip status, true for on, false for off.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setFlip (boolean isFlipped);


    /**
     * Resets the object to initial state.
     * After this call, the object is in the same state as after
     * creation with the default constructor.
    */
    public native void clear();


    /**
     * Apply presentation state to attached image and return image bitmap.
     * This method sets all parameter required to correctly render the pixel data
     * in the image attached to the presentation state and then creates the
     * required pixel data which contains all grayscale transformations but none
     * of the none-grayscale transformations of the presentation state "burned in"
     * into the pixel data. The pixel data returned is already corrected by a
     * Barten transform for the current display device and can be mapped directly
     * to digital driving levels of the graphics board. The pointer to the pixel
     * data remains valid until the next call to this function, or until the
     * image is detached or the presentation state is deleted.
     * @param pixel this object contains a reference to the pixeldata and the height
     *        and width of the image. This object must be created before passed as param.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getPixelData (jDVPrStateParam_GetPixelData pixel);


    /**
     * Adds a reference to an image to this presentation state.
     * This method checks if the given SOP class and Study UID match
     * for this presentation state and returns an error code otherwise.
     * @param studyUID the Study Instance UID of the image reference to be added.
     * @param seriesUID the Series Instance UID of the image reference to be added.
     * @param sopclassUID the SOP class UID of the image reference to be added.
     * @param instanceUID the SOP instance UID of the image reference to be added.
     * @param frames a list of frame numbers in DICOM IS format
     *   (integer numbers separated by '\' characters). Default: frame numbers absent.
     *   The frame numbers are required if the referenced image is a multiframe image.
     * @param aetitle the series retrieveAETitle. Must be a valid DICOM 'AE' value.
     *          Default: value absent.
     * @param filesetID the series storageMediaFileSetID. Must be a valid DICOM 'SH' value.
     *          Default: value absent.
     * @param filesetUID the series storageMediaFileSetUID. Must be a valid DICOM UID.
     *          Default: value absent.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int addImageReference (String studyUID, String seriesUID,
                      String sopClassUID, String instanceUID, String frames,
                      String aetitle, String filesetID, String filesetUID);


    /**
     * adds a reference to the currently attached image to this
     * presentation state. This method checks if the given image
     * is not yet referenced and if its Study UID and SOP class
     * match for this presentation state and returns an error code otherwise.
     * @param aetitle the series retrieveAETitle. Must be a valid DICOM 'AE' value.
     *        Default: value absent.
     * @param filesetID the series storageMediaFileSetID. Must be a valid DICOM 'SH' value.
     *        Default: value absent.
     * @param filesetUID the series storageMediaFileSetUID. Must be a valid DICOM UID.
     *        Default: value absent.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int addImageReferenceAttached (String aetitle, String filesetID, String filesetUID);


    /**
     * Removes a reference to an image from this presentation state.
     * @param studyUID the Study Instance UID of the image reference to be removed.
     * @param seriesUID the Series Instance UID of the image reference to be removed.
     * @param instanceUID the SOP instance UID of the image reference to be removed.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int removeImageReference (String studyUID, String seriesUID, String instanceUID);


    /**
     * Removes a reference to the currently attached image from this presentation state.
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int removeImageReferenceAttached ();


    /**
     * Gets the number of frames of the current (attached) image.
     * This method may only be called when an image is attached to the
     * presentation state.
     * @param frames upon success, the number of frames is returned in this parameter.
     *        Param frames must be created before passing to this method!
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getImageNumberOfFrames(jIntByRef frames);


    /**
     * Selects one frame of a multiframe image. This affects the image bitmap
     * that is rendered, the overlay bitmaps and the visibility of graphic and text
     * objects. This method may only be called when an image is attached to the
     * presentation state.
     * @param frame frame number in the range [1..getImageNumberOfFrames()]
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int selectImageFrameNumber(int frame);


    /** gets the index of the currently selected frame in a multi-frame image.
     *  @return index of the currently selected frame, 0 if an error occurred
     */
    public native int getSelectedImageFrameNumber();


    /**
     * Gets the current Presentation LUT type.
     * @return the current presentation LUT type (from jDVPSPresentationLUTType).
    */
    public native int getPresentationLUT();


    /**
     * Checks if a real Presentation LUT (not shape)
     * is available in the presentation state.
     * @return OFTrue if the presentation state contains
     *   a presentation LUT, no matter if it is activated or not.
     *   Returns OFFalse otherwise.
    */
    public native boolean havePresentationLookupTable();


    /**
     * Gets the presentation size mode for the current image and frame.
     * This method may only be called when an image is attached to the presentation state.
     * @return presentation size mode (from jDVPSPresentationSizeMode)
    */
    public native int getDisplayedAreaPresentationSizeMode();


    /**
     * Gets the presentation pixel aspect ratio for for the current image and frame.
     * Pixel aspect ratio is defined here as the width of a pixel divided
     * by the height of a pixel (x/y).
     * This method may only be called when an image is attached to the presentation state.
     * @return pixel aspect ratio
    */
    public native double getDisplayedAreaPresentationPixelAspectRatio();


    /**
     * Gets the displayed area top lefthand corner and
     * bottom righthand corner for the current potentially rotated and flipped image and frame.
     * This method may only be called when an image is attached to the presentation state.
     * The params tlhcXY and brhcXY must be created before passing to this method.
     * @param tlhcX the displayed area top lefthand corner X value is returned in this parameter
     * @param tlhcY the displayed area top lefthand corner Y value is returned in this parameter
     * @param brhcX the displayed area bottom righthand corner X value is returned in this parameter
     * @param brhcY the displayed area bottom righthand corner Y value is returned in this parameter
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int getStandardDisplayedArea(jIntByRef tlhcX, jIntByRef tlhcY, jIntByRef brhcX, jIntByRef brhcY);


    /**
     * Gets the displayed area top lefthand corner and 
    *  bottom righthand corner for the current image and frame, as if the image was unrotated
     * This method may only be called when an image is attached to the presentation state.
     * The params tlhcXY and brhcXY must be created before passing to this method.
     * @param tlhcX the displayed area top lefthand corner X value is returned in this parameter
     * @param tlhcY the displayed area top lefthand corner Y value is returned in this parameter
     * @param brhcX the displayed area bottom righthand corner X value is returned in this parameter
     * @param brhcY the displayed area bottom righthand corner Y value is returned in this parameter
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int getImageRelativeDisplayedArea(jIntByRef tlhcX, jIntByRef tlhcY, jIntByRef brhcX, jIntByRef brhcY);


    /**
     * Gets the presentation pixel spacing for the current image and frame if it is known.
     * @param x the horizontal pixel spacing (mm) is returned in this parameter upon success
     * @param y the vertical pixel spacing (mm) is returned in this parameter upon success
     * @return EC_Normal if successful, an error code if no presentation pixel spacing is available (from jE_Condition).
    */
    public native int getDisplayedAreaPresentationPixelSpacing(jDoubleByRef x, jDoubleByRef y);


    /**
     * Gets the presentation pixel magnification ratio for the current image and frame if it is present.
     * @param magnification the magnification ratio is returned in this parameter upon success
     * @return EC_Normal if successful, an error code if no magnification ratio is available (from jE_Condition).
    */
    public native int getDisplayedAreaPresentationPixelMagnificationRatio(jDoubleByRef magnification);


    /**
     * Checks if "TRUE SIZE" can be used as presentation size mode for the current image and frame
     * (i.e. pixel spacing is known).
     * @return true if TRUE SIZE mode is available, false otherwise.
    */
    public native boolean canUseDisplayedAreaTrueSize();


    /**
     * Sets the displayed area and size mode (for the current frame, the current image
     *  or all images referenced by the presentation state object).
     * @param sizeMode presentation size mode (from jDVPSPresentationSizeMode)
     * @param tlhcX displayed area top lefthand corner X
     * @param tlhcY displayed area top lefthand corner Y
     * @param brhcX displayed area bottom righthand corner X
     * @param brhcY displayed area bottom righthand corner Y
     * @param magnification magnification factor - ignored unless sizeMode==DVPSD_magnify.
     * @param applicability defines the applicability of the new displayed area definition.
     *    Possible choices are: DVPSB_currentFrame - current frame only,
     *    DVPSB_currentImage - all frames of current image (default),
     *    and DVPSB_allImages -  all images referenced by this presentation state.
     *    The last choice should be used with care
     *    because it will also cause the pixel spacing or pixel aspect ratio of the current image
     *    to be applied to all images referenced by the presentation state.
     *    (from jDVPSObjectApplicability)
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setStandardDisplayedArea(int sizeMode, int tlhcX,
                                 int tlhcY, int brhcX, int brhcY,
                                 double magnification, int applicability);


    /**
     * Sets the displayed area and size mode (for the current frame, the current image
     *  or all images referenced by the presentation state object).
     *  Treats the image as if it was neither rotated nor flipped.
     * @param sizeMode presentation size mode (from jDVPSPresentationSizeMode)
     * @param tlhcX displayed area top lefthand corner X
     * @param tlhcY displayed area top lefthand corner Y
     * @param brhcX displayed area bottom righthand corner X
     * @param brhcY displayed area bottom righthand corner Y
     * @param magnification magnification factor - ignored unless sizeMode==DVPSD_magnify.
     * @param applicability defines the applicability of the new displayed area definition.
     *    Possible choices are: DVPSB_currentFrame - current frame only,
     *    DVPSB_currentImage - all frames of current image (default),
     *    and DVPSB_allImages -  all images referenced by this presentation state.
     *    The last choice should be used with care
     *    because it will also cause the pixel spacing or pixel aspect ratio of the current image
     *    to be applied to all images referenced by the presentation state.
     *    (from jDVPSObjectApplicability)
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setImageRelativeDisplayedArea(int sizeMode, int tlhcX,
                                 int tlhcY, int brhcX, int brhcY,
                                 double magnification, int applicability);


    /**
     * Gets rectangular shutter left vertical edge.
     * May only be called if a rectangular shutter is active.
     * @return the rect shutter LV edge.
    */
    public native int getRectShutterLV ();


    /**
     * Gets rectangular shutter right vertical edge.
     * May only be called if a rectangular shutter is active.
     * @return the rect shutter RV edge.
    */
    public native int getRectShutterRV ();


    /**
     * Gets rectangular shutter upper horitontal edge.
     * May only be called if a rectangular shutter is active.
     * @return the rect shutter UH edge.
    */
    public native int getRectShutterUH ();


    /**
     * Gets rectangular shutter lower horiztonal edge.
     * May only be called if a rectangular shutter is active.
     * @return the rect shutter LH edge.
    */
    public native int getRectShutterLH ();


    /**
     * Sets and activates rectangular display shutter.
     * If a bitmap shutter is exists, it is deactivated if this
     * method returns successfully. If no shutter display value exists,
     * a default of 0 (black) is set.
     * @param lv the left vertical edge
     * @param rv the right vertical edge
     * @param uh the upper horizontal edge
     * @param lh the lower horizontal edge
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setRectShutter (int lv, int rv, int uh, int lh);


    /**
     * Checks if a display shutter of given type is active.
     * @param type the shutter type (from jDVPSShutterType)
     * @return true if this type of shutter is currently active.
    */
    public native boolean haveShutter (int type);


    /**
     * Sets the current Presentation LUT type.
     * DVPSP_table can only be used if the presentation state
     * contains a lookup table, i.e. if havePresentationLookupTable() returns OFTrue.
     * @param newType the new presentation LUT type (from jDVPSPresentationLUTType).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setCurrentPresentationLUT (int type);


    /**
     * Gets a description of the current presentation LUT.
     * For well-known presentation LUT shapes, a standard text
     * is returned. For presentation LUTs, the LUT explanation
     * is returned if it exists and a standard text otherwise.
     * This method never returns (null).
     * @return A string describing the current presentation LUT.
    */
    public native String getCurrentPresentationLUTExplanation();


    /**
     * Returns the LUT explanation of the presentation LUT
     * if it exists and is non-empty.
     * Otherwise returns (null).
     * @return A string with the explanation.
    */
    public native String getPresentationLUTExplanation();


    /**
     * Gets circular shutter center x component.
     * May only be called if a circular shutter is active.
     * @return the circ shutter center x component
    */
    public native int getCenterOfCircularShutter_x();


    /**
     * Gets circular shutter center y component.
     * May only be called if a circular shutter is active.
     * @return the circ shutter center y component
    */
    public native int getCenterOfCircularShutter_y();


    /**
     * Gets circular shutter radius.
     * May only be called if a circular shutter is active.
     * Note: In DICOM, a circular shutter must be rendered
     * with consideration of the image pixel aspect ratio.
     * The radius returned by this method is the number
     * of pixels describing a horizontal line from the
     * center of the circle to its border. See sample figures
     * in NEMA PS3.3:1998.
     * @return the circ shutter radius
    */
    public native int getRadiusOfCircularShutter();


    /**
     * Sets and activates circular display shutter.
     * If a bitmap shutter is exists, it is deactivated if this
     * method returns successfully. If no shutter display value exists,
     * a default of 0 (black) is set.
     * @param centerX the X component of the shutter center
     * @param centerY the Y component of the shutter center
     * @param radius the (horizontal) radius of the shutter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setCircularShutter(int centerX, int centerY, int radius);


    /**
     * Gets polygonal shutter number of points.
     * May only be called if a polygonal shutter is active.
     * @return the number of points describing the poly shutter
    */
    public native int getNumberOfPolyShutterVertices();


    /**
     * Sets polygonal display shutter origin.
     * This method creates a
     * polygonal shutter consisting only of a single point.
     * The polygonal display shutter is deactivated after this method.
     * @param x the x component of the shutter origin
     * @param y the x component of the shutter origin
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setPolyShutterOrigin(int x, int y);


    /**
     * Sets polygonal display shutter point.
     * This method adds a point to the polygonal display shutter,
     * which must already have at least an origin.
     * If the point set with this method is identical to the
     * origin of the shutter, the shutter is activated and
     * a possible bitmap shutter is deactivated. If no shutter display value exists,
     * a default of 0 (black) is set.
     * @param x the x component of the shutter origin
     * @param y the x component of the shutter origin
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int addPolyShutterVertex(int x, int y);


    /**
     * Get polygonal shutter point.
     * May only be called if a polygonal shutter is active.
     * Shutter points are relative to the origin 1\1 which is
     * the left upper edge of the image. Param xy must be an existing object before passing
     * it to this method!
     * @param idx the index of the shutter point, must be < getNumberOfPolyShutterVertices()
     * @param xy returns the x and y component of the point. xy must be created before passing to this method!
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getPolyShutterVertex(int idx, Point xy);


    /**
     * Gets the shutter presentation value. If no shutter display
     * value exists, a default of 0 (black) is set.
     * @return the shutter presentation value as 16bit unsigned P-value
    */
    public native int getShutterPresentationValue();


    /**
     * Sets the shutter presentation value to the given P-value.
     * @param pvalue the shutter presentation value.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setShutterPresentationValue(int pvalue);


    /**
     * Sorts the graphic layers according to
     * the graphic layer order. Layers with lower order have lower
     * indices after sorting which means that the layers can be
     * drawn to the screen in ascending index order.
     * Calling this routine may result in a re-numbering
     * of the graphic layer orders in a way that does not affect
     * their sequence.
    */
    public native void sortGraphicLayers();


    /**
     * Returns the number of graphic layers.
     * @return number of graphic layers
    */
    public native int getNumberOfGraphicLayers();


    /**
     * Gets the unique name of the graphic
     * layer with the given index. If no layer for the given
     * index exists, NULL is returned.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return name of the graphic layer
    */
    public native String getGraphicLayerName(int idx);


    /**
     * Gets the index of the graphic
     * layer with the given unique name. If no matching layer
     * is found, (-1) is returned.
     * @param name name of the graphic layer
     * @return index of the graphic layer
    */
    public native int getGraphicLayerIndex(String name);


    /**
     * Gets a description string for the graphic
     * layer with the given index. If no layer for the given
     * index exists, or if the description is empty, NULL is returned.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return description of the graphic layer
    */
    public native String getGraphicLayerDescription(int idx);


    /**
     * Checks whether a recommended display value (grayscale, color or both) for
     * the given graphic layer exists.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return true if a recommended display value exists
    */
    public native boolean haveGraphicLayerRecommendedDisplayValue(int idx);



    /**
     * Gets the recommended grayscale display value for the given graphic layer.
     * If the graphic layer contains an RGB display value but no grayscale
     * display value, the RGB value is implicitly converted to grayscale.
     * Param gray must be created before passing to this method!
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param gray the recommended display value as an unsigned 16-bit P-value
     *   is returned in this parameter. gray must be created before passing to this
     *   method!
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getGraphicLayerRecommendedDisplayValueGray(int idx, jIntByRef gray);


    /**
     * Gets the recommended RGB display value for the given graphic layer.
     * If the graphic layer contains a grayscale display value but no RGB
     * display value, the grayscale value is implicitly converted to RGB. The objects
     * r, g, b must be created before passing to this method!
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param r returns the R component of the recommended display value as unsigned 16-bit P-value.
     *        Must be created before passing to this method!
     * @param g returns the G component of the recommended display value as unsigned 16-bit P-value
     *        Must be created before passing to this method!
     * @param b returns the B component of the recommended display value as unsigned 16-bit P-value
     *        Must be created before passing to this method!
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getGraphicLayerRecommendedDisplayValueRGB(int idx, jIntByRef r, jIntByRef g, jIntByRef b);


    /**
     * Set graphic layer recommended grayscale display value for the given graphic layer.
     * This method does not affect (set or modify) the recommended RGB display value
     * which should be set separately.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param gray the recommended display value as an unsigned 16-bit P-value
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setGraphicLayerRecommendedDisplayValueGray(int idx, int gray);


    /**
     * Set graphic layer recommended RGB display value for the given graphic layer.
     * This method does not affect (set or modify) the recommended grayscale display value
     * which should be set separately.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param r the R component of the recommended display value as unsigned 16-bit P-value
     * @param g the G component of the recommended display value as unsigned 16-bit P-value
     * @param b the B component of the recommended display value as unsigned 16-bit P-value
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setGraphicLayerRecommendedDisplayValueRGB(int idx, int r, int g, int b);


    /**
     * Removes recommended display values for the given graphic layer.
     * @param rgb if true, the RGB recommended display value is removed
     * @param monochrome if true the monochrome recommended display value is removed
    */
    public native void removeGraphicLayerRecommendedDisplayValue(int idx, boolean rgb, boolean monochrome);


    /**
     * Assigns a new unique name to the given graphic layer.
     * The new name must be unique, otherwise an error code is returned.
     * Upon success, all references (for graphic annotations, curves and overlays) to the given
     * graphic layer are also renamed so that the presentation state remains
     * consistent.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param name the new name of the graphic layer. Must be a valid DICOM Code String.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setGraphicLayerName(int idx, String name);


    /**
     * Sets a new description to the given graphic layer.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param descr description of the graphic layer. Must be a valid DICOM Long String.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setGraphicLayerDescription(int idx, String descr);


    /**
     * Makes a graphic layer the highest layer for display.
     * This method assigns a graphic layer order higher than all
     * existing graphic layer orders to the given graphic layer,
     * sorts and renumbers the list of graphic layers. Upon success,
     * the given graphic layer is guaranteed to have the new index
     * (getNumberOfGraphicLayers()-1).
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int toFrontGraphicLayer(int idx);


    /**
     * Makes a graphic layer the lowest layer for display.
     * This method assigns a graphic layer order lower than all
     * existing graphic layer orders to the given graphic layer,
     * sorts and renumbers the list of graphic layers. Upon success,
     * the given graphic layer is guaranteed to have the new index 0.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int toBackGraphicLayer(int idx);


    /**
     * Creates a new graphic layer with the given
     * name and optional description.
     * The new name must be unique, otherwise an error code is returned.
     * The toFrontGraphicLayer() method is implicitly called for the new layer.
     * @param gLayer the name of the graphic layer. Must be a valid DICOM Code String.
     * @param gLayerDescription the optional description of the graphic layer.
     *    Must be a valid DICOM Long String.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int addGraphicLayer(String gLayer, String gLayerDescription);


    /**
     * Removes and deletes a graphic layer. All text, graphic, curve
     * and overlay objects on this graphic layer are also deleted or deactivated, respectively.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int removeGraphicLayer(int idx);


    /**
     * Deactivates display shutter of given type.
     * After a call to this method haveShutter(type) will return OFFalse.
     * @param type the shutter type (from jDVPSShutterType).
    */
    public native void removeShutter(int type);


    /**
     * Returns the number of text objects for the given
     * graphic layer.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return number of text objects
    */
    public native int getNumberOfTextObjects(int layer);


    /**
     * Gets the text object with the given index
     * on the given layer. If the text object or the graphic layer does
     * not exist, NULL is returned.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the text object, must be < getNumberOfTextObjects(layer)
     * @return the text object.
    */
    public jDVPSTextObject getTextObject(int layer, int idx)
    {
        long addr = getTextObjectN(layer, idx);
        if (addr == 0) return null;
        return new jDVPSTextObject (addr);
    }

    private native long getTextObjectN(int layer, int idx);


    /**
     * Creates a new text object on the given layer.
     * returns a pointer to the new text object. If the graphic layer
     * does not exist or if the creation of the text object fails, NULL is returned.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param applicability defines to which images/frames the new object applies.
     *   Default: all images referenced by the presentation state. (from jDVPSObjectApplicability)
     * @return a pointer to the new text object
    */
    public jDVPSTextObject addTextObject(int layer, int applicability)
    {
        long addr = addTextObjectN(layer, applicability);
        if (addr == 0) return null;
        return new jDVPSTextObject (addr);
    }

    private native long addTextObjectN (int layer, int applicability);


    /**
     * Deletes the text object with the given index
     * on the given layer.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the text object, must be < getNumberOfTextObjects(layer)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int removeTextObject(int layer, int idx);


    /**
     * Moves the text object with the given index on the given
     * layer to a different layer.
     * @param old_layer index of the graphic layer on which the text object is,
     *   must be < getNumberOfGraphicLayers()
     * @param idx index of the text object, must be < getNumberOfTextObjects(layer)
     * @param new_layer index of the graphic layer to which the text object is moved,
     *   must be < getNumberOfGraphicLayers()
     * @param applicability defines to which images/frames the new object applies from
     * now on. Default: all images referenced by the presentation state. (from jDVPSObjectApplicability)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int moveTextObject(int old_layer, int idx, int new_layer, int applicability);


    /**
     * Returns the number of graphic objects for the given
     * graphic layer.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return number of graphic objects
    */
    public native int getNumberOfGraphicObjects(int layer);


    /**
     * Gets the graphic object with the given index
     * on the given layer. If the graphic object or the graphic layer does
     * not exist, NULL is returned.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the graphic object, must be < getNumberOfGraphicObjects(layer)
     * @return the graphic object.
    */
    public jDVPSGraphicObject getGraphicObject(int layer, int idx)
    {
        long addr = getGraphicObjectN(layer, idx);
        if (addr == 0) return null;
        return new jDVPSGraphicObject (addr);
    }

    private native long getGraphicObjectN(int layer, int idx);


    /**
     * Creates a new graphic object on the given layer.
     * Returns a pointer to the new graphic object. If the graphic layer
     * does not exist or if the creation of the graphic object fails, NULL is returned.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param applicability defines to which images/frames the new object applies from
     * now on. Default: all images referenced by the presentation state. (from jDVPSObjectApplicability)
     * @return a pointer to the new graphic object
    */
    public jDVPSGraphicObject addGraphicObject(int layer, int applicability)
    {
        long addr = addGraphicObjectN(layer, applicability);
        if (addr == 0) return null;
        return new jDVPSGraphicObject (addr);
    }

    private native long addGraphicObjectN(int layer, int applicability);


    /**
     * Deletes the graphic object with the given index
     * on the given layer.
     * Only the objects that are applicable to the current (attached) image
     * and the selected frame number are used by this method.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the graphic object, must be < getNumberOfGraphicObjects(layer)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int removeGraphicObject(int layer, int idx);


    /**
     * Moves the graphic object with the given index on the given
     * layer to a different layer.
     * @param old_layer index of the graphic layer on which the graphic object is,
     *   must be < getNumberOfGraphicLayers()
     * @param idx index of the graphic object, must be < getNumberOfGraphicObjects(layer)
     * @param new_layer index of the graphic layer to which the graphic object is moved,
     *   must be < getNumberOfGraphicLayers()
     * @param applicability defines to which images/frames the new object applies from
     *   now on. Default: all images referenced by the presentation state. (from jDVPSObjectApplicability)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int moveGraphicObject(int old_layer, int idx, int new_layer, int applicability);


    /**
     * Detaches and frees the image attached to the presentation state.
    */
    public native void detachImage();


    /**
     * Sets the specific character set for this presentation state.
     * @param charset the new character set for this text object (from jDVPScharacterSet).
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int setCharset(int charset);


    /**
     * Gets the specific character set for this presentation state.
     * @return character set identifier (from jDVPScharacterSet).
    */
    public native int getCharset();


    /**
     * Gets the specific character set string for this presentation state.
     * @return character set if present, NULL otherwise
    */
    public native String getCharsetString();


    /**
     * Returns the number of curve activations for the given
     * graphic layer.
     * @param idx index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return number of curves
    */
    public native int getNumberOfCurves(int layer);


    /**
     * Gets the curve with the given index
     * on the given layer. If the curve or the graphic layer does
     * not exist, NULL is returned.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the curve, must be < getNumberOfCurves(layer)
     * @return a pointer to the curve
    */
    public jDVPSCurve getCurve(int layer, int idx)
    {
        long addr = getCurveN(layer, idx);
        if (addr == 0) return null;
        return new jDVPSCurve (addr);
    }

    private native long getCurveN (int layer, int idx);


    /**
     * Returns the number of curves in the attached image
     * that could be activated in the presentation state.
     * @return number of available curves
    */
    public native int getNumberOfCurvesInImage();


    /**
     * Gets the curve with the given index
     * from the attached image. If the curve does
     * not exist, NULL is returned.
     * @param idx index of the curve, must be < getNumberOfCurvesInImage()
     * @return the curve
    */
    public jDVPSCurve getCurveInImage(int idx)
    {
        long addr = getCurveInImageN(idx);
        if (addr == 0) return null;
        return new jDVPSCurve (addr);
    }

    private native long getCurveInImageN (int idx);


    /**
     * Activates curve in presentation state.
     * This method adds an activation for the given curve from the
     * attached image to the given graphic layer in the presentation state.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param curveidxinimage index of the curve in the attached image,
     *   must be < getNumberOfCurvesInImage()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int addCurve(int layer, int curveidxinimage);


    /**
     * Deletes the curve activation with the given index
     * on the given layer.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the curve activation, must be < getNumberOfCurves(layer)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int removeCurve(int layer, int idx);


    /**
     * Moves the curve activation with the given index on the given
     * layer to a different layer.
     * @param old_layer index of the graphic layer on which the curve is,
     *   must be < getNumberOfGraphicLayers()
     * @param idx index of the curve activation, must be < getNumberOfCurves(layer)
     * @param new_layer index of the graphic layer to which the curve is moved,
     *   must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int moveCurve(int old_layer, int idx, int new_layer);


	/**
	 * Check if a VOI window is currently active.
     * @return true if a VOI window is active
    */
    public native boolean haveActiveVOIWindow();


    /**
     * Check if a VOI LUT is currently active.
     * @return OFTrue if a VOI LUT is active
    */
    public native boolean haveActiveVOILUT();


    /**
     * Returns a description string for a currently active VOI transform.
     * If no description is available, NULL is returned.
     * @return a pointer to a string or NULL.
    */
    public native String getCurrentVOIDescription();


    /**
     * Gets the width of the current VOI window.
     * May only be called if haveActiveVOIWindow() is OFTrue.
     * @param w the window width is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getCurrentWindowWidth(jDoubleByRef w);


    /**
     * Get the center of the current VOI window.
     * May only be called if haveActiveVOIWindow() is OFTrue.
     * @param c the window center is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getCurrentWindowCenter(jDoubleByRef c);


    /**
     * Gets the number of VOI LUTs available in the attached image.
     * @return number of VOI LUTs.
    */
    public native int getNumberOfVOILUTsInImage();

    /**
     * Gets the number of VOI Windows available in the attached image.
     * @return number of VOI Windows.
    */
    public native int getNumberOfVOIWindowsInImage();


    /**
     * Returns a description string for the given VOI LUT in the attached
     * image.
     * If no description for the given index is available, NULL is returned.
     * @param idx index, must be < getNumberOfVOILUTsInImage()
     * @return a pointer to a string or NULL.
    */
    public native String getDescriptionOfVOILUTsInImage(int idx);


    /**
     * Returns a description string for the given VOI Window
     * in the attached image.
     * If no description for the given index is available, NULL is returned.
     * @param idx index, must be < getNumberOfVOIWindowsInImage()
     * @return a pointer to a string or NULL.
    */
    public native String getDescriptionOfVOIWindowsInImage(int idx);


    /**
     * Activates one of the VOI LUTs from the attached image.
     * @param idx index of the VOI transform, must be < getNumberOfVOILUTsInImage().
     * @param applicability defines the applicability of the new VOI transform (from jDVPSObjectApplicability)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setVOILUTFromImage(int idx, int applicability);


    /**
     * Activates one of the VOI Windows from the attached image.
     * @param idx index of the VOI transform, must be < getNumberOfVOIWindowsInImage().
     * @param applicability defines the applicability of the new VOI transform (from jDVPSObjectApplicability)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setVOIWindowFromImage(int idx, int applicability);


    /**
     * Sets a user defined VOI window center and width.
     * @param wCenter the window center
     * @param wWidth  the window width
     * @param description an optional description. Default: absent.
     * @param applicability defines the applicability of the new VOI transform (from jDVPSObjectApplicability)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int setVOIWindow(double wCenter, double wWidth, String description, int applicability);


    /**
     * Deactivates the current VOI transformation.
     * @param applicability defines the applicability of the new VOI transform (from jDVPSObjectApplicability)
     * After a call to this method, no VOI transform is active.
    */
    public native void deactivateVOI(int applicability);


    /** stores VOI lookup table with a gamma curve shape in the presentation state.
     *  If a VOI window is currently active the center and width values are used to specify
     *  the number of LUT entries and the first value mapped, otherwise the full pixel range
     *  is used. The output range of the LUT is always 16 bit (data is stored as OW).
     *  This method stores a VOI lookup table in the presentation state and activates it.
     *  The LUT is copied to the presentation state.
     *  If the method returns an error code, an old LUT is left unchanged.
     *  The applicability of the VOI LUT is controlled by the applicability parameter.
     *  @param gammaValue gamma value used to create the VOI LUT data
     *  @param applicability defines the applicability of the new VOI transform.
     *  @return EC_Normal if successful, an error code otherwise.
     */
    public native int setGammaVOILUT(double gammaValue, int applicability);


    /**
     * Gets the number of overlays that are currently activated
     * on the given graphic layer.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @return number of active overlays
    */
    public native int getNumberOfActiveOverlays(int layer);


    /**
     * Gets the repeating group number of the given activated overlay.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfActiveOverlays().
     * @return repeating group number if found, 0 otherwise.
    */
    public native int getActiveOverlayGroup(int layer, int idx);


    /**
     * Gets the overlay label of the given activated overlay.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfActiveOverlays().
     * @return label string if it exists, NULL otherwise.
    */
    public native String getActiveOverlayLabel(int layer, int idx);


    /**
     * Gets the overlay description of the given activated overlay.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfActiveOverlays().
     * @return description string if it exists, NULL otherwise.
    */
    public native String getActiveOverlayDescription(int layer, int idx);


    /**
     * Checks whether the given activated overlay is a ROI
     * (region of interest) overlay.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfActiveOverlays().
     * @return true if overlay exists and is ROI, false otherwise.
    */
    public native boolean activeOverlayIsROI(int layer, int idx);


    /**
     * Gets one overlay bitmap.
     * This method may only be called if has beed called before
     * for the current presentation state settings.
     * The parameter is an object from class jDVPrStateParam_GetOverlayData.
     * It must be created and set with the IN-params before passing to this
     * method! OUT-params will be set by this method.
     * @param od.layer index of the graphic layer on which this overlay is
     *   activated, must be < getNumberOfGraphicLayers().
     * @param od.idx index of the overlay activation on the given layer,
     *   must be < getNumberOfActiveOverlays(layer).
     * @param od.overlayData upon success a pointer to the overlay plane is passed back
     *   in this parameter. The overlay plane is organized as one byte per pixel.
     *   The byte values are already transformed from pvalues to DDLs.
     * @param od.width upon success the width of the overlay bitmap in pixels is returned in this parameter.
     * @param od.height upon success the height of the overlay bitmap in pixels is returned in this parameter.
     * @param od.left upon success the horizontal position of the overlay relative to the image
     *  is returned. 0 means that the overlay is left aligned with the image.
     *  Since the overlay is cropped at the borders of the image, values < 0 are impossible.
     * @param od.top upon success the vertical position of the overlay relative to the image
     *  is returned.
     * @param od.isROI returns OFTrue if the overlay is ROI, OFFalse if the overlay is Graphic.
     * @param od.frame frame number of the image. Since overlays can differ for different image frames,
     *  the image frame also selects the overlays. Default: first frame.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getOverlayData(jDVPrStateParam_GetOverlayData od);


    /**
     * Gets the number of overlays which are embedded in the
     *  image currently attached to the presentation state. Overlays in the image are counted only
     *  if they are not shadowed by overlays that are embedded in the presentation state
     *  and use the same repeating group number.
     *  @return number of overlays in attached image
    */
    public native int getNumberOfOverlaysInImage();


    /**
     * Gets the repeating group number of the given overlay in the attached image.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return repeating group number if found, 0 otherwise.
    */
    public native int getOverlayInImageGroup(int idx);


    /**
     * Gets the overlay label of the given overlay in the attached image.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return label string if it exists, NULL otherwise.
    */
    public native String getOverlayInImageLabel(int idx);


    /**
     * Gets the overlay description of the given overlay in the attached image.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return description string if it exists, NULL otherwise.
    */
    public native String getOverlayInImageDescription(int idx);


    /**
     * Gets the index of the activation layer on which the given
     * overlay from the attached image is activated.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return layer index (which is < getNumberOfGraphicLayers()) if overlay exists
     *    and is activated, (-1) otherwise.
    */
    public native int getOverlayInImageActivationLayer(int idx);


    /**
     * Checks whether the given overlay in the attached image is a ROI
     * (region of interest) overlay.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return true if overlay exists and is ROI, false otherwise.
    */
    public native boolean overlayInImageIsROI(int idx);


    /**
     * Gets the number of overlays which are embedded in the
     * presentation state.
     * @return number of overlays in presentation state
    */
    public native int getNumberOfOverlaysInPresentationState();


    /**
     * Gets the repeating group number of the given overlay in the presentation state.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return repeating group number if found, 0 otherwise.
     */
    public native int getOverlayInPresentationStateGroup(int idx);


    /**
     * Gets the overlay label of the given overlay in the presentation state.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return label string if it exists, NULL otherwise.
    */
    public native String getOverlayInPresentationStateLabel(int idx);


    /**
     * Gets the overlay description of the given overlay in the presentation state.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return description string if it exists, NULL otherwise.
    */
    public native String getOverlayInPresentationStateDescription(int idx);


    /**
     * Gets the index of the activation layer on which the given
     * overlay from the presentation state is activated. If an overlay is used
     * as a bitmap shutter, it is reported as being not activated by this method.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return layer index (which is < getNumberOfGraphicLayers()) if overlay exists
     *    and is activated, DVPS_IDX_NONE otherwise.
    */
    public native int getOverlayInPresentationStateActivationLayer(int idx);


    /**
     * Checks if the given overlay in the presentation state
     * is currently activated as a bitmap shutter.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return true if overlay exists and is activated as bitmap shutter, false otherwise.
    */
    public native boolean overlayIsBitmapShutter(int idx);


    /**
     * Checks whether the given overlay in the presentation state is a ROI
     * (region of interest) overlay.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return true if overlay exists and is ROI, false otherwise.
    */
    public native boolean overlayInPresentationStateIsROI(int idx);


    /**
     * Removes an overlay from the presentation state.
     * If the overlay is activated, the activation is also removed.
     * Since overlays in the presentation state can shadow overlays in the attached image,
     * execution of this method may change the number of overlays reported in the attached image.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int removeOverlayFromPresentationState(int idx);


    /**
     * Changes the repeating group used for an overlay in the presentation state.
     * Since overlays in the presentation state can shadow overlays in the attached image,
     * execution of this method may change the number of overlays reported in the attached image.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @param newGroup new repeating group number 0x6000-0x601F (even). If this optional parameter is omitted,
     *   the method attemps to automatically determine a new group number so that no overlay in the
     *   attached image is shadowed any more. If this is impossible, the method fails and leaves
     *   the overlay repeating group unchanged.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int changeOverlayGroupInPresentationState(int idx, int newGroup);


    /**
     * Checks if an overlay from the presentation state is suitable
     * for use as a bitmap shutter. An overlay is suitable if it is a graphic overlay
     * with the same size as the attached image and with the origin 1\1.
     * This method does not check wether the overlay is already activated as overlay
     * or bitmap shutter.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return true if overlay can be used as display shutter.
    */
    public native boolean overlayIsSuitableAsBitmapShutter(int idx);


    /**
     * Activates the given overlay from the attached image
     * on the given graphic layer.
     * If the overlay is already activated (i.e.
     * getOverlayInImageActivationLayer(idx) != DVPS_IDX_NONE) this method fails.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfOverlaysInImage().
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int activateOverlayInImage(int layer, int idx);


    /**
     * Activates the given overlay from the presentation state
     * on the given graphic layer.
     * If the overlay is already activated or used as a bitmap overlay (i.e.
     * getOverlayInPresentationStateActivationLayer(idx) != DVPS_IDX_NONE or
     * overlayIsBitmapShutter(idx) == OFTrue) this method fails.
     * @param layer index of the graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int activateOverlayInPresentationState(int layer, int idx);


    /**
     * Activates an overlay as bitmap shutter.
     * The overlay must not be activated on a graphic layer (i.e.
     * getOverlayInPresentationStateActivationLayer(idx) != DVPS_IDX_NONE,
     * otherwise this method fails.
     * @param idx index of the overlay, must be < getNumberOfOverlaysInPresentationState().
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int activateOverlayAsBitmapShutter(int idx);


    /**
     * Removes activation for an overlay which may be
     * embedded in the attached image or part of the presentation state.
     * @param layer index of the graphic layer on which this overlay is
     *   activated, must be < getNumberOfGraphicLayers().
     * @param idx index of the overlay activation on the given layer,
     *   must be < getNumberOfActiveOverlays(layer).
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int deactivateOverlay(int layer, int idx);


    /**
     * Moves the overlay activation with the given index on the given
     * layer to a different layer.
     * @param old_layer index of the graphic layer on which the curve is,
     *   must be < getNumberOfGraphicLayers()
     * @param idx index of the overlay activation, must be < getNumberOfActiveOverlays(layer)
     * @param new_layer index of the graphic layer to which the curve is moved,
     *   must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int moveOverlay(int old_layer, int idx, int new_layer);



    /**
     * Gets smallest and biggest possible pixel value in the attached image.
     * These values are defined as the smallest and biggest number that
     * could possibly be contained in the image after application of the Modality transform,
     * but before any VOI, Presentation or Barten transform.
     * This method may only be called when an image is attached to the
     * presentation state.
     * @param minValue upon success, the smallest value is returned in this parameter.
     * @param maxValue upon success, the biggest value is returned in this parameter.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getImageMinMaxPixelRange(jDoubleByRef minValue, jDoubleByRef maxValue);


    /**
     * Gets smallest and biggest occuring pixel value in the attached image.
     * These values are defined as the smallest and biggest number that
     * are actually contained in the image after application of the Modality transform,
     * but before any VOI, Presentation or Barten transform.
     * This method may only be called when an image is attached to the
     * presentation state.
     * @param minValue upon success, the smallest value is returned in this parameter.
     * @param maxValue upon success, the biggest value is returned in this parameter.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getImageMinMaxPixelValue(jDoubleByRef minValue, jDoubleByRef maxValue);


    /**
     * Gets the width of the attached image.
     * The rotation status of the presentation state is not taken
     * into account, i.e. the width of an unrotated image is returned.
     * This method may only be called when an image is attached to the
     * presentation state. Param width must be created before passing to
     * this method.
     * @param width upon success, the image width (pixels) is returned in this parameter.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getImageWidth(jIntByRef width);


    /**
     * Gets the height of the attached image.
     * The rotation status of the presentation state is not taken
     * into account, i.e. the height of an unrotated image is returned.
     * This method may only be called when an image is attached to the
     * presentation state. Param height must be created before passing to
     * this method.
     * @param height upon success, the image height (pixels) is returned in this parameter.
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int getImageHeight(jIntByRef height);


    /**
     * Gets the number of image references in all series managed by this list.
     * @return number of image references
    */
    public native int numberOfImageReferences();


    /**
     * Gets an image reference with the given index.
     * @param idx index, must be < numberOfImageReferences().
     * @param param.studyUID the Study Instance UID is returned in this string
     * @param param.seriesUID the Series Instance UID is returned in this string
     * @param param.sopclassUID the SOP Class UID is returned in this string
     * @param param.instanceUID the SOP Instance UID is returned in this string
     * @param param.frames the list of frames is returned in this string
     * @param param.aetitle the aetitle
     * @param param.filesetID the fileset ID
     * @param param.filesetUID the fileset UID
     * @return EC_Normal if successful, an error code otherwise (from jE_Condition).
    */
    public native int getImageReference(int idx, jDVPrStateParam_GetImageReference param);


    /**
     * Exchanges the layer order of the two graphic layers with
     * the given indices. This method does not sort or renumber
     * the graphic layers.
     * @param idx1 index of the first graphic layer, must be < getNumberOfGraphicLayers()
     * @param idx2 index of the second graphic layer, must be < getNumberOfGraphicLayers()
     * @return EC_Normal upon success, an error code otherwise (from jE_Confition).
    */
    public native int exchangeGraphicLayers(int idx1, int idx2);


    /**
     * Converts a 16-bit P-Value to an 8-bit DDL value for on-sceen display.
     * If a display function is set and enabled (see setDisplayTransform()),
     * the DDL is corrected for the nonlinearity of the display, otherwise
     * a simple linear mapping is performed.
     * @param pvalue P-Value 0..0xFFFF
     * @param bits bits used(8 or 12). Default is 8.
     * @return display driving level (DDL), 0..0xFF
    */
    public native short convertPValueToDDL (int pvalue, int bits);


    /**
     * Generates a new SOP Instance UID which is used when writing the
     * Presentation State to file.
     * @return new SOP Instance UID if successfully set
    */
    public native String createInstanceUID();


    /**
     * Same as method above apart from the fact that the storage area is handled
     * externally.
     * @param pixelData pointer to storage area where the pixel data is copied to.
     *   The storage area must be allocated and deleted from the calling method.
     * @param size specifies size of the storage area in bytes.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int getPixelData(byte[] pixelData, long size);


    /**
     * Inverts image by changing presentation state LUT or presentation state LUT
     * shape. Pixel data has to be re-get after this transformation.
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition).
    */
    public native int invertImage();


    /**
     * Checks whether image is inverse (shape, plut or mono1).
     * @return OFTrue if image is inverse, OFFalse otherwise.
    */
    public native boolean isInverse();


    /**
     * Gets number of bytes used for the print bitmap.
     * (depends on width, height and depth)
     * @return number of bytes used for the print bitmap
    */
    public native long getPrintBitmapSize();


    /**
     * Sets the minimum print bitmap width and height.
     * Smaller images are scaled up by an appropriate integer factor. Both maximum
     * values need to be twice greater than the maximum of the minimum values.
     * @param width minimum width of print bitmap (in pixels)
     * @param height minimum height of print bitmap (in pixels)
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition)
    */
    public native int setMinimumPrintBitmapWidthHeight(long width, long height);


    /**
     * Sets the maximum print bitmap width and height.
     * Larger images are scaled down by an appropriate integer factor. Both maximum
     * values need to be twice greater than the maximum of the minimum values.
     * @param width maximum width of print bitmap (in pixels)
     * @param height maximum height of print bitmap (in pixels)
     * @return EC_Normal upon success, an error code otherwise  (from jE_Condition)
    */
    public native int setMaximumPrintBitmapWidthHeight(long width, long height);


    /**
     * Gets width and height of print bitmap.
     * Bitmap size depends on implicit scaling, a heuristic is used for very small images
     * The return values depend on the current minimum/maximum print bitmaps width/height values!
     * @param width upon success, the image width (in pixels) is returned in this parameter
     * @param height upon success, the image height (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition)
    */
    public native int getPrintBitmapWidthHeight(jIntByRef width, jIntByRef height);


    /**
     * Gets width of print bitmap.
     * Bitmap size depends on implicit scaling, a heuristic is used for very small images.
     * The return value depends on the current minimum/maximum print bitmaps width/height values!
     * @param width upon success, the image width (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition)
    */
    public native int getPrintBitmapWidth(jIntByRef width);


    /**
     * Gets height of print bitmap.
     * bitmap size depends on implicit scaling, a heuristic is used for very small images
     * The return value depends on the current minimum/maximum print bitmaps width/height values!
     * @param height upon success, the image height (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise (from jE_Condition)
    */
    public native int getPrintBitmapHeight(jIntByRef height);


    /**
     * Gets the presentation pixel aspect ratio for the print bitmap.
     * Pixel aspect ratio is defined here as the width of a pixel divided
     * by the height of a pixel (x/y).
     * @return pixel aspect ratio
    */
    public native double getPrintBitmapPixelAspectRatio();


    /**
     * Gets requested image size for print bitmap.
     * If the presentation state mode is DVPSD_trueSize, this method computes
     * the true physical width (in mm) of the print image (under consideration of the
     * rotation status) and writes it to the requestedImageSize string.
     * @param requestedImageSize requested image size is written to this parameter upon
     *   successful return. Otherwise string is empty upon return.
     * @return EC_Normal upon success, an error code otherwise(from jE_Condition)
    */
    public native int getPrintBitmapRequestedImageSize(jStringByRef requestedImageSize);


    /**
     * Writes the bitmap data into the given buffer.
     * The bitmap has the format: 12 bits stored and 16 bits allocated. This method is used
     * to create the preformatted bitmap where the annotations are later burned in.
     * Implicit scaling is performed if the bitmap is too small (see minimum bitmap size).
     * The storage area must be allocated and deleted from the calling method.
     * @param bitmap pointer to storage area where the pixel data is copied to.
     * @param size specifies size of the storage area in bytes
     * @return EC_Normal upon success, an error code otherwise(from jE_Condition)
    */
    public native int getPrintBitmap(short[] bitmap, long size);


    /**
     * Gets the modality of the attached image.
     * @return modality string if it exists, NULL or empty string otherwise.
    */
    public native String getCurrentImageModality();


    /**
     * Resets the Presentation LUT to the default LUT shape
     * which is DVPSP_identity for MONOCHROME2 images and DVPSP_inverse for MONOCHROME1.
     * DVPSP_table can only be used if the presentation state
     * @return EC_Normal if successful, an error code otherwise.
    */
    public native int setDefaultPresentationLUTShape();


    /**
     * Creates a new preview image based on the current image and pstate.
     * The maximum size of this image is specified by the two parameters maxWidth and maxHeight.
     * The actual size should be determined using one of the following appropriate methods (e.g.
     * getPreviewImageWidthHeight) since the original pixel aspect ratio is alsways considered.
     * The preview image includes all grayscale and spatial transformations performed on the
     * current image so far. The method renderPixelData also renders the preview image (if existing).
     * Therefore the preview image is always held consistent with the current image.
     * Overlays, bitmapped shutters and any other annotations are not rendered into the preview image.
     * @param width the maximum width used to create the preview image
     * @param height the maximum height used to create the preview image
     * @param clipMode specifies whether to clip the preview image to the displayed area (not implemented!)
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int createPreviewImage(int maxWidth, int maxHeight, boolean clipMode);


    /**
     * Deletes and disables the current preview image.
    */
    public native void deletePreviewImage();


    /**
     * Gets number of bytes used for the preview image bitmap.
     * (depends on width and height)
     * @return number of bytes used for the preview image bitmap
    */
    public native long getPreviewImageSize();


    /**
     * Gets current width and height of the preview image.
     * @param width upon success, the image width (in pixels) is returned in this parameter
     * @param height upon success, the image height (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int getPreviewImageWidthHeight(jIntByRef width, jIntByRef height);


    /**
     * Gets current width of the preview image.
     * @param width upon success, the image width (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int getPreviewImageWidth(jIntByRef width);


    /**
     * Gets current height of the preview image.
     * @param height upon success, the image height (in pixels) is returned in this parameter
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int getPreviewImageHeight(jIntByRef height);


    /**
     * Writes the bitmap data of the preview image into the given buffer.
     * The storage area must be allocated and deleted from the calling method.
     * @param bitmap pointer to storage area where the pixel data is copied to
     * @param size specifies size of the storage area in bytes
     * @return EC_Normal upon success, an error code otherwise
    */
    public native int getPreviewImageBitmap(byte[] bitmap, long size);

    /**
     * Gets the currently selected display transform.
     * Display transform will only be performed if switched on _and_
     * a valid monitor characteristics description exists.
     * Default after creation of a presentation state is "on".
     * @return current display transform if on, DVPSD_none if off (of class jDVPSDisplayTransform).
    */
    public native int getDisplayTransform();


    /**
     * Activates or deactivates display correction.
     * Display transform will only be performed if switched on
     * _and_ a valid display function object exists.
     * @param transform display transform to be set, DVPSD_none to switch off.(of class jDVPSDisplayTransform).
    */
    public native void setDisplayTransform(int transform);

    // -------------------------------------------------------------
    // -- Method for transformations of the image
    //
    // The following methods are not part of the relating c++-class DVPresentationState !!!
    //


    /**
     * Gets the pixel data and perfom zooming and scaling with all existing layers.
     * A part of the original data is chosen by point TLHC, a zoom factor and a scaling for
     * x and y. The output size is defined by the array size passed to this method.
     * @return true on success, false otherwise.
    */
    public native boolean getScreenData (jDVPrStateParam_GetScreenData info);

}

/*
 *  CVS Log
 *  $Log: jDVPresentationState.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
