/*
 *
 *  Copyright (C) 1999-2003, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package J2Ci;

/** 
 * The class <em>jDVPSPeerType</em> represents the C++-enumeration
 * DVPSPeerType.
 *
 * @author 	Andreas Schroeter, Joerg Riesmeier
*/
public class jDVPSPeerType
{
  /** 
   * Storage SCP peer
  */
  public static final int DVPSE_storage = 0;
  
  /**
   * local Storage SCP
  */
  public static final int DVPSE_receiver = 1;

  /** 
   * remote Print Management SCP
  */
  public static final int DVPSE_printRemote = 2;
    
  /** 
   * local Print Management SCP
  */
  public static final int DVPSE_printLocal = 3;  

  /** 
   * local or remote Print Management SCP
  */
  public static final int DVPSE_printAny = 4;

  /** 
   * any type of peer
  */
  public static final int DVPSE_any = 5;
  
}

/*
 *  CVS Log
 *  $Log: jDVPSPeerType.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
