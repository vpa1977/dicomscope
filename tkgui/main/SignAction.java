/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.*;
import J2Ci.*;
/**
* Contains the GUI for the status line.
* The is context sensitve. For each DSComponentType
* There is a seperate view.
*/
public  class SignAction extends AbstractAction
{
    
    public static final String REPORT="Report";
    public static final String IMAGE="Image";
    public static final String PRESENTATIONSTATE="Presentation State";
    public static final String COMBINATION="Combination";
    
    /**
    * Contains the current status.
    * The possible values are defined in jDVPSSignatureStatus 
    */
    private int status = 0;
    
    /** no digital signatures are present
     */
    private Icon unsignedIcon;

    /** one or more digital signatures are present and have been successfully verified
     */

    private Icon signed_OKIcon;

    /** one or more digital signatures are present and at least one of them
     *  could not be successfully verified because it was corrupt or created
     *  with a certificate issued by an unknown CA.
     */
    private Icon signed_corruptIcon;
    
    /** one or more digital signatures are present and at least one of them
     *  could not be successfully verified because it was corrupt or created
     *  with a certificate issued by an unknown CA.
     */
    private Icon signed_unknownCAIcon;
    
    /**
    * Contains the type of the String
    */
    private String type;
    viewer.gui.SignatureOverview signatureOverview;
    public SignAction( viewer.gui.SignatureOverview signatureOverview ,
                            String type,
                            Icon unsignedIcon,
                            Icon signed_corruptIcon,
                            Icon signed_OKIcon,
                            Icon signed_unknownCAIcon)
    { 
        super();
        this.signatureOverview = signatureOverview;
        this.type = type;
        putValue(Action.SMALL_ICON, unsignedIcon);
        this.signed_corruptIcon=signed_corruptIcon;
        this.unsignedIcon = unsignedIcon;
        this.signed_OKIcon = signed_OKIcon;
        this.signed_unknownCAIcon = signed_unknownCAIcon;
        
    }
    public void actionPerformed(java.awt.event.ActionEvent e)
    {
        signatureOverview.setVisible(true,  type); 
    }
    public void changeStatus(int status)
    {
        switch (status)
        {
            case jDVPSSignatureStatus.DVPSW_unsigned: 
                putValue(Action.SMALL_ICON, unsignedIcon);
                                             
                break;
            case jDVPSSignatureStatus.DVPSW_signed_OK: 
                putValue(Action.SMALL_ICON, signed_OKIcon);
                break;
            case jDVPSSignatureStatus.DVPSW_signed_corrupt: 
                putValue(Action.SMALL_ICON, signed_corruptIcon);
                break;
            
            case jDVPSSignatureStatus.DVPSW_signed_unknownCA: 
                putValue(Action.SMALL_ICON, signed_unknownCAIcon);
                break;
                
            default:
                throw new IllegalArgumentException("Invalid value for status: "+ status);
        }
        this.status = status;
        
    }
    
                            
}
/*
 *  CVS Log
 *  $Log: SignAction.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
