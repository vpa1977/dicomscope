/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.io.*;


/**
* DSEvents for Actions
*/
public class DbActionEvent extends DSEvent 
{
    public static final int LOAD_DB = 0;
    public static final int LOAD_PS = 1;
    public static final int LOAD_IM = 2;
    public static final int SAVE_DB = 3;
    public static final int SAVE_AS = 4;
    public static final int SAVE_SCREEN = 5;
    public static final int DELETE = 6;
    public static final int REFRESH = 7;
    public static final int SENDTO = 8;
    public static final int LOAD_SR = 9;
    public static final int PRINTHC = 100;
    public static final int DUMP = 101;
    public static final int CHECKIOD = 102;
    public static final int PRINTSTTOPRINTER = 103;
    public static final int PRINTST = 104;
    public int type;
    
    public static final int LOAD_PS_FOR_SR = 300;
    public static final int LOAD_IMAGE_FOR_SR = 301;
    public static final int LOAD_SR_FOR_SR = 302;
    public static final int SAVE_DB_SR = 303;
    
    
    public String instanceUid;
    public String sopClassUid;
    public String psInstanceUid;
    public String psSOPClassUid;
    public int[] frames = null;
    public boolean savePS = true;
    public DbActionEvent(Object o, int type)
    {
        super(o);
        this.type = type;
    }
    public DbActionEvent(Object o, int type, boolean savePS)
    {
        super(o);
        this.type = type;
        this.savePS = savePS;
    }
    public DbActionEvent(Object o, int type, String sopClassUid,String instanceUid,int[] frames )
    {
        super(o);
        this.type = type;
        this.sopClassUid = sopClassUid;
        this.instanceUid = instanceUid;
        this.frames = frames;
    }
    public DbActionEvent(Object o, int type, String sopClassUid,String instanceUid, String psSOPClassUid, String psInstanceUid, int[] frames)
    {
        super(o);
        this.type = type;
        this.sopClassUid = sopClassUid;
        this.instanceUid = instanceUid;
        this.psInstanceUid = psInstanceUid;
        this.psSOPClassUid = psSOPClassUid;
        this.frames = frames;
    }
     
}
/*
 *  CVS Log
 *  $Log: DbActionEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
