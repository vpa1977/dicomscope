/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import J2Ci.*;

/**
* This class contains all attributes of codes structured report code.
* Furthermore an attriute is inseted calling Context Group.
* A Context Group groups the codes so that they can be used 
* in a special context. For example there can be a Context Group
* for all measurements.
*
* @author Klaus Kleber
*/
public class SRCode 
{
    public static final String CONTEXT_GROUP_NAME_DOCUMENT_TITLE = "Document Title";       
    public static final String CONTEXT_GROUP_NAME_SECTION_HEADING = "Section Heading";       
    public static final String CONTEXT_GROUP_NAME_REPORT_ELEMENT = "Report Element";       
    public static final String CONTEXT_GROUP_NAME_IMAGE_REFERENCE = "Image Reference";       
    public static final String CONTEXT_GROUP_NAME_TEMPORALCOORD = "Temporal coordinate";       
    public static final String CONTEXT_GROUP_NAME_SPATIALCOORD = "Spatial coordinate";       
    public static final String CONTEXT_GROUP_NAME_DATE = "Date";       
    public static final String CONTEXT_GROUP_NAME_TIME = "Time";       
    public static final String CONTEXT_GROUP_NAME_DATETIME = "Date/Time";       
    public static final String CONTEXT_GROUP_NAME_ORGANIZATIONAL_ROLE = "Organizational Role";       
    public static final String CONTEXT_GROUP_NAME_NUMERIC_MEASUREMENTS = "Numeric Measurement";       
    public static final String CONTEXT_GROUP_NAME_MEASUREMENT_UNIT = "Measurement Unit";       
    public static final String CONTEXT_GROUP_NAME_UID_REFERENCE = "UID Reference";       
    public static final String CONTEXT_GROUP_NAME_CODE = "Code";       
    public static final String CONTEXT_GROUP_NAME_WAVEFORM = "Waveform";       
    public static final String CONTEXT_GROUP_NAME_COMPOSITE = "Composite";       
    /**
    * Contains the Context Group
    */
    private  String contextGroup;
    
    /**
    * Contains the Coding Scheme Designator
    */
    private String codingSchemeDesignator;
    
    /**
    * Contains the Coding Scheme Version
    */
    private String codingSchemeVersion;
    
    /**
    * Contains the Code Value
    */
    private String codeValue;
    
    /**
    * Contains the Code Meaning
    */
    private String codeMeaning;
    
    
    /**
    * Contains the  name of the Context Group
    */
    private String identifier;
    
    
    /**
    * If the code is not a default code.
    * A code is not a default code if the user 
    * inserted the code manually or if a SR report
    * contains a code which is unknown.
    */
    private boolean isDefault = true;
    
    public SRCode()
    {}
    public SRCode( String contextGroup,
                    String codingSchemeDesignator,
                    String codingSchemeVersion,
                    String codeValue,
                    String codeMeaning)
    {
        this.codeMeaning = codeMeaning;
        this.codeValue = codeValue;
        if(codeValue==null) codeValue="";
        this.codingSchemeDesignator = codingSchemeDesignator;
        if(codingSchemeDesignator==null) codingSchemeDesignator="";
        this.codingSchemeVersion = codingSchemeVersion;
        if(codingSchemeVersion==null) codingSchemeVersion="";
        this.contextGroup = contextGroup;
        
        
    }
    public String toValueString()
    {
        return new String("Context Group: " + contextGroup+ 
                         "\nCoding Scheme Designator: " + codingSchemeDesignator+
                         "\nCoding Scheme Version: " + codingSchemeVersion+
                         "\nCode Value: " + codeValue+
                         "\nCode Meaning: " + codeMeaning);
    }
    
    /**
    * Sets the isDefault value. 
    * A code is not a default code if the user 
    * inserted the code manually or if a SR report
    * contains a code which is unknown.
    */  
    public void  setDefault(boolean isDefault)
    {
        this.isDefault = isDefault;
    }
    public String getIdentifier()
    {
        return codeValue+"/" + codingSchemeDesignator+"/"+codingSchemeVersion;
    }
    public String toString()
    {
        if (!isDefault) return "* " + codeMeaning;
        else return codeMeaning;
    }
         
    
    /**
    * Returns the Code Value
    * @return The Code Value
    */
    public String getCodeValue()
    {
        return codeValue;
    }
    /**
    * Returns the Code Meaning
    * @return The Code Meaning
    */
    public String getCodeMeaning()
    {
        return codeMeaning;
    }
    
    /**
    * Returns the Context Group
    * @return The Context Group
    */
    public String getContextGroup()
    {
        return contextGroup;
    }
    /**
    * Returns the Coding Scheme Designator
    * @return The Coding Scheme Designator
    */
    public String getCodingSchemeDesignator()
    {
        return codingSchemeDesignator;
    }
    /**
    * Returns the Coding Scheme Version
    * @return The Coding Scheme Version
    */
    public String getCodingSchemeVersion()
    {
        return codingSchemeVersion;
    }
    /**
    * Sets the Coding Scheme Designator
    * @param codingSchemeDesignator Coding Scheme Designator
    */
    public void setCodingSchemeDesignator(String codingSchemeDesignator)
    {
        this.codingSchemeDesignator= codingSchemeDesignator;
        if (this.codingSchemeDesignator == null) codingSchemeDesignator = "";
    }
    /**
    * Sets the Coding Scheme Version
    * @param codingSchemeVersion Coding Scheme Version
    */
    public void setCodingSchemeVersion(String codingSchemeVersion)
    {
        this.codingSchemeVersion= codingSchemeVersion;
        if (this.codingSchemeVersion == null) codingSchemeVersion = "";
    }
    /**
    * Sets the Context Group
    * @param contextGroup Context Group
    */
    public void setContextGroup(String contextGroup)
    {
        this.contextGroup= contextGroup;
        if (this.contextGroup == null) contextGroup = "";
    }
    
    /**
    * Sets the Code Value
    * @param codeValue Code Value
    */
    public void setCodeValue(String codeValue)
    {
        this.codeValue= codeValue;
        if (this.codeValue == null) codeValue = "";
    }
    
    /**
    * Sets the Code Meaning
    * @param codeMeaning Code Meaning
    */
    public void setCodeMeaning(String codeMeaning)
    {
        this.codeMeaning= codeMeaning;
        if (this.codeMeaning == null) codeMeaning = "";
    }
    
}
/*
 *  CVS Log
 *  $Log: SRCode.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
