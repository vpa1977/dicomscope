/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
/**
* Event for the status line
*/
public  class StatusLineEvent extends DSEvent
{
    
    public static final int SET_DES = 0;
    public static final int SET_TEXT1 = 1;
    public static final int SET_TEXT2 = 2;
    public static final int SET_TEXT3 = 3;
    public static final int SET_TEXT4 = 4;
    public static final int SET_ALL = 10;
    
    public String dsComponentType;
    public String description;
    public String  text1;
    public String  text2;
    public String  text3;
    public String  text4;
    public int type;
    
    public StatusLineEvent(Object o, int type,String dsComponentType, String value)
    {
        super(o);
        this.type = type;
        this.dsComponentType = dsComponentType;
        if (type ==SET_TEXT1) text1 = value;
        else if (type ==SET_TEXT2) text2 = value;
        else if (type ==SET_TEXT3) text3 = value;
        else if (type ==SET_TEXT4) text4 = value;
        else description = value;
        
    }
}
/*
 *  CVS Log
 *  $Log: StatusLineEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
