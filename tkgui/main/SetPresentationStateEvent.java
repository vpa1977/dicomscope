/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
import viewer.presentation.*;
public  class SetPresentationStateEvent extends DSEvent
{
    
    public boolean haveRectShutter;
    public boolean haveCircShutter;
    public boolean havePolyShutter;
    public boolean haveBmpShutter;
    public boolean isFlip;
    public boolean isInvert;
    public int rot;
    public boolean psOn;
    public int curve;
    public int psLut; 
    
    
    
    public SetPresentationStateEvent(   Object o,
                                        boolean haveRectShutter,
                                        boolean haveCircShutter,
                                        boolean havePolyShutter,
                                        boolean haveBmpShutter,
                                        boolean isFlip,
                                        boolean isInvert,
                                        int rot,
                                        boolean psOn, 
                                        int curve, 
                                        int psLut)
                                        
    {
        super(o);
        this.havePolyShutter = havePolyShutter;
        this.haveRectShutter = haveRectShutter;
        this.haveCircShutter = haveCircShutter;
        this.haveBmpShutter = haveBmpShutter;
        this.isFlip = isFlip;
        this.isInvert = isInvert;
        this.rot = rot;
        this.psOn = psOn;
        this.curve = curve;
        this.psLut = psLut;
        
    }
}
/*
 *  CVS Log
 *  $Log: SetPresentationStateEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
