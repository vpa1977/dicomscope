/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
/**
* This class contains image move events .
*/
public  class MoveImageEvent extends DSEvent
{
    public static final int IMAGE_FIRST = 0;
    public static final int IMAGE_LAST = 1;
    public static final int IMAGE_NEXT= 2;
    public static final int IMAGE_PREVIOUS=3;
    public static final int IMAGE_SET = 4;
    
    public static final int FRAME_FIRST = 10;
    public static final int FRAME_LAST = 11;
    public static final int FRAME_NEXT= 12;
    public static final int FRAME_PREVIOUS=13;
    public static final int FRAME_SET = 14;
    
    public static final int APPLYALL = 20;
    public static final int APPLYIMAGE = 21;
    public static final int APPLYFRAME = 22;
    
    public int value;
    
    public int type;
    public MoveImageEvent(Object o, int type)
    {
        super(o);
        this.type = type;
    }
    public MoveImageEvent(Object o, int type, int value)
    {
        super(o);
        this.type = type;
        this.value = value;
    }
    
}
/*
 *  CVS Log
 *  $Log: MoveImageEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
