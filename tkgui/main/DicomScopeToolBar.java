/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import javax.swing.*;
import javax.swing.event.*;
import jToolkit.gui.*;
import J2Ci.*;
import java.awt.*;
import java.util.*;
/**
* Contains the GUI for Toolbar
*/ 
public class DicomScopeToolBar extends JToolBar implements MainListener
{
    
    
    
    private String view;
    private boolean imageSet = false;
    private GuiComponents gui;
    private boolean imageFunktionsOn = true;
    boolean print;
    public DicomScopeToolBar(Hashtable config, boolean print)
    {
        Controller.instance().addMainListener(this);
        gui =  GuiComponents.getInstance();
        setConfiguration(config,  true);
        this.print = print;
        
    }
   private void changeToolbar(String view)
   {
      this.view = view;
      removeAll();
      this.add(gui.loadImageButton);
      if (view.equals("Browser"))
      {
        this.add(gui.loadButton);
        
        this.add(gui.loadSRButton);
        this.add(gui.sendToButton);
        this.add(gui.refreshButton);
        this.add(gui.deleteButton);
        if ( print)this.add(gui.printHcButton);
        this.add(gui.printStToPrinterButton);
        this.add(gui.dumpButton);
        this.add(gui.checkIODButton);
      }
      this.addSeparator();
      if (view.equals("Viewer"))
      {
           this.addSeparator();
           this.add(gui.loadSRButton);
        if (imageSet)
        {
            
            this.add(gui.loadPSButton);
            this.add(gui.saveButton);
            this.add(gui.saveAsButton);
            this.add(gui.saveScreenButton);
            if ( print)this.add(gui.printButton);
        }
        
        if (imageSet&&imageFunktionsOn)
        {
            
            
            this.addSeparator();
            this.add(gui.presentationLutButton);
            this.add(gui.lutButton);
            this.add(gui.psButton);
            this.add(gui.resetButton);
            this.addSeparator();
            this.add(gui.invertButton);
            this.add(gui.flipButton);
            this.add(gui.rotButton);
            this.add(gui.zoomButton);
            this.add(gui.zoom1to1Button);
            this.add(gui.zoomFitButton);
            this.add(gui.voiButton);
            this.addSeparator();
            
            this.add(gui.pointButton);
            this.add(gui.lineButton);
            this.add(gui.rectButton);
            this.add(gui.polyButton);
           // this.add(gui.interpolatedButton);
            this.add(gui.circleButton);
            this.add(gui.ellipseButton);
            this.add(gui.textButton);
            this.add(gui.editButton);
            this.add(gui.editLayerButton);
            this.add(gui.filledButton);
            this.add(gui.imageDisplayButton);
            this.addSeparator();
            this.add(gui.rectShButton);
            this.add(gui.circleShButton);
            this.add(gui.polyShButton);
            this.add(gui.bmpShButton);
            this.add(gui.optShButton);
        }
      }
      if (view.equals("About"))
      {
      }
      this.addSeparator();
      this.add(gui.optionButton);
   }
	public boolean processEvent (DSEvent e)
	{
	    if (e instanceof TabbedChangeEvent)
	    {
	        changeToolbar(((TabbedChangeEvent)e).getTabbedTitle());
	    }
	    if (e instanceof ImageActionEvent)
	    {
	        ImageActionEvent iae = (ImageActionEvent)e;
	        if (iae.type == ImageActionEvent.ACTION_SETNEWIMAGE)
	        {
	            imageSet = true;
	            changeToolbar(view);
	            return true;
	        }
	    }
	    if (e instanceof ChangeOptionsEvent)
	    {
            
            setConfiguration(((ChangeOptionsEvent)e).getConfig(), false);
            
        }
	    return false;
    }
    public void setConfiguration(Hashtable config, boolean init)
    {
            if (config.containsKey("PaintPanelPlacement"))
                
            {   
                if  (((String)config.get("PaintPanelPlacement")).equals("Toolbar")) 
                {
                    imageFunktionsOn = true;
                }
                else imageFunktionsOn =false;
            }
            if (!init) changeToolbar(view);
    }
}
/*
 *  CVS Log
 *  $Log: DicomScopeToolBar.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
