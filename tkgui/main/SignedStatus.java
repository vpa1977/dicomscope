/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.util.*;
import jToolkit.gui.CommandJButton;
import J2Ci.jDVPSSignatureStatus;
/**
* Contains the GUI for the status line.
* The is context sensitve. For each DSComponentType
* There is a seperate view.
*/
public class SignedStatus extends JPanel 
{
    
    /**
    * Contains the current status.
    * The possible values are defined in jDVPSSignatureStatus 
    */
    private int status = 0;
    
    /** no digital signatures are present
     */
    private Icon unsignedIcon;

    /** one or more digital signatures are present and have been successfully verified
     */

    private Icon signed_OKIcon;

    /** one or more digital signatures are present and at least one of them
     *  could not be successfully verified because it was corrupt or created
     *  with a certificate issued by an unknown CA.
     */
    private Icon signed_corruptIcon;
    
    /** one or more digital signatures are present and at least one of them
     *  could not be successfully verified because it was corrupt or created
     *  with a certificate issued by an unknown CA.
     */
    private Icon signed_Icon;
    
    /**
    * Contains the type of the String
    */
    private String type;
    
    private JButton button;
    
    Action action; 
    
    /**
    *
    */
    public  SignedStatus(   String type,
                            Icon unsignedIcon,
                            Icon signed_corruptIcon,
                            Icon signed_OKIcon,
                            Icon signed_Icon,
                            Action action)
    {
        super();
        if (action == null) throw new IllegalArgumentException ("Action shouldn't be null");
        this.type = type;
        this.signed_corruptIcon=signed_corruptIcon;
        this.signed_Icon = signed_Icon;
        this.signed_OKIcon = signed_OKIcon;
        this.unsignedIcon = unsignedIcon;
        this.action = action;
        setLayout(new FlowLayout(0,0,0));
        button = new JButton(unsignedIcon);
        CommandJButton.setButton(button);
        button.setMargin(new Insets(0,0,0,0));
        button.setBorderPainted(false);
        button.setAction(action);
        action.putValue(Action.SMALL_ICON, unsignedIcon);
        
        add(button);
        
    }
                            
}
/*
 *  CVS Log
 *  $Log: SignedStatus.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
