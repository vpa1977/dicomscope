/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;
import java.util.*;
/**
* This class contains image action events .
*/
public  class ImageActionEvent extends DSEvent
{
    public final static int ACTION_SETNEWIMAGE = 0;
    public final static int ACTION_FLIP = 1;
    public final static int ACTION_ROT = 2;
    public final static int ACTION_INVERT = 3;
    public final static int ACTION_PS = 4;
    public final static int ACTION_BARTEN = 5;
    public final static int ACTION_RESET = 6;
    public final static int ACTION_SELECTLAYER = 7;
    public final static int ACTION_EDITLAYER = 8;
    public final static int ACTION_EDITSHUTTER = 9;
    public final static int ACTION_VOI = 10;
    public final static int ACTION_PRESENTATIONLUT = 11;
    public final static int ACTION_WININFRAME = 12;
    public final static int SET_FILLED = 20;
    public final static int SET_IMAGERELATIVE = 21;
    public final static int PAINT_RECT = 100;
    public final static int PAINT_CIRCLE = 101;
    public final static int PAINT_LINE = 102;
    public final static int PAINT_POINT = 103;
    public final static int PAINT_INTERPOLATED = 104;
    public final static int PAINT_ELLIPSE = 105;
    public final static int PAINT_POLYLINE = 106;
    public final static int PAINT_TEXT = 107;
    public final static int PAINT_EDIT = 108;
    public final static int MODE_MAGNIFY = 300;
    public final static int MODE_SCALE = 301;
    public final static int MODE_TRUESIZE = 302;
    public final static int PRINT = 1000;
    public final static int ACTION_ZOOM = 500;
    public final static int ACTION_ZOOMFIT = 501;
    public final static int ACTION_ZOOM1TO1 = 502;
    public final static int ACTION_SETNEW_SR = 600;
    public final static int ACTION_COMPOSITE_SR = 601;
    
    public int type;
    
    public String sopClassUid;
    public String sopInstanceUid;
    public int numberOfImages;
    public int numberOfFrames;
    
    public ImageActionEvent(Object o, int type)
    {
        super(o);
        this.type = type;
    }
    public ImageActionEvent(Object o, int type, int numberOfImages, int numberOfFrames)
    {
        super(o);
        this.type = type;
        this.numberOfFrames = numberOfFrames;
        this.numberOfImages = numberOfImages;
    }
    
    public ImageActionEvent(Object o, int type, String sopClassUid, String sopInstanceUid)
    {
        super(o);
        this.type = type;
        this.sopClassUid = sopClassUid;
        this.sopInstanceUid = sopInstanceUid;
    }
    
}
/*
 *  CVS Log
 *  $Log: ImageActionEvent.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
