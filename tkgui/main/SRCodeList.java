/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/
package main;


import java.util.*;
import java.io.*;
/**
* This class contains functions for the administration of a list of{@link SRCode} 
* objects
* @author Klaus Kleber
*/
public class SRCodeList 
{
   
    
    /**
    * Contains a map of {@link SRCode} objects
    * The keys are the identifies of the {@link SRCode}
    * the values are the {@link SRCode}.
    */
    private Hashtable listSRCode = new Hashtable();
    
    /**
    * Contains a dictionary with all Context Groups.
    * The keys are the Context Group Name and the 
    * values are a Vector containing a list of all
    * known {@link SRCode} objects in the Context Group
    */
    private Hashtable contextGroupTable = new Hashtable();
    
    /**
    * Constructor. A number of {@link SRcodes] will read from the specified
    * text file. This text file contains a dictionary specifying all codes which can be
    * selected from the DICOMscope 3.x application.  Many codes (at least the
    * code meanings) are directly taken from the current "public comment" draft
    * of Supplement 57 "DICOM Content Mapping Resource (DCMR)".
    * <p>
    * Each line represents an entry in the code dictionary.  Each line has 5
    * fields (Context Group, Coding Scheme Designator, Coding Scheme Version,
    * Code Value, Code Meaning).  Each field must be separated by a comma and
    * surrounded by quotation marks. 
    * <p>
    * Comments have a '#' at the beginning of the line.
    * @param fileName The name of the text file. 
    */
    public SRCodeList(String fileName)
    {
        contextGroupTable.put(SRCode.CONTEXT_GROUP_NAME_CODE, new Vector());
        
        BufferedReader in = null;
        try
        {
            in= new BufferedReader(new FileReader(new File(fileName)));
        }
        catch(IOException e)
        {
            System.err.println("Code table not found: " + e);
            return;
        }
        try
        {
            String nextLine ;
            nextLine = in.readLine().trim();
           
            SRCode code = null;
            
            while(nextLine!=null)
            {
                if (!nextLine.startsWith("#"))
                {
                    code = getSRCode(nextLine);
                    
                    instertCode(code);
                }
  
                 nextLine = in.readLine();
               
            }
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }
   
    /**
    * Inserts a code in {@link listSRCode} and in the 
    * correct {@link contextGroupTable}. If the Context Group of the
    * {@link SRCode} is not specified a new Context Group 
    * will be inseted in the {@link contextGroupTable}.
    * @param code New code.
    */
    public void instertCode(SRCode code)
    {
        listSRCode.put(code.getIdentifier(), code);
        Object o = contextGroupTable.get(code.getContextGroup());
        if (o == null)
        {
            Vector v = new Vector();
            v.add(code);
            contextGroupTable.put(code.getContextGroup(), v);
        }
        else
        {
            Vector v = (Vector)o;
            v.add(code);
        }
    }
    
    /**
    * Searchs for the index of a {@link SRCode} object in the s
    * specified Vector of SRCodes. 
    * @param v The Vector containing a list of {@link SRCode}
    * @param id The identifier of a {@link SRCode}
    * @return The index of the {@link SRCode} or -1 if not found.
    */
    public static int getIndexCode(Vector v, String id)
    {
        if (v== null ||id == null) return -1;
        for (int i = 0; i <v.size(); i++)
        {
            if (((SRCode)v.elementAt(i)).getIdentifier().equals(id)) return i;
        }
        return -1;
    }
   
    /**
    * Returns a Vetor with all {@link SRCode} objects of
    * the specified Context Group
    * @param contextGroupName Specifies the Context Group
    * @return A Vetor with all {@link SRCode} objects of
    * the specified Context Group
    */
    public Vector getContextGroup(String contextGroupName)
    {
        return (Vector) contextGroupTable.get(contextGroupName);
    }
   
   
   
   /**
   * Reads a {@link SRCode} from the specified String
   * @return The {@link SRCode} readed from the String
   */
   private SRCode getSRCode(String line)
    {
        SRCode code =new SRCode();
        StringTokenizer st = new StringTokenizer(line, ",");
        code.setContextGroup ( readInlineString(st.nextToken().trim()));
        code.setCodingSchemeDesignator( readInlineString(st.nextToken().trim()));
        code.setCodingSchemeVersion(readInlineString(st.nextToken().trim()));
        code.setCodeValue( readInlineString(st.nextToken().trim()));
        code.setCodeMeaning( readInlineString(st.nextToken().trim()));
        return code;
    }
    /**
    * Return the specified String without the first and the last 
    * character. 
    * @param s Specified the original String
    * @return The specified String without the first and the last 
    * character. 
    */
    private String readInlineString(String s)
    {
        return s.substring(1, s.length()-1);
    }
}
/*
 *  CVS Log
 *  $Log: SRCodeList.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
