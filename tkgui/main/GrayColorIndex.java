/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package main;
import java.awt.image.*;
import java.awt.*;
/**
 * This class creates color models for this programm 
 * 
 * @author Klaus Kleber
 * @since 30.04.99
 */
public class GrayColorIndex extends java.lang.Object
{

    /**
     * IndexColorModel for image data which contains 1 byte per pixel.
     * Every pixel will be interpreted as gray color.
     * 
     * @since 30.04.1999
     */
    private static  IndexColorModel gray12BitColorModel;

    /**
     * IndexColorModel for image data which contains 12 bits per pixel.
     * Every value will be interpreted as a gray color.
     * 
     * @since 30.04.1999
     */
    private static  IndexColorModel grayColorModel;
    private static IndexColorModel overlayGrayColorModel;
    private static IndexColorModel overlayGray12ColorModel;
    private static int overlayColorBit = 255;
    private static int overlay12ColorBit = 4095;
    
    
    public static void fillGrayColorModel()
    {
        int bits = 8;
        int size = 256;
        byte[] r = new byte[size];
        byte[] g = new byte[size];
        byte[] b = new byte[size];
        byte[] a = new byte[size];
        for (int i = 0; i < size; i++)
        {
            r[i] = (byte)i;
            g[i] = (byte)i;
            b[i]= (byte)i;
            a[i]= (byte)0xFF;
        }
        //Transparent Color
        grayColorModel = new IndexColorModel(bits, size, r, g, b);
        
    }
    public static void fillGray12BitColorModel()
    {
        int bits = 16;
        int size = 4096; //(12**8)
        byte[] r = new byte[size];
        byte[] g = new byte[size];
        byte[] b = new byte[size];
        byte[] a = new byte[size];
        int x = 0;
        int y = 0;
        for (int i = 0; i < size; i++)
        {
            x++;
            if ( x ==256)
            {
                x = 0;
                y++;
            }
            r[i] = (byte)(x & 0xff);
            g[i] = (byte)(y &0xff);
            b[i]= (byte)0;
            a[i]= (byte)0xFF;
        }
        //Transparent Color
        gray12BitColorModel = new IndexColorModel(bits, size, r, g, b);
        
    }
    public static void  fillIndexColorModel()
    {
        
        fillGrayColorModel();
        fillGray12BitColorModel();
        fillOverlayGrayColorModel(overlayColorBit);
        fillOverlay12BitColorModel(overlay12ColorBit);
    
    }

    /**
     * Returns the IndexColorModel for gray image data.
     * 
     * @return IndexColorModel for image data.
     * @since 30.04.1999
     */
    public static IndexColorModel getGrayColorModel()
    {
        return grayColorModel;
    }
    
    /**
     * Returns a IndexColorModel for overlay data.This ColorModel contains the same Colors as the grayColorModel. 
     * There are only one difference: One Color is transparent. This Color has an alpha-value of 0x00.
     * 
     * 
     * @return IndexColorModel for image data.
     * @since 30.04.1999
     */
    public static void fillOverlayGrayColorModel(int colorBit)
    {
        overlayColorBit = colorBit;
        int bits = 8;
        int size = 256;
        byte[] r = new byte[size];
        byte[] g = new byte[size];
        byte[] b = new byte[size];
        byte[] a = new byte[size];
        for (int i = 0; i < size; i++)
        {
            r[i] = (byte)(i&0xff);
            g[i] = (byte)(i&0xff);
            b[i]= (byte)(i&0xff);
            a[i]= (byte)0x00;
        }
        
        a[colorBit] = (byte)0xFF;
        //Transparent Color
        overlayGrayColorModel = new IndexColorModel(bits, size, r, g, b,a);
        
    }
    public static void fillOverlay12BitColorModel(int colorBit)
    {
        overlay12ColorBit = colorBit;
        
        int bits = 16;
        int size = 4096; //(12**8)
        byte[] r = new byte[size];
        byte[] g = new byte[size];
        byte[] b = new byte[size];
        byte[] a = new byte[size];
        int x = 0;
        int y = 0;
        for (int i = 0; i < size; i++)
        {
            x++;
            if ( x ==256)
            {
                x = 0;
                y++;
            }
            r[i] = (byte)(x&0xff);
            g[i] = (byte)(y&0xff);
            b[i]= (byte)0x00;
            a[i]= (byte)0x00;
        }
        //Transparent Color
        a[colorBit] = (byte)0xFF;
        //Transparent Color
        overlayGray12ColorModel = new IndexColorModel(bits, size, r, g, b,a);
        
    }
    
    
    public static IndexColorModel getOverlayGrayColorModel(int colorBit)
    {
        
        if (colorBit != overlayColorBit) fillOverlayGrayColorModel(colorBit);
        return overlayGrayColorModel;
    }
    public static IndexColorModel getOverlayGray12ColorModel(int colorBit)
    {
        if (colorBit != overlay12ColorBit) fillOverlay12BitColorModel(colorBit);
        return overlayGray12ColorModel;
    }
        
    public static IndexColorModel getGray12BitColorModel()
    {
        return gray12BitColorModel;
    }
    public static Color getColor(int grayIndex)
    {
        return new Color(grayIndex, grayIndex,grayIndex);
    }
    public static Color get12Color(int grayIndex)
    {
        int r = grayIndex%256;
        int g = grayIndex/256;
        int b = 0;
        return new Color (r,g,b);
    }
    /**
     * Conts the values of the specified array.
     * 
     * @param colorArray Specifies the array.
     * @return The number of values in the array.
     * @since 30.04.1999
     */
    public static int[] countValues(byte[] colorArray)
    {
        int[] returnArray = new int [256];
        for (int i = 0; i < colorArray.length; i++)
        {
            returnArray[colorArray[i]&0xff] = 1;
        }
        for (int i = 0; i < 256; i++)
        {
            System.out.println(" das ist der Wert i: " + i + " wert: " + returnArray[i]);
        }
        return returnArray;
     }
    /**
     * Conts the values of the specified array.
     * 
     * @param colorArray Specifies the array.
     * @return The number of values in the array.
     * @since 30.04.1999
     */
    public static int[]  countValues(short[] colorArray)
    {
        int[] returnArray = new int [256*256];
        for (int i = 0; i < colorArray.length; i++)
        {
            returnArray[colorArray[i]] = 1;
        }
        for (int i = 0; i < returnArray.length; i++)
        {
            if (returnArray[i] != 0)System.out.println(" das ist der Wert i: " + i + " wert: " + returnArray[i]);
        }
        return returnArray;
     }
    
}

/*
 *  CVS Log
 *  $Log: GrayColorIndex.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
