/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package jToolkit.gui;

import javax.swing.*;

/**
 * Command-Class for i.e. CommandJButton, CommandAWTButton.
 * The exit-code will be the button-ID.
 *
 * @author Andreas Schroeter
 * @since 30.03.
*/
public class DisposeCommand implements CommandButtonListener
{
    private JDialog dlg;
    private boolean wasClicked = false;

    public DisposeCommand (JDialog dlg)
    {
        this.dlg = dlg;
    }	

    public void buttonClicked (int ID)
    {
        dlg.setVisible(false);
        wasClicked = true;
    }
    
    public boolean wasClicked()
    {
        return wasClicked;
    }
}
/*
 *  CVS Log
 *  $Log: DisposeCommand.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
