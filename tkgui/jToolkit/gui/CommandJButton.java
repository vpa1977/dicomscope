/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package jToolkit.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * The CommandJButton extends the normal (Swing-)JButton with an automatic command
 * call when the button is pressed. 
 *
 * Usage:
 * 
 * class FooClass extends JPanel implements CommandJButton
 * {
 *  final int IDOK = 1;
 *  final int IDCANCEL = 2;
 *
 *  void Constructor ()
 *  {
 *      CommandJButton ok = new CommandJButton ("OK", this, IDOK);
 *      CommandJButton cancel = new CommandJButton ("Cancel", this, IDCANCEL);
 *      add (ok);
 *      add (cancel);
 *  }
 * 
 *  void ButtonClicked (int ID);
 *  {
 *      if (ID == IDOK) doOK(); // OK pressed
 *      if (ID == IDCANCEL) doCancel(); // Cancel pres
 *  }
 * } 
 *
 * @author Andreas Schroeter
 * @since 30.03.
*/
public class CommandJButton extends JButton implements ActionListener
{
    private int ID;
    static int gaps = 2;
    private CommandButtonListener cbl;
    public int getID(){ return ID;}
    public CommandButtonListener getCommandButtonListener(){ return cbl;}
    public CommandJButton(CommandJButton b)
    {
        super();
        this.ID = b.getID();
        this.cbl = b.getCommandButtonListener();
        this.setMargin(new Insets(gaps,gaps,gaps,gaps));
        this.setIcon(b.getIcon());
        this.setText(b.getText());
        this.setBorderPainted(b.isBorderPainted());
        
        this.setSelected(b.isSelected());
        this.setAlignmentX(b.getAlignmentX());
        this.setAlignmentY(b.getAlignmentY());
        this.setToolTipText(b.getToolTipText());
        addActionListener (this);
        
    }
    public static final void setButton(JButton b)
    {
        b.setAlignmentX(0.5f);
        b.setAlignmentY(0.5f);
        b.setMargin(new Insets(gaps,gaps,gaps,gaps));
        
        //b.setBorderPainted(false);
        
    }
    public CommandJButton (String label, CommandButtonListener cbl, int ID)
    {
        super (label);
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(gaps,gaps,gaps,gaps));
        setAlignmentX(0.5f);
        
        setAlignmentY(0.5f);
        addActionListener (this);
    }

    public CommandJButton (CommandButtonListener cbl, int ID)
    {
        super ();
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(gaps,gaps,gaps,gaps));
        setAlignmentX(0.5f);
        
        setAlignmentY(0.5f);
        addActionListener (this);
    }

    public CommandJButton(String text, Icon icon, CommandButtonListener cbl, int ID)
    {
        super (text, icon);
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(gaps,gaps,gaps,gaps));
        setAlignmentX(0.5f);
        //setBorderPainted(false);
        
        setAlignmentY(0.5f);
        addActionListener (this);
    }


    public CommandJButton(Icon icon, CommandButtonListener cbl, int ID)
    {
        super (icon);
        this.ID = ID;
        this.cbl = cbl;
        addActionListener (this);
        this.setMargin(new Insets(gaps,gaps,gaps,gaps));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
        //setBorderPainted(false);
        
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this) cbl.buttonClicked (ID);
    }
}
/*
 *  CVS Log
 *  $Log: CommandJButton.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
