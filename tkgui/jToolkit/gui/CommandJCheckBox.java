/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package jToolkit.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * The CommandJCheckBox extends the normal (Swing-)JCheckBox with an automatic command
 * call when the ActionEvent is fired. 
 *
 * @author Andreas Schroeter
 * @since 30.03.
*/
public class CommandJCheckBox extends JCheckBox implements ActionListener
{
    private int insetSize = 2;
    private int ID;
    private CommandToggleButtonListener  cbl;
    public int getID(){ return ID;}
    public CommandToggleButtonListener getCommandToggleButtonListener(){ return cbl;}
    public CommandJCheckBox (String label, boolean selected, CommandToggleButtonListener cbl, int ID)
    {
        super (label,selected);
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(insetSize,insetSize,insetSize,insetSize));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
        addActionListener (this);
    }

    public CommandJCheckBox (CommandToggleButtonListener cbl, int ID)
    {
        super ();
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(insetSize,insetSize,insetSize,insetSize));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
        addActionListener (this);
    }
    public CommandJCheckBox(CommandJToggleButton b)
    {
        super();
        this.ID = b.getID();
        this.cbl = b.getCommandToggleButtonListener();
        this.setMargin(b.getMargin());
        this.setIcon(b.getIcon());
        this.setText(b.getText());
        this.setSelected(b.isSelected());
        this.setAlignmentX(b.getAlignmentX());
        this.setAlignmentY(b.getAlignmentY());
        this.setToolTipText(b.getToolTipText());
        addActionListener (this);
        
    }
    public CommandJCheckBox(String text, Icon icon,boolean selected, CommandToggleButtonListener cbl, int ID)
    {
        super (text, icon,selected);
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(insetSize,insetSize,insetSize,insetSize));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
        addActionListener (this);
    }


    public CommandJCheckBox(Icon icon, boolean selected,CommandToggleButtonListener cbl, int ID)
    {
        super (icon,selected);
        this.ID = ID;
        this.cbl = cbl;
        this.setMargin(new Insets(insetSize,insetSize,insetSize,insetSize));
        setAlignmentX(0.5f);
        setAlignmentY(0.5f);
        addActionListener (this);
    }
    public void actionPerformed(ActionEvent e)
    {
        JCheckBox tb = (JCheckBox)e.getSource();
        //if (tb.isSelected()) tb.setBackground(new Color(Color.red));
        //else tb.setBackground(new Color(Color.blue));
        if (e.getSource() == this) cbl.buttonClicked (ID,((JCheckBox)(e.getSource())).isSelected());
    }
}
/*
 *  CVS Log
 *  $Log: CommandJCheckBox.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
