/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package jToolkit.io;

import java.io.*;

/**
 * Creates a PrintStream to null-device. This class can be used to avoid console
 * output (System.setOut (new NullPrintStream());  ).
 *
 * @author Andreas Schroeter
 * @since 30.03.
*/
public class NullPrintStream extends PrintStream
{
	public NullPrintStream() {super (new ByteArrayOutputStream());}
	public NullPrintStream(OutputStream out) {super (out); }
	public NullPrintStream(OutputStream out, boolean autoFlush) {super (out,autoFlush);}
	    
    public void flush() {}
	public void close() {}	
    public boolean checkError() { return false; }	
    protected void setError() {}	
    public void write(int b) {}	
    public void write(byte buf[], int off, int len) {}
	public void print(boolean b) {}	
    public void print(char c) {}	
    public void print(int i) {}	
    public void print(long l) {}	
    public void print(float f) {}	
    public void print(double d) {}	
    public void print(char s[]) {}	
    public void print(String s) {}	
    public void print(Object obj) {}	
    public void println() {}	
    public void println(boolean x) {}	
    public void println(char x) {}	
    public void println(int x) {}	
    public void println(long x) {}	
    public void println(float x) {}	
    public void println(double x) {}	
    public void println(char x[]) {}	
    public void println(String x) {}	
    public void println(Object x) {}
}

/*
 *  CVS Log
 *  $Log: NullPrintStream.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
