/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package dicomPrint;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.image.*;
import J2Ci.*;
import main.*;

/**
 * Objects of this class represent a preview for one place on the current page 
 * in the current print job. A PrintImagePreview have a border (with size = gaps)
 * and a inlinePanel containig the ImagePreview.
 * @author Klaus Kleber
 */
public class PrintImagePreview extends JPanel
{
    
    /**
    * Contains the Preview
    */
    private BufferedImage previewImage;
    
    
    /**
     * Contains the size of the gaps on each side.
     */
    private int gaps = 2;

    /**
     * Contains index of the image if exits.
     */
    private int index;

    /**
     * Contains the GUI of the PrintImagePreview without the border
     */
    private JPanel inlinePanel;

    /**
     * if true the image have his own print settings
     */
    private boolean haveParameter = false;

    /**
     * if true the PrintImagePreview refers to an image
     */
    private boolean referToImage = false;

    /**
     * Contains the color of the selecting border.
     */
    private Color markedColor= Color.red;
    
    /**
     * Contains the background color if the PrintImagePreview does not reffer to an image.
     */
    private Color emptyImageColor;
    
    /**
     * Contains the background color for PrintImagePreviews  refering to an image.
     */
    private Color referColor ;
    
    /**
    * Contains the jDVInterface
    */
    private  jDVInterface dvi;
    
    boolean printLUT = true;
    
    public boolean notifyBrowser = false;
    /**
     * Constructor
     * 
     * @param index a
     */
    public PrintImagePreview(int index,Color emptyImageColor, Color referColor, jDVInterface dvi, boolean printLUT)
    {
        this.dvi = dvi;
        this.index = index;
        this.referColor = referColor;
        this.emptyImageColor = emptyImageColor;
        this.printLUT = printLUT;
        setLayout(new BorderLayout());
        
        inlinePanel = new JPanel();
        
        
        setBackground(emptyImageColor);
        inlinePanel.setBackground(emptyImageColor);
        
        EmptyBorder eb = new EmptyBorder(gaps, gaps, gaps, gaps);
        setBorder(eb);
        add(inlinePanel, BorderLayout.CENTER);
    }

    /**
     * Paints the PrintImagePreview. 
     * If the PrintImagePreview refers to an image the a preview will be created.
     * @param g Contains the curren Graphics object
     */
    public void paint(Graphics g)
    {
        super.paint(g);
         
        //Current Size
        int sizeWidth = this.getSize().width-2*gaps;
        int sizeHeight = this.getSize().height-2*gaps;
        
        //if the PrintImagePreview refer to an image the preview will be 
        // calculated
        if (referToImage)
        {
            
            //only the previewImage does not exits
            if (previewImage == null)
            {
                //Loads the preview
                dvi.setMaxPrintPreviewWidthHeight(sizeWidth, sizeHeight);
                int status = dvi.loadPrintPreview(index, printLUT,true);
                jIntByRef widthP = new jIntByRef();
                jIntByRef widthH = new jIntByRef();
                status = dvi.getPrintPreviewWidthHeight(widthP, widthH);
                
                DataBufferByte dbb = new DataBufferByte(new byte[(int)dvi.getPrintPreviewSize()],
                                        (int)dvi.getPrintPreviewSize());
	        status = dvi.getPrintPreviewBitmap(dbb.getData(), (long) dbb.getSize());
    	        dvi.unloadPrintPreview();
	         //Create new Image
	         
	        if (status == jE_Condition.EC_Normal) 
                {
                    int bandOffsets[] = {0};
                    WritableRaster wr = Raster.createInterleavedRaster(dbb,
                                                        widthP.value,
                                                        widthH.value,
                                                            widthP.value,
                                                        1, bandOffsets,null);
                    previewImage = new BufferedImage(MainContext.instance().getIndexColorModel(), wr, MainContext.instance().getIndexColorModel().isAlphaPremultiplied(), null);
                        
                }
            
                
            }
            
            //Drwas the image
            if (previewImage != null)
            {
                Graphics2D g2 = (Graphics2D)g;
                int h = (sizeHeight-previewImage.getHeight())/2;
                int w = (sizeWidth-previewImage.getWidth())/2;
                g2.drawImage(previewImage,w+gaps,h+gaps, this);
            }
        } 
        
        //Draw the 
        if (haveParameter)
        {
            g.setColor(Color.red);
            g.drawString("OPTIONS", sizeWidth/2-2*gaps, sizeHeight/2);
        }
         
        if (notifyBrowser)   
        {
           
            Controller.instance().fireEvent(new UpdateBrowserEvent(this));
            notifyBrowser = false;
        }
        
    }

    
    /**
     * Marked/unmarked the image with a  border if a preview exits.
     * @param marked true sets the border
     */
    public void setMarked(boolean marked)
    {
        if (referToImage)
        {
            if (marked) setBackground(markedColor);
            else setBackground(referColor);
        }
    }

    /**
     * Sets/resets referToImage.
     * @param referToImage 
     */
    public void setReferToImage()
    {
        referToImage = true;
        setBackground(emptyImageColor);
        inlinePanel.setBackground(emptyImageColor);
    } 

    
    
    /**
     * Gets index of the image.
     * @return The index
     */
    public int getIndex()
    {
        return index;
    }

    /**
     * Return true if the PrintImagePreview refers to an image.
     * 
     * @return a
     */
    public boolean getReferToImage()
    {
        return referToImage;
    }

    /**
     * Set haveParameter. This value inicates that the image of the PrintImagePreview
     * has his own print settings.
     * @param haveParameter a
     */
    public void setParameter(boolean haveParameter)
    {
        
        this.haveParameter = haveParameter;
        repaint();
    }
}
/*
 *  CVS Log
 *  $Log: PrintImagePreview.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
