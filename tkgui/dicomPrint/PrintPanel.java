/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:30 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package dicomPrint;

import java.awt.*;
import java.util.*;
import  de.microtherapy.tools.text.document.general.*;

import javax.swing.*;
import javax.swing.border.*;

import J2Ci.*;
import jToolkit.gui.*;
import main.*;

/**
 * This class contains the GUI and with the interactions 
 * for the Print - Panel of DICOMscope.
 * 
 * @author Klaus Kleber
 * @since 01.10.99
 */
public class PrintPanel extends JPanel implements CommandButtonListener, CommandTextListener, PrintListener, CommandToggleButtonListener
{
    private boolean diplayStoredPrintObjectMode = false;

    /**
     * Contains the print Button
     */
    private CommandJButton printButton;
    
    /**
     * Contains the remove-Button
     */
    private CommandJButton removeButton;

    /**
     * Contains the ComboBox for changing the printer
     */
    private CommandJComboBox printerCombo;

    /**
     * Contains the ComboBox for changing the printer
     */
    private CommandJComboBox storedPrintPrinterCombo;
    
    
    /**
     * Contains the ComboBox for changing the medium
     */
    private CommandJComboBox mediumCombo;;

    /**
     * Contains the ComboBox for changing the film size
     */
    private CommandJComboBox filmSizeCombo;


    /**
     * Contains the ComboBox for changing the orientation
     */
    private CommandJComboBox orientationCombo;
    
    /**
     * Contains the ComboBox for changing the priority
     */
    private CommandJComboBox priorityCombo;
    
    /**
     * Contains the ComboBox for changing the priority
     */
    private CommandJTextField copiesText;
    
    /**
     * Contains the ComboBox for changing the magnification type
     */
    private CommandJComboBox magnificationCombo;

    /**
     * Contains the ComboBox for changing the smoothing type
     */
    private CommandJComboBox smoothingCombo;

    /**
     * Contains the ComboBox for changing the configuration
     */
    private CommandJComboBox configurationCombo;

    /**
     * Contains the ComboBox for changing the resolution
     */
    private CommandJComboBox resolutionCombo;

    /**
     * Contains the ComboBox for changing the trim setting
     */
    private CommandJComboBox trimCombo;

    /**
     * Contains the ComboBox for changing the cropping setting
     */
    private CommandJComboBox croppingCombo;

    /**
     * Contains the ComboBox for changing the border setting
     */
    private CommandJComboBox borderCombo;

    /**
     * Contains the ComboBox for changing the empty image setting
     */
    private CommandJComboBox emptyImageCombo;
    
    /**
     * Contains the ComboBox for changing the min density setting
     */
    private CommandJComboBox minDensityCombo;

    /**
     * Contains the ComboBox for changing the max density setting
     */
    private CommandJComboBox maxDensityCombo;
    /**
     * Contains the TextField for illumination
     */
    private CommandJTextField illuminationText;

    /**
     * Contains the TextField for reflection
     */
    private CommandJTextField reflectionText;
    
    /**
     * Contains the TextField for annatation text
     */
    private JTextField annotationText;

    /**
     * Contains the Checkbox for annatation date
     */
    private JCheckBox annDateCheckBox;
    
    /**
     * Contains the Checkbox for annatation date
     */
    private JCheckBox annPrinterNameCheckBox;
    
    /**
     * Contains the Checkbox for Illumination/Light date
     */
    private JCheckBox annIllCheckBox;
    
    private CommandJRadioButton annOnRadioButton;
    private CommandJRadioButton annOffRadioButton;
    
    /**
     * Contains the ComboBox for rows/cols
     */
    private CommandJComboBox layoutCombo;
    /**
     * Contains the ComboBox for presentation Lut
     */
    private CommandJComboBox presentationLutCombo;
    /**
     * Contains the ComboBox for the rendering mode
     */
    private CommandJComboBox renderingCombo;

    
    private boolean activeListener = false;
    
    //Ids for handling the action Events
    static final int ID_PRINTER = 0;
    static final int ID_MEDIUM = 1;
    static final int ID_FILMSIZE = 2;
    static final int ID_ORIENTATION = 3;
    static final int ID_LAYOUT = 200;
    static final int ID_MAGNIFICATION = 4;
    static final int ID_SMOOTHING = 5;
    static final int ID_CONFIGURATION = 6;
    static final int ID_RESOLUTION = 7;
    static final int ID_TRIM = 8;
    static final int ID_CROPPING = 9;
    static final int ID_BORDER = 10;
    static final int ID_EMPTYIMAGE = 11;
    static final int ID_MAXDENSITY = 12;
    static final int ID_MINDENSITY = 13;
    static final int ID_PRIORITY = 14;
    static final int ID_COPIES = 15;
    static final int ID_PRESENTATIONLUT = 50;
    static final int ID_ILLUMINATION = 51;
    static final int ID_REFLECTION = 52;
    static final int ID_RENDERING = 53;
    static final int ID_PRINT = 100;
    static final int ID_REMOVE = 101;
    static final int ID_ANNON = 200;
    static final int ID_ANNOFF = 201;
    static final int ID_STOREDPRINTER = 300;
    static final int ID_SELECTSTOREDPRINTER = 301;
    static final int ID_CLEARSTOREDPRINT = 302;
    
    
    
    
    /**
    * Default value for the max density 
    */
    private int defaultMaxDensityValue = 300;
    
    /**
    * Default value for the max density 
    */
    private int defaultMinDensityValue = 20;
    /**
     * Contains information about the number of images and pages in the current print job
     */
    private JLabel imageLabel = new JLabel();
    
    
    /**
     * Contains the current rows settings.
     */
    private long printerCurrentRows = 1;
    
    /**
     * Contains the current cols settings.
     */
    private long printerCurrentCols = 1;
    
    /**
     * Contains the target printer of the print job
     */
    private String target;
    
    /**
     * Contains current jDVPSStoredPrint object with informations about the print settings of the print job
     */
    private jDVPSStoredPrint storedPrint;
    
    /**
     * Contains the DVInterface.
     */
    private jDVInterface dvi ;

    /**
     * This object contains the GUI for the print preview
     */
    private PreviewPanel previewPanel;
    
    
    /**
     * Contains the mouse handler for all objects of this PrintPanel
     */
    private PreviewMouseHandler previewMouseHandler;
    
    /**
     * Contains the Color for the imageLabel
     */
    private Color labelColor = Color.white;

    /**
    * Contains the current Presentation State
    */
    private jDVPresentationState ps;
    
    /**
     * If true cols and rows will be changed if orientation will be changed.
     */
    private boolean currentOrientationIsPortrait = true;
    
     /**
     *
     */
      JTabbedPane tabpane = new JTabbedPane();
      private  JPanel mainPanel = new JPanel();
      private JPanel  additionalPanel = new JPanel();
      private  JPanel buttonPanel = new JPanel();
      private  JPanel annPanel = new JPanel();
      private  JPanel storedPrintPanel = new JPanel();
      private  JPanel pLutPanel = new JPanel();
      private  GridBagConstraints fGbc = new GridBagConstraints();
    
    /**
     * Constructor. Builds the GUI and initializes the widgets.
     * @param dvi Contains the current jDVInterface.
     * @param config Configuration.
     */
    public PrintPanel(jDVInterface dvi, Hashtable config )
    {
        this.dvi = dvi;
        this.ps = ps;
        setLayout(new BorderLayout());
        
        //init MouseHandler
        previewMouseHandler = new PreviewMouseHandler(this);
        
        Controller.instance().addPrintListener(this);
        storedPrint = dvi.getPrintHandler();
        //builds the GUI
        build();
        
        //Init printerCombo
        initPrinterCombo();
       
        
        init();
        reset();
        activeListener = true;
        //reset max density
        orientationCombo.setSelectedItem("Portrait");
        storedPrint.setFilmOrientation(jDVPSFilmOrientation.DVPSF_portrait);
        
    }

    /**
     * Initializes the printerCombo from the jDVInterface
     */
    public void initPrinterCombo()
    {
        
        ////////////////////////////////////////////////
        //fill printerCombo
        ///////////////////////////////////////////////////
        int sizeFor = dvi.getNumberOfTargets(jDVPSPeerType.DVPSE_printAny);
        printerCombo.removeAllItems();
        storedPrintPrinterCombo.removeAllItems();
        for (int i = 0; i < sizeFor; i++)
        {
            
            target = dvi.getTargetID(i, jDVPSPeerType.DVPSE_printAny);
            printerCombo.addItem(dvi.getTargetDescription(target));
            storedPrintPrinterCombo.addItem(dvi.getTargetDescription(target));
        }
        
        
        //Hier mu� normalerweise Targetfestgelegt werden.
        if (dvi.getCurrentPrinter() == null ) 
        {
            
            target = dvi.getTargetID(0, jDVPSPeerType.DVPSE_printAny);
            dvi.setCurrentPrinter(target);
        }
        else target = dvi.getCurrentPrinter();
        
    }
    
    
    /**
     * Build the GUI of the PrintPanel.
     * 
     */
    public void build()
    {
        
        //Contains the PreviewPanel.
        JPanel centerPanel = new JPanel();
        
        
        centerPanel.setLayout(new BorderLayout());
        
        //Contains the buttonPanel and the labelPanel.
        JPanel downPanel = new JPanel(new GridLayout(1,1));
        
        //Contains information about the print job
        JPanel labelPanel = new JPanel();
        labelPanel.add(imageLabel);
        //labelPanel.setBackground(Color.lightGray);
        downPanel.add(labelPanel);
        
        //Contains the button bar
        printButton = new CommandJButton("Print", this, ID_PRINT);
        removeButton = new CommandJButton("Clear", this, ID_REMOVE);
        buttonPanel.add(printButton);
        buttonPanel.add(removeButton);
       
        
        
       
        centerPanel.add(downPanel, BorderLayout.SOUTH);
        
        //Init previewPanel
        previewPanel = new PreviewPanel(previewMouseHandler,1,1,storedPrint, dvi);
        centerPanel.add(previewPanel,BorderLayout.CENTER);
        
        add(centerPanel, BorderLayout.CENTER);
        
        
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.NORTHWEST;
        
        /////////////////////////////////////////////////////////////
        //Print job settings
        ////////////////////////////////////////////////////////////
        
        //Contains the settings for the print job
        JLabel sizeLabel = new JLabel("MMMMMMMMMMMMMMMMMMMM");
        Dimension prefSize = new Dimension(sizeLabel.getPreferredSize().width, sizeLabel.getPreferredSize().height+sizeLabel.getPreferredSize().height/4) ;
        //mainPanel.setPreferredSize(prefSize);
        //
       
        //Contains the layout settings
        JPanel layoutPanel = new JPanel();
        layoutPanel.setLayout(new GridBagLayout());
        layoutPanel.setBorder(new TitledBorder("Options"));
        
        //Printer
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Printer"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        printerCombo = new CommandJComboBox(this, ID_PRINTER);
        printerCombo.setPreferredSize(prefSize);
        layoutPanel.add(printerCombo,gbc);
        
        //Medium
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Medium"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        mediumCombo = new CommandJComboBox(this, ID_MEDIUM);
        mediumCombo.setPreferredSize(prefSize);
        layoutPanel.add(mediumCombo,gbc);
        
        //film Size
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Film Size"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        filmSizeCombo = new CommandJComboBox(this, ID_FILMSIZE);
        filmSizeCombo.setPreferredSize(prefSize);
        layoutPanel.add(filmSizeCombo,gbc);
        
        //Orientation
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Orientation"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        orientationCombo = new CommandJComboBox(this, ID_ORIENTATION);
        orientationCombo.setPreferredSize(prefSize);
        
        layoutPanel.add(orientationCombo,gbc);
        
        
        //Priority
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Priority"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        priorityCombo = new CommandJComboBox(this, ID_PRIORITY);
        priorityCombo.setPreferredSize(prefSize);
        
        layoutPanel.add(priorityCombo,gbc);
        
        
        //Layout
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Layout"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        layoutCombo = new CommandJComboBox(this, ID_LAYOUT);
        layoutCombo.setPreferredSize(prefSize);
        layoutPanel.add(layoutCombo,gbc);
        
        //Copies
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        layoutPanel.add(new JLabel("Copies"),gbc);
        layoutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        copiesText = new CommandJTextField(2,this, ID_COPIES);
        copiesText.setDocument(new IntegerDocument(1));
        layoutPanel.add(copiesText,gbc);
        
        /////////////////////////////////
        //Contains the additional layout settings
        //////////////////////////////////
        JPanel addPanel = new JPanel();
        addPanel.setBorder(new TitledBorder("Additional Options"));
        
        
        JPanel addLeftPanel = new JPanel(new GridBagLayout());
        
        //Magnification
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Magnification"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        magnificationCombo = new CommandJComboBox(this, ID_MAGNIFICATION);
        magnificationCombo.setPreferredSize(prefSize);
        addLeftPanel.add(magnificationCombo,gbc);
        
        //Smoothing
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Smoothing"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        smoothingCombo = new CommandJComboBox(this, ID_SMOOTHING);
        smoothingCombo.setPreferredSize(prefSize);
        addLeftPanel.add(smoothingCombo,gbc);
                
        //Configuration
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Configuration"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        configurationCombo = new CommandJComboBox(this, ID_CONFIGURATION);
        configurationCombo.setPreferredSize(prefSize);
        
        addLeftPanel.add(configurationCombo,gbc);
        
        //Resolution
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Resolution"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        resolutionCombo = new CommandJComboBox(this, ID_RESOLUTION);
        resolutionCombo.setPreferredSize(prefSize);
        addLeftPanel.add(resolutionCombo,gbc);
        
        
        
        JPanel addRightPanel = new JPanel(new GridBagLayout());
        
        //Trim
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Trim"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        trimCombo = new CommandJComboBox(this, ID_TRIM);
        trimCombo.setPreferredSize(prefSize);
        addLeftPanel.add(trimCombo,gbc);
        
        //Cropping
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Cropping"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        croppingCombo = new CommandJComboBox(this, ID_CROPPING);
        croppingCombo.setPreferredSize(prefSize);
        addLeftPanel.add(croppingCombo,gbc);
        
        //Border
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Border"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        borderCombo = new CommandJComboBox(this, ID_BORDER);
        borderCombo.setPreferredSize(prefSize);
        addLeftPanel.add(borderCombo,gbc);
        
        //Empty Image
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Empty Image"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        emptyImageCombo = new CommandJComboBox(this, ID_EMPTYIMAGE);
        emptyImageCombo.setPreferredSize(prefSize);
        addLeftPanel.add(emptyImageCombo,gbc);
        
        //MinDesity
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Min Density"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        minDensityCombo = new CommandJComboBox(this, ID_MINDENSITY);
        minDensityCombo.setPreferredSize(prefSize);
        addLeftPanel.add(minDensityCombo,gbc);
        
        //MaxDesity
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        addLeftPanel.add(new JLabel("Max Density"),gbc);
        addLeftPanel.add(Box.createHorizontalStrut(10),gbc);
               
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        maxDensityCombo = new CommandJComboBox(this, ID_MAXDENSITY);
        maxDensityCombo.setPreferredSize(prefSize);
        addLeftPanel.add(maxDensityCombo,gbc);
        addPanel.add(addLeftPanel);
        
        //////////////////////////////////////////////
        //Contains the presentation lut settings
        ////////////////////////////////////////
        pLutPanel.setLayout(new GridBagLayout());
        pLutPanel.setBorder(new TitledBorder("Presentation LUT Options"));
        
        
        //Illumination
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pLutPanel.add(new JLabel("Illumination (cd/m^2)"),gbc);
        pLutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        illuminationText = new CommandJTextField(10,this, ID_ILLUMINATION);
        illuminationText.setDocument(new IntegerDocument(10));
        pLutPanel.add(illuminationText,gbc);
        
        //Illumination
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pLutPanel.add(new JLabel("Reflection (cd/m^2)"),gbc);
        pLutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        reflectionText = new CommandJTextField(10,this, ID_REFLECTION);
        reflectionText.setDocument(new IntegerDocument(10));
        pLutPanel.add(reflectionText,gbc);
        
        //Presentation LUT
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pLutPanel.add(new JLabel("Presentation LUT"),gbc);
        pLutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        presentationLutCombo = new CommandJComboBox(this, ID_PRESENTATIONLUT);
        presentationLutCombo.setPreferredSize(prefSize);
        pLutPanel.add(presentationLutCombo,gbc);
        
        //Reendering as
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pLutPanel.add(new JLabel("Rendering as"),gbc);
        pLutPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        renderingCombo = new CommandJComboBox(this, ID_RENDERING);
        renderingCombo.setPreferredSize(prefSize);
        pLutPanel.add(renderingCombo,gbc);
        
        ///////////////////////////////
        //
        /////////////////////////////////////////////////////
        
        
        annPanel.setLayout(new GridBagLayout());
        annPanel.setBorder(new TitledBorder("Annotation"));
        
        
        annOnRadioButton = new CommandJRadioButton("on", false, this, ID_ANNON);
        annOffRadioButton = new CommandJRadioButton("off", true, this, ID_ANNOFF);
        ButtonGroup bg = new ButtonGroup();
        bg.add(annOnRadioButton);
        bg.add(annOffRadioButton);
        
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        annPanel.add(annOffRadioButton,gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        annPanel.add(annOnRadioButton,gbc);
        
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        annPanel.add(new JLabel("   "),gbc);
        annPanel.add(Box.createHorizontalStrut(10),gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        annPanel.add(new JLabel("Text:"),gbc);
        annPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1.0;
        annotationText = new JTextField();
        annPanel.add(annotationText,gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        annPanel.add(new JLabel("   "),gbc);
        annPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        annDateCheckBox = new JCheckBox("Date/Time");
        annPanel.add(annDateCheckBox,gbc);
        
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        annPanel.add(new JLabel("   "),gbc);
        annPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        annPrinterNameCheckBox = new JCheckBox("Printer Name");
        annPanel.add(annPrinterNameCheckBox,gbc);
        
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        annPanel.add(new JLabel("   "),gbc);
        annPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        annIllCheckBox = new JCheckBox("Illumination/Reflection");
        annPanel.add(annIllCheckBox,gbc);
       

        //Contains the layout settings
        storedPrintPanel = new JPanel();
        storedPrintPanel.setLayout(new GridBagLayout());
        storedPrintPanel.setBorder(new TitledBorder("Select Printer"));
        
        //Printer
        gbc.fill = GridBagConstraints.NONE;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        storedPrintPanel.add(new CommandJButton("Clear",this,ID_CLEARSTOREDPRINT),gbc);
        storedPrintPanel.add(Box.createHorizontalStrut(10),gbc);
        gbc.weightx = 1.0;
        storedPrintPanel.add(new CommandJButton("Select",this,ID_SELECTSTOREDPRINTER),gbc);
        storedPrintPanel.add(Box.createHorizontalStrut(10),gbc);
                
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 2.0;
        storedPrintPrinterCombo = new CommandJComboBox(this, ID_STOREDPRINTER);
        storedPrintPrinterCombo.setPreferredSize(prefSize);
        storedPrintPanel.add(storedPrintPrinterCombo,gbc);
    
        
        mainPanel.setLayout(new GridBagLayout());
        
        
        fGbc.anchor = GridBagConstraints.NORTH;
       // fGbc.gridwidth = 1;
        //fGbc.gridheight = 1;
        
        fGbc.gridx = GridBagConstraints.REMAINDER;
        fGbc.fill = GridBagConstraints.HORIZONTAL;
        
        mainPanel.add(layoutPanel,fGbc);
        mainPanel.add(pLutPanel,fGbc);
        mainPanel.add(buttonPanel,fGbc);
        
        additionalPanel.setLayout(new GridBagLayout());
        additionalPanel.add(addPanel,fGbc);
        additionalPanel.add(annPanel,fGbc);
        
        JPanel t1 = new JPanel();
        t1.setLayout(new BorderLayout());
        t1.add(mainPanel,BorderLayout.NORTH);
        tabpane.add(t1, "Main");
        
        JPanel t2 = new JPanel();
        t2.setLayout(new BorderLayout());
        t2.add(additionalPanel,BorderLayout.NORTH);
        
        tabpane.add(t2, "Additional");
        add(tabpane, BorderLayout.EAST);
            
            
    }

    private void initStoredPrint()
    {
        activeListener = false;
        String value= null;      
       
        String name = storedPrint.getPrinterName();
        if (name != null)value = dvi.getTargetDescription(storedPrint.getPrinterName());
        if(value == null) value = name;
        if(value == null) value = "";
        printerCombo.removeAllItems();
        printerCombo.addItem(value);
        
        mediumCombo.removeAllItems();
        mediumCombo.addItem("<Default>");
        
        filmSizeCombo.removeAllItems();
        value = storedPrint.getFilmSizeID();
        if(value == null) value = "<Default>";
        filmSizeCombo.addItem(value);
        
        orientationCombo.removeAllItems();
        int or = storedPrint.getFilmOrientation();
        switch (or)
        {
            case jDVPSFilmOrientation.DVPSF_portrait: value = "PORTRAIT";
                                 break;
            case jDVPSFilmOrientation.DVPSF_landscape   : value = "LANDSCAPE";
                                 break;
            case jDVPSFilmOrientation.DVPSF_default: value = "<Default>";
                                 break;
        }                    
        orientationCombo.addItem(value);
        
        priorityCombo.removeAllItems();
        priorityCombo.addItem("<Default>");
        
        layoutCombo.removeAllItems();
        printerCurrentRows= storedPrint.getImageDisplayFormatRows();
        printerCurrentCols= storedPrint.getImageDisplayFormatColumns();
        
         value = "" + printerCurrentRows + "/" + printerCurrentCols;
        layoutCombo.addItem(value);
        
       
        copiesText.setText("1");
       
        
        magnificationCombo.removeAllItems();
        value = storedPrint.getMagnificationType();
        if(value == null) value = "<Default>";
        magnificationCombo.addItem(value);
        
        
        smoothingCombo.removeAllItems();
        value = storedPrint.getSmoothingType();
        if(value == null) value = "<Default>";
        smoothingCombo.addItem(value);
        
        configurationCombo.removeAllItems();
        value = storedPrint.getConfigurationInformation();
        if(value == null) value = "<Default>";
        configurationCombo.addItem(value);
        
        resolutionCombo.removeAllItems();
        value = storedPrint.getResolutionID();
        if(value == null) value = "<Default>";
        resolutionCombo.addItem(value);
       
        
        
        
        
        trimCombo.removeAllItems();
        int tr = storedPrint.getTrim();
        switch (tr)
        {
            case jDVPSTrimMode.DVPSH_trim_on: value = "On";
                                 break;
            case jDVPSTrimMode.DVPSH_trim_off   : value = "Off";
                                 break;
            case jDVPSTrimMode.DVPSH_default: value = "<Default>";
                                 break;
        }                    
        trimCombo.addItem(value);
        
        
        
        croppingCombo.removeAllItems();
        int cr = storedPrint.getRequestedDecimateCropBehaviour();
        switch (cr)
        {
            case jDVPSDecimateCropBehaviour.DVPSI_decimate: value = "DECIMATE";
                                 break;
            case jDVPSDecimateCropBehaviour.DVPSI_crop   : value = "CROP";
                                 break;
            case jDVPSDecimateCropBehaviour.DVPSI_fail   : value = "FAIL";
                                 break;
            case jDVPSDecimateCropBehaviour.DVPSI_default: value = "<Default>";
                                 break;
        }                    
        croppingCombo.addItem(value);
        
        borderCombo.removeAllItems();
        value = storedPrint.getBorderDensity();
        if(value == null) value = "<Default>";
        borderCombo.addItem(value);
        

        emptyImageCombo.removeAllItems();
        value = storedPrint.getEmtpyImageDensity();
        if(value == null) value = "<Default>";
        emptyImageCombo.addItem(value);

        minDensityCombo.removeAllItems();
        value = storedPrint.getMinDensity();
        if(value == null) value = "<Default>";
        minDensityCombo.addItem(value);
        
        maxDensityCombo.removeAllItems();
        value = storedPrint.getMaxDensity();
        if(value == null) value = "<Default>";
        maxDensityCombo.addItem(value);
        
        try
        {
            illuminationText.setText(new Integer(storedPrint.getPrintIllumination()).toString());
        }
        catch (Exception e)
        {
            illuminationText.setText(new String("-1"));
        }
        try
        {
            reflectionText.setText(new Integer(storedPrint.getPrintReflectedAmbientLight()).toString());
        }
        catch (Exception e)
        {
            reflectionText.setText(new String("-1"));
        }
       
        
       
    }
    
    /**
     * Initializes the widgets with values form the DVIInterface
     */
    public void init()
    {
        
        ///////////////////////////////
        //Fill medium for current Printer
        /////////////////////////////////
        int forSize = dvi.getNumberOfLUTs();;
        presentationLutCombo.removeAllItems();
        for (int i = 0; i < forSize;i++)
        {
            //dvi.getTargetDescription(String)
            presentationLutCombo.addItem( dvi.getLUTDescription(dvi.getLUTID(i)));
        }
        presentationLutCombo.addItem(new String("IDENTITY"));
        presentationLutCombo.addItem(new String("LIN OD"));
        presentationLutCombo.addItem(new String("<Default>"));
        
        renderingCombo.removeAllItems();
        renderingCombo.addItem("Hardcopy");
        renderingCombo.addItem("Softcopy");
        
        
        ///////////////////////////////
        //Fill medium for current Printer
        /////////////////////////////////
        mediumCombo.removeAllItems();
        int s = dvi.getTargetPrinterNumberOfMediumTypes(target);
        for (int i = 0; i < s;i++)
        {
            mediumCombo.addItem(dvi.getTargetPrinterMediumType(target, i ));
        }
        mediumCombo.addItem(new String("<Default>"));
        
        
        
        ///////////////////////////////
        //Fill Film size for current Printer
        /////////////////////////////////
        filmSizeCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfFilmSizeIDs(target);
        
        for (int i = 0; i < forSize;i++)
        {
            filmSizeCombo.addItem(dvi.getTargetPrinterFilmSizeID(target, i ));
        }
        
        filmSizeCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //Orietation
        /////////////////////////////////
        orientationCombo.removeAllItems();
        orientationCombo.addItem(new String("Portrait"));
        orientationCombo.addItem(new String("Landscape"));
        orientationCombo.addItem(new String("<Default>"));
        
        priorityCombo.removeAllItems();
        priorityCombo.addItem(new String("HIGH"));
        priorityCombo.addItem(new String("MED"));
        priorityCombo.addItem(new String("LOW"));
        priorityCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //Fill Magnification for current Printer
        /////////////////////////////////
        magnificationCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfMagnificationTypes(target);
        for (int i = 0; i < forSize;i++)
        {
            magnificationCombo.addItem( dvi.getTargetPrinterMagnificationType(target, i ));
        }
        magnificationCombo.addItem(new String("<Default>"));
       
        ///////////////////////////////
        //Fehler getTargetPrinterConfigurationSetting
        /////////////////////////////////
        configurationCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfConfigurationSettings(target);
        for (int i = 0; i < forSize;i++)
        {
            configurationCombo.addItem(dvi.getTargetPrinterConfigurationSetting(target, i ));
        }
        configurationCombo.addItem(new String("<Default>"));

        ///////////////////////////////
        //Resolution
        /////////////////////////////////
        resolutionCombo.removeAllItems();
        
        forSize = dvi.getTargetPrinterNumberOfPrinterResolutionIDs(target);
        for (int i = 0; i < forSize;i++)
        {
            resolutionCombo.addItem( dvi.getTargetPrinterResolutionID(target, i ));
        }
        resolutionCombo.addItem(new String("<Default>"));

        ///////////////////////////////
        //Trim
        /////////////////////////////////
        trimCombo.removeAllItems();
        
        if (dvi.getTargetPrinterSupportsTrim(target))
        {
            trimCombo.addItem(new String("On"));
            trimCombo.addItem(new String("Off"));
            
        }
        trimCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //crop
        /////////////////////////////////
        croppingCombo.removeAllItems();
        if (dvi.getTargetPrinterSupportsDecimateCrop(target))
        {
            croppingCombo.addItem(new String("DECIMATE"));
            croppingCombo.addItem(new String("CROP"));
            croppingCombo.addItem(new String("FAIL"));
            
        }
        croppingCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //Border Error
        /////////////////////////////////
        borderCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfBorderDensities(target);
        for (int i = 0; i < forSize;i++)
        {
            borderCombo.addItem(dvi.getTargetPrinterBorderDensity(target, i ));
        }
        borderCombo.addItem(new String("<Default>"));
        
        
        ///////////////////////////////
        //EmptyImages
        /////////////////////////////////
        emptyImageCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfEmptyImageDensities(target);
        for (int i = 0; i < forSize;i++)
        {
            emptyImageCombo.addItem(dvi.getTargetPrinterEmptyImageDensity(target, i) );
        }
        emptyImageCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //Min Density
        /////////////////////////////////
        minDensityCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfMinDensities(target);
        for (int i = 0; i < forSize;i++)
        {
            minDensityCombo.addItem(dvi.getTargetPrinterMinDensity(target, i ));
        }
        minDensityCombo.addItem(new String("<Default>"));
        
        ///////////////////////////////
        //Min Density
        /////////////////////////////////
        maxDensityCombo.removeAllItems();
        forSize = dvi.getTargetPrinterNumberOfMaxDensities(target);
        for (int i = 0; i < forSize;i++)
        {
            maxDensityCombo.addItem(dvi.getTargetPrinterMaxDensity(target, i ));
        }
        maxDensityCombo.addItem(new String("<Default>"));
        ///////////////////////////////
        //Illumination/Reflection Light
        /////////////////////////////////
        illuminationText.setText(new Integer(storedPrint.getPrintIllumination()).toString());
        reflectionText.setText(new Integer(storedPrint.getPrintReflectedAmbientLight()).toString());
       
    }

    /**
     * Initialize the smoothingCombo.
     */
    public void initSmoothing()
    {
        smoothingCombo.removeAllItems();
        if ((storedPrint.getMagnificationType() != null) &&storedPrint.getMagnificationType().equals("CUBIC"))
        {
            int forSize = dvi.getTargetPrinterNumberOfSmoothingTypes(target);
            for (int i = 0; i < forSize;i++)
            {
                smoothingCombo.addItem(dvi.getTargetPrinterSmoothingType(target, i ));
            }
            
        }
        smoothingCombo.addItem(new String("<Default>"));
        smoothingCombo.setSelectedIndex(smoothingCombo.getItemCount()-1);
        storedPrint.setSmoothingType(new String(""));
            
        
        
    }

    /**
     * Handles the action events from Buttons and ComboBoxes
     * 
     * @param ID ID of the clicked widget
     */
    public void buttonClicked (int ID)
    {
        if (activeListener)
        {
           activeListener = false;
            
            switch (ID)
            {
               
                case ID_PRINT: 
                            if (annOffRadioButton.isSelected())
                            {
                                dvi.setActiveAnnotation(false);
                            }
                            else
                            {
                                dvi.setActiveAnnotation(true);
                                dvi.setPrependDateTime(annDateCheckBox.isSelected());
                                dvi.setAnnotationText(annotationText.getText());
                                dvi.setPrependLighting(annIllCheckBox.isSelected());
                                dvi.setPrependPrinterName(annPrinterNameCheckBox.isSelected());
                                
                            }
                            
                            
                            int status = dvi.spoolPrintJob(true);
                            previewPanel.updatePrintPreview();
                            setLabel();
                            break;
                case ID_RENDERING:
                            if (renderingCombo.getSelectedIndex() == 0)
                            {
                                previewPanel.setPrintLUT(true);
                                previewPanel.updatePrintPreview();
                            }
                            else
                            {
                                previewPanel.setPrintLUT(false);
                                previewPanel.updatePrintPreview();
                                
                            }
                            break;
                case ID_PRESENTATIONLUT:
                            
                            
                            int num = presentationLutCombo.getItemCount();
                             
                            if (presentationLutCombo.getSelectedIndex() == num-1)storedPrint.setDefaultPresentationLUT();
                            else if(presentationLutCombo.getSelectedIndex() == num-2) storedPrint.setPresentationLUTShape(jDVPSPresentationLUTType.DVPSP_lin_od);
                            else if(presentationLutCombo.getSelectedIndex() == num-3) storedPrint.setPresentationLUTShape(jDVPSPresentationLUTType.DVPSP_identity);
                            else
                            {
                                
                                int i = presentationLutCombo.getSelectedIndex();
                                status = dvi.selectPrintPresentationLUT(dvi.getLUTID(i));
                                
                            }
                            previewPanel.updatePrintPreview();
                            
                            break;
                
                case ID_PRINTER:
                            target = dvi.getTargetID(printerCombo.getSelectedIndex(), jDVPSPeerType.DVPSE_printAny);
                            dvi.setCurrentPrinter(target);
                            activeListener = false;
                            init();
                            reset();
                            activeListener = true;
                            //previewPanel.updatePrintPreview();
                            break;
                            
                case ID_REMOVE:
                            storedPrint.deleteMultipleImages(storedPrint.getNumberOfImages());
                            previewPanel.updatePrintPreview();
                            setLabel();
                            break;
                            
                case ID_MEDIUM: 
                            if ( mediumCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMediumTypes(target)) dvi.setPrinterMediumType(new String(""));
                            else dvi.setPrinterMediumType((String)mediumCombo.getSelectedItem());
                            break;
                         
                case ID_FILMSIZE: 
                            if ( filmSizeCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfFilmSizeIDs(target)) 
                            {
                                previewPanel.setDefaultFilmSize();
                                storedPrint.setFilmSizeID(new String(""));
                                
                            }
                            else 
                            {
                                
                                storedPrint.setFilmSizeID((String)filmSizeCombo.getSelectedItem());
                                String s = (String)filmSizeCombo.getSelectedItem();
                                
                                try
                                {
                                    int startFirstDigit = 0;
                                    int endFirstDigit = 0;
                                    int startSecondDigit = 0;
                                    int endSecondDigit = 0;
                                    
                                    for (;;startFirstDigit++)if (Character.isDigit(s.charAt(startFirstDigit)))break;
                                    
                                    endFirstDigit = startFirstDigit;
                                    for (;;endFirstDigit++)if (!Character.isDigit(s.charAt(endFirstDigit)))break;
                                    
                                    startSecondDigit = endFirstDigit+1;
                                    for (;;startSecondDigit++)if (Character.isDigit(s.charAt(startSecondDigit)))break;
                                    endSecondDigit = startSecondDigit;
                                    for (;;endSecondDigit++)if (!Character.isDigit(s.charAt(endSecondDigit)))break;
                                
                                    int scaleW = new Integer(s.substring(startFirstDigit,endFirstDigit)).intValue();
                                    int scaleH = new Integer(s.substring(startSecondDigit,endSecondDigit)).intValue();
                                    previewPanel.setFilmSize(scaleW,scaleH);
                                }
                                catch(Exception e)
                                {
		                    dvi.writeLogMessage(jDVPSLogMessageLevel.DVPSM_warning,"GUI - PRINT","Unable to parse String for FILM SIZE: " + s + ", printer: " + storedPrint.getPrinterName());
                                    previewPanel.setDefaultFilmSize();
                                    
                                }
                                
                            }
                            break;
                case ID_PRIORITY: 
                            if ( priorityCombo.getSelectedItem().equals("<Default>")) dvi.setPrinterPriority(new String(""));
                            else dvi.setPrinterPriority((String)priorityCombo.getSelectedItem());
                            break;
                            
                case ID_ORIENTATION:
                            activeListener = false;
                            boolean hasChanged = false;
                            if ( orientationCombo.getSelectedItem().equals("<Default>")) 
                            {
                                storedPrint.setFilmOrientation(jDVPSFilmOrientation.DVPSF_default);
                                if (!currentOrientationIsPortrait )
                                {
                                    currentOrientationIsPortrait = true;
                                    hasChanged = true;
                                }
                            }
                            else if ( orientationCombo.getSelectedItem().equals("Landscape"))
                            {
                                storedPrint.setFilmOrientation(jDVPSFilmOrientation.DVPSF_landscape);
                                if (currentOrientationIsPortrait )
                                {
                                    currentOrientationIsPortrait = false;
                                    hasChanged = true;
                                }
                            }
                            else if ( orientationCombo.getSelectedItem().equals("Portrait"))
                            {
                                storedPrint.setFilmOrientation(jDVPSFilmOrientation.DVPSF_portrait);
                                if (!currentOrientationIsPortrait )
                                {
                                    currentOrientationIsPortrait = true;
                                    hasChanged = true;
                                }
                            }
                            
                            if (hasChanged)
                            {
                                
                                if (orientationCombo.getSelectedIndex()== 1) previewPanel.setOrientation(false);
                                else previewPanel.setOrientation(true);
                                
                                long help = printerCurrentRows;
                                printerCurrentRows= printerCurrentCols ;
                                printerCurrentCols = help;
                                setLayoutCombo();
                                
                            }
                            break;
                          
                case ID_MAGNIFICATION: 
                            if ( magnificationCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMagnificationTypes(target)) storedPrint.setMagnificationType(new String(""));
                            else storedPrint.setMagnificationType((String)magnificationCombo.getSelectedItem());
                            initSmoothing();
                            break;
                             
                case ID_SMOOTHING: 
                            if ( smoothingCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfSmoothingTypes(target)) storedPrint.setSmoothingType(new String(""));
                            else storedPrint.setSmoothingType((String)smoothingCombo.getSelectedItem());
                            break;
            
                case ID_RESOLUTION: 
                            if ( resolutionCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfPrinterResolutionIDs(target)) storedPrint.setResolutionID(new String(""));
                            else storedPrint.setResolutionID((String)resolutionCombo.getSelectedItem());
                            break;
                 
                case ID_CONFIGURATION: 
                            if ( configurationCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfConfigurationSettings(target)) storedPrint.setConfigurationInformation(new String(""));
                            else storedPrint.setConfigurationInformation((String)configurationCombo.getSelectedItem());
                            break;
                
                case ID_TRIM: 
                            if (dvi.getTargetPrinterSupportsTrim(target))
                            {
                                if (trimCombo.getSelectedIndex() == 0) storedPrint.setTrim(jDVPSTrimMode.DVPSH_trim_on);
                                if (trimCombo.getSelectedIndex() == 1) storedPrint.setTrim(jDVPSTrimMode.DVPSH_trim_off);
                                if (trimCombo.getSelectedIndex() == 2) storedPrint.setTrim(jDVPSTrimMode.DVPSH_default);
                            }
                            break;
                case ID_CROPPING: 
                            if (dvi.getTargetPrinterSupportsDecimateCrop(target))
                            {
                                if (croppingCombo.getSelectedIndex() == 0) storedPrint.setRequestedDecimateCropBehaviour(jDVPSDecimateCropBehaviour.DVPSI_decimate);
                                if (croppingCombo.getSelectedIndex() == 1) storedPrint.setRequestedDecimateCropBehaviour(jDVPSDecimateCropBehaviour.DVPSI_crop);
                                if (croppingCombo.getSelectedIndex() == 2) storedPrint.setRequestedDecimateCropBehaviour(jDVPSDecimateCropBehaviour.DVPSI_fail);
                                if (croppingCombo.getSelectedIndex() == 3) storedPrint.setRequestedDecimateCropBehaviour(jDVPSDecimateCropBehaviour.DVPSI_default);
                            }
                            break;
                case ID_BORDER: 
                            if ( borderCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfBorderDensities(target))
                            {
                                storedPrint.setBorderDensity(new String(""));
                               
                            }
                            else  storedPrint.setBorderDensity((String)borderCombo.getSelectedItem());
                            previewPanel.setFilmColor(getBorderColor());
                            break;
               
                case ID_EMPTYIMAGE: 
                            if ( emptyImageCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfEmptyImageDensities(target)) 
                            {
                                storedPrint.setEmtpyImageDensity(new String(""));
                                previewPanel.setDefaultEmptyImageColor();
                            }
                            else storedPrint.setEmtpyImageDensity((String)emptyImageCombo.getSelectedItem());
                            previewPanel.setEmptyImageColor(getEmptyImageColor());
                            break;
                case ID_MINDENSITY: 
                            if ( minDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMinDensities(target)) storedPrint.setMinDensity(new String(""));
                            else storedPrint.setMinDensity((String)minDensityCombo.getSelectedItem());
                            //previewPanel.setFilmAndEmptyImageColor(getBorderColor(),getEmptyImageColor());
                            break;
                case ID_MAXDENSITY: 
                            if ( maxDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMaxDensities(target)) storedPrint.setMaxDensity(new String(""));
                            else storedPrint.setMaxDensity((String)maxDensityCombo.getSelectedItem());
                            //previewPanel.setFilmAndEmptyImageColor(getBorderColor(),getEmptyImageColor());
                            break;
                case ID_LAYOUT:
                            int index = layoutCombo.getSelectedIndex();
                            printerCurrentRows = dvi.getTargetPrinterPortraitDisplayFormatRows(target, index );
                            printerCurrentCols = dvi.getTargetPrinterPortraitDisplayFormatColumns(target, index );
                            if (!currentOrientationIsPortrait)
                            {
                                long help = printerCurrentRows;
                                printerCurrentRows = printerCurrentCols;
                                printerCurrentCols = help;
                            }
                                //set value
                            storedPrint.setImageDisplayFormat(printerCurrentCols, printerCurrentRows);
                                    
                            //change previewPanel
                            previewPanel.setRowsCols((int) printerCurrentRows,(int)printerCurrentCols);
                                    
                                    
                            setLabel();
                            
                            break;
               
                            
            }
            activeListener = true;
            
        }
        if (ID == ID_SELECTSTOREDPRINTER)
        {
            activeListener = false;
            
            String medium =(String) mediumCombo.getItemAt(0);
            String filmSize =(String) filmSizeCombo.getItemAt(0);
            String orientation =(String) orientationCombo.getItemAt(0);
            String magnification =(String) magnificationCombo.getItemAt(0);
            //Pririty Defualt
            String smoothing =(String) smoothingCombo.getItemAt(0);
            String configuration =(String) configurationCombo.getItemAt(0);
            
            String resolution =(String) resolutionCombo.getItemAt(0);
            String trim =(String) trimCombo.getItemAt(0);
            String cropping =(String) croppingCombo.getItemAt(0);
            String border =(String) borderCombo.getItemAt(0);
            String emptyImage =(String) emptyImageCombo.getItemAt(0);
           
            String minDensity =(String) minDensityCombo.getItemAt(0);
            String maxDensity =(String) maxDensityCombo.getItemAt(0);
            
            target = dvi.getTargetID(storedPrintPrinterCombo.getSelectedIndex(), jDVPSPeerType.DVPSE_printAny);
            dvi.setCurrentPrinter(target);
           initPrinterCombo();
            init();
            reset();
             
                mainPanel.remove(storedPrintPanel);
                additionalPanel.add(annPanel,fGbc);
                mainPanel.add(buttonPanel,fGbc);
            pLutPanel.setEnabled(true);
                presentationLutCombo.setEnabled(true);
                renderingCombo.setEnabled(true);
                illuminationText.setEnabled(true);
                reflectionText.setEnabled(true);
                copiesText.setEnabled(true);
            
            activeListener = true;
            return;
            
        }
        if (ID == ID_CLEARSTOREDPRINT)
        {
            activeListener = false;
           initPrinterCombo();
            init();
            reset();
            storedPrint.deleteMultipleImages(storedPrint.getNumberOfImages());
            previewPanel.updatePrintPreview();
            setLabel();
             
                mainPanel.remove(storedPrintPanel);
                additionalPanel.add(annPanel,fGbc);
                mainPanel.add(buttonPanel,fGbc);
                           
                presentationLutCombo.setEnabled(true);
                renderingCombo.setEnabled(true);
                illuminationText.setEnabled(true);
                reflectionText.setEnabled(true);
                copiesText.setEnabled(true);
            
        
            activeListener = true;
            
        }
        
    }
    public   void buttonClicked (int ID, boolean selected)
    {
        if (activeListener)
        {
       
            switch (ID)
            {
                case ID_ANNOFF: 
                        annotationText.setEnabled(false);
                        annDateCheckBox.setEnabled(false);
                        annPrinterNameCheckBox.setEnabled(false);
                        annIllCheckBox.setEnabled(false);
                        
                        break;
                        
                case ID_ANNON: 
                        annotationText.setEnabled(true);
                        annDateCheckBox.setEnabled(true);
                        annPrinterNameCheckBox.setEnabled(true);
                        annIllCheckBox.setEnabled(true);
                        break;
            }
        }
                    
    }

    public int getBorderColor()
    {
        if ( borderCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfBorderDensities(target))
        {
            return -1;
        }
        else 
        {
            String s = (String)borderCombo.getSelectedItem();
            if (s.equals("BLACK"))return 0;
            else if (s.equals("WHITE"))return 255;
            else
            {
                int minD = defaultMinDensityValue; 
                int maxD= defaultMaxDensityValue; 
                /*
                if ( minDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMinDensities(target)) 
                {
                    minD = defaultMinDensityValue;
                }
                else 
                {
                    String sD = (String)minDensityCombo.getSelectedItem();
                    try 
                    {
                        minD = new Integer(sD).intValue();  
                    }
                    catch(Exception e)
                    {
                        minD = defaultMinDensityValue;
                    }
                }
                if ( maxDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMaxDensities(target)) 
                {
                    maxD = defaultMaxDensityValue;
                }
                else 
                {
                    String sD = (String)maxDensityCombo.getSelectedItem();
                    try 
                    {
                        maxD = new Integer(sD).intValue();  
                    }
                    catch(Exception e)
                    {
                        maxD = defaultMaxDensityValue;
                    }
                }
                */
                try
                {
                    int density = new Integer(s).intValue();
                    int colorValue = storedPrint.convertODtoPValue(density, 8);
                    if (colorValue < 0) return -1;
                    else return colorValue;
                }
                catch(Exception e)
                {
                   // System.err.println("Hallo Err: " + e);
                    return -1;
                }
                                    
            }
            //dvi.convertODtoLum(int density, int min, int max, int bits);
                                
        }
                           
        
    }
    
    public int getEmptyImageColor()
    {
        if ( emptyImageCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfEmptyImageDensities(target))
        {
            return -1;
        }
        else 
        {
            String s = (String)emptyImageCombo.getSelectedItem();
            if (s.equals("BLACK"))return 0;
            else if (s.equals("WHITE"))return 255;
            else
            {
                int minD; 
                if ( minDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMinDensities(target)) 
                {
                    minD = defaultMinDensityValue;
                }
                else 
                {
                    String sD = (String)minDensityCombo.getSelectedItem();
                    try 
                    {
                        minD = new Integer(sD).intValue();  
                    }
                    catch(Exception e)
                    {
                        minD = defaultMinDensityValue;
                    }
                }
                int maxD; 
                if ( maxDensityCombo.getSelectedIndex() >= dvi.getTargetPrinterNumberOfMaxDensities(target)) 
                {
                    maxD = defaultMaxDensityValue;
                }
                else 
                {
                    String sD = (String)maxDensityCombo.getSelectedItem();
                    try 
                    {
                        maxD = new Integer(sD).intValue();  
                    }
                    catch(Exception e)
                    {
                        maxD = defaultMaxDensityValue;
                    }
                }
                try
                {
                    int density = new Integer(s).intValue();
                    int colorValue = storedPrint.convertODtoPValue(density, 8);
                    /*System.err.println("Color: " + colorValue);
                    System.err.println("minD: " + minD);
                    System.err.println("maxD: " + maxD);
                    System.err.println("density: " + density);*/
                    if (colorValue < 0) return -1;
                    else return colorValue;
                }
                catch(Exception e)
                {
                    ///System.err.println("Hallo Err: " + e);
                    return -1;
                }
                                    
            }
            //dvi.convertODtoLum(int density, int min, int max, int bits);
                                
        }
       
    }
    /**
     * Handles the ActionEvents and FocusLostEvents from TextFields
     * 
     * @param ID ID of the fired widget
     * @param text The new text
     */
    public void setText(int ID, String text)
    {
        if (activeListener)
        {
            switch (ID)
            {
                    
                case ID_ILLUMINATION: 
                                if (text.trim().equals(""))
                                {
                                    JOptionPane.showMessageDialog(null,"Value must be => 0");
                                    illuminationText.setText("0");
                                }
                                
                                storedPrint.setPrintIllumination(new Integer(text).intValue());
                                previewPanel.updatePrintPreview();
                                break;
                case ID_REFLECTION: 
                                if (text.trim().equals(""))
                                {
                                    JOptionPane.showMessageDialog(null,"Value must be >= 0");
                                    reflectionText.setText("0");
                                }
                                storedPrint.setPrintReflectedAmbientLight(new Integer(text).intValue());
                                previewPanel.updatePrintPreview();
                                
                                break;
               case ID_COPIES:
                                if (text.equals(""))dvi.setPrinterNumberOfCopies(0);
                                else 
                                {
                                    int copies = (new Integer(text)).intValue();
                                    if (copies < 1) 
                                    {
                                        JOptionPane.showMessageDialog(null,"Value must be > 1");
                                        copiesText.setText("1");
                                        dvi.setPrinterNumberOfCopies(1);
                                    }
                                    else 
                                    {
                                        if (copies > 9)
                                        {
                                            JOptionPane.showMessageDialog(null,"Value must be <= 9");
                                            copiesText.setText("9");
                                             dvi.setPrinterNumberOfCopies(9);
                                        }
                                        else 
                                        {
                                            dvi.setPrinterNumberOfCopies(copies);
                                        }
                                    }
                                }
                                break;
               
            }
        }
    }

    /**
     * resets the settings for the print job.
     * Necessary if changing the current printer.
     */
    public void reset()
    {
        
        
        storedPrint = dvi.getPrintHandler();
        previewPanel.setStoredPrint(storedPrint);
        
        
        storedPrint.newPrinter();
            
        renderingCombo.setSelectedIndex(0);
        storedPrint.setDefaultPresentationLUT();
        presentationLutCombo.setSelectedIndex(presentationLutCombo.getItemCount()-1);
        if (!dvi.getTargetPrinterSupportsAnnotation(dvi.getCurrentPrinter()))
        {
            annOffRadioButton.setSelected(true);
            annOnRadioButton.setEnabled(false);
            annotationText.setEnabled(false);
            annDateCheckBox.setEnabled(false);
            annPrinterNameCheckBox.setEnabled(false);
            annIllCheckBox.setEnabled(false);
        }
        else annOnRadioButton.setEnabled(true);
            
        
        copiesText.setText("1");
        dvi.setPrinterNumberOfCopies(new Integer(copiesText.getText()).intValue());
        
        //reset medium type
        dvi.setPrinterMediumType(new String(""));
        mediumCombo.setSelectedIndex(mediumCombo.getItemCount()-1);
        
        //reset filmSize
        //storedPrint.setFilmSizeID(new String(""));
        filmSizeCombo.setSelectedIndex(filmSizeCombo.getItemCount()-1);
        
        
        //reset magnification type
        //storedPrint.setMagnificationType(new String(""));
        magnificationCombo.setSelectedIndex(magnificationCombo.getItemCount()-1);
        
        //reset smoothing
        initSmoothing();
        
        
        //reset filmSize
        //storedPrint.setFilmSizeID(new String(""));
        priorityCombo.setSelectedIndex(priorityCombo.getItemCount()-1);
        
        //reset configuration
        //storedPrint.setConfigurationInformation(new String(""));
        configurationCombo.setSelectedIndex(configurationCombo.getItemCount()-1);
        
        //reset resolution
        resolutionCombo.setSelectedIndex(resolutionCombo.getItemCount()-1);
        storedPrint.setResolutionID(new String(""));
        
        //reset trim
        //storedPrint.setTrim(jDVPSTrimMode.DVPSH_default);
        trimCombo.setSelectedIndex(trimCombo.getItemCount()-1);
        
        //reset requested decimate crop behaviour
        //storedPrint.setRequestedDecimateCropBehaviour(jDVPSDecimateCropBehaviour.DVPSI_default);
        croppingCombo.setSelectedIndex(croppingCombo.getItemCount()-1);
        
        //reset border
        borderCombo.setSelectedIndex(borderCombo.getItemCount()-1);
        storedPrint.setBorderDensity(new String(""));
        
        //reset empty image
        emptyImageCombo.setSelectedIndex(emptyImageCombo.getItemCount()-1);
        storedPrint.setEmtpyImageDensity(new String(""));
        
        //reset min density
        minDensityCombo.setSelectedIndex(minDensityCombo.getItemCount()-1);
        storedPrint.setMinDensity(new String(""));
        
        //reset max density
        maxDensityCombo.setSelectedIndex(maxDensityCombo.getItemCount()-1);
        storedPrint.setMaxDensity(new String(""));
        previewPanel.setFilmAndEmptyImageColorWithoutUpdate(getBorderColor(), getEmptyImageColor());
        
        setLayoutCombo();
    }

    /**
     * Handle  DSEvents from other components. 
     * 
     * @param e
     */
    public boolean processPrint(DSEvent e)
    {
       //Notification about receiveing and new print hardcopy.
       if (e instanceof PrintEvent)
       {
           PrintEvent pe = (PrintEvent)e;
           if (pe.type == pe.ID_HC) 
           {
                activeListener = false;
                
                previewPanel.instertPrint();
                setLabel();
                activeListener = true;
           }
           else
           {
                activeListener = false;
                
                storedPrint = dvi.getPrintHandler();
                diplayStoredPrintObjectMode = true;
                //inset a new image preview into the PreviewPanel.
                additionalPanel.remove(annPanel);
                mainPanel.remove(buttonPanel);
                mainPanel.add(storedPrintPanel,fGbc);
                initStoredPrint();
                boolean portrait = true;
                if (storedPrint.getFilmOrientation()==  jDVPSFilmOrientation.DVPSF_landscape) portrait = false;
                previewPanel.updatePrintPreview(storedPrint,portrait, (int)storedPrint.getImageDisplayFormatRows(),(int)storedPrint.getImageDisplayFormatColumns());
                setLabel();
                presentationLutCombo.setEnabled(false);
                renderingCombo.setEnabled(false);
                illuminationText.setEnabled(false);
                reflectionText.setEnabled(false);
                copiesText.setEnabled(false);
                
                activeListener = true;
                
           }
           
           //
       }
        return false;
    }

    /**
     * Fills the imageLabel.
     */
    public void setLabel()
    {
        int pagesInJob = Math.max(storedPrint.getNumberOfImages()/(int)(printerCurrentRows*printerCurrentCols),1);
        
        //if (storedPrint.getNumberOfImages()%(int)(printerCurrentRows*printerCurrentCols)>0)pagesInJob++;
        imageLabel.setText("Images in Queue: " + storedPrint.getNumberOfImages()+ ", Pages: " + 1 + " out of " + pagesInJob);
    }

    /**
     * Gets the current jDVPSStoredPrint
     * 
     * @return The current jDVPSStoredPrint
     */
    public jDVPSStoredPrint getStored()
    {
        return  storedPrint;
    }


    /**
     * Gets the current jDVInterface
     * 
     * @return The current jDVInterface
     */
    public jDVInterface getDvi()
    {
        return dvi;
    }
     private void setLayoutCombo()
     {
         //target = dvi.getCurrentPrinter();  
         boolean portrait = true;
         
         if (orientationCombo.getSelectedItem().equals("Landscape"))portrait = false;
        
        
        ///////////////////////////////
        //Fill Film size for current Printer
        /////////////////////////////////
       layoutCombo.removeAllItems();
       int  forSize = dvi.getTargetPrinterNumberOfPortraitDisplayFormats(target);
       String t = null;
        if (forSize == 0 ) 
        {
            JOptionPane.showMessageDialog(null,"Missing Printer Layout in Configuration for Printer: " +dvi.getTargetDescription(target));
            System.exit(0);
            
        }
        else
        {   
            for (int i = 0; i < forSize;i++)
            {
                if (portrait) t  = dvi.getTargetPrinterPortraitDisplayFormatRows(target, i )+ "/" + dvi.getTargetPrinterPortraitDisplayFormatColumns(target, i );
                else  t  = dvi.getTargetPrinterPortraitDisplayFormatColumns(target, i )+ "/" + dvi.getTargetPrinterPortraitDisplayFormatRows(target, i );
                layoutCombo.addItem(t);
            }
        }
         
        
        int cols = 1;
        int rows = 1;;
        int bestIndex= 0;
        int nextCols=1;
        int nextRows=1;
        
        for (int i = 0; i < forSize;i++)
        {
            nextCols = dvi.getTargetPrinterPortraitDisplayFormatColumns(target, i );
            nextRows  = dvi.getTargetPrinterPortraitDisplayFormatRows(target, i );
            if (portrait) 
            {
                if ((nextCols <= printerCurrentCols) &&(nextRows <= printerCurrentRows)
                    &&(nextCols >= cols)&&(nextRows >= rows))
                {
                    bestIndex = i;
                    cols = nextCols;
                    rows = nextRows;
                }
            }
            else
            {
                if ((nextCols <= printerCurrentRows) &&(nextRows <= printerCurrentCols)
                    &&(nextCols >= rows)&&(nextRows >= cols))
                {
                    bestIndex = i;
                    cols = nextRows;
                    rows = nextCols;
                }
                
            }
        }
         
         printerCurrentCols = cols ;
         printerCurrentRows = rows;
         layoutCombo.setSelectedIndex(bestIndex);
        storedPrint.setImageDisplayFormat(printerCurrentCols, printerCurrentRows);
                                    
        //change previewPanel
        previewPanel.setRowsCols((int) printerCurrentRows,(int)printerCurrentCols);
                                    
        setLabel();
         
        
     }
    
    /**
     * Deletes the image with the specified index form the print job
     * @param index Index of the specified image
     */
    public void deleteImage(int index)
    {
        storedPrint.deleteImage(index);
        previewPanel.updatePrintPreview();
        setLabel();
    }
}
/*
 *  CVS Log
 *  $Log: PrintPanel.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:30  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
