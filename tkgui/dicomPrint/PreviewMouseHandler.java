/*
 *
 *  Copyright (C) 1999, Institute for MicroTherapy
 *
 *  This software and supporting documentation were developed by
 *
 *    University of Witten/Herdecke
 *    Department of Radiology and MicroTherapy
 *    Institute for MicroTherapy
 *    Medical computer science
 *    
 *    Universitaetsstrasse 142
 *    44799 Bochum, Germany
 *    
 *    http://www.microtherapy.de/go/cs
 *    mailto:computer.science@microtherapy.de
 *
 *  THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND THE INSTITUTE MAKES  NO 
 *  WARRANTY REGARDING THE SOFTWARE, ITS PERFORMANCE, ITS MERCHANTABILITY
 *  OR FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES 
 *  OR ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY 
 *  AND PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 *
 *
 *  Author :      $Author: kleber $
 *  Last update : $Date: 2001/06/06 10:32:29 $
 *  Revision :    $Revision: 1.1.1.1 $
 *  State:        $State: Exp $
*/

package dicomPrint;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;


/**
 * Handles the MouseEvents of the PrintPanel
 * 
 * @author Klaus Kleber
 */
public class PreviewMouseHandler extends MouseAdapter
{

    /**
     * Contains the current PrintPanel
     */
    PrintPanel printPanel;

    /**
     * Contains the current marked PrintImagePreview
     * 
     */
    PrintImagePreview currentMarkedPreview;
    
    /**
     * Constructor
     * 
     * @param printPanel Contains the current PrintPanel
     */
    public PreviewMouseHandler (PrintPanel printPanel)
    {
        this.printPanel = printPanel;
    }
    
    /**
     * Handles the MouseClick Events. DoubleClick on a PrintImagePreview results in opening an ImageSettingDialog and
     * a normal click results in marking the clicked PrintImagePreview.
     * 
     * @param e MuseEvent
     */
    public void mouseClicked(MouseEvent e)
    {
        PrintImagePreview nextPreview = (PrintImagePreview)e.getSource();
        
        if (currentMarkedPreview != null) currentMarkedPreview.setMarked(false);
        nextPreview.setMarked(true);
        currentMarkedPreview = nextPreview;
        
        //handles the doubleClick
        if (e.getClickCount() == 2) 
        {
            
            if (nextPreview.getReferToImage())(new ImageSettingDialog(nextPreview,printPanel )).setVisible(true);
            
        }
    }
}

/*
 *  CVS Log
 *  $Log: PreviewMouseHandler.java,v $
 *  Revision 1.1.1.1  2001/06/06 10:32:29  kleber
 *  Init commit for DICOMscope 3.5
 *  Create new CVS
 *
*/
